/*
 * Copyright 2021 The ChromiumOS Authors
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

package org.chromium.arc.testapp.cameraintent;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/** App to listen for result of camera intent. */
public class MainActivity extends Activity {
    private static final String KEY_ACTION = "action";
    private static final String KEY_DATA = "data";
    private static final String KEY_HAS_URI = "has_uri";
    private static final int EXPECT_NOTHING = 0;
    private static final int EXPECT_IMAGE_DATA = 1;
    private static final int EXPECT_VIDEO_URI = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        final Button button = findViewById(R.id.send_intent);
        button.setOnClickListener(v -> {
            sendIntent();
            button.setOnClickListener(null);
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (!isValidRequestCode(requestCode)) {
            throw new IllegalArgumentException("Invalid request code: " + requestCode);
        }
        if (!isValidResultCode(resultCode)) {
            throw new IllegalArgumentException("Invalid result code: " + resultCode);
        }

        String msg = Integer.toString(resultCode);

        // Checks if the result is successfully returned and the result is valid or not.
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == EXPECT_IMAGE_DATA) {
                Bitmap bitmap = data.getParcelableExtra(KEY_DATA);
                if (bitmap == null) {
                    msg = "Failed to get returned bitmap";
                } else if (bitmap.getWidth() == 0 || bitmap.getHeight() == 0) {
                    msg = "Invalid width/height of bitmap";
                }
            } else if (requestCode == EXPECT_VIDEO_URI) {
                Uri uri = data.getData();
                if (uri == null) {
                    msg = "Failed to get returned video URI";
                } else if (Uri.EMPTY.equals(uri)) {
                    msg = "URI is empty";
                }
            }
        }
        setResult(msg);
    }

    private void sendIntent() {
        final String action = getIntent().getStringExtra(KEY_ACTION);
        if (!isSupportedAction(action)) {
            throw new IllegalArgumentException("Unsupported action: " + action);
        }

        final boolean hasUri = getIntent().getBooleanExtra(KEY_HAS_URI, false);
        final Intent intent = new Intent(action);
        if (hasUri) {
            final Uri outputUri = getOutputUri(action);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, outputUri);
        }

        if (isImageCaptureAction(action) && !hasUri) {
            startActivityForResult(intent, EXPECT_IMAGE_DATA);
        } else if (isVideoCaptureAction(action) && !hasUri) {
            startActivityForResult(intent, EXPECT_VIDEO_URI);
        } else {
            startActivityForResult(intent, EXPECT_NOTHING);
        }
    }

    private Uri getOutputUri(final String action) {
        String fileName = null;
        String mimeType = null;
        if (isImageCaptureAction(action)) {
            fileName = "test.jpg";
            mimeType = "image/jpg";
        } else if (isVideoCaptureAction(action)) {
            fileName = "test.mp4";
            mimeType = "video/mp4";
        } else {
            throw new IllegalArgumentException(
                "URI cannot be brought with the action: " + action);
        }

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) {
            // For ARC-P, we simply use the file provider provided by IntentHelper package as a
            // workaround since we are not able to use FileProvider class provided by AndroidX
            // library in the test app.
            final String uriString =
                "content://org.chromium.arc.intent_helper.fileprovider/download/" + fileName;
            return Uri.parse(uriString);
        }

        final ContentValues values = new ContentValues();
        values.put(MediaStore.MediaColumns.TITLE, fileName);
        values.put(MediaStore.MediaColumns.DISPLAY_NAME, fileName);
        values.put(MediaStore.MediaColumns.MIME_TYPE, mimeType);

        return getContentResolver().insert(
            MediaStore.Downloads.getContentUri(MediaStore.VOLUME_EXTERNAL_PRIMARY), values);
    }

    private boolean isImageCaptureAction(final String action) {
        return MediaStore.ACTION_IMAGE_CAPTURE.equals(action)
                || MediaStore.ACTION_IMAGE_CAPTURE_SECURE.equals(action);
    }

    private boolean isVideoCaptureAction(final String action) {
        return MediaStore.ACTION_VIDEO_CAPTURE.equals(action);
    }

    private void setResult(String text) {
        final TextView textView = findViewById(R.id.text);
        textView.setText(text);
        textView.setVisibility(View.VISIBLE);
    }

    private boolean isSupportedAction(String action) {
        return MediaStore.ACTION_IMAGE_CAPTURE.equals(action)
                || MediaStore.ACTION_IMAGE_CAPTURE_SECURE.equals(action)
                || MediaStore.ACTION_VIDEO_CAPTURE.equals(action)
                || MediaStore.INTENT_ACTION_STILL_IMAGE_CAMERA.equals(action)
                || MediaStore.INTENT_ACTION_STILL_IMAGE_CAMERA_SECURE.equals(action)
                || MediaStore.INTENT_ACTION_VIDEO_CAMERA.equals(action);
    }

    private boolean isValidRequestCode(int code) {
        return code == EXPECT_NOTHING || code == EXPECT_IMAGE_DATA || code == EXPECT_VIDEO_URI;
    }

    private boolean isValidResultCode(int code) {
        return code == Activity.RESULT_OK || code == Activity.RESULT_CANCELED;
    }
}
