// Copyright 2021 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package fixture

// Fixtures defined in go.chromium.org/tast-tests/cros/local/kioskmode/fixtures/kiosk.go.
const (
	// KioskLoggedInAsh
	KioskLoggedInAsh = "kioskLoggedInAsh"
	// KioskLoggedInLacros
	KioskLoggedInLacros = "kioskLoggedInLacros"
)
