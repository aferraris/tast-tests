// Copyright 2021 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package fixture

// Fixture defined in go.chromium.org/tast-tests/cros/remote/policyutil/enrolled_fixture.go.
const (
	// Enrolled is a fixture name.
	Enrolled = "enrolled"
)

// Fixture defined in go.chromium.org/tast-tests/cros/remote/policyutil/clean_ownership.go.
const (
	CleanOwnership = "cleanOwnership"
)

// Fixtures defined in go.chromium.org/tast-tests/cros/local/policyutil/fixtures/fakedms.go.
const (
	// FakeDMS is a fixture name.
	FakeDMS = "fakeDMS"
	// FakeDMSEnrolled is a fixture name.
	FakeDMSEnrolled = "fakeDMSEnrolled"
	// FakeDMSUpdateEngineEnrolled is a fixture name.
	FakeDMSUpdateEngineEnrolled = "fakeDMSUpdateEngineEnrolled"
)

// Fixtures defined in go.chromium.org/tast-tests/cros/local/policyutil/fixtures/chrome.go.
const (
	// ChromePolicyLoggedIn is a fixture name.
	ChromePolicyLoggedIn = "chromePolicyLoggedIn"
	// ChromePolicyLoggedInLockscreen is a fixture name.
	ChromePolicyLoggedInLockscreen = "chromePolicyLoggedInLockscreen"
	// ChromeEnrolledLoggedInShortMetricsInterval is a fixture name.
	ChromeEnrolledLoggedInShortMetricsInterval = "chromeEnrolledLoggedInShortMetricsInterval"
	// ChromePolicyLoggedInIsolatedApp is a fixture name.
	ChromePolicyLoggedInIsolatedApp = "chromePolicyLoggedInIsolatedApp"
	// ChromePolicyLoggedInFeatureJourneys is a fixture name.
	ChromePolicyLoggedInFeatureJourneys = "chromePolicyLoggedInFeatureJourneys"
	// ChromePolicyLoggedInFeatureChromeLabs is a fixture name.
	ChromePolicyLoggedInFeatureChromeLabs = "chromePolicyLoggedInFeatureChromeLabs"
	// ChromePolicyLoggedInARCFilesUXEnabled is a fixture name.
	ChromePolicyLoggedInARCFilesUXEnabled = "chromePolicyLoggedInARCFilesUXEnabled"
	// ChromePolicyLoggedInFilesUXEnabled is a fixture name.
	ChromePolicyLoggedInFilesUXEnabled = "chromePolicyLoggedInFilesUXEnabled"
	// ChromePolicyLoggedInBruschetta is a fixture name.
	ChromePolicyLoggedInBruschetta = "chromePolicyLoggedInBruschetta"
	// ChromePolicyLoggedInBruschettaWithFieldtrialConfig is a fixture name.
	ChromePolicyLoggedInBruschettaWithFieldtrialConfig = "chromePolicyLoggedInBruschettaWithFieldtrialConfig"
	// ChromeEnrolledLoggedIn is a fixture name.
	ChromeEnrolledLoggedIn = "chromeEnrolledLoggedIn"
	// ChromeUpdateEngineEnrolledLoggedIn is a fixture name.
	ChromeUpdateEngineEnrolledLoggedIn = "chromeUpdateEngineEnrolledLoggedIn"
	// ChromeEnrolledLoggedInARC is a fixture name.
	ChromeEnrolledLoggedInARC = "chromeEnrolledLoggedInARC"
	// ChromeAdminDeskTemplatesLoggedIn is a fixture name.
	ChromeAdminDeskTemplatesLoggedIn = "chromeAdminDeskTemplatesLoggedIn"
	// ChromePolicyRealUserLoggedIn is a fixture name.
	ChromePolicyRealUserLoggedIn = "chromePolicyRealUserLoggedIn"
	// ChromePolicyLoggedInDevToolsAvailable is a fixture name.
	ChromePolicyLoggedInDevToolsAvailable = "chromePolicyLoggedInDevToolsAvailable"
	// ChromePolicyLoggedInAdvancedProtection is a fixture name.
	ChromePolicyLoggedInAdvancedProtection = "chromePolicyLoggedInAdvancedProtection"
)

// Fixtures defined in go.chromium.org/tast-tests/cros/local/policyutil/fixtures/tape.go.
const (
	// ChromeTAPELoggedIn is a fixture name.
	ChromeTAPELoggedIn = "chromeTAPELoggedIn"
	// ChromeTAPEEnrolledLoggedIn is a fixture name.
	ChromeTAPEEnrolledLoggedIn = "chromeTAPEEnrolledLoggedIn"
)

// Fixtures defined in go.chromium.org/tast-tests/cros/local/mgs/fixture.go.
const (
	ManagedGuestSessionWithPWA       = "managedGuestSessionWithPWA"
	ManagedGuestSessionWithPWALacros = "managedGuestSessionWithPWALacros"
)

// Fixtures defined in go.chromium.org/tast-tests/cros/local/policyutil/fixtures/lacros.go.
const (
	// LacrosPolicyLoggedIn is a fixture name.
	LacrosPolicyLoggedIn = "lacrosPolicyLoggedIn"
	// LacrosPolicyLoggedInWithKeepAlive is a fixture name.
	LacrosPolicyLoggedInWithKeepAlive = "lacrosPolicyLoggedInWithKeepAlive"
	// LacrosPolicyLoggedInFeatureJourneys is a fixture name.
	LacrosPolicyLoggedInFeatureJourneys = "lacrosPolicyLoggedInFeatureJourneys"
	// LacrosPolicyLoggedInFeatureChromeLabs is a fixture name.
	LacrosPolicyLoggedInFeatureChromeLabs = "lacrosPolicyLoggedInFeatureChromeLabs"
	// LacrosPolicyLoggedInFilesUXEnabled is a fixture name.
	LacrosPolicyLoggedInFilesUXEnabled = "lacrosPolicyLoggedInFilesUXEnabled"
	// LacrosPolicyLoggedInRealUser is a fixture name.
	LacrosPolicyLoggedInRealUser = "lacrosPolicyLoggedInRealUser"
	// LacrosPolicyRealUserLoggedIn is a fixture name.
	LacrosPolicyRealUserLoggedIn = "lacrosPolicyRealUserLoggedIn"
	// LacrosAdminDeskTemplatesLoggedIn is a fixture name.
	LacrosAdminDeskTemplatesLoggedIn = "lacrosAdminDeskTemplatesLoggedIn"
	// LacrosEnrolledLoggedIn is a fixture name.
	LacrosEnrolledLoggedIn = "lacrosEnrolledLoggedIn"
	// LacrosEnrolledLoggedInShortMetricsInterval is a fixture name.
	LacrosEnrolledLoggedInShortMetricsInterval = "lacrosEnrolledLoggedInShortMetricsInterval"
	// LacrosPolicyLoggedInAdvancedProtection is a fixture name.
	LacrosPolicyLoggedInAdvancedProtection = "lacrosPolicyLoggedInAdvancedProtection"
	// LacrosPolicyLoggedInBruschettaWithLacros is a fixture name.
	LacrosPolicyLoggedInBruschetta = "lacrosPolicyLoggedInBruschetta"
)

// Fixtures defined in go.chromium.org/tast-tests/cros/local/policyutil/fixtures/persistent.go.
const (
	// PersistentLacros is a fixture name.
	PersistentLacros = "persistentLacros"
	// PersistentLacrosEnrolled is a fixture name.
	PersistentLacrosEnrolled = "persistentLacrosEnrolled"
	// PersistentLacrosRealUser is a fixture name.
	PersistentLacrosRealUser = "persistentLacrosRealUser"
	// PersistentFamilyLink is a fixture name.
	PersistentFamilyLink = "persistentFamilyLink"
	// PersistentFamilyLinkARC is a fixture name.
	PersistentFamilyLinkARC = "persistentFamilyLinkARC"
	// PersistentGellerARC is a fixture name.
	PersistentGellerARC = "persistentGellerARC"
	// PersistentProjectorEDU is a fixture name.
	PersistentProjectorEDU = "persistentProjectorEDU"
	// PersistentProjectorChild is a fixture name.
	PersistentProjectorChild = "persistentProjectorChild"
)

// Fixture defined in go.chromium.org/tast-tests/cros/remote/policyutil/tape_account.go.
const (
	// TAPEAccount is a fixture name.
	TAPEAccount = "tapeAccount"
	//	EnterpriseConnectorsMGSAshWebProtectDisabledAccount is a fixture name.
	EnterpriseConnectorsMGSAshWebProtectDisabledAccount = "enterpriseConnectorsMGSAshWebProtectDisabledAccount"
	//	EnterpriseConnectorsMGSAshWebProtectEnabledAllowAccount is a fixture name.
	EnterpriseConnectorsMGSAshWebProtectEnabledAllowAccount = "enterpriseConnectorsMGSAshWebProtectEnabledAllowAccount"
	//	EnterpriseConnectorsMGSAshWebProtectEnabledBlockAccount is a fixture name.
	EnterpriseConnectorsMGSAshWebProtectEnabledBlockAccount = "enterpriseConnectorsMGSAshWebProtectEnabledBlockAccount"
	//	EnterpriseConnectorsMGSLacrosWebProtectDisabledAccount is a fixture name.
	EnterpriseConnectorsMGSLacrosWebProtectDisabledAccount = "enterpriseConnectorsMGSLacrosWebProtectDisabledAccount"
	//	EnterpriseConnectorsMGSLacrosWebProtectEnabledAllowAccount is a fixture name.
	EnterpriseConnectorsMGSLacrosWebProtectEnabledAllowAccount = "enterpriseConnectorsMGSLacrosWebProtectEnabledAllowAccount"
	//	EnterpriseConnectorsMGSLacrosWebProtectEnabledBlockAccount is a fixture name.
	EnterpriseConnectorsMGSLacrosWebProtectEnabledBlockAccount = "enterpriseConnectorsMGSLacrosWebProtectEnabledBlockAccount"
)

// Fixture defined in go.chromium.org/tast-tests/cros/remote/policyutil/tape_enrolled.go.
const (
	// TAPEEnrolled is a fixture name.
	TAPEEnrolled = "tapeEnrolled"
	//	EnterpriseConnectorsMGSAshWebProtectDisabledEnrolled is a fixture name.
	EnterpriseConnectorsMGSAshWebProtectDisabledEnrolled = "enterpriseConnectorsMGSAshWebProtectDisabledEnrolled"
	//	EnterpriseConnectorsMGSAshWebProtectEnabledAllowEnrolled is a fixture name.
	EnterpriseConnectorsMGSAshWebProtectEnabledAllowEnrolled = "enterpriseConnectorsMGSAshWebProtectEnabledAllowEnrolled"
	//	EnterpriseConnectorsMGSAshWebProtectEnabledBlockEnrolled is a fixture name.
	EnterpriseConnectorsMGSAshWebProtectEnabledBlockEnrolled = "enterpriseConnectorsMGSAshWebProtectEnabledBlockEnrolled"
	//	EnterpriseConnectorsMGSLacrosWebProtectDisabledEnrolled is a fixture name.
	EnterpriseConnectorsMGSLacrosWebProtectDisabledEnrolled = "enterpriseConnectorsMGSLacrosWebProtectDisabledEnrolled"
	//	EnterpriseConnectorsMGSLacrosWebProtectEnabledAllowEnrolled is a fixture name.
	EnterpriseConnectorsMGSLacrosWebProtectEnabledAllowEnrolled = "enterpriseConnectorsMGSLacrosWebProtectEnabledAllowEnrolled"
	//	EnterpriseConnectorsMGSLacrosWebProtectEnabledBlockEnrolled is a fixture name.
	EnterpriseConnectorsMGSLacrosWebProtectEnabledBlockEnrolled = "enterpriseConnectorsMGSLacrosWebProtectEnabledBlockEnrolled"
)

// TAPEAccountData contains TAPE account data.
type TAPEAccountData struct {
	Username    string
	Password    string
	RequestID   string
	CustomerID  string
	OrgUnitPath string
}
