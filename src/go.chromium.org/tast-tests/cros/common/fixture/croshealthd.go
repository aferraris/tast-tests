// Copyright 2023 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package fixture

const (
	// CrosHealthdRebootDUT is a private fixture.
	// Reboot the DUT at the start of the fixture to make sure tests are run after
	// a reboot.
	CrosHealthdRebootDUT = "crosHealthdRebootDUT"
)
