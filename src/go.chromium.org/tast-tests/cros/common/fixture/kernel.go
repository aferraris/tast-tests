// Copyright 2024 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package fixture

// Fixtures defined in src/go.chromium.org/tast-tests/cros/local/kernel/fixture.go
const (
	// SchedRT is a fixture to set realtime scheduler.
	SchedRT = "schedRT"
	// SchedRT with gpuWatchHangs as parent.
	SchedRTGpuWatchHangs = "schedRTGpuWatchHangs"
)
