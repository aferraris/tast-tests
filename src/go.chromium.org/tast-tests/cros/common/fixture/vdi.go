// Copyright 2021 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package fixture

// Fixtures defined in go.chromium.org/tast-tests/cros/local/vdi/fixtures/vdi.go.
const (
	// CitrixLaunched is a fixture name.
	CitrixLaunched = "citrixLaunched"
	// VmwareLaunched is a fixture name.
	VmwareLaunched = "vmwareLaunched"
	// LacrosCitrixLaunched is a fixture name.
	LacrosCitrixLaunched = "lacrosCitrixLaunched"
)

// Fixtures defined in go.chromium.org/tast-tests/cros/local/vdi/fixtures/vdi_msg.go.
const (
	// MgsCitrixLaunched is a fixture name.
	MgsCitrixLaunched = "mgsCitrixLaunched"
	// MgsVmwareLaunched is a fixture name.
	MgsVmwareLaunched = "mgsVmwareLaunched"
	// MgsLacrosCitrixLaunched is a fixture name.
	MgsLacrosCitrixLaunched = "mgsLacrosCitrixLaunched"
)

// Fixtures defined in go.chromium.org/tast-tests/cros/local/vdi/fixtures/vdi_kiosk.go.
const (
	// KioskCitrixLaunched is a fixture name.
	KioskCitrixLaunched = "kioskCitrixLaunched"
	// KioskVmwareLaunched is a fixture name.
	KioskVmwareLaunched = "kioskVmwareLaunched"
	// KioskLacrosCitrixLaunched is a fixture name.
	KioskLacrosCitrixLaunched = "kioskLacrosCitrixLaunched"
)
