// Copyright 2022 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package fixture

// Fixtures defined in go.chromium.org/tast-tests/cros/local/audio/
const (
	CrasStopped = "crasStopped"

	// For the testbed with Chameleon
	ChameleonAudioTestbed = "chameleonAudioTestbed"

	// For Audio Latency Toolkit Teensy
	AudioLatencyToolkit = "audioLatencyToolkit"

	FakeCrasClient = "fakeCrasClient"
)
