// Copyright 2024 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package usb contains functions to support flashing usb drives.
// Reviews for this package can be sent to tast-fw-library-reviewers@ (before tast-owners@ review).
package usb
