// Copyright 2023 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package futility implements futility commands for TAST tests.
// Reviews for this package can be sent to tast-fw-library-reviewers@ (before tast-owners@ review).
package futility
