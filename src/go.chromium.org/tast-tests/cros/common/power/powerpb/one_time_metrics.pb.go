// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.27.1
// 	protoc        v4.23.3
// source: one_time_metrics.proto

package powerpb

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type OneTimeMetrics struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	BatteryChargeDesignSize   *float64 `protobuf:"fixed64,1,opt,name=battery_charge_design_size,json=batteryChargeDesignSize,proto3,oneof" json:"battery_charge_design_size,omitempty"`
	BatteryChargeSize         *float64 `protobuf:"fixed64,2,opt,name=battery_charge_size,json=batteryChargeSize,proto3,oneof" json:"battery_charge_size,omitempty"`
	BatteryEnergySize         *float64 `protobuf:"fixed64,3,opt,name=battery_energy_size,json=batteryEnergySize,proto3,oneof" json:"battery_energy_size,omitempty"`
	BatteryShutdownPercent    *float64 `protobuf:"fixed64,4,opt,name=battery_shutdown_percent,json=batteryShutdownPercent,proto3,oneof" json:"battery_shutdown_percent,omitempty"`
	BacklightLinearPercent    float64  `protobuf:"fixed64,5,opt,name=backlight_linear_percent,json=backlightLinearPercent,proto3" json:"backlight_linear_percent,omitempty"`
	BacklightNonlinearPercent float64  `protobuf:"fixed64,6,opt,name=backlight_nonlinear_percent,json=backlightNonlinearPercent,proto3" json:"backlight_nonlinear_percent,omitempty"`
}

func (x *OneTimeMetrics) Reset() {
	*x = OneTimeMetrics{}
	if protoimpl.UnsafeEnabled {
		mi := &file_one_time_metrics_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *OneTimeMetrics) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*OneTimeMetrics) ProtoMessage() {}

func (x *OneTimeMetrics) ProtoReflect() protoreflect.Message {
	mi := &file_one_time_metrics_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use OneTimeMetrics.ProtoReflect.Descriptor instead.
func (*OneTimeMetrics) Descriptor() ([]byte, []int) {
	return file_one_time_metrics_proto_rawDescGZIP(), []int{0}
}

func (x *OneTimeMetrics) GetBatteryChargeDesignSize() float64 {
	if x != nil && x.BatteryChargeDesignSize != nil {
		return *x.BatteryChargeDesignSize
	}
	return 0
}

func (x *OneTimeMetrics) GetBatteryChargeSize() float64 {
	if x != nil && x.BatteryChargeSize != nil {
		return *x.BatteryChargeSize
	}
	return 0
}

func (x *OneTimeMetrics) GetBatteryEnergySize() float64 {
	if x != nil && x.BatteryEnergySize != nil {
		return *x.BatteryEnergySize
	}
	return 0
}

func (x *OneTimeMetrics) GetBatteryShutdownPercent() float64 {
	if x != nil && x.BatteryShutdownPercent != nil {
		return *x.BatteryShutdownPercent
	}
	return 0
}

func (x *OneTimeMetrics) GetBacklightLinearPercent() float64 {
	if x != nil {
		return x.BacklightLinearPercent
	}
	return 0
}

func (x *OneTimeMetrics) GetBacklightNonlinearPercent() float64 {
	if x != nil {
		return x.BacklightNonlinearPercent
	}
	return 0
}

var File_one_time_metrics_proto protoreflect.FileDescriptor

var file_one_time_metrics_proto_rawDesc = []byte{
	0x0a, 0x16, 0x6f, 0x6e, 0x65, 0x5f, 0x74, 0x69, 0x6d, 0x65, 0x5f, 0x6d, 0x65, 0x74, 0x72, 0x69,
	0x63, 0x73, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x19, 0x74, 0x61, 0x73, 0x74, 0x2e, 0x63,
	0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x2e, 0x70, 0x6f, 0x77, 0x65, 0x72, 0x2e, 0x70, 0x6f, 0x77, 0x65,
	0x72, 0x70, 0x62, 0x22, 0xe1, 0x03, 0x0a, 0x0e, 0x4f, 0x6e, 0x65, 0x54, 0x69, 0x6d, 0x65, 0x4d,
	0x65, 0x74, 0x72, 0x69, 0x63, 0x73, 0x12, 0x40, 0x0a, 0x1a, 0x62, 0x61, 0x74, 0x74, 0x65, 0x72,
	0x79, 0x5f, 0x63, 0x68, 0x61, 0x72, 0x67, 0x65, 0x5f, 0x64, 0x65, 0x73, 0x69, 0x67, 0x6e, 0x5f,
	0x73, 0x69, 0x7a, 0x65, 0x18, 0x01, 0x20, 0x01, 0x28, 0x01, 0x48, 0x00, 0x52, 0x17, 0x62, 0x61,
	0x74, 0x74, 0x65, 0x72, 0x79, 0x43, 0x68, 0x61, 0x72, 0x67, 0x65, 0x44, 0x65, 0x73, 0x69, 0x67,
	0x6e, 0x53, 0x69, 0x7a, 0x65, 0x88, 0x01, 0x01, 0x12, 0x33, 0x0a, 0x13, 0x62, 0x61, 0x74, 0x74,
	0x65, 0x72, 0x79, 0x5f, 0x63, 0x68, 0x61, 0x72, 0x67, 0x65, 0x5f, 0x73, 0x69, 0x7a, 0x65, 0x18,
	0x02, 0x20, 0x01, 0x28, 0x01, 0x48, 0x01, 0x52, 0x11, 0x62, 0x61, 0x74, 0x74, 0x65, 0x72, 0x79,
	0x43, 0x68, 0x61, 0x72, 0x67, 0x65, 0x53, 0x69, 0x7a, 0x65, 0x88, 0x01, 0x01, 0x12, 0x33, 0x0a,
	0x13, 0x62, 0x61, 0x74, 0x74, 0x65, 0x72, 0x79, 0x5f, 0x65, 0x6e, 0x65, 0x72, 0x67, 0x79, 0x5f,
	0x73, 0x69, 0x7a, 0x65, 0x18, 0x03, 0x20, 0x01, 0x28, 0x01, 0x48, 0x02, 0x52, 0x11, 0x62, 0x61,
	0x74, 0x74, 0x65, 0x72, 0x79, 0x45, 0x6e, 0x65, 0x72, 0x67, 0x79, 0x53, 0x69, 0x7a, 0x65, 0x88,
	0x01, 0x01, 0x12, 0x3d, 0x0a, 0x18, 0x62, 0x61, 0x74, 0x74, 0x65, 0x72, 0x79, 0x5f, 0x73, 0x68,
	0x75, 0x74, 0x64, 0x6f, 0x77, 0x6e, 0x5f, 0x70, 0x65, 0x72, 0x63, 0x65, 0x6e, 0x74, 0x18, 0x04,
	0x20, 0x01, 0x28, 0x01, 0x48, 0x03, 0x52, 0x16, 0x62, 0x61, 0x74, 0x74, 0x65, 0x72, 0x79, 0x53,
	0x68, 0x75, 0x74, 0x64, 0x6f, 0x77, 0x6e, 0x50, 0x65, 0x72, 0x63, 0x65, 0x6e, 0x74, 0x88, 0x01,
	0x01, 0x12, 0x38, 0x0a, 0x18, 0x62, 0x61, 0x63, 0x6b, 0x6c, 0x69, 0x67, 0x68, 0x74, 0x5f, 0x6c,
	0x69, 0x6e, 0x65, 0x61, 0x72, 0x5f, 0x70, 0x65, 0x72, 0x63, 0x65, 0x6e, 0x74, 0x18, 0x05, 0x20,
	0x01, 0x28, 0x01, 0x52, 0x16, 0x62, 0x61, 0x63, 0x6b, 0x6c, 0x69, 0x67, 0x68, 0x74, 0x4c, 0x69,
	0x6e, 0x65, 0x61, 0x72, 0x50, 0x65, 0x72, 0x63, 0x65, 0x6e, 0x74, 0x12, 0x3e, 0x0a, 0x1b, 0x62,
	0x61, 0x63, 0x6b, 0x6c, 0x69, 0x67, 0x68, 0x74, 0x5f, 0x6e, 0x6f, 0x6e, 0x6c, 0x69, 0x6e, 0x65,
	0x61, 0x72, 0x5f, 0x70, 0x65, 0x72, 0x63, 0x65, 0x6e, 0x74, 0x18, 0x06, 0x20, 0x01, 0x28, 0x01,
	0x52, 0x19, 0x62, 0x61, 0x63, 0x6b, 0x6c, 0x69, 0x67, 0x68, 0x74, 0x4e, 0x6f, 0x6e, 0x6c, 0x69,
	0x6e, 0x65, 0x61, 0x72, 0x50, 0x65, 0x72, 0x63, 0x65, 0x6e, 0x74, 0x42, 0x1d, 0x0a, 0x1b, 0x5f,
	0x62, 0x61, 0x74, 0x74, 0x65, 0x72, 0x79, 0x5f, 0x63, 0x68, 0x61, 0x72, 0x67, 0x65, 0x5f, 0x64,
	0x65, 0x73, 0x69, 0x67, 0x6e, 0x5f, 0x73, 0x69, 0x7a, 0x65, 0x42, 0x16, 0x0a, 0x14, 0x5f, 0x62,
	0x61, 0x74, 0x74, 0x65, 0x72, 0x79, 0x5f, 0x63, 0x68, 0x61, 0x72, 0x67, 0x65, 0x5f, 0x73, 0x69,
	0x7a, 0x65, 0x42, 0x16, 0x0a, 0x14, 0x5f, 0x62, 0x61, 0x74, 0x74, 0x65, 0x72, 0x79, 0x5f, 0x65,
	0x6e, 0x65, 0x72, 0x67, 0x79, 0x5f, 0x73, 0x69, 0x7a, 0x65, 0x42, 0x1b, 0x0a, 0x19, 0x5f, 0x62,
	0x61, 0x74, 0x74, 0x65, 0x72, 0x79, 0x5f, 0x73, 0x68, 0x75, 0x74, 0x64, 0x6f, 0x77, 0x6e, 0x5f,
	0x70, 0x65, 0x72, 0x63, 0x65, 0x6e, 0x74, 0x42, 0x36, 0x5a, 0x34, 0x67, 0x6f, 0x2e, 0x63, 0x68,
	0x72, 0x6f, 0x6d, 0x69, 0x75, 0x6d, 0x2e, 0x6f, 0x72, 0x67, 0x2f, 0x74, 0x61, 0x73, 0x74, 0x2d,
	0x74, 0x65, 0x73, 0x74, 0x73, 0x2f, 0x63, 0x72, 0x6f, 0x73, 0x2f, 0x63, 0x6f, 0x6d, 0x6d, 0x6f,
	0x6e, 0x2f, 0x70, 0x6f, 0x77, 0x65, 0x72, 0x2f, 0x70, 0x6f, 0x77, 0x65, 0x72, 0x70, 0x62, 0x62,
	0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_one_time_metrics_proto_rawDescOnce sync.Once
	file_one_time_metrics_proto_rawDescData = file_one_time_metrics_proto_rawDesc
)

func file_one_time_metrics_proto_rawDescGZIP() []byte {
	file_one_time_metrics_proto_rawDescOnce.Do(func() {
		file_one_time_metrics_proto_rawDescData = protoimpl.X.CompressGZIP(file_one_time_metrics_proto_rawDescData)
	})
	return file_one_time_metrics_proto_rawDescData
}

var file_one_time_metrics_proto_msgTypes = make([]protoimpl.MessageInfo, 1)
var file_one_time_metrics_proto_goTypes = []interface{}{
	(*OneTimeMetrics)(nil), // 0: tast.common.power.powerpb.OneTimeMetrics
}
var file_one_time_metrics_proto_depIdxs = []int32{
	0, // [0:0] is the sub-list for method output_type
	0, // [0:0] is the sub-list for method input_type
	0, // [0:0] is the sub-list for extension type_name
	0, // [0:0] is the sub-list for extension extendee
	0, // [0:0] is the sub-list for field type_name
}

func init() { file_one_time_metrics_proto_init() }
func file_one_time_metrics_proto_init() {
	if File_one_time_metrics_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_one_time_metrics_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*OneTimeMetrics); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	file_one_time_metrics_proto_msgTypes[0].OneofWrappers = []interface{}{}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_one_time_metrics_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   1,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_one_time_metrics_proto_goTypes,
		DependencyIndexes: file_one_time_metrics_proto_depIdxs,
		MessageInfos:      file_one_time_metrics_proto_msgTypes,
	}.Build()
	File_one_time_metrics_proto = out.File
	file_one_time_metrics_proto_rawDesc = nil
	file_one_time_metrics_proto_goTypes = nil
	file_one_time_metrics_proto_depIdxs = nil
}
