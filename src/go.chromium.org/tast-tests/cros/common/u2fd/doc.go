// Copyright 2024 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package u2fd implements the libraries and utilities which are used for
// both local and remote bundles with the same name 'u2fd'.
package u2fd
