// Copyright 2024 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package utils defines utility functions common to local and remote tests.
package utils

// WiFi Direct - related constants.
const (
	// P2PGOIPAddress is a static P2P Group Owner IP address.
	P2PGOIPAddress string = "192.160.0.1"
	// P2PClientIPAddress is a static P2P Client IP address.
	P2PClientIPAddress string = "192.160.0.2"
)
