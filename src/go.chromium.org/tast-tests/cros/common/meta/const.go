// Copyright 2024 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package meta

var (
	// LocalFeature is used for parameterized fixture testing.
	LocalFeature = "localFeature"
	// LocalFeatureA is used for parameterized fixture testing.
	LocalFeatureA = "localFeatureA"
	// LocalFeatureB is used for parameterized fixture testing.
	LocalFeatureB = "localFeatureB"
	// RemoteFeature is used for parameterized fixture testing.
	RemoteFeature = "remoteFeature"
	// RemoteFeatureA is used for parameterized fixture testing.
	RemoteFeatureA = "remoteFeatureA"
	// RemoteFeatureB is used for parameterized fixture testing.
	RemoteFeatureB = "remoteFeatureB"
)
