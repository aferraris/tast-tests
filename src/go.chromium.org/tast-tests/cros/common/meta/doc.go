// Copyright 2022 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package meta contains types and data used for testing Tast.
// Please don't use it outside meta package.
package meta
