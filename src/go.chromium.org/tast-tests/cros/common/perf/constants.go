// Copyright 2023 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package perf is the supporting library that facilitates device metrics
// collection.
package perf

// Constants for Checkpoints unit tests.
const (
	idle1StartMs = 500
	idle1EndMs   = 1000

	work1StartMs = 5000
	work2StartMs = 6000
	work2EndMs   = 7000
	work3StartMs = 7100
	work1EndMs   = 8000
	work3EndMs   = 8500

	idle2StartMs = 9000
	idle2EndMs   = 10000

	tsStartMs = 5100
	tsEndMs   = 7240
)
