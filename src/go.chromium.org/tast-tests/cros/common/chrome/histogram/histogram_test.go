// Copyright 2024 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package histogram

import (
	"reflect"
	"strconv"
	"strings"
	"testing"
)

// makeHist parses a sequence of pipe-separated "<min> <max> <count>" buckets,
// e.g. "0 5 1 | 5 10 3 | 10 15 2" for [0,5):1 [5,10):3 [10,15):2.
func makeHist(t *testing.T, s string) *Histogram {
	t.Helper()

	parseNum := func(s string) int64 {
		n, err := strconv.ParseInt(s, 10, 64)
		if err != nil {
			t.Fatalf("Failed parsing %q: %v", s, err)
		}
		return n
	}

	h := &Histogram{}
	for _, b := range strings.Split(s, "|") {
		nums := strings.Fields(b)
		if len(nums) == 0 {
			continue
		} else if len(nums) != 3 {
			t.Fatalf("Didn't find \"<min> <max> <samples>\" in %q", b)
		}
		h.Buckets = append(h.Buckets, Bucket{parseNum(nums[0]), parseNum(nums[1]), parseNum(nums[2])})
		// Compute the sum from the bucket for testing. For now, it estimates all
		// samples are in the minimum value.
		h.Sum += parseNum(nums[0]) * parseNum(nums[2])
	}
	return h
}

func TestHistogramValidate(t *testing.T) {
	for _, tc := range []struct {
		buckets string // passed to makeHist
		valid   bool
	}{
		{"", true},
		{"0 3 4", true},
		{"6 8 1", true},
		{"-1 1 1", true},
		{"0 3 4 | 3 6 8", true},
		{"0 1 4 | 1 2 8 | 2 3 5", true},
		{"2 2 5", false},          // max must be >= min
		{"3 2 5", false},          // max must be >= min
		{"5 6 1 | 4 5 2", false},  // buckets must be increasing
		{"0 5 1 | 4 10 2", false}, // buckets can't overlap
	} {
		h := makeHist(t, tc.buckets)
		err := h.Validate()
		if err != nil && tc.valid {
			t.Errorf("Validate() failed for %v: %v", *h, err)
		} else if err == nil && !tc.valid {
			t.Errorf("Validate() unexpectedly succeeded for %v", *h)
		}
	}
}

func TestHistogramDiff(t *testing.T) {
	const expectErr = "err"
	for _, tc := range []struct {
		before, after, diff string // passed to makeHist
	}{
		{
			before: "0 5 1 | 5 10 2 | 10 20 2",
			after:  "0 5 3 | 5 10 6 | 10 20 3",
			diff:   "0 5 2 | 5 10 4 | 10 20 1",
		},
		{
			before: "0 5 1 | 5 10 2 |        ",
			after:  "0 5 3 | 5 10 6 | 10 20 3",
			diff:   "0 5 2 | 5 10 4 | 10 20 3",
		},
		{
			before: "0 5 1 |        | 10 20 2",
			after:  "0 5 3 | 5 10 6 | 10 20 3",
			diff:   "0 5 2 | 5 10 6 | 10 20 1",
		},
		{
			before: "      | 5 10 1 | 10 20 2",
			after:  "0 5 3 | 5 10 6 | 10 20 3",
			diff:   "0 5 3 | 5 10 5 | 10 20 1",
		},
		{
			before: "      |        |        ",
			after:  "0 5 3 | 5 10 6 | 10 20 3",
			diff:   "0 5 3 | 5 10 6 | 10 20 3",
		},
		{
			before: "      | 5 10 2 |        ",
			after:  "0 5 3 | 5 10 6 | 10 20 3",
			diff:   "0 5 3 | 5 10 4 | 10 20 3",
		},
		{
			before: "0 5 1 | 5 10 2 | 10 20 2",
			after:  "0 5 1 | 5 10 2 | 10 20 2",
			diff:   "", // unchanged (same values)
		},
		{
			before: "",
			after:  "",
			diff:   "", // unchanged (both empty)
		},
		{
			before: "0 5 2",
			after:  "0 5 1",
			diff:   expectErr, // count decreased
		},
		{
			before: "0 4 1",
			after:  "0 5 2",
			diff:   expectErr, // same min but different max
		},
		{
			before: "0 5 1",
			after:  "1 5 2",
			diff:   expectErr, // same max but before min is less
		},
		{
			before: "1 5 1",
			after:  "0 5 2",
			diff:   expectErr, // same max but before min is greater
		},
		{
			before: "0 5 1 | 5 10 2 | 10 20 3",
			after:  "0 5 2 |        | 10 20 4",
			diff:   expectErr, // before has bucket not in after
		},
		{
			before: "1 5 1",
			after:  "",
			diff:   expectErr, // after is empty
		},
	} {
		before := makeHist(t, tc.before)
		after := makeHist(t, tc.after)
		diff, err := after.Diff(before)
		if err != nil {
			if tc.diff != expectErr {
				t.Errorf("%v.Diff(%v) failed: %v", after, before, err)
			}
		} else {
			if tc.diff == expectErr {
				t.Errorf("%v.Diff(%v) = %v; want error", after, before, diff)
			} else {
				exp := makeHist(t, tc.diff)
				if !reflect.DeepEqual(*diff, *exp) {
					t.Errorf("%v.Diff(%v) = %v; want %v", after, before, diff, exp)
				}
			}
		}
	}
}

func TestHistogramPercentile(t *testing.T) {
	const expectErr = "err"
	for _, tc := range []struct {
		hist       string
		percentile int
		result     float64
		err        bool
	}{
		{
			hist:       "0 1 1",
			percentile: -1,
			result:     0,
			err:        true, // Percentile less than 0.
		},
		{
			hist:       "0 1 1",
			percentile: 101,
			result:     0,
			err:        true, // Percentile greater than 100.
		},
		{
			hist:       "",
			percentile: 50,
			result:     0,
			err:        true, // No histogram data.
		},
		{
			hist:       "0 1 100",
			percentile: 0,
			result:     0.5,
			err:        false,
		},
		{
			hist:       "0 1 100",
			percentile: 100,
			result:     0.5,
			err:        false,
		},
		{
			hist:       "0 1 1 | 1 2 1 | 2 3 1 | 3 4 1",
			percentile: 75,
			result:     2.5,
			err:        false,
		},
		{
			hist:       "0 1 1 | 1 2 1 | 2 3 1 | 3 4 1",
			percentile: 76,
			result:     3.5,
			err:        false,
		},
	} {
		hist := makeHist(t, tc.hist)
		result, err := hist.Percentile(tc.percentile)
		if err != nil {
			if !tc.err {
				t.Errorf("%v.Percentile(%d) failed: %v", hist, tc.percentile, err)
			}
		} else {
			if tc.err {
				t.Errorf("%v.Percentile(%d) = %v; want error", hist, tc.percentile, result)
			} else if result != tc.result {
				t.Errorf("%v.Percentile(%d) = %v; want %v", hist, tc.percentile, result, tc.result)
			}
		}
	}
}
