// Copyright 2023 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package tbdep contains utilities and constants used for setting TestBedDeps.
//
// For details about what dependencies are supported and how they are parsed,
// see the converter and reverter functions in
// https://source.chromium.org/chromium/infra/infra/+/main:go/src/infra/libs/skylab/inventory/autotest/labels/.
//
// Note: If you do not see a function/constant for the dep you want, please add
// one. Adding usage documentation is highly encouraged.
package tbdep
