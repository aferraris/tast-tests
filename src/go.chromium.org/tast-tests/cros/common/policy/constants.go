// Copyright 2023 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package policy

// Constants for the DMServer Environments go/dmserver-envs.

// DMServerAlphaURL defines the pre-prod DMServer endpoint.
const DMServerAlphaURL = "https://crosman-alpha.sandbox.google.com/devicemanagement/data/api"

// DMServerStagingURL defines the staging DMServer endpoint.
const DMServerStagingURL = "https://crosman-staging.sandbox.google.com/devicemanagement/data/api"

// DMServerProdURL defines the prod DMServer endpoint.
const DMServerProdURL = "https://m.google.com/devicemanagement/data/api"
