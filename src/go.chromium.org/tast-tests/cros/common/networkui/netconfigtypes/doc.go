// Copyright 2023 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package netconfigtypes contains types used by cros network config.
// This package lives in the common folder so that remote tests can also access types.
package netconfigtypes
