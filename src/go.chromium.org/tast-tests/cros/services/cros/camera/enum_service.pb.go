// Copyright 2024 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.27.1
// 	protoc        v4.23.3
// source: enum_service.proto

package camera

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	emptypb "google.golang.org/protobuf/types/known/emptypb"
	reflect "reflect"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

var File_enum_service_proto protoreflect.FileDescriptor

var file_enum_service_proto_rawDesc = []byte{
	0x0a, 0x12, 0x65, 0x6e, 0x75, 0x6d, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x70,
	0x72, 0x6f, 0x74, 0x6f, 0x12, 0x10, 0x74, 0x61, 0x73, 0x74, 0x2e, 0x63, 0x72, 0x6f, 0x73, 0x2e,
	0x63, 0x61, 0x6d, 0x65, 0x72, 0x61, 0x1a, 0x1b, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2f, 0x70,
	0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2f, 0x65, 0x6d, 0x70, 0x74, 0x79, 0x2e, 0x70, 0x72,
	0x6f, 0x74, 0x6f, 0x32, 0x55, 0x0a, 0x0b, 0x45, 0x6e, 0x75, 0x6d, 0x53, 0x65, 0x72, 0x76, 0x69,
	0x63, 0x65, 0x12, 0x46, 0x0a, 0x12, 0x43, 0x68, 0x65, 0x63, 0x6b, 0x42, 0x75, 0x69, 0x6c, 0x74,
	0x69, 0x6e, 0x43, 0x61, 0x6d, 0x65, 0x72, 0x61, 0x12, 0x16, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c,
	0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x45, 0x6d, 0x70, 0x74, 0x79,
	0x1a, 0x16, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62,
	0x75, 0x66, 0x2e, 0x45, 0x6d, 0x70, 0x74, 0x79, 0x22, 0x00, 0x42, 0x36, 0x5a, 0x34, 0x67, 0x6f,
	0x2e, 0x63, 0x68, 0x72, 0x6f, 0x6d, 0x69, 0x75, 0x6d, 0x2e, 0x6f, 0x72, 0x67, 0x2f, 0x74, 0x61,
	0x73, 0x74, 0x2d, 0x74, 0x65, 0x73, 0x74, 0x73, 0x2f, 0x63, 0x72, 0x6f, 0x73, 0x2f, 0x73, 0x65,
	0x72, 0x76, 0x69, 0x63, 0x65, 0x73, 0x2f, 0x63, 0x72, 0x6f, 0x73, 0x2f, 0x63, 0x61, 0x6d, 0x65,
	0x72, 0x61, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var file_enum_service_proto_goTypes = []interface{}{
	(*emptypb.Empty)(nil), // 0: google.protobuf.Empty
}
var file_enum_service_proto_depIdxs = []int32{
	0, // 0: tast.cros.camera.EnumService.CheckBuiltinCamera:input_type -> google.protobuf.Empty
	0, // 1: tast.cros.camera.EnumService.CheckBuiltinCamera:output_type -> google.protobuf.Empty
	1, // [1:2] is the sub-list for method output_type
	0, // [0:1] is the sub-list for method input_type
	0, // [0:0] is the sub-list for extension type_name
	0, // [0:0] is the sub-list for extension extendee
	0, // [0:0] is the sub-list for field type_name
}

func init() { file_enum_service_proto_init() }
func file_enum_service_proto_init() {
	if File_enum_service_proto != nil {
		return
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_enum_service_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   0,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_enum_service_proto_goTypes,
		DependencyIndexes: file_enum_service_proto_depIdxs,
	}.Build()
	File_enum_service_proto = out.File
	file_enum_service_proto_rawDesc = nil
	file_enum_service_proto_goTypes = nil
	file_enum_service_proto_depIdxs = nil
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConnInterface

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion6

// EnumServiceClient is the client API for EnumService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type EnumServiceClient interface {
	// CheckBuiltinCamera checks whether built-in cameras are all enumerated.
	// This call returns an error if at least one built-in camera was not
	// enumerated. Otherwise, the error is nil.
	CheckBuiltinCamera(ctx context.Context, in *emptypb.Empty, opts ...grpc.CallOption) (*emptypb.Empty, error)
}

type enumServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewEnumServiceClient(cc grpc.ClientConnInterface) EnumServiceClient {
	return &enumServiceClient{cc}
}

func (c *enumServiceClient) CheckBuiltinCamera(ctx context.Context, in *emptypb.Empty, opts ...grpc.CallOption) (*emptypb.Empty, error) {
	out := new(emptypb.Empty)
	err := c.cc.Invoke(ctx, "/tast.cros.camera.EnumService/CheckBuiltinCamera", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// EnumServiceServer is the server API for EnumService service.
type EnumServiceServer interface {
	// CheckBuiltinCamera checks whether built-in cameras are all enumerated.
	// This call returns an error if at least one built-in camera was not
	// enumerated. Otherwise, the error is nil.
	CheckBuiltinCamera(context.Context, *emptypb.Empty) (*emptypb.Empty, error)
}

// UnimplementedEnumServiceServer can be embedded to have forward compatible implementations.
type UnimplementedEnumServiceServer struct {
}

func (*UnimplementedEnumServiceServer) CheckBuiltinCamera(context.Context, *emptypb.Empty) (*emptypb.Empty, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CheckBuiltinCamera not implemented")
}

func RegisterEnumServiceServer(s *grpc.Server, srv EnumServiceServer) {
	s.RegisterService(&_EnumService_serviceDesc, srv)
}

func _EnumService_CheckBuiltinCamera_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(emptypb.Empty)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(EnumServiceServer).CheckBuiltinCamera(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/tast.cros.camera.EnumService/CheckBuiltinCamera",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(EnumServiceServer).CheckBuiltinCamera(ctx, req.(*emptypb.Empty))
	}
	return interceptor(ctx, in, info, handler)
}

var _EnumService_serviceDesc = grpc.ServiceDesc{
	ServiceName: "tast.cros.camera.EnumService",
	HandlerType: (*EnumServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "CheckBuiltinCamera",
			Handler:    _EnumService_CheckBuiltinCamera_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "enum_service.proto",
}
