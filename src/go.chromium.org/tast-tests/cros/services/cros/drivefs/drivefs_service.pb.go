// Copyright 2023 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.27.1
// 	protoc        v3.21.9
// source: drivefs_service.proto

package drivefs

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

// Waits for the `username` drivefs process to become mounted.
type WaitForMountRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Username string `protobuf:"bytes,1,opt,name=username,proto3" json:"username,omitempty"`
}

func (x *WaitForMountRequest) Reset() {
	*x = WaitForMountRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_drivefs_service_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *WaitForMountRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*WaitForMountRequest) ProtoMessage() {}

func (x *WaitForMountRequest) ProtoReflect() protoreflect.Message {
	mi := &file_drivefs_service_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use WaitForMountRequest.ProtoReflect.Descriptor instead.
func (*WaitForMountRequest) Descriptor() ([]byte, []int) {
	return file_drivefs_service_proto_rawDescGZIP(), []int{0}
}

func (x *WaitForMountRequest) GetUsername() string {
	if x != nil {
		return x.Username
	}
	return ""
}

type WaitForMountResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	MountPath string `protobuf:"bytes,1,opt,name=MountPath,proto3" json:"MountPath,omitempty"`
}

func (x *WaitForMountResponse) Reset() {
	*x = WaitForMountResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_drivefs_service_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *WaitForMountResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*WaitForMountResponse) ProtoMessage() {}

func (x *WaitForMountResponse) ProtoReflect() protoreflect.Message {
	mi := &file_drivefs_service_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use WaitForMountResponse.ProtoReflect.Descriptor instead.
func (*WaitForMountResponse) Descriptor() ([]byte, []int) {
	return file_drivefs_service_proto_rawDescGZIP(), []int{1}
}

func (x *WaitForMountResponse) GetMountPath() string {
	if x != nil {
		return x.MountPath
	}
	return ""
}

var File_drivefs_service_proto protoreflect.FileDescriptor

var file_drivefs_service_proto_rawDesc = []byte{
	0x0a, 0x15, 0x64, 0x72, 0x69, 0x76, 0x65, 0x66, 0x73, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63,
	0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x11, 0x74, 0x61, 0x73, 0x74, 0x2e, 0x63, 0x72,
	0x6f, 0x73, 0x2e, 0x64, 0x72, 0x69, 0x76, 0x65, 0x66, 0x73, 0x22, 0x31, 0x0a, 0x13, 0x57, 0x61,
	0x69, 0x74, 0x46, 0x6f, 0x72, 0x4d, 0x6f, 0x75, 0x6e, 0x74, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73,
	0x74, 0x12, 0x1a, 0x0a, 0x08, 0x75, 0x73, 0x65, 0x72, 0x6e, 0x61, 0x6d, 0x65, 0x18, 0x01, 0x20,
	0x01, 0x28, 0x09, 0x52, 0x08, 0x75, 0x73, 0x65, 0x72, 0x6e, 0x61, 0x6d, 0x65, 0x22, 0x34, 0x0a,
	0x14, 0x57, 0x61, 0x69, 0x74, 0x46, 0x6f, 0x72, 0x4d, 0x6f, 0x75, 0x6e, 0x74, 0x52, 0x65, 0x73,
	0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x1c, 0x0a, 0x09, 0x4d, 0x6f, 0x75, 0x6e, 0x74, 0x50, 0x61,
	0x74, 0x68, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x4d, 0x6f, 0x75, 0x6e, 0x74, 0x50,
	0x61, 0x74, 0x68, 0x32, 0x73, 0x0a, 0x0e, 0x44, 0x72, 0x69, 0x76, 0x65, 0x46, 0x73, 0x53, 0x65,
	0x72, 0x76, 0x69, 0x63, 0x65, 0x12, 0x61, 0x0a, 0x0c, 0x57, 0x61, 0x69, 0x74, 0x46, 0x6f, 0x72,
	0x4d, 0x6f, 0x75, 0x6e, 0x74, 0x12, 0x26, 0x2e, 0x74, 0x61, 0x73, 0x74, 0x2e, 0x63, 0x72, 0x6f,
	0x73, 0x2e, 0x64, 0x72, 0x69, 0x76, 0x65, 0x66, 0x73, 0x2e, 0x57, 0x61, 0x69, 0x74, 0x46, 0x6f,
	0x72, 0x4d, 0x6f, 0x75, 0x6e, 0x74, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x27, 0x2e,
	0x74, 0x61, 0x73, 0x74, 0x2e, 0x63, 0x72, 0x6f, 0x73, 0x2e, 0x64, 0x72, 0x69, 0x76, 0x65, 0x66,
	0x73, 0x2e, 0x57, 0x61, 0x69, 0x74, 0x46, 0x6f, 0x72, 0x4d, 0x6f, 0x75, 0x6e, 0x74, 0x52, 0x65,
	0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x22, 0x00, 0x42, 0x37, 0x5a, 0x35, 0x67, 0x6f, 0x2e, 0x63,
	0x68, 0x72, 0x6f, 0x6d, 0x69, 0x75, 0x6d, 0x2e, 0x6f, 0x72, 0x67, 0x2f, 0x74, 0x61, 0x73, 0x74,
	0x2d, 0x74, 0x65, 0x73, 0x74, 0x73, 0x2f, 0x63, 0x72, 0x6f, 0x73, 0x2f, 0x73, 0x65, 0x72, 0x76,
	0x69, 0x63, 0x65, 0x73, 0x2f, 0x63, 0x72, 0x6f, 0x73, 0x2f, 0x64, 0x72, 0x69, 0x76, 0x65, 0x66,
	0x73, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_drivefs_service_proto_rawDescOnce sync.Once
	file_drivefs_service_proto_rawDescData = file_drivefs_service_proto_rawDesc
)

func file_drivefs_service_proto_rawDescGZIP() []byte {
	file_drivefs_service_proto_rawDescOnce.Do(func() {
		file_drivefs_service_proto_rawDescData = protoimpl.X.CompressGZIP(file_drivefs_service_proto_rawDescData)
	})
	return file_drivefs_service_proto_rawDescData
}

var file_drivefs_service_proto_msgTypes = make([]protoimpl.MessageInfo, 2)
var file_drivefs_service_proto_goTypes = []interface{}{
	(*WaitForMountRequest)(nil),  // 0: tast.cros.drivefs.WaitForMountRequest
	(*WaitForMountResponse)(nil), // 1: tast.cros.drivefs.WaitForMountResponse
}
var file_drivefs_service_proto_depIdxs = []int32{
	0, // 0: tast.cros.drivefs.DriveFsService.WaitForMount:input_type -> tast.cros.drivefs.WaitForMountRequest
	1, // 1: tast.cros.drivefs.DriveFsService.WaitForMount:output_type -> tast.cros.drivefs.WaitForMountResponse
	1, // [1:2] is the sub-list for method output_type
	0, // [0:1] is the sub-list for method input_type
	0, // [0:0] is the sub-list for extension type_name
	0, // [0:0] is the sub-list for extension extendee
	0, // [0:0] is the sub-list for field type_name
}

func init() { file_drivefs_service_proto_init() }
func file_drivefs_service_proto_init() {
	if File_drivefs_service_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_drivefs_service_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*WaitForMountRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_drivefs_service_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*WaitForMountResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_drivefs_service_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   2,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_drivefs_service_proto_goTypes,
		DependencyIndexes: file_drivefs_service_proto_depIdxs,
		MessageInfos:      file_drivefs_service_proto_msgTypes,
	}.Build()
	File_drivefs_service_proto = out.File
	file_drivefs_service_proto_rawDesc = nil
	file_drivefs_service_proto_goTypes = nil
	file_drivefs_service_proto_depIdxs = nil
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConnInterface

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion6

// DriveFsServiceClient is the client API for DriveFsService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type DriveFsServiceClient interface {
	WaitForMount(ctx context.Context, in *WaitForMountRequest, opts ...grpc.CallOption) (*WaitForMountResponse, error)
}

type driveFsServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewDriveFsServiceClient(cc grpc.ClientConnInterface) DriveFsServiceClient {
	return &driveFsServiceClient{cc}
}

func (c *driveFsServiceClient) WaitForMount(ctx context.Context, in *WaitForMountRequest, opts ...grpc.CallOption) (*WaitForMountResponse, error) {
	out := new(WaitForMountResponse)
	err := c.cc.Invoke(ctx, "/tast.cros.drivefs.DriveFsService/WaitForMount", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// DriveFsServiceServer is the server API for DriveFsService service.
type DriveFsServiceServer interface {
	WaitForMount(context.Context, *WaitForMountRequest) (*WaitForMountResponse, error)
}

// UnimplementedDriveFsServiceServer can be embedded to have forward compatible implementations.
type UnimplementedDriveFsServiceServer struct {
}

func (*UnimplementedDriveFsServiceServer) WaitForMount(context.Context, *WaitForMountRequest) (*WaitForMountResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method WaitForMount not implemented")
}

func RegisterDriveFsServiceServer(s *grpc.Server, srv DriveFsServiceServer) {
	s.RegisterService(&_DriveFsService_serviceDesc, srv)
}

func _DriveFsService_WaitForMount_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(WaitForMountRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(DriveFsServiceServer).WaitForMount(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/tast.cros.drivefs.DriveFsService/WaitForMount",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(DriveFsServiceServer).WaitForMount(ctx, req.(*WaitForMountRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _DriveFsService_serviceDesc = grpc.ServiceDesc{
	ServiceName: "tast.cros.drivefs.DriveFsService",
	HandlerType: (*DriveFsServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "WaitForMount",
			Handler:    _DriveFsService_WaitForMount_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "drivefs_service.proto",
}
