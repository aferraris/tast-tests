// Copyright 2023 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.27.1
// 	protoc        v4.23.3
// source: nebraska.proto

package nebraska

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	emptypb "google.golang.org/protobuf/types/known/emptypb"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type StartRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Port                  *int32 `protobuf:"varint,1,opt,name=port,proto3,oneof" json:"port,omitempty"`
	ConfigureUpdateEngine bool   `protobuf:"varint,2,opt,name=configure_update_engine,json=configureUpdateEngine,proto3" json:"configure_update_engine,omitempty"`
}

func (x *StartRequest) Reset() {
	*x = StartRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_nebraska_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *StartRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*StartRequest) ProtoMessage() {}

func (x *StartRequest) ProtoReflect() protoreflect.Message {
	mi := &file_nebraska_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use StartRequest.ProtoReflect.Descriptor instead.
func (*StartRequest) Descriptor() ([]byte, []int) {
	return file_nebraska_proto_rawDescGZIP(), []int{0}
}

func (x *StartRequest) GetPort() int32 {
	if x != nil && x.Port != nil {
		return *x.Port
	}
	return 0
}

func (x *StartRequest) GetConfigureUpdateEngine() bool {
	if x != nil {
		return x.ConfigureUpdateEngine
	}
	return false
}

type UpdatePayloadRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Update *Payload `protobuf:"bytes,1,opt,name=update,proto3" json:"update,omitempty"`
}

func (x *UpdatePayloadRequest) Reset() {
	*x = UpdatePayloadRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_nebraska_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *UpdatePayloadRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*UpdatePayloadRequest) ProtoMessage() {}

func (x *UpdatePayloadRequest) ProtoReflect() protoreflect.Message {
	mi := &file_nebraska_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use UpdatePayloadRequest.ProtoReflect.Descriptor instead.
func (*UpdatePayloadRequest) Descriptor() ([]byte, []int) {
	return file_nebraska_proto_rawDescGZIP(), []int{1}
}

func (x *UpdatePayloadRequest) GetUpdate() *Payload {
	if x != nil {
		return x.Update
	}
	return nil
}

type Payload struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Address        string `protobuf:"bytes,1,opt,name=address,proto3" json:"address,omitempty"`
	MetadataFolder string `protobuf:"bytes,2,opt,name=metadata_folder,json=metadataFolder,proto3" json:"metadata_folder,omitempty"`
}

func (x *Payload) Reset() {
	*x = Payload{}
	if protoimpl.UnsafeEnabled {
		mi := &file_nebraska_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Payload) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Payload) ProtoMessage() {}

func (x *Payload) ProtoReflect() protoreflect.Message {
	mi := &file_nebraska_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Payload.ProtoReflect.Descriptor instead.
func (*Payload) Descriptor() ([]byte, []int) {
	return file_nebraska_proto_rawDescGZIP(), []int{2}
}

func (x *Payload) GetAddress() string {
	if x != nil {
		return x.Address
	}
	return ""
}

func (x *Payload) GetMetadataFolder() string {
	if x != nil {
		return x.MetadataFolder
	}
	return ""
}

type StartResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Port        int32  `protobuf:"varint,1,opt,name=port,proto3" json:"port,omitempty"`
	RuntimeRoot string `protobuf:"bytes,3,opt,name=runtime_root,json=runtimeRoot,proto3" json:"runtime_root,omitempty"`
}

func (x *StartResponse) Reset() {
	*x = StartResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_nebraska_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *StartResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*StartResponse) ProtoMessage() {}

func (x *StartResponse) ProtoReflect() protoreflect.Message {
	mi := &file_nebraska_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use StartResponse.ProtoReflect.Descriptor instead.
func (*StartResponse) Descriptor() ([]byte, []int) {
	return file_nebraska_proto_rawDescGZIP(), []int{3}
}

func (x *StartResponse) GetPort() int32 {
	if x != nil {
		return x.Port
	}
	return 0
}

func (x *StartResponse) GetRuntimeRoot() string {
	if x != nil {
		return x.RuntimeRoot
	}
	return ""
}

type ReadLogResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Data []byte `protobuf:"bytes,1,opt,name=data,proto3" json:"data,omitempty"`
}

func (x *ReadLogResponse) Reset() {
	*x = ReadLogResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_nebraska_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ReadLogResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ReadLogResponse) ProtoMessage() {}

func (x *ReadLogResponse) ProtoReflect() protoreflect.Message {
	mi := &file_nebraska_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ReadLogResponse.ProtoReflect.Descriptor instead.
func (*ReadLogResponse) Descriptor() ([]byte, []int) {
	return file_nebraska_proto_rawDescGZIP(), []int{4}
}

func (x *ReadLogResponse) GetData() []byte {
	if x != nil {
		return x.Data
	}
	return nil
}

type SetInvalidateLastUpdateRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	InvalidateLastUpdate bool `protobuf:"varint,1,opt,name=invalidateLastUpdate,proto3" json:"invalidateLastUpdate,omitempty"`
}

func (x *SetInvalidateLastUpdateRequest) Reset() {
	*x = SetInvalidateLastUpdateRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_nebraska_proto_msgTypes[5]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *SetInvalidateLastUpdateRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*SetInvalidateLastUpdateRequest) ProtoMessage() {}

func (x *SetInvalidateLastUpdateRequest) ProtoReflect() protoreflect.Message {
	mi := &file_nebraska_proto_msgTypes[5]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use SetInvalidateLastUpdateRequest.ProtoReflect.Descriptor instead.
func (*SetInvalidateLastUpdateRequest) Descriptor() ([]byte, []int) {
	return file_nebraska_proto_rawDescGZIP(), []int{5}
}

func (x *SetInvalidateLastUpdateRequest) GetInvalidateLastUpdate() bool {
	if x != nil {
		return x.InvalidateLastUpdate
	}
	return false
}

type SetIsRollbackRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	IsRollback bool `protobuf:"varint,1,opt,name=isRollback,proto3" json:"isRollback,omitempty"`
}

func (x *SetIsRollbackRequest) Reset() {
	*x = SetIsRollbackRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_nebraska_proto_msgTypes[6]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *SetIsRollbackRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*SetIsRollbackRequest) ProtoMessage() {}

func (x *SetIsRollbackRequest) ProtoReflect() protoreflect.Message {
	mi := &file_nebraska_proto_msgTypes[6]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use SetIsRollbackRequest.ProtoReflect.Descriptor instead.
func (*SetIsRollbackRequest) Descriptor() ([]byte, []int) {
	return file_nebraska_proto_rawDescGZIP(), []int{6}
}

func (x *SetIsRollbackRequest) GetIsRollback() bool {
	if x != nil {
		return x.IsRollback
	}
	return false
}

type SetCriticalUpdateRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	IsCritical bool `protobuf:"varint,1,opt,name=isCritical,proto3" json:"isCritical,omitempty"`
}

func (x *SetCriticalUpdateRequest) Reset() {
	*x = SetCriticalUpdateRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_nebraska_proto_msgTypes[7]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *SetCriticalUpdateRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*SetCriticalUpdateRequest) ProtoMessage() {}

func (x *SetCriticalUpdateRequest) ProtoReflect() protoreflect.Message {
	mi := &file_nebraska_proto_msgTypes[7]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use SetCriticalUpdateRequest.ProtoReflect.Descriptor instead.
func (*SetCriticalUpdateRequest) Descriptor() ([]byte, []int) {
	return file_nebraska_proto_rawDescGZIP(), []int{7}
}

func (x *SetCriticalUpdateRequest) GetIsCritical() bool {
	if x != nil {
		return x.IsCritical
	}
	return false
}

var File_nebraska_proto protoreflect.FileDescriptor

var file_nebraska_proto_rawDesc = []byte{
	0x0a, 0x0e, 0x6e, 0x65, 0x62, 0x72, 0x61, 0x73, 0x6b, 0x61, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f,
	0x12, 0x12, 0x74, 0x61, 0x73, 0x74, 0x2e, 0x63, 0x72, 0x6f, 0x73, 0x2e, 0x6e, 0x65, 0x62, 0x72,
	0x61, 0x73, 0x6b, 0x61, 0x1a, 0x1b, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2f, 0x70, 0x72, 0x6f,
	0x74, 0x6f, 0x62, 0x75, 0x66, 0x2f, 0x65, 0x6d, 0x70, 0x74, 0x79, 0x2e, 0x70, 0x72, 0x6f, 0x74,
	0x6f, 0x22, 0x68, 0x0a, 0x0c, 0x53, 0x74, 0x61, 0x72, 0x74, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73,
	0x74, 0x12, 0x17, 0x0a, 0x04, 0x70, 0x6f, 0x72, 0x74, 0x18, 0x01, 0x20, 0x01, 0x28, 0x05, 0x48,
	0x00, 0x52, 0x04, 0x70, 0x6f, 0x72, 0x74, 0x88, 0x01, 0x01, 0x12, 0x36, 0x0a, 0x17, 0x63, 0x6f,
	0x6e, 0x66, 0x69, 0x67, 0x75, 0x72, 0x65, 0x5f, 0x75, 0x70, 0x64, 0x61, 0x74, 0x65, 0x5f, 0x65,
	0x6e, 0x67, 0x69, 0x6e, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x08, 0x52, 0x15, 0x63, 0x6f, 0x6e,
	0x66, 0x69, 0x67, 0x75, 0x72, 0x65, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x45, 0x6e, 0x67, 0x69,
	0x6e, 0x65, 0x42, 0x07, 0x0a, 0x05, 0x5f, 0x70, 0x6f, 0x72, 0x74, 0x22, 0x4b, 0x0a, 0x14, 0x55,
	0x70, 0x64, 0x61, 0x74, 0x65, 0x50, 0x61, 0x79, 0x6c, 0x6f, 0x61, 0x64, 0x52, 0x65, 0x71, 0x75,
	0x65, 0x73, 0x74, 0x12, 0x33, 0x0a, 0x06, 0x75, 0x70, 0x64, 0x61, 0x74, 0x65, 0x18, 0x01, 0x20,
	0x01, 0x28, 0x0b, 0x32, 0x1b, 0x2e, 0x74, 0x61, 0x73, 0x74, 0x2e, 0x63, 0x72, 0x6f, 0x73, 0x2e,
	0x6e, 0x65, 0x62, 0x72, 0x61, 0x73, 0x6b, 0x61, 0x2e, 0x50, 0x61, 0x79, 0x6c, 0x6f, 0x61, 0x64,
	0x52, 0x06, 0x75, 0x70, 0x64, 0x61, 0x74, 0x65, 0x22, 0x4c, 0x0a, 0x07, 0x50, 0x61, 0x79, 0x6c,
	0x6f, 0x61, 0x64, 0x12, 0x18, 0x0a, 0x07, 0x61, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x18, 0x01,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x61, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x12, 0x27, 0x0a,
	0x0f, 0x6d, 0x65, 0x74, 0x61, 0x64, 0x61, 0x74, 0x61, 0x5f, 0x66, 0x6f, 0x6c, 0x64, 0x65, 0x72,
	0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0e, 0x6d, 0x65, 0x74, 0x61, 0x64, 0x61, 0x74, 0x61,
	0x46, 0x6f, 0x6c, 0x64, 0x65, 0x72, 0x22, 0x4c, 0x0a, 0x0d, 0x53, 0x74, 0x61, 0x72, 0x74, 0x52,
	0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x12, 0x0a, 0x04, 0x70, 0x6f, 0x72, 0x74, 0x18,
	0x01, 0x20, 0x01, 0x28, 0x05, 0x52, 0x04, 0x70, 0x6f, 0x72, 0x74, 0x12, 0x21, 0x0a, 0x0c, 0x72,
	0x75, 0x6e, 0x74, 0x69, 0x6d, 0x65, 0x5f, 0x72, 0x6f, 0x6f, 0x74, 0x18, 0x03, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x0b, 0x72, 0x75, 0x6e, 0x74, 0x69, 0x6d, 0x65, 0x52, 0x6f, 0x6f, 0x74, 0x4a, 0x04,
	0x08, 0x02, 0x10, 0x03, 0x22, 0x25, 0x0a, 0x0f, 0x52, 0x65, 0x61, 0x64, 0x4c, 0x6f, 0x67, 0x52,
	0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x12, 0x0a, 0x04, 0x64, 0x61, 0x74, 0x61, 0x18,
	0x01, 0x20, 0x01, 0x28, 0x0c, 0x52, 0x04, 0x64, 0x61, 0x74, 0x61, 0x22, 0x54, 0x0a, 0x1e, 0x53,
	0x65, 0x74, 0x49, 0x6e, 0x76, 0x61, 0x6c, 0x69, 0x64, 0x61, 0x74, 0x65, 0x4c, 0x61, 0x73, 0x74,
	0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x32, 0x0a,
	0x14, 0x69, 0x6e, 0x76, 0x61, 0x6c, 0x69, 0x64, 0x61, 0x74, 0x65, 0x4c, 0x61, 0x73, 0x74, 0x55,
	0x70, 0x64, 0x61, 0x74, 0x65, 0x18, 0x01, 0x20, 0x01, 0x28, 0x08, 0x52, 0x14, 0x69, 0x6e, 0x76,
	0x61, 0x6c, 0x69, 0x64, 0x61, 0x74, 0x65, 0x4c, 0x61, 0x73, 0x74, 0x55, 0x70, 0x64, 0x61, 0x74,
	0x65, 0x22, 0x36, 0x0a, 0x14, 0x53, 0x65, 0x74, 0x49, 0x73, 0x52, 0x6f, 0x6c, 0x6c, 0x62, 0x61,
	0x63, 0x6b, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x1e, 0x0a, 0x0a, 0x69, 0x73, 0x52,
	0x6f, 0x6c, 0x6c, 0x62, 0x61, 0x63, 0x6b, 0x18, 0x01, 0x20, 0x01, 0x28, 0x08, 0x52, 0x0a, 0x69,
	0x73, 0x52, 0x6f, 0x6c, 0x6c, 0x62, 0x61, 0x63, 0x6b, 0x22, 0x3a, 0x0a, 0x18, 0x53, 0x65, 0x74,
	0x43, 0x72, 0x69, 0x74, 0x69, 0x63, 0x61, 0x6c, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x52, 0x65,
	0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x1e, 0x0a, 0x0a, 0x69, 0x73, 0x43, 0x72, 0x69, 0x74, 0x69,
	0x63, 0x61, 0x6c, 0x18, 0x01, 0x20, 0x01, 0x28, 0x08, 0x52, 0x0a, 0x69, 0x73, 0x43, 0x72, 0x69,
	0x74, 0x69, 0x63, 0x61, 0x6c, 0x32, 0xcd, 0x04, 0x0a, 0x07, 0x53, 0x65, 0x72, 0x76, 0x69, 0x63,
	0x65, 0x12, 0x4e, 0x0a, 0x05, 0x53, 0x74, 0x61, 0x72, 0x74, 0x12, 0x20, 0x2e, 0x74, 0x61, 0x73,
	0x74, 0x2e, 0x63, 0x72, 0x6f, 0x73, 0x2e, 0x6e, 0x65, 0x62, 0x72, 0x61, 0x73, 0x6b, 0x61, 0x2e,
	0x53, 0x74, 0x61, 0x72, 0x74, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x21, 0x2e, 0x74,
	0x61, 0x73, 0x74, 0x2e, 0x63, 0x72, 0x6f, 0x73, 0x2e, 0x6e, 0x65, 0x62, 0x72, 0x61, 0x73, 0x6b,
	0x61, 0x2e, 0x53, 0x74, 0x61, 0x72, 0x74, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x22,
	0x00, 0x12, 0x38, 0x0a, 0x04, 0x53, 0x74, 0x6f, 0x70, 0x12, 0x16, 0x2e, 0x67, 0x6f, 0x6f, 0x67,
	0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x45, 0x6d, 0x70, 0x74,
	0x79, 0x1a, 0x16, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f,
	0x62, 0x75, 0x66, 0x2e, 0x45, 0x6d, 0x70, 0x74, 0x79, 0x22, 0x00, 0x12, 0x53, 0x0a, 0x0d, 0x55,
	0x70, 0x64, 0x61, 0x74, 0x65, 0x50, 0x61, 0x79, 0x6c, 0x6f, 0x61, 0x64, 0x12, 0x28, 0x2e, 0x74,
	0x61, 0x73, 0x74, 0x2e, 0x63, 0x72, 0x6f, 0x73, 0x2e, 0x6e, 0x65, 0x62, 0x72, 0x61, 0x73, 0x6b,
	0x61, 0x2e, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x50, 0x61, 0x79, 0x6c, 0x6f, 0x61, 0x64, 0x52,
	0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x16, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e,
	0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x45, 0x6d, 0x70, 0x74, 0x79, 0x22, 0x00,
	0x12, 0x48, 0x0a, 0x07, 0x52, 0x65, 0x61, 0x64, 0x4c, 0x6f, 0x67, 0x12, 0x16, 0x2e, 0x67, 0x6f,
	0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x45, 0x6d,
	0x70, 0x74, 0x79, 0x1a, 0x23, 0x2e, 0x74, 0x61, 0x73, 0x74, 0x2e, 0x63, 0x72, 0x6f, 0x73, 0x2e,
	0x6e, 0x65, 0x62, 0x72, 0x61, 0x73, 0x6b, 0x61, 0x2e, 0x52, 0x65, 0x61, 0x64, 0x4c, 0x6f, 0x67,
	0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x22, 0x00, 0x12, 0x67, 0x0a, 0x17, 0x53, 0x65,
	0x74, 0x49, 0x6e, 0x76, 0x61, 0x6c, 0x69, 0x64, 0x61, 0x74, 0x65, 0x4c, 0x61, 0x73, 0x74, 0x55,
	0x70, 0x64, 0x61, 0x74, 0x65, 0x12, 0x32, 0x2e, 0x74, 0x61, 0x73, 0x74, 0x2e, 0x63, 0x72, 0x6f,
	0x73, 0x2e, 0x6e, 0x65, 0x62, 0x72, 0x61, 0x73, 0x6b, 0x61, 0x2e, 0x53, 0x65, 0x74, 0x49, 0x6e,
	0x76, 0x61, 0x6c, 0x69, 0x64, 0x61, 0x74, 0x65, 0x4c, 0x61, 0x73, 0x74, 0x55, 0x70, 0x64, 0x61,
	0x74, 0x65, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x16, 0x2e, 0x67, 0x6f, 0x6f, 0x67,
	0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x45, 0x6d, 0x70, 0x74,
	0x79, 0x22, 0x00, 0x12, 0x53, 0x0a, 0x0d, 0x53, 0x65, 0x74, 0x49, 0x73, 0x52, 0x6f, 0x6c, 0x6c,
	0x62, 0x61, 0x63, 0x6b, 0x12, 0x28, 0x2e, 0x74, 0x61, 0x73, 0x74, 0x2e, 0x63, 0x72, 0x6f, 0x73,
	0x2e, 0x6e, 0x65, 0x62, 0x72, 0x61, 0x73, 0x6b, 0x61, 0x2e, 0x53, 0x65, 0x74, 0x49, 0x73, 0x52,
	0x6f, 0x6c, 0x6c, 0x62, 0x61, 0x63, 0x6b, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x16,
	0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66,
	0x2e, 0x45, 0x6d, 0x70, 0x74, 0x79, 0x22, 0x00, 0x12, 0x5b, 0x0a, 0x11, 0x53, 0x65, 0x74, 0x43,
	0x72, 0x69, 0x74, 0x69, 0x63, 0x61, 0x6c, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x12, 0x2c, 0x2e,
	0x74, 0x61, 0x73, 0x74, 0x2e, 0x63, 0x72, 0x6f, 0x73, 0x2e, 0x6e, 0x65, 0x62, 0x72, 0x61, 0x73,
	0x6b, 0x61, 0x2e, 0x53, 0x65, 0x74, 0x43, 0x72, 0x69, 0x74, 0x69, 0x63, 0x61, 0x6c, 0x55, 0x70,
	0x64, 0x61, 0x74, 0x65, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x16, 0x2e, 0x67, 0x6f,
	0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x45, 0x6d,
	0x70, 0x74, 0x79, 0x22, 0x00, 0x42, 0x38, 0x5a, 0x36, 0x67, 0x6f, 0x2e, 0x63, 0x68, 0x72, 0x6f,
	0x6d, 0x69, 0x75, 0x6d, 0x2e, 0x6f, 0x72, 0x67, 0x2f, 0x74, 0x61, 0x73, 0x74, 0x2d, 0x74, 0x65,
	0x73, 0x74, 0x73, 0x2f, 0x63, 0x72, 0x6f, 0x73, 0x2f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65,
	0x73, 0x2f, 0x63, 0x72, 0x6f, 0x73, 0x2f, 0x6e, 0x65, 0x62, 0x72, 0x61, 0x73, 0x6b, 0x61, 0x62,
	0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_nebraska_proto_rawDescOnce sync.Once
	file_nebraska_proto_rawDescData = file_nebraska_proto_rawDesc
)

func file_nebraska_proto_rawDescGZIP() []byte {
	file_nebraska_proto_rawDescOnce.Do(func() {
		file_nebraska_proto_rawDescData = protoimpl.X.CompressGZIP(file_nebraska_proto_rawDescData)
	})
	return file_nebraska_proto_rawDescData
}

var file_nebraska_proto_msgTypes = make([]protoimpl.MessageInfo, 8)
var file_nebraska_proto_goTypes = []interface{}{
	(*StartRequest)(nil),                   // 0: tast.cros.nebraska.StartRequest
	(*UpdatePayloadRequest)(nil),           // 1: tast.cros.nebraska.UpdatePayloadRequest
	(*Payload)(nil),                        // 2: tast.cros.nebraska.Payload
	(*StartResponse)(nil),                  // 3: tast.cros.nebraska.StartResponse
	(*ReadLogResponse)(nil),                // 4: tast.cros.nebraska.ReadLogResponse
	(*SetInvalidateLastUpdateRequest)(nil), // 5: tast.cros.nebraska.SetInvalidateLastUpdateRequest
	(*SetIsRollbackRequest)(nil),           // 6: tast.cros.nebraska.SetIsRollbackRequest
	(*SetCriticalUpdateRequest)(nil),       // 7: tast.cros.nebraska.SetCriticalUpdateRequest
	(*emptypb.Empty)(nil),                  // 8: google.protobuf.Empty
}
var file_nebraska_proto_depIdxs = []int32{
	2, // 0: tast.cros.nebraska.UpdatePayloadRequest.update:type_name -> tast.cros.nebraska.Payload
	0, // 1: tast.cros.nebraska.Service.Start:input_type -> tast.cros.nebraska.StartRequest
	8, // 2: tast.cros.nebraska.Service.Stop:input_type -> google.protobuf.Empty
	1, // 3: tast.cros.nebraska.Service.UpdatePayload:input_type -> tast.cros.nebraska.UpdatePayloadRequest
	8, // 4: tast.cros.nebraska.Service.ReadLog:input_type -> google.protobuf.Empty
	5, // 5: tast.cros.nebraska.Service.SetInvalidateLastUpdate:input_type -> tast.cros.nebraska.SetInvalidateLastUpdateRequest
	6, // 6: tast.cros.nebraska.Service.SetIsRollback:input_type -> tast.cros.nebraska.SetIsRollbackRequest
	7, // 7: tast.cros.nebraska.Service.SetCriticalUpdate:input_type -> tast.cros.nebraska.SetCriticalUpdateRequest
	3, // 8: tast.cros.nebraska.Service.Start:output_type -> tast.cros.nebraska.StartResponse
	8, // 9: tast.cros.nebraska.Service.Stop:output_type -> google.protobuf.Empty
	8, // 10: tast.cros.nebraska.Service.UpdatePayload:output_type -> google.protobuf.Empty
	4, // 11: tast.cros.nebraska.Service.ReadLog:output_type -> tast.cros.nebraska.ReadLogResponse
	8, // 12: tast.cros.nebraska.Service.SetInvalidateLastUpdate:output_type -> google.protobuf.Empty
	8, // 13: tast.cros.nebraska.Service.SetIsRollback:output_type -> google.protobuf.Empty
	8, // 14: tast.cros.nebraska.Service.SetCriticalUpdate:output_type -> google.protobuf.Empty
	8, // [8:15] is the sub-list for method output_type
	1, // [1:8] is the sub-list for method input_type
	1, // [1:1] is the sub-list for extension type_name
	1, // [1:1] is the sub-list for extension extendee
	0, // [0:1] is the sub-list for field type_name
}

func init() { file_nebraska_proto_init() }
func file_nebraska_proto_init() {
	if File_nebraska_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_nebraska_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*StartRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_nebraska_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*UpdatePayloadRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_nebraska_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Payload); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_nebraska_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*StartResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_nebraska_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ReadLogResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_nebraska_proto_msgTypes[5].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*SetInvalidateLastUpdateRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_nebraska_proto_msgTypes[6].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*SetIsRollbackRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_nebraska_proto_msgTypes[7].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*SetCriticalUpdateRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	file_nebraska_proto_msgTypes[0].OneofWrappers = []interface{}{}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_nebraska_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   8,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_nebraska_proto_goTypes,
		DependencyIndexes: file_nebraska_proto_depIdxs,
		MessageInfos:      file_nebraska_proto_msgTypes,
	}.Build()
	File_nebraska_proto = out.File
	file_nebraska_proto_rawDesc = nil
	file_nebraska_proto_goTypes = nil
	file_nebraska_proto_depIdxs = nil
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConnInterface

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion6

// ServiceClient is the client API for Service service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type ServiceClient interface {
	Start(ctx context.Context, in *StartRequest, opts ...grpc.CallOption) (*StartResponse, error)
	Stop(ctx context.Context, in *emptypb.Empty, opts ...grpc.CallOption) (*emptypb.Empty, error)
	UpdatePayload(ctx context.Context, in *UpdatePayloadRequest, opts ...grpc.CallOption) (*emptypb.Empty, error)
	ReadLog(ctx context.Context, in *emptypb.Empty, opts ...grpc.CallOption) (*ReadLogResponse, error)
	SetInvalidateLastUpdate(ctx context.Context, in *SetInvalidateLastUpdateRequest, opts ...grpc.CallOption) (*emptypb.Empty, error)
	SetIsRollback(ctx context.Context, in *SetIsRollbackRequest, opts ...grpc.CallOption) (*emptypb.Empty, error)
	SetCriticalUpdate(ctx context.Context, in *SetCriticalUpdateRequest, opts ...grpc.CallOption) (*emptypb.Empty, error)
}

type serviceClient struct {
	cc grpc.ClientConnInterface
}

func NewServiceClient(cc grpc.ClientConnInterface) ServiceClient {
	return &serviceClient{cc}
}

func (c *serviceClient) Start(ctx context.Context, in *StartRequest, opts ...grpc.CallOption) (*StartResponse, error) {
	out := new(StartResponse)
	err := c.cc.Invoke(ctx, "/tast.cros.nebraska.Service/Start", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *serviceClient) Stop(ctx context.Context, in *emptypb.Empty, opts ...grpc.CallOption) (*emptypb.Empty, error) {
	out := new(emptypb.Empty)
	err := c.cc.Invoke(ctx, "/tast.cros.nebraska.Service/Stop", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *serviceClient) UpdatePayload(ctx context.Context, in *UpdatePayloadRequest, opts ...grpc.CallOption) (*emptypb.Empty, error) {
	out := new(emptypb.Empty)
	err := c.cc.Invoke(ctx, "/tast.cros.nebraska.Service/UpdatePayload", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *serviceClient) ReadLog(ctx context.Context, in *emptypb.Empty, opts ...grpc.CallOption) (*ReadLogResponse, error) {
	out := new(ReadLogResponse)
	err := c.cc.Invoke(ctx, "/tast.cros.nebraska.Service/ReadLog", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *serviceClient) SetInvalidateLastUpdate(ctx context.Context, in *SetInvalidateLastUpdateRequest, opts ...grpc.CallOption) (*emptypb.Empty, error) {
	out := new(emptypb.Empty)
	err := c.cc.Invoke(ctx, "/tast.cros.nebraska.Service/SetInvalidateLastUpdate", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *serviceClient) SetIsRollback(ctx context.Context, in *SetIsRollbackRequest, opts ...grpc.CallOption) (*emptypb.Empty, error) {
	out := new(emptypb.Empty)
	err := c.cc.Invoke(ctx, "/tast.cros.nebraska.Service/SetIsRollback", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *serviceClient) SetCriticalUpdate(ctx context.Context, in *SetCriticalUpdateRequest, opts ...grpc.CallOption) (*emptypb.Empty, error) {
	out := new(emptypb.Empty)
	err := c.cc.Invoke(ctx, "/tast.cros.nebraska.Service/SetCriticalUpdate", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// ServiceServer is the server API for Service service.
type ServiceServer interface {
	Start(context.Context, *StartRequest) (*StartResponse, error)
	Stop(context.Context, *emptypb.Empty) (*emptypb.Empty, error)
	UpdatePayload(context.Context, *UpdatePayloadRequest) (*emptypb.Empty, error)
	ReadLog(context.Context, *emptypb.Empty) (*ReadLogResponse, error)
	SetInvalidateLastUpdate(context.Context, *SetInvalidateLastUpdateRequest) (*emptypb.Empty, error)
	SetIsRollback(context.Context, *SetIsRollbackRequest) (*emptypb.Empty, error)
	SetCriticalUpdate(context.Context, *SetCriticalUpdateRequest) (*emptypb.Empty, error)
}

// UnimplementedServiceServer can be embedded to have forward compatible implementations.
type UnimplementedServiceServer struct {
}

func (*UnimplementedServiceServer) Start(context.Context, *StartRequest) (*StartResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Start not implemented")
}
func (*UnimplementedServiceServer) Stop(context.Context, *emptypb.Empty) (*emptypb.Empty, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Stop not implemented")
}
func (*UnimplementedServiceServer) UpdatePayload(context.Context, *UpdatePayloadRequest) (*emptypb.Empty, error) {
	return nil, status.Errorf(codes.Unimplemented, "method UpdatePayload not implemented")
}
func (*UnimplementedServiceServer) ReadLog(context.Context, *emptypb.Empty) (*ReadLogResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ReadLog not implemented")
}
func (*UnimplementedServiceServer) SetInvalidateLastUpdate(context.Context, *SetInvalidateLastUpdateRequest) (*emptypb.Empty, error) {
	return nil, status.Errorf(codes.Unimplemented, "method SetInvalidateLastUpdate not implemented")
}
func (*UnimplementedServiceServer) SetIsRollback(context.Context, *SetIsRollbackRequest) (*emptypb.Empty, error) {
	return nil, status.Errorf(codes.Unimplemented, "method SetIsRollback not implemented")
}
func (*UnimplementedServiceServer) SetCriticalUpdate(context.Context, *SetCriticalUpdateRequest) (*emptypb.Empty, error) {
	return nil, status.Errorf(codes.Unimplemented, "method SetCriticalUpdate not implemented")
}

func RegisterServiceServer(s *grpc.Server, srv ServiceServer) {
	s.RegisterService(&_Service_serviceDesc, srv)
}

func _Service_Start_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(StartRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ServiceServer).Start(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/tast.cros.nebraska.Service/Start",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ServiceServer).Start(ctx, req.(*StartRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Service_Stop_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(emptypb.Empty)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ServiceServer).Stop(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/tast.cros.nebraska.Service/Stop",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ServiceServer).Stop(ctx, req.(*emptypb.Empty))
	}
	return interceptor(ctx, in, info, handler)
}

func _Service_UpdatePayload_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UpdatePayloadRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ServiceServer).UpdatePayload(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/tast.cros.nebraska.Service/UpdatePayload",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ServiceServer).UpdatePayload(ctx, req.(*UpdatePayloadRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Service_ReadLog_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(emptypb.Empty)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ServiceServer).ReadLog(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/tast.cros.nebraska.Service/ReadLog",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ServiceServer).ReadLog(ctx, req.(*emptypb.Empty))
	}
	return interceptor(ctx, in, info, handler)
}

func _Service_SetInvalidateLastUpdate_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(SetInvalidateLastUpdateRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ServiceServer).SetInvalidateLastUpdate(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/tast.cros.nebraska.Service/SetInvalidateLastUpdate",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ServiceServer).SetInvalidateLastUpdate(ctx, req.(*SetInvalidateLastUpdateRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Service_SetIsRollback_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(SetIsRollbackRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ServiceServer).SetIsRollback(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/tast.cros.nebraska.Service/SetIsRollback",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ServiceServer).SetIsRollback(ctx, req.(*SetIsRollbackRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Service_SetCriticalUpdate_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(SetCriticalUpdateRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ServiceServer).SetCriticalUpdate(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/tast.cros.nebraska.Service/SetCriticalUpdate",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ServiceServer).SetCriticalUpdate(ctx, req.(*SetCriticalUpdateRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _Service_serviceDesc = grpc.ServiceDesc{
	ServiceName: "tast.cros.nebraska.Service",
	HandlerType: (*ServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "Start",
			Handler:    _Service_Start_Handler,
		},
		{
			MethodName: "Stop",
			Handler:    _Service_Stop_Handler,
		},
		{
			MethodName: "UpdatePayload",
			Handler:    _Service_UpdatePayload_Handler,
		},
		{
			MethodName: "ReadLog",
			Handler:    _Service_ReadLog_Handler,
		},
		{
			MethodName: "SetInvalidateLastUpdate",
			Handler:    _Service_SetInvalidateLastUpdate_Handler,
		},
		{
			MethodName: "SetIsRollback",
			Handler:    _Service_SetIsRollback_Handler,
		},
		{
			MethodName: "SetCriticalUpdate",
			Handler:    _Service_SetCriticalUpdate_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "nebraska.proto",
}
