// Copyright 2024 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

syntax = "proto3";

package tast.cros.audio;

option go_package = "go.chromium.org/tast-tests/cros/services/cros/audio";

// CrasNodeID is the ID of the cras ionode.
// Source: adhd::cras/src/include/cras_types.h::cras_node_id_t
message CrasNodeID {
  int32 deviceIndex = 1;
  int32 nodeIndex = 2;
}

// CrasNodeType refers to different cras node types printed by
// cras_test_service. Source:
// adhd::cras/include/cras_types.h::cras_node_type_to_str
enum CrasNodeType {
  CRAS_NODE_TYPE_UNKNOWN = 0;
  CRAS_NODE_TYPE_INTERNAL_SPEAKER = 1;
  CRAS_NODE_TYPE_HEADPHONE = 2;
  CRAS_NODE_TYPE_HDMI = 3;
  CRAS_NODE_TYPE_HAPTIC = 4;
  CRAS_NODE_TYPE_INTERNAL_MIC = 5;
  CRAS_NODE_TYPE_FRONT_MIC = 6;
  CRAS_NODE_TYPE_REAR_MIC = 7;
  CRAS_NODE_TYPE_KEYBOARD_MIC = 8;
  CRAS_NODE_TYPE_MIC = 9;
  CRAS_NODE_TYPE_HOTWORD = 10;
  CRAS_NODE_TYPE_LINEOUT = 11;
  CRAS_NODE_TYPE_POST_MIX_LOOPBACK = 12;
  CRAS_NODE_TYPE_POST_DSP_LOOPBACK = 13;
  CRAS_NODE_TYPE_POST_DSP_DELAYED_LOOPBACK = 14;
  CRAS_NODE_TYPE_USB = 15;
  CRAS_NODE_TYPE_BLUETOOTH = 16;
  CRAS_NODE_TYPE_BLUETOOTH_NB_MIC = 17;
  CRAS_NODE_TYPE_FALLBACK_NORMAL = 18;
  CRAS_NODE_TYPE_FALLBACK_ABNORMAL = 19;
  CRAS_NODE_TYPE_ECHO_REFERENCE = 20;
  CRAS_NODE_TYPE_ALSA_LOOPBACK = 21;
  CRAS_NODE_TYPE_FLEXIBLE_LOOPBACK = 22;
  CRAS_NODE_TYPE_FLEXIBLE_LOOPBACK_INTERNAL = 23;
}

// CrasIODevType refers to different test IO dev types supported by
// cras_test_service. Source: adhd::cras/include/cras_types.h::TEST_IODEV_TYPE
enum CrasIODevType {
  CRAS_IO_DEV_TYPE_HOTWORD = 0;
}

// CrasStreamType refers to types of audio streams.
// Source1: adhd::cras/client/cras-sys/src/lib.rs::CRAS_STREAM_TYPE
// Source2: adhd::cras/include/cras_types.h::CRAS_STREAM_TYPE
enum CrasStreamType {
  CRAS_STREAM_TYPE_DEFAULT = 0;
  CRAS_STREAM_TYPE_MULTIMEDIA = 1;
  CRAS_STREAM_TYPE_VOICE_COMMUNICATION = 2;
  CRAS_STREAM_TYPE_SPEECH_RECOGNITION = 3;
  CRAS_STREAM_TYPE_PRO_AUDIO = 4;
  CRAS_STREAM_TYPE_ACCESSIBILITY = 5;
}

// CrasClientType refers to different types of audio clients.
// Source1: adhd::cras/client/cras-sys/src/lib.rs::CRAS_CLIENT_TYPE
// Source2: adhd::cras/include/cras_types.h::CRAS_CLIENT_TYPE
enum CrasClientType {
  CRAS_CLIENT_TYPE_UNKNOWN = 0;
  CRAS_CLIENT_TYPE_LEGACY = 1;
  CRAS_CLIENT_TYPE_TEST = 2;
  CRAS_CLIENT_TYPE_PCM = 3;
  CRAS_CLIENT_TYPE_CHROME = 4;
  CRAS_CLIENT_TYPE_ARC = 5;
  CRAS_CLIENT_TYPE_CROSVM = 6;
  CRAS_CLIENT_TYPE_SERVER_STREAM = 7;
  CRAS_CLIENT_TYPE_LACROS = 8;
}

// ConnectionType enumerates cras_client connection types.
// Source: adhd::cras/include/cras_types.h::CRAS_CONNECTION_TYPE
enum CrasConnectionType {
  CRAS_CONNECTION_TYPE_CONTROL = 0;
  CRAS_CONNECTION_TYPE_PLAYBACK = 1;
  CRAS_CONNECTION_TYPE_CAPTURE = 2;
  CRAS_CONNECTION_TYPE_VMS_LEGACY = 3;
  CRAS_CONNECTION_TYPE_VMS_UNIFIED = 4;
  CRAS_CONNECTION_TYPE_PLUGIN_PLAYBACK = 5;
  CRAS_CONNECTION_TYPE_PLUGIN_UNIFIED = 6;
}

// SampleFormat refers to audio sample formats supported by CRAS.
// Source: adhd::cras/client/cras_tests/src/arguments.rs::SampleFormatArg
enum SampleFormat {
  SAMPLE_FORMAT_U8 = 0;
  SAMPLE_FORMAT_S16_LE = 1;
  SAMPLE_FORMAT_S24_LE = 2;
  SAMPLE_FORMAT_S32_LE = 3;
}

// CaptureEffect enumerates different capture effects.
enum CaptureEffect {
  // Specifies the AEC effect ("0x01" bitmask).
  CAPTURE_EFFECT_AEC = 0;

  // Specifies the NS effect ("0x02" bitmask).
  CAPTURE_EFFECT_NS = 1;

  // Specifies the AGC effect ("0x04" bitmask).
  CAPTURE_EFFECT_AGC = 2;

  // Specifies the VAD effect ("0x08 bitmask).
  CAPTURE_EFFECT_VAD = 3;

  // Specifies the AEC on DSP allowed effect ("0x10" bitmask).
  CAPTURE_EFFECT_AEC_ON_DSP_ALLOWED = 4;

  // Specifies the NS on DSP allowed effect ("0x20" bitmask).
  CAPTURE_EFFECT_NS_ON_DSP_ALLOWED = 5;

  // Specifies the AGC on DSP allowed effect ("0x40" bitmask).
  CAPTURE_EFFECT_AGC_ON_DSP_ALLOWED = 6;
}

//
// Source: adhd::cras/include/cras_iodev_info.h::CRAS_IODEV_LAST_OPEN_RESULT
enum CrasIODevLastOpenResult {
  IO_DEV_LAST_OPEN_RESULT_UNKNOWN = 0;
  IO_DEV_LAST_OPEN_RESULT_SUCCESS = 1;
  IO_DEV_LAST_OPEN_RESULT_FAILURE = 2;
}

// Represents a CrasNode, with data limited what is relevant for testing.
message CrasNode {
  // False for output nodes, true for input nodes.
  bool is_input = 1;

  // The id of this node. It is unique among all nodes including both output and
  // input nodes.
  uint64 id = 2;

  // The type of this node.
  CrasNodeType type = 3;

  // The name of the device that this node belongs to. For example,
  // "HDA Intel PCH: CA0132 Analog:0,0" or "Creative SB Arena Headset".
  string device_name = 4;

  // Whether this node is currently used for output/input. There is one active
  // node for output and one active node for input.
  bool active = 5;

  // The node volume indexed from 0 to 100.
  uint64 node_volume = 6;
}