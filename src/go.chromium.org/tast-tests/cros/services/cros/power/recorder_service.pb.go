// Copyright 2023 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.27.1
// 	protoc        v4.23.3
// source: recorder_service.proto

package power

import (
	context "context"
	perfpb "go.chromium.org/tast-tests/cros/common/perf/perfpb"
	powerpb "go.chromium.org/tast-tests/cros/common/power/powerpb"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	emptypb "google.golang.org/protobuf/types/known/emptypb"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type RecorderRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	IntervalSec int64 `protobuf:"varint,1,opt,name=interval_sec,json=intervalSec,proto3" json:"interval_sec,omitempty"`
}

func (x *RecorderRequest) Reset() {
	*x = RecorderRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_recorder_service_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *RecorderRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*RecorderRequest) ProtoMessage() {}

func (x *RecorderRequest) ProtoReflect() protoreflect.Message {
	mi := &file_recorder_service_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use RecorderRequest.ProtoReflect.Descriptor instead.
func (*RecorderRequest) Descriptor() ([]byte, []int) {
	return file_recorder_service_proto_rawDescGZIP(), []int{0}
}

func (x *RecorderRequest) GetIntervalSec() int64 {
	if x != nil {
		return x.IntervalSec
	}
	return 0
}

type RecorderResult struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	PerfMetrics    *perfpb.Values          `protobuf:"bytes,1,opt,name=perf_metrics,json=perfMetrics,proto3" json:"perf_metrics,omitempty"`
	OneTimeMetrics *powerpb.OneTimeMetrics `protobuf:"bytes,2,opt,name=one_time_metrics,json=oneTimeMetrics,proto3" json:"one_time_metrics,omitempty"`
	DeviceInfo     *powerpb.DeviceInfo     `protobuf:"bytes,3,opt,name=device_info,json=deviceInfo,proto3" json:"device_info,omitempty"`
}

func (x *RecorderResult) Reset() {
	*x = RecorderResult{}
	if protoimpl.UnsafeEnabled {
		mi := &file_recorder_service_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *RecorderResult) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*RecorderResult) ProtoMessage() {}

func (x *RecorderResult) ProtoReflect() protoreflect.Message {
	mi := &file_recorder_service_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use RecorderResult.ProtoReflect.Descriptor instead.
func (*RecorderResult) Descriptor() ([]byte, []int) {
	return file_recorder_service_proto_rawDescGZIP(), []int{1}
}

func (x *RecorderResult) GetPerfMetrics() *perfpb.Values {
	if x != nil {
		return x.PerfMetrics
	}
	return nil
}

func (x *RecorderResult) GetOneTimeMetrics() *powerpb.OneTimeMetrics {
	if x != nil {
		return x.OneTimeMetrics
	}
	return nil
}

func (x *RecorderResult) GetDeviceInfo() *powerpb.DeviceInfo {
	if x != nil {
		return x.DeviceInfo
	}
	return nil
}

var File_recorder_service_proto protoreflect.FileDescriptor

var file_recorder_service_proto_rawDesc = []byte{
	0x0a, 0x16, 0x72, 0x65, 0x63, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69,
	0x63, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x0f, 0x74, 0x61, 0x73, 0x74, 0x2e, 0x63,
	0x72, 0x6f, 0x73, 0x2e, 0x70, 0x6f, 0x77, 0x65, 0x72, 0x1a, 0x1b, 0x67, 0x6f, 0x6f, 0x67, 0x6c,
	0x65, 0x2f, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2f, 0x65, 0x6d, 0x70, 0x74, 0x79,
	0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x1a, 0x0c, 0x76, 0x61, 0x6c, 0x75, 0x65, 0x73, 0x2e, 0x70,
	0x72, 0x6f, 0x74, 0x6f, 0x1a, 0x16, 0x6f, 0x6e, 0x65, 0x5f, 0x74, 0x69, 0x6d, 0x65, 0x5f, 0x6d,
	0x65, 0x74, 0x72, 0x69, 0x63, 0x73, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x1a, 0x11, 0x64, 0x65,
	0x76, 0x69, 0x63, 0x65, 0x5f, 0x69, 0x6e, 0x66, 0x6f, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x22,
	0x34, 0x0a, 0x0f, 0x52, 0x65, 0x63, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x52, 0x65, 0x71, 0x75, 0x65,
	0x73, 0x74, 0x12, 0x21, 0x0a, 0x0c, 0x69, 0x6e, 0x74, 0x65, 0x72, 0x76, 0x61, 0x6c, 0x5f, 0x73,
	0x65, 0x63, 0x18, 0x01, 0x20, 0x01, 0x28, 0x03, 0x52, 0x0b, 0x69, 0x6e, 0x74, 0x65, 0x72, 0x76,
	0x61, 0x6c, 0x53, 0x65, 0x63, 0x22, 0xf1, 0x01, 0x0a, 0x0e, 0x52, 0x65, 0x63, 0x6f, 0x72, 0x64,
	0x65, 0x72, 0x52, 0x65, 0x73, 0x75, 0x6c, 0x74, 0x12, 0x42, 0x0a, 0x0c, 0x70, 0x65, 0x72, 0x66,
	0x5f, 0x6d, 0x65, 0x74, 0x72, 0x69, 0x63, 0x73, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x1f,
	0x2e, 0x74, 0x61, 0x73, 0x74, 0x2e, 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x2e, 0x70, 0x65, 0x72,
	0x66, 0x2e, 0x70, 0x65, 0x72, 0x66, 0x70, 0x62, 0x2e, 0x56, 0x61, 0x6c, 0x75, 0x65, 0x73, 0x52,
	0x0b, 0x70, 0x65, 0x72, 0x66, 0x4d, 0x65, 0x74, 0x72, 0x69, 0x63, 0x73, 0x12, 0x53, 0x0a, 0x10,
	0x6f, 0x6e, 0x65, 0x5f, 0x74, 0x69, 0x6d, 0x65, 0x5f, 0x6d, 0x65, 0x74, 0x72, 0x69, 0x63, 0x73,
	0x18, 0x02, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x29, 0x2e, 0x74, 0x61, 0x73, 0x74, 0x2e, 0x63, 0x6f,
	0x6d, 0x6d, 0x6f, 0x6e, 0x2e, 0x70, 0x6f, 0x77, 0x65, 0x72, 0x2e, 0x70, 0x6f, 0x77, 0x65, 0x72,
	0x70, 0x62, 0x2e, 0x4f, 0x6e, 0x65, 0x54, 0x69, 0x6d, 0x65, 0x4d, 0x65, 0x74, 0x72, 0x69, 0x63,
	0x73, 0x52, 0x0e, 0x6f, 0x6e, 0x65, 0x54, 0x69, 0x6d, 0x65, 0x4d, 0x65, 0x74, 0x72, 0x69, 0x63,
	0x73, 0x12, 0x46, 0x0a, 0x0b, 0x64, 0x65, 0x76, 0x69, 0x63, 0x65, 0x5f, 0x69, 0x6e, 0x66, 0x6f,
	0x18, 0x03, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x25, 0x2e, 0x74, 0x61, 0x73, 0x74, 0x2e, 0x63, 0x6f,
	0x6d, 0x6d, 0x6f, 0x6e, 0x2e, 0x70, 0x6f, 0x77, 0x65, 0x72, 0x2e, 0x70, 0x6f, 0x77, 0x65, 0x72,
	0x70, 0x62, 0x2e, 0x44, 0x65, 0x76, 0x69, 0x63, 0x65, 0x49, 0x6e, 0x66, 0x6f, 0x52, 0x0a, 0x64,
	0x65, 0x76, 0x69, 0x63, 0x65, 0x49, 0x6e, 0x66, 0x6f, 0x32, 0xce, 0x02, 0x0a, 0x0f, 0x52, 0x65,
	0x63, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x53, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x12, 0x44, 0x0a,
	0x06, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x12, 0x20, 0x2e, 0x74, 0x61, 0x73, 0x74, 0x2e, 0x63,
	0x72, 0x6f, 0x73, 0x2e, 0x70, 0x6f, 0x77, 0x65, 0x72, 0x2e, 0x52, 0x65, 0x63, 0x6f, 0x72, 0x64,
	0x65, 0x72, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x16, 0x2e, 0x67, 0x6f, 0x6f, 0x67,
	0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x45, 0x6d, 0x70, 0x74,
	0x79, 0x22, 0x00, 0x12, 0x3c, 0x0a, 0x08, 0x43, 0x6f, 0x6f, 0x6c, 0x64, 0x6f, 0x77, 0x6e, 0x12,
	0x16, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75,
	0x66, 0x2e, 0x45, 0x6d, 0x70, 0x74, 0x79, 0x1a, 0x16, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65,
	0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x45, 0x6d, 0x70, 0x74, 0x79, 0x22,
	0x00, 0x12, 0x39, 0x0a, 0x05, 0x53, 0x74, 0x61, 0x72, 0x74, 0x12, 0x16, 0x2e, 0x67, 0x6f, 0x6f,
	0x67, 0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x45, 0x6d, 0x70,
	0x74, 0x79, 0x1a, 0x16, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74,
	0x6f, 0x62, 0x75, 0x66, 0x2e, 0x45, 0x6d, 0x70, 0x74, 0x79, 0x22, 0x00, 0x12, 0x41, 0x0a, 0x04,
	0x53, 0x74, 0x6f, 0x70, 0x12, 0x16, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x70, 0x72,
	0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x45, 0x6d, 0x70, 0x74, 0x79, 0x1a, 0x1f, 0x2e, 0x74,
	0x61, 0x73, 0x74, 0x2e, 0x63, 0x72, 0x6f, 0x73, 0x2e, 0x70, 0x6f, 0x77, 0x65, 0x72, 0x2e, 0x52,
	0x65, 0x63, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x52, 0x65, 0x73, 0x75, 0x6c, 0x74, 0x22, 0x00, 0x12,
	0x39, 0x0a, 0x05, 0x43, 0x6c, 0x6f, 0x73, 0x65, 0x12, 0x16, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c,
	0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x45, 0x6d, 0x70, 0x74, 0x79,
	0x1a, 0x16, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62,
	0x75, 0x66, 0x2e, 0x45, 0x6d, 0x70, 0x74, 0x79, 0x22, 0x00, 0x42, 0x35, 0x5a, 0x33, 0x67, 0x6f,
	0x2e, 0x63, 0x68, 0x72, 0x6f, 0x6d, 0x69, 0x75, 0x6d, 0x2e, 0x6f, 0x72, 0x67, 0x2f, 0x74, 0x61,
	0x73, 0x74, 0x2d, 0x74, 0x65, 0x73, 0x74, 0x73, 0x2f, 0x63, 0x72, 0x6f, 0x73, 0x2f, 0x73, 0x65,
	0x72, 0x76, 0x69, 0x63, 0x65, 0x73, 0x2f, 0x63, 0x72, 0x6f, 0x73, 0x2f, 0x70, 0x6f, 0x77, 0x65,
	0x72, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_recorder_service_proto_rawDescOnce sync.Once
	file_recorder_service_proto_rawDescData = file_recorder_service_proto_rawDesc
)

func file_recorder_service_proto_rawDescGZIP() []byte {
	file_recorder_service_proto_rawDescOnce.Do(func() {
		file_recorder_service_proto_rawDescData = protoimpl.X.CompressGZIP(file_recorder_service_proto_rawDescData)
	})
	return file_recorder_service_proto_rawDescData
}

var file_recorder_service_proto_msgTypes = make([]protoimpl.MessageInfo, 2)
var file_recorder_service_proto_goTypes = []interface{}{
	(*RecorderRequest)(nil),        // 0: tast.cros.power.RecorderRequest
	(*RecorderResult)(nil),         // 1: tast.cros.power.RecorderResult
	(*perfpb.Values)(nil),          // 2: tast.common.perf.perfpb.Values
	(*powerpb.OneTimeMetrics)(nil), // 3: tast.common.power.powerpb.OneTimeMetrics
	(*powerpb.DeviceInfo)(nil),     // 4: tast.common.power.powerpb.DeviceInfo
	(*emptypb.Empty)(nil),          // 5: google.protobuf.Empty
}
var file_recorder_service_proto_depIdxs = []int32{
	2, // 0: tast.cros.power.RecorderResult.perf_metrics:type_name -> tast.common.perf.perfpb.Values
	3, // 1: tast.cros.power.RecorderResult.one_time_metrics:type_name -> tast.common.power.powerpb.OneTimeMetrics
	4, // 2: tast.cros.power.RecorderResult.device_info:type_name -> tast.common.power.powerpb.DeviceInfo
	0, // 3: tast.cros.power.RecorderService.Create:input_type -> tast.cros.power.RecorderRequest
	5, // 4: tast.cros.power.RecorderService.Cooldown:input_type -> google.protobuf.Empty
	5, // 5: tast.cros.power.RecorderService.Start:input_type -> google.protobuf.Empty
	5, // 6: tast.cros.power.RecorderService.Stop:input_type -> google.protobuf.Empty
	5, // 7: tast.cros.power.RecorderService.Close:input_type -> google.protobuf.Empty
	5, // 8: tast.cros.power.RecorderService.Create:output_type -> google.protobuf.Empty
	5, // 9: tast.cros.power.RecorderService.Cooldown:output_type -> google.protobuf.Empty
	5, // 10: tast.cros.power.RecorderService.Start:output_type -> google.protobuf.Empty
	1, // 11: tast.cros.power.RecorderService.Stop:output_type -> tast.cros.power.RecorderResult
	5, // 12: tast.cros.power.RecorderService.Close:output_type -> google.protobuf.Empty
	8, // [8:13] is the sub-list for method output_type
	3, // [3:8] is the sub-list for method input_type
	3, // [3:3] is the sub-list for extension type_name
	3, // [3:3] is the sub-list for extension extendee
	0, // [0:3] is the sub-list for field type_name
}

func init() { file_recorder_service_proto_init() }
func file_recorder_service_proto_init() {
	if File_recorder_service_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_recorder_service_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*RecorderRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_recorder_service_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*RecorderResult); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_recorder_service_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   2,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_recorder_service_proto_goTypes,
		DependencyIndexes: file_recorder_service_proto_depIdxs,
		MessageInfos:      file_recorder_service_proto_msgTypes,
	}.Build()
	File_recorder_service_proto = out.File
	file_recorder_service_proto_rawDesc = nil
	file_recorder_service_proto_goTypes = nil
	file_recorder_service_proto_depIdxs = nil
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConnInterface

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion6

// RecorderServiceClient is the client API for RecorderService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type RecorderServiceClient interface {
	Create(ctx context.Context, in *RecorderRequest, opts ...grpc.CallOption) (*emptypb.Empty, error)
	Cooldown(ctx context.Context, in *emptypb.Empty, opts ...grpc.CallOption) (*emptypb.Empty, error)
	Start(ctx context.Context, in *emptypb.Empty, opts ...grpc.CallOption) (*emptypb.Empty, error)
	Stop(ctx context.Context, in *emptypb.Empty, opts ...grpc.CallOption) (*RecorderResult, error)
	Close(ctx context.Context, in *emptypb.Empty, opts ...grpc.CallOption) (*emptypb.Empty, error)
}

type recorderServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewRecorderServiceClient(cc grpc.ClientConnInterface) RecorderServiceClient {
	return &recorderServiceClient{cc}
}

func (c *recorderServiceClient) Create(ctx context.Context, in *RecorderRequest, opts ...grpc.CallOption) (*emptypb.Empty, error) {
	out := new(emptypb.Empty)
	err := c.cc.Invoke(ctx, "/tast.cros.power.RecorderService/Create", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *recorderServiceClient) Cooldown(ctx context.Context, in *emptypb.Empty, opts ...grpc.CallOption) (*emptypb.Empty, error) {
	out := new(emptypb.Empty)
	err := c.cc.Invoke(ctx, "/tast.cros.power.RecorderService/Cooldown", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *recorderServiceClient) Start(ctx context.Context, in *emptypb.Empty, opts ...grpc.CallOption) (*emptypb.Empty, error) {
	out := new(emptypb.Empty)
	err := c.cc.Invoke(ctx, "/tast.cros.power.RecorderService/Start", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *recorderServiceClient) Stop(ctx context.Context, in *emptypb.Empty, opts ...grpc.CallOption) (*RecorderResult, error) {
	out := new(RecorderResult)
	err := c.cc.Invoke(ctx, "/tast.cros.power.RecorderService/Stop", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *recorderServiceClient) Close(ctx context.Context, in *emptypb.Empty, opts ...grpc.CallOption) (*emptypb.Empty, error) {
	out := new(emptypb.Empty)
	err := c.cc.Invoke(ctx, "/tast.cros.power.RecorderService/Close", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// RecorderServiceServer is the server API for RecorderService service.
type RecorderServiceServer interface {
	Create(context.Context, *RecorderRequest) (*emptypb.Empty, error)
	Cooldown(context.Context, *emptypb.Empty) (*emptypb.Empty, error)
	Start(context.Context, *emptypb.Empty) (*emptypb.Empty, error)
	Stop(context.Context, *emptypb.Empty) (*RecorderResult, error)
	Close(context.Context, *emptypb.Empty) (*emptypb.Empty, error)
}

// UnimplementedRecorderServiceServer can be embedded to have forward compatible implementations.
type UnimplementedRecorderServiceServer struct {
}

func (*UnimplementedRecorderServiceServer) Create(context.Context, *RecorderRequest) (*emptypb.Empty, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Create not implemented")
}
func (*UnimplementedRecorderServiceServer) Cooldown(context.Context, *emptypb.Empty) (*emptypb.Empty, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Cooldown not implemented")
}
func (*UnimplementedRecorderServiceServer) Start(context.Context, *emptypb.Empty) (*emptypb.Empty, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Start not implemented")
}
func (*UnimplementedRecorderServiceServer) Stop(context.Context, *emptypb.Empty) (*RecorderResult, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Stop not implemented")
}
func (*UnimplementedRecorderServiceServer) Close(context.Context, *emptypb.Empty) (*emptypb.Empty, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Close not implemented")
}

func RegisterRecorderServiceServer(s *grpc.Server, srv RecorderServiceServer) {
	s.RegisterService(&_RecorderService_serviceDesc, srv)
}

func _RecorderService_Create_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(RecorderRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RecorderServiceServer).Create(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/tast.cros.power.RecorderService/Create",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RecorderServiceServer).Create(ctx, req.(*RecorderRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _RecorderService_Cooldown_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(emptypb.Empty)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RecorderServiceServer).Cooldown(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/tast.cros.power.RecorderService/Cooldown",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RecorderServiceServer).Cooldown(ctx, req.(*emptypb.Empty))
	}
	return interceptor(ctx, in, info, handler)
}

func _RecorderService_Start_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(emptypb.Empty)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RecorderServiceServer).Start(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/tast.cros.power.RecorderService/Start",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RecorderServiceServer).Start(ctx, req.(*emptypb.Empty))
	}
	return interceptor(ctx, in, info, handler)
}

func _RecorderService_Stop_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(emptypb.Empty)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RecorderServiceServer).Stop(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/tast.cros.power.RecorderService/Stop",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RecorderServiceServer).Stop(ctx, req.(*emptypb.Empty))
	}
	return interceptor(ctx, in, info, handler)
}

func _RecorderService_Close_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(emptypb.Empty)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RecorderServiceServer).Close(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/tast.cros.power.RecorderService/Close",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RecorderServiceServer).Close(ctx, req.(*emptypb.Empty))
	}
	return interceptor(ctx, in, info, handler)
}

var _RecorderService_serviceDesc = grpc.ServiceDesc{
	ServiceName: "tast.cros.power.RecorderService",
	HandlerType: (*RecorderServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "Create",
			Handler:    _RecorderService_Create_Handler,
		},
		{
			MethodName: "Cooldown",
			Handler:    _RecorderService_Cooldown_Handler,
		},
		{
			MethodName: "Start",
			Handler:    _RecorderService_Start_Handler,
		},
		{
			MethodName: "Stop",
			Handler:    _RecorderService_Stop_Handler,
		},
		{
			MethodName: "Close",
			Handler:    _RecorderService_Close_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "recorder_service.proto",
}
