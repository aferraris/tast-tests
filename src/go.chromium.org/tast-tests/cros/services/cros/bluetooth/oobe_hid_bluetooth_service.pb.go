// Copyright 2022 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.27.1
// 	protoc        v4.23.3
// source: oobe_hid_bluetooth_service.proto

package bluetooth

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	emptypb "google.golang.org/protobuf/types/known/emptypb"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type NewChromeRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// Key used in initializing chrome to OOBE.
	SigninProfileTestExtension string `protobuf:"bytes,1,opt,name=signinProfileTestExtension,proto3" json:"signinProfileTestExtension,omitempty"`
}

func (x *NewChromeRequest) Reset() {
	*x = NewChromeRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_oobe_hid_bluetooth_service_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *NewChromeRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*NewChromeRequest) ProtoMessage() {}

func (x *NewChromeRequest) ProtoReflect() protoreflect.Message {
	mi := &file_oobe_hid_bluetooth_service_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use NewChromeRequest.ProtoReflect.Descriptor instead.
func (*NewChromeRequest) Descriptor() ([]byte, []int) {
	return file_oobe_hid_bluetooth_service_proto_rawDescGZIP(), []int{0}
}

func (x *NewChromeRequest) GetSigninProfileTestExtension() string {
	if x != nil {
		return x.SigninProfileTestExtension
	}
	return ""
}

var File_oobe_hid_bluetooth_service_proto protoreflect.FileDescriptor

var file_oobe_hid_bluetooth_service_proto_rawDesc = []byte{
	0x0a, 0x20, 0x6f, 0x6f, 0x62, 0x65, 0x5f, 0x68, 0x69, 0x64, 0x5f, 0x62, 0x6c, 0x75, 0x65, 0x74,
	0x6f, 0x6f, 0x74, 0x68, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x70, 0x72, 0x6f,
	0x74, 0x6f, 0x12, 0x13, 0x74, 0x61, 0x73, 0x74, 0x2e, 0x63, 0x72, 0x6f, 0x73, 0x2e, 0x62, 0x6c,
	0x75, 0x65, 0x74, 0x6f, 0x6f, 0x74, 0x68, 0x1a, 0x1b, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2f,
	0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2f, 0x65, 0x6d, 0x70, 0x74, 0x79, 0x2e, 0x70,
	0x72, 0x6f, 0x74, 0x6f, 0x22, 0x52, 0x0a, 0x10, 0x4e, 0x65, 0x77, 0x43, 0x68, 0x72, 0x6f, 0x6d,
	0x65, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x3e, 0x0a, 0x1a, 0x73, 0x69, 0x67, 0x6e,
	0x69, 0x6e, 0x50, 0x72, 0x6f, 0x66, 0x69, 0x6c, 0x65, 0x54, 0x65, 0x73, 0x74, 0x45, 0x78, 0x74,
	0x65, 0x6e, 0x73, 0x69, 0x6f, 0x6e, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x1a, 0x73, 0x69,
	0x67, 0x6e, 0x69, 0x6e, 0x50, 0x72, 0x6f, 0x66, 0x69, 0x6c, 0x65, 0x54, 0x65, 0x73, 0x74, 0x45,
	0x78, 0x74, 0x65, 0x6e, 0x73, 0x69, 0x6f, 0x6e, 0x32, 0x9a, 0x03, 0x0a, 0x17, 0x4f, 0x6f, 0x62,
	0x65, 0x48, 0x69, 0x64, 0x42, 0x6c, 0x75, 0x65, 0x74, 0x6f, 0x6f, 0x74, 0x68, 0x53, 0x65, 0x72,
	0x76, 0x69, 0x63, 0x65, 0x12, 0x4c, 0x0a, 0x09, 0x4e, 0x65, 0x77, 0x43, 0x68, 0x72, 0x6f, 0x6d,
	0x65, 0x12, 0x25, 0x2e, 0x74, 0x61, 0x73, 0x74, 0x2e, 0x63, 0x72, 0x6f, 0x73, 0x2e, 0x62, 0x6c,
	0x75, 0x65, 0x74, 0x6f, 0x6f, 0x74, 0x68, 0x2e, 0x4e, 0x65, 0x77, 0x43, 0x68, 0x72, 0x6f, 0x6d,
	0x65, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x16, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c,
	0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x45, 0x6d, 0x70, 0x74, 0x79,
	0x22, 0x00, 0x12, 0x3f, 0x0a, 0x0b, 0x43, 0x6c, 0x6f, 0x73, 0x65, 0x43, 0x68, 0x72, 0x6f, 0x6d,
	0x65, 0x12, 0x16, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f,
	0x62, 0x75, 0x66, 0x2e, 0x45, 0x6d, 0x70, 0x74, 0x79, 0x1a, 0x16, 0x2e, 0x67, 0x6f, 0x6f, 0x67,
	0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x45, 0x6d, 0x70, 0x74,
	0x79, 0x22, 0x00, 0x12, 0x4b, 0x0a, 0x17, 0x50, 0x72, 0x6f, 0x67, 0x72, 0x65, 0x73, 0x73, 0x54,
	0x6f, 0x57, 0x65, 0x6c, 0x63, 0x6f, 0x6d, 0x65, 0x53, 0x63, 0x72, 0x65, 0x65, 0x6e, 0x12, 0x16,
	0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66,
	0x2e, 0x45, 0x6d, 0x70, 0x74, 0x79, 0x1a, 0x16, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e,
	0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x45, 0x6d, 0x70, 0x74, 0x79, 0x22, 0x00,
	0x12, 0x55, 0x0a, 0x21, 0x44, 0x69, 0x73, 0x61, 0x62, 0x6c, 0x65, 0x42, 0x6c, 0x75, 0x65, 0x74,
	0x6f, 0x6f, 0x74, 0x68, 0x46, 0x72, 0x6f, 0x6d, 0x51, 0x75, 0x69, 0x63, 0x6b, 0x53, 0x65, 0x74,
	0x74, 0x69, 0x6e, 0x67, 0x73, 0x12, 0x16, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x70,
	0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x45, 0x6d, 0x70, 0x74, 0x79, 0x1a, 0x16, 0x2e,
	0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e,
	0x45, 0x6d, 0x70, 0x74, 0x79, 0x22, 0x00, 0x12, 0x4c, 0x0a, 0x18, 0x56, 0x65, 0x72, 0x69, 0x66,
	0x79, 0x42, 0x6c, 0x75, 0x65, 0x74, 0x6f, 0x6f, 0x74, 0x68, 0x49, 0x73, 0x45, 0x6e, 0x61, 0x62,
	0x6c, 0x65, 0x64, 0x12, 0x16, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f,
	0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x45, 0x6d, 0x70, 0x74, 0x79, 0x1a, 0x16, 0x2e, 0x67, 0x6f,
	0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x45, 0x6d,
	0x70, 0x74, 0x79, 0x22, 0x00, 0x42, 0x39, 0x5a, 0x37, 0x67, 0x6f, 0x2e, 0x63, 0x68, 0x72, 0x6f,
	0x6d, 0x69, 0x75, 0x6d, 0x2e, 0x6f, 0x72, 0x67, 0x2f, 0x74, 0x61, 0x73, 0x74, 0x2d, 0x74, 0x65,
	0x73, 0x74, 0x73, 0x2f, 0x63, 0x72, 0x6f, 0x73, 0x2f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65,
	0x73, 0x2f, 0x63, 0x72, 0x6f, 0x73, 0x2f, 0x62, 0x6c, 0x75, 0x65, 0x74, 0x6f, 0x6f, 0x74, 0x68,
	0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_oobe_hid_bluetooth_service_proto_rawDescOnce sync.Once
	file_oobe_hid_bluetooth_service_proto_rawDescData = file_oobe_hid_bluetooth_service_proto_rawDesc
)

func file_oobe_hid_bluetooth_service_proto_rawDescGZIP() []byte {
	file_oobe_hid_bluetooth_service_proto_rawDescOnce.Do(func() {
		file_oobe_hid_bluetooth_service_proto_rawDescData = protoimpl.X.CompressGZIP(file_oobe_hid_bluetooth_service_proto_rawDescData)
	})
	return file_oobe_hid_bluetooth_service_proto_rawDescData
}

var file_oobe_hid_bluetooth_service_proto_msgTypes = make([]protoimpl.MessageInfo, 1)
var file_oobe_hid_bluetooth_service_proto_goTypes = []interface{}{
	(*NewChromeRequest)(nil), // 0: tast.cros.bluetooth.NewChromeRequest
	(*emptypb.Empty)(nil),    // 1: google.protobuf.Empty
}
var file_oobe_hid_bluetooth_service_proto_depIdxs = []int32{
	0, // 0: tast.cros.bluetooth.OobeHidBluetoothService.NewChrome:input_type -> tast.cros.bluetooth.NewChromeRequest
	1, // 1: tast.cros.bluetooth.OobeHidBluetoothService.CloseChrome:input_type -> google.protobuf.Empty
	1, // 2: tast.cros.bluetooth.OobeHidBluetoothService.ProgressToWelcomeScreen:input_type -> google.protobuf.Empty
	1, // 3: tast.cros.bluetooth.OobeHidBluetoothService.DisableBluetoothFromQuickSettings:input_type -> google.protobuf.Empty
	1, // 4: tast.cros.bluetooth.OobeHidBluetoothService.VerifyBluetoothIsEnabled:input_type -> google.protobuf.Empty
	1, // 5: tast.cros.bluetooth.OobeHidBluetoothService.NewChrome:output_type -> google.protobuf.Empty
	1, // 6: tast.cros.bluetooth.OobeHidBluetoothService.CloseChrome:output_type -> google.protobuf.Empty
	1, // 7: tast.cros.bluetooth.OobeHidBluetoothService.ProgressToWelcomeScreen:output_type -> google.protobuf.Empty
	1, // 8: tast.cros.bluetooth.OobeHidBluetoothService.DisableBluetoothFromQuickSettings:output_type -> google.protobuf.Empty
	1, // 9: tast.cros.bluetooth.OobeHidBluetoothService.VerifyBluetoothIsEnabled:output_type -> google.protobuf.Empty
	5, // [5:10] is the sub-list for method output_type
	0, // [0:5] is the sub-list for method input_type
	0, // [0:0] is the sub-list for extension type_name
	0, // [0:0] is the sub-list for extension extendee
	0, // [0:0] is the sub-list for field type_name
}

func init() { file_oobe_hid_bluetooth_service_proto_init() }
func file_oobe_hid_bluetooth_service_proto_init() {
	if File_oobe_hid_bluetooth_service_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_oobe_hid_bluetooth_service_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*NewChromeRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_oobe_hid_bluetooth_service_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   1,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_oobe_hid_bluetooth_service_proto_goTypes,
		DependencyIndexes: file_oobe_hid_bluetooth_service_proto_depIdxs,
		MessageInfos:      file_oobe_hid_bluetooth_service_proto_msgTypes,
	}.Build()
	File_oobe_hid_bluetooth_service_proto = out.File
	file_oobe_hid_bluetooth_service_proto_rawDesc = nil
	file_oobe_hid_bluetooth_service_proto_goTypes = nil
	file_oobe_hid_bluetooth_service_proto_depIdxs = nil
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConnInterface

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion6

// OobeHidBluetoothServiceClient is the client API for OobeHidBluetoothService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type OobeHidBluetoothServiceClient interface {
	// NewChrome starts new chrome session in oobe, verifies that HID detection screen
	// is shown. CloseChrome must be called later to clean up the associated resources.
	NewChrome(ctx context.Context, in *NewChromeRequest, opts ...grpc.CallOption) (*emptypb.Empty, error)
	// CloseChrome releases the resources obtained by NewChrome.
	CloseChrome(ctx context.Context, in *emptypb.Empty, opts ...grpc.CallOption) (*emptypb.Empty, error)
	// ProgressToWelcomeScreen Progresses from Oobe HID screen to welcome screen.
	ProgressToWelcomeScreen(ctx context.Context, in *emptypb.Empty, opts ...grpc.CallOption) (*emptypb.Empty, error)
	// DisableBluetoothFromQuickSettings powers off the bluetooth adapter from quick settings.
	DisableBluetoothFromQuickSettings(ctx context.Context, in *emptypb.Empty, opts ...grpc.CallOption) (*emptypb.Empty, error)
	// VerifyBluetoothIsEnabled verifies Bluetooth is enabled and mouse device is connected.
	VerifyBluetoothIsEnabled(ctx context.Context, in *emptypb.Empty, opts ...grpc.CallOption) (*emptypb.Empty, error)
}

type oobeHidBluetoothServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewOobeHidBluetoothServiceClient(cc grpc.ClientConnInterface) OobeHidBluetoothServiceClient {
	return &oobeHidBluetoothServiceClient{cc}
}

func (c *oobeHidBluetoothServiceClient) NewChrome(ctx context.Context, in *NewChromeRequest, opts ...grpc.CallOption) (*emptypb.Empty, error) {
	out := new(emptypb.Empty)
	err := c.cc.Invoke(ctx, "/tast.cros.bluetooth.OobeHidBluetoothService/NewChrome", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *oobeHidBluetoothServiceClient) CloseChrome(ctx context.Context, in *emptypb.Empty, opts ...grpc.CallOption) (*emptypb.Empty, error) {
	out := new(emptypb.Empty)
	err := c.cc.Invoke(ctx, "/tast.cros.bluetooth.OobeHidBluetoothService/CloseChrome", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *oobeHidBluetoothServiceClient) ProgressToWelcomeScreen(ctx context.Context, in *emptypb.Empty, opts ...grpc.CallOption) (*emptypb.Empty, error) {
	out := new(emptypb.Empty)
	err := c.cc.Invoke(ctx, "/tast.cros.bluetooth.OobeHidBluetoothService/ProgressToWelcomeScreen", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *oobeHidBluetoothServiceClient) DisableBluetoothFromQuickSettings(ctx context.Context, in *emptypb.Empty, opts ...grpc.CallOption) (*emptypb.Empty, error) {
	out := new(emptypb.Empty)
	err := c.cc.Invoke(ctx, "/tast.cros.bluetooth.OobeHidBluetoothService/DisableBluetoothFromQuickSettings", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *oobeHidBluetoothServiceClient) VerifyBluetoothIsEnabled(ctx context.Context, in *emptypb.Empty, opts ...grpc.CallOption) (*emptypb.Empty, error) {
	out := new(emptypb.Empty)
	err := c.cc.Invoke(ctx, "/tast.cros.bluetooth.OobeHidBluetoothService/VerifyBluetoothIsEnabled", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// OobeHidBluetoothServiceServer is the server API for OobeHidBluetoothService service.
type OobeHidBluetoothServiceServer interface {
	// NewChrome starts new chrome session in oobe, verifies that HID detection screen
	// is shown. CloseChrome must be called later to clean up the associated resources.
	NewChrome(context.Context, *NewChromeRequest) (*emptypb.Empty, error)
	// CloseChrome releases the resources obtained by NewChrome.
	CloseChrome(context.Context, *emptypb.Empty) (*emptypb.Empty, error)
	// ProgressToWelcomeScreen Progresses from Oobe HID screen to welcome screen.
	ProgressToWelcomeScreen(context.Context, *emptypb.Empty) (*emptypb.Empty, error)
	// DisableBluetoothFromQuickSettings powers off the bluetooth adapter from quick settings.
	DisableBluetoothFromQuickSettings(context.Context, *emptypb.Empty) (*emptypb.Empty, error)
	// VerifyBluetoothIsEnabled verifies Bluetooth is enabled and mouse device is connected.
	VerifyBluetoothIsEnabled(context.Context, *emptypb.Empty) (*emptypb.Empty, error)
}

// UnimplementedOobeHidBluetoothServiceServer can be embedded to have forward compatible implementations.
type UnimplementedOobeHidBluetoothServiceServer struct {
}

func (*UnimplementedOobeHidBluetoothServiceServer) NewChrome(context.Context, *NewChromeRequest) (*emptypb.Empty, error) {
	return nil, status.Errorf(codes.Unimplemented, "method NewChrome not implemented")
}
func (*UnimplementedOobeHidBluetoothServiceServer) CloseChrome(context.Context, *emptypb.Empty) (*emptypb.Empty, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CloseChrome not implemented")
}
func (*UnimplementedOobeHidBluetoothServiceServer) ProgressToWelcomeScreen(context.Context, *emptypb.Empty) (*emptypb.Empty, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ProgressToWelcomeScreen not implemented")
}
func (*UnimplementedOobeHidBluetoothServiceServer) DisableBluetoothFromQuickSettings(context.Context, *emptypb.Empty) (*emptypb.Empty, error) {
	return nil, status.Errorf(codes.Unimplemented, "method DisableBluetoothFromQuickSettings not implemented")
}
func (*UnimplementedOobeHidBluetoothServiceServer) VerifyBluetoothIsEnabled(context.Context, *emptypb.Empty) (*emptypb.Empty, error) {
	return nil, status.Errorf(codes.Unimplemented, "method VerifyBluetoothIsEnabled not implemented")
}

func RegisterOobeHidBluetoothServiceServer(s *grpc.Server, srv OobeHidBluetoothServiceServer) {
	s.RegisterService(&_OobeHidBluetoothService_serviceDesc, srv)
}

func _OobeHidBluetoothService_NewChrome_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(NewChromeRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(OobeHidBluetoothServiceServer).NewChrome(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/tast.cros.bluetooth.OobeHidBluetoothService/NewChrome",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(OobeHidBluetoothServiceServer).NewChrome(ctx, req.(*NewChromeRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _OobeHidBluetoothService_CloseChrome_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(emptypb.Empty)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(OobeHidBluetoothServiceServer).CloseChrome(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/tast.cros.bluetooth.OobeHidBluetoothService/CloseChrome",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(OobeHidBluetoothServiceServer).CloseChrome(ctx, req.(*emptypb.Empty))
	}
	return interceptor(ctx, in, info, handler)
}

func _OobeHidBluetoothService_ProgressToWelcomeScreen_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(emptypb.Empty)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(OobeHidBluetoothServiceServer).ProgressToWelcomeScreen(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/tast.cros.bluetooth.OobeHidBluetoothService/ProgressToWelcomeScreen",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(OobeHidBluetoothServiceServer).ProgressToWelcomeScreen(ctx, req.(*emptypb.Empty))
	}
	return interceptor(ctx, in, info, handler)
}

func _OobeHidBluetoothService_DisableBluetoothFromQuickSettings_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(emptypb.Empty)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(OobeHidBluetoothServiceServer).DisableBluetoothFromQuickSettings(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/tast.cros.bluetooth.OobeHidBluetoothService/DisableBluetoothFromQuickSettings",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(OobeHidBluetoothServiceServer).DisableBluetoothFromQuickSettings(ctx, req.(*emptypb.Empty))
	}
	return interceptor(ctx, in, info, handler)
}

func _OobeHidBluetoothService_VerifyBluetoothIsEnabled_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(emptypb.Empty)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(OobeHidBluetoothServiceServer).VerifyBluetoothIsEnabled(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/tast.cros.bluetooth.OobeHidBluetoothService/VerifyBluetoothIsEnabled",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(OobeHidBluetoothServiceServer).VerifyBluetoothIsEnabled(ctx, req.(*emptypb.Empty))
	}
	return interceptor(ctx, in, info, handler)
}

var _OobeHidBluetoothService_serviceDesc = grpc.ServiceDesc{
	ServiceName: "tast.cros.bluetooth.OobeHidBluetoothService",
	HandlerType: (*OobeHidBluetoothServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "NewChrome",
			Handler:    _OobeHidBluetoothService_NewChrome_Handler,
		},
		{
			MethodName: "CloseChrome",
			Handler:    _OobeHidBluetoothService_CloseChrome_Handler,
		},
		{
			MethodName: "ProgressToWelcomeScreen",
			Handler:    _OobeHidBluetoothService_ProgressToWelcomeScreen_Handler,
		},
		{
			MethodName: "DisableBluetoothFromQuickSettings",
			Handler:    _OobeHidBluetoothService_DisableBluetoothFromQuickSettings_Handler,
		},
		{
			MethodName: "VerifyBluetoothIsEnabled",
			Handler:    _OobeHidBluetoothService_VerifyBluetoothIsEnabled_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "oobe_hid_bluetooth_service.proto",
}
