// Copyright 2023 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

syntax = "proto3";

package tast.cros.bluetooth;

import "google/protobuf/empty.proto";

option go_package = "go.chromium.org/tast-tests/cros/services/cros/bluetooth";

// BluetoothUIService allows bluetooth remote tests make calls to local,
// bluetooth-specific functions using UI actions necessary for testing bluetooth
// features.
//
// For direct management of bluetooth services, use the BluetoothService
// instead.
service BluetoothUIService {
  // PairWithFastPairNotification will attempt to pair a fast pair device with
  // the fast pair notification.
  rpc PairWithFastPairNotification(PairWithFastPairNotificationRequest)
      returns (google.protobuf.Empty) {}

  // CloseNotifications closes all open notifications.
  rpc CloseNotifications(google.protobuf.Empty)
      returns (google.protobuf.Empty) {}

  // ConfirmSavedDevicesState will attempt to confirm the state of Saved Devices
  // on the Saved Devices subpage. The array of devices should be in the
  // expected order. Fails if the list of Saved Devices doesn't match the one
  // provided.
  rpc ConfirmSavedDevicesState(ConfirmSavedDevicesStateRequest)
      returns (google.protobuf.Empty) {}

  // RemoveAllSavedDevices will attempt to remove all of the devices from the
  // Saved Devices subpage.
  rpc RemoveAllSavedDevices(google.protobuf.Empty)
      returns (google.protobuf.Empty) {}

  // PairDeviceWithQuickSettings pairs with the specified device through UI
  // interactions with the Quick Settings.
  rpc PairDeviceWithQuickSettings(PairDeviceWithQuickSettingsRequest)
      returns (google.protobuf.Empty) {}

  // ForgetBluetoothDevice will attempt to navigate to the Device Detail subpage
  // for the device specified in the request, then click "Forget" to forget the
  // device.
  rpc ForgetBluetoothDevice(ForgetBluetoothDeviceRequest)
      returns (google.protobuf.Empty) {}

  // CollectDeviceList will attempt to collect and list all available Bluetooth
  // devices in the Bluetooth page in the OS-Settings.
  rpc CollectDeviceList(google.protobuf.Empty)
      returns (CollectDeviceListResponse) {}

  // BluetoothDeviceDetail will attempt to navigate to the Device Detail subpage
  // for the device specified in the request, then retrieves the information of
  // this particular device.
  rpc BluetoothDeviceDetail(BluetoothDeviceDetailRequest)
      returns (BluetoothDeviceDetailResponse) {}

  // RenameBluetoothDevice will attempt to navigate to the Device Detail subpage
  // for the device specified in the request, then renames the Bluetooth device.
  rpc RenameBluetoothDevice(RenameBluetoothDeviceRequest)
      returns (google.protobuf.Empty) {}
}

enum FastPairProtocol {
  FAST_PAIR_PROTOCOL_NULL = 0;
  FAST_PAIR_PROTOCOL_INITIAL = 1;
  FAST_PAIR_PROTOCOL_SUBSEQUENT = 2;
  FAST_PAIR_PROTOCOL_RETROACTIVE = 3;
}

message PairWithFastPairNotificationRequest { FastPairProtocol protocol = 1; }

message ConfirmSavedDevicesStateRequest { repeated string device_names = 1; }

message PairDeviceWithQuickSettingsRequest { string advertisedName = 1; }

message ForgetBluetoothDeviceRequest { string device_name = 1; }

message Battery {
  optional string percentage = 1 [ deprecated = true ];

  string value_in_percentage = 2;
}

message Device {
  string name = 1;
  bool is_connected = 2;

  repeated Battery battery_information = 3 [ deprecated = true ];

  // Battery information is only provided if the Bluetooth device is connected
  // with LE.
  optional Battery battery = 4;
}

message CollectDeviceListResponse { repeated Device devices = 1; }

message BluetoothDeviceDetailRequest {
  enum MatchOption {
    // MATCH_OPTION_CONNECTED specifies that the RPC should wait for the device
    // to be connected before responding.
    MATCH_OPTION_CONNECTED = 0;
    // MATCH_OPTION_DISCONNECTED specifies that the RPC should wait for the
    // device to be disconnected before responding.
    MATCH_OPTION_DISCONNECTED = 1;
    // MATCH_OPTION_REPORT_BATTERY specifies that the RPC should wait for the
    // device battery information to be reported by the OS-Settings before
    // responding.
    MATCH_OPTION_REPORT_BATTERY = 2;
    // MATCH_OPTION_NOT_REPORT_BATTERY specifies that the RPC should wait for
    // the device battery information within the OS-Settings is no longer
    // available before responding.
    MATCH_OPTION_NOT_REPORT_BATTERY = 3;
  }

  string name = 1;

  // Device detail may not be up to date on UI immediately,
  // this option specifies a particular indicator that must match before this
  // RPC can respond.
  optional MatchOption match_option = 2;
}

message BluetoothDeviceDetailResponse { Device device = 1; }

message RenameBluetoothDeviceRequest {
  Device device = 1;
  string custom_name = 2;
}
