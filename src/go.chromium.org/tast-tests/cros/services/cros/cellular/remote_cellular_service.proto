// Copyright 2022 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

syntax = "proto3";

package tast.cros.cellular;

import "google/protobuf/empty.proto";

option go_package = "go.chromium.org/tast-tests/cros/services/cros/cellular";

// RemoteCellularService provides RPC methods to interact with the cellular environment on the DUT.
service RemoteCellularService {
  // SetUp initialize the DUT for cellular testing.
  rpc SetUp(google.protobuf.Empty) returns (google.protobuf.Empty) {}

  // PreTest runs before every cellular test.
  rpc PreTest(google.protobuf.Empty) returns (google.protobuf.Empty) {}

  // PostTest runs after every cellular test.
  rpc PostTest(google.protobuf.Empty) returns (google.protobuf.Empty) {}

  // TearDown releases any held resources and reverts the changes made in SetUp.
  rpc TearDown(google.protobuf.Empty) returns (google.protobuf.Empty) {}

  // Enable enables cellular on the DUT.
  rpc Enable(google.protobuf.Empty) returns (EnableResponse) {}

  // Disable disables cellular on the DUT.
  rpc Disable(google.protobuf.Empty) returns (DisableResponse) {}

  // Connect attempts to connect to the cellular service.
  rpc Connect(google.protobuf.Empty) returns (ConnectResponse) {}

  // Disconnect attempts to disconnect from a cellular service.
  rpc Disconnect(google.protobuf.Empty) returns (DisconnectResponse) {}

  // QueryService returns information about the available cellular service.
  rpc QueryService(google.protobuf.Empty) returns (QueryServiceResponse) {}

  // QueryInterface returns information about the cellular device interface.
  rpc QueryInterface(google.protobuf.Empty) returns (QueryInterfaceResponse) {}

  // QueryLTESignal returns information about the attached LTE signal.
  rpc QueryLTESignal(google.protobuf.Empty) returns (QueryLTESignalResponse) {}

  // VerifyIPConnectivity verifies that the DUT can ping the provided addresses.
  rpc VerifyIPConnectivity(VerifyIPConnectivityRequest) returns (VerifyIPConnectivityResponse) {}

  // DisableSar disables dynamic SAR on the DUT.
  rpc DisableSar(google.protobuf.Empty) returns (google.protobuf.Empty) {}

  // EnableSar enables dynamic SAR on the DUT.
  rpc EnableSar(google.protobuf.Empty) returns (google.protobuf.Empty) {}

  // ConfigureSar configures the dynamic SAR power level on the DUT.
  rpc ConfigureSar(ConfigureSarRequest) returns (google.protobuf.Empty) {}

  // WaitForNextSms waits until a single sms added signal is received.
  rpc WaitForNextSms(google.protobuf.Empty) returns (WaitForNextSmsResponse) {}

  // QueryModemType returns dut variant/model.
  rpc QueryModemType(google.protobuf.Empty) returns (QueryModemTypeResponse) {}

  // QuerySignalBars returns dut signal bars count from ui.
  rpc QuerySignalBars(google.protobuf.Empty) returns (QuerySignalBarsResponse) {}
}

message EnableResponse {
  int64 enable_time = 1;
}

message DisableResponse {
  int64 disable_time = 1;
}

message ConnectResponse {
  int64 connect_time = 1;
}

message DisconnectResponse {
  int64 disconnect_time = 1;
}

message QueryServiceResponse {
  string name = 1;
  string device = 2;
  string state = 3;
  bool is_connected = 4;
  int64 strength = 5;
  string network_technology = 6;
}

message QueryInterfaceResponse {
  string name = 1;
}

message QueryLTESignalResponse {
  double rsrp = 1;
  double rsrq = 2;
  double snr = 3;
}

message ConfigureSarRequest {
  int32 power_level = 1;
}

message SmsMessage {
  string text = 1;
}

message WaitForNextSmsResponse {
  SmsMessage message = 1;
}

message QueryModemTypeResponse {
  uint32 modem_type = 1;
}

message VerifyIPConnectivityRequest {
  string IPv4Address = 1;
  string IPv6Address = 2;
  enum CheckMethod {
    Ping = 0;
    Curl = 1;
  }
  CheckMethod method = 3;
}

message VerifyIPConnectivityResponse {
  bool ipv4 = 1;
  bool ipv6 = 2;
}

message QuerySignalBarsResponse {
    int32 count = 1;
}
