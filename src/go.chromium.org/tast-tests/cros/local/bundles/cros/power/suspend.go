// Copyright 2023 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package power

import (
	"bufio"
	"context"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"syscall"
	"time"

	"go.chromium.org/tast-tests/cros/common/testexec"
	"go.chromium.org/tast-tests/cros/local/input"
	"go.chromium.org/tast-tests/cros/local/power"
	"go.chromium.org/tast-tests/cros/local/power/setup"
	"go.chromium.org/tast-tests/cros/local/power/suspend"
	"go.chromium.org/tast/core/errors"
	"go.chromium.org/tast/core/testing"
	"go.chromium.org/tast/core/testing/hwdep"

	"golang.org/x/sys/unix"
)

var (
	// These models are affected by an issue where reads done via
	// /dev/drm_dp_aux1 cause fwupd to not be freezeable when the kernel is
	// suspending the device. b/319036849
	nofwupdFilteredModels = []string{"tentacool", "tentacruel", "elm", "hana", "homestar", "quackingstick", "wormdingler", "kingoftown", "lazor", "limozeen", "pazquel", "pompom"}

	// These nami models are filtered out because Nami/sona seems to be unstable
	// when suspending. b/324533891
	namiFilteredModels = []string{"sona"}

	// These octopus models are filtered out because they have a touchpad issue
	// on kernel-upstream (b/329161200)
	octopusFilteredModels = []string{"foob", "foob360"}

	allFilteredModels = append(append(nofwupdFilteredModels, namiFilteredModels...), octopusFilteredModels...)

	// For Intel S0ix debugging
	substateRequirementsPath = "/sys/kernel/debug/pmc_core/substate_requirements"
)

type fwupdMode int

const (
	fwupdOff fwupdMode = iota
	fwupdNoChange
)

type suspendConfig struct {
	Fwupd      fwupdMode
	Iterations int
}

func init() {
	testing.AddTest(&testing.Test{
		Func:         Suspend,
		LacrosStatus: testing.LacrosVariantUnneeded,
		Desc:         "Simple, single-cycle Suspend and Resume",
		Contacts: []string{
			"chromeos-platform-power@google.com",
		},
		BugComponent: "b:1361410",
		Attr:         []string{"group:mainline"},
		Timeout:      4 * time.Minute,
		// TODO(b/319036849): when the issues with these devices are resolved, remove parameterised
		// versions of this test and also remove the hwdeps - this should be run on all devices
		Params: []testing.Param{
			{
				ExtraHardwareDeps: hwdep.D(hwdep.SkipOnModel(allFilteredModels...)),
				Val:               suspendConfig{Fwupd: fwupdNoChange, Iterations: 1},
			}, {
				Name:              "twice",
				ExtraHardwareDeps: hwdep.D(hwdep.SkipOnModel(allFilteredModels...)),
				Val:               suspendConfig{Fwupd: fwupdNoChange, Iterations: 2},
				ExtraAttr:         []string{"informational"},
			}, {
				Name:              "unstable",
				ExtraHardwareDeps: hwdep.D(hwdep.Model(allFilteredModels...)),
				Val:               suspendConfig{Fwupd: fwupdNoChange, Iterations: 1},
				ExtraAttr:         []string{"informational"},
			}, {
				Name:              "nofwupd",
				ExtraHardwareDeps: hwdep.D(hwdep.Model(nofwupdFilteredModels...)),
				Val:               suspendConfig{Fwupd: fwupdOff, Iterations: 1},
			},
		},
	})
}

func startEvtestLog(ctx context.Context, path string) (func() (string, error), error) {
	cmd := testexec.CommandContext(ctx, "evtest", path)
	stdout, err := cmd.StdoutPipe()
	if err != nil {
		return nil, errors.Wrap(err, "failed to create evtest stdout pipe for "+path)
	}
	if err := cmd.Start(); err != nil {
		return nil, errors.Wrap(err, "failed to start evtest "+path)
	}
	testing.ContextLogf(ctx, "started evtest %s", path)

	resultChannel := make(chan string)
	go func() {
		scanner := bufio.NewScanner(stdout)
		var lines []string
		testing.ContextLogf(ctx, "started soaking output of evtest %s", path)
		for scanner.Scan() {
			lines = append(lines, scanner.Text())
		}
		testing.ContextLogf(ctx, "end of output for evtest %s", path)
		resultChannel <- strings.Join(lines, "\n")
	}()

	stopCallback := func() (string, error) {
		if err := cmd.Kill(); err != nil {
			testing.ContextLogf(ctx, "Error killing evtest %s: %v", path, err)
		}
		testing.ContextLogf(ctx, "killed evtest %s", path)
		if err := cmd.Wait(); err != nil {
			status := cmd.ProcessState.Sys().(syscall.WaitStatus)
			signaled := status.Signaled()
			signal := status.Signal()
			// Expect it to be killed.
			if !signaled || signal != unix.SIGKILL {
				return "", errors.Wrap(err, "evtest "+path+" not finished by expected SIGKILL")
			}
		}

		select {
		case output := <-resultChannel:
			testing.ContextLogf(ctx, "received output for evtest %s", path)
			return output, nil
		case <-ctx.Done():
			return "", errors.Wrap(ctx.Err(), "failed waiting for evtest "+path)
		}
	}

	return stopCallback, nil
}

func startEvTestLogging(ctx context.Context, s *testing.State) (func(), error) {
	infos, err := input.ReadInputDevices("")
	if err != nil {
		return nil, errors.Wrap(err, "couldn't ReadInputDevices")
	}

	stopCallbacks := make(map[string]func() (string, error))

	for _, info := range infos {
		cb, err := startEvtestLog(ctx, info.Path)
		if err != nil {
			testing.ContextLog(ctx, "Couldn't startEvTestLog: ", err)
			continue
		}
		stopCallbacks[info.Name+" @ "+info.Path] = cb
	}

	stopCallback := func() {
		var i int = 0
		for name, stopCb := range stopCallbacks {
			output, err := stopCb()
			if err != nil {
				testing.ContextLogf(ctx, "Error with %s stop callback: %v", name, err)
				continue
			}
			filename := "evtest" + strconv.Itoa(i) + ".txt"
			i = i + 1
			testing.ContextLogf(ctx, "Writing evtest output for %s to %s", name, filename)
			if err := ioutil.WriteFile(filepath.Join(s.OutDir(), filename), []byte(output), 0644); err != nil {
				testing.ContextLogf(ctx, "Failed to write %s: %v", filename, err)
			}
		}
	}

	return stopCallback, nil
}

func setupSubstateRequirements(ctx context.Context, s *testing.State) (bool, string, error) {
	var hasSubstateRequirements = false
	var pmcCoreDir = filepath.Join(s.OutDir(), "pmc_core")
	if _, err := os.Stat(substateRequirementsPath); err == nil {
		hasSubstateRequirements = true
		if err = os.MkdirAll(pmcCoreDir, 0755); err != nil {
			testing.ContextLogf(ctx, "Could not create %s: %v", pmcCoreDir, err)
			return false, pmcCoreDir, err
		}
	} else {
		testing.ContextLogf(ctx, "stat(%s): %v", substateRequirementsPath, err)
		return false, pmcCoreDir, nil
	}
	return hasSubstateRequirements, pmcCoreDir, nil
}

func saveSubstateRequirements(ctx context.Context, s *testing.State, outdir string, i int) error {
	outFileName := filepath.Join(outdir, fmt.Sprintf("substate_requirements.%d", i))
	reqs, err := ioutil.ReadFile(substateRequirementsPath)
	if err != nil {
		testing.ContextLog(ctx, "Could not read "+substateRequirementsPath)
		return err
	}

	if err = ioutil.WriteFile(outFileName, reqs, 0644); err != nil {
		testing.ContextLogf(ctx, "Failed to write %s: %v", outFileName, err)
		return err
	}

	return nil
}

// Suspend suspends the DUT and wakes again. If the suspend fails, an
// error is returned. If the resume fails, the DUT may stay suspended
// indefinitely, causing the test infrastucture to mark the test as failed.
// TODO: add different kinds of suspend test, including stress test
func Suspend(ctx context.Context, s *testing.State) {
	params, ok := s.Param().(suspendConfig)
	if !ok {
		s.Fatal("Failed to convert test suspendConfig")
	}

	// TODO(b/324513129): remove this once we have a better solution in place.
	// **DO NOT COPY-PASTE THIS CODE IF YOU ARE NOT AFFECTED BY b/324513129**
	// If you are affected by b/324513129, please add a comment on that bug
	// and retain this comment block in the new location.
	_, err := setup.EnableService(ctx, "powerd")
	if err != nil {
		s.Fatal("Did not start powerd: ", err)
	}
	testing.ContextLog(ctx, "waiting for powerd to become ready")
	if _, err := power.NewPowerManager(ctx); err != nil {
		s.Fatal("Failed to connect to PowerManager DBus interface after restarting powerd: ", err)
	}

	if params.Fwupd == fwupdOff {
		// Sometimes fwupd may end up being stuck in a lengthy transfer from a device making it non-
		// suspendable. Stop fwupd before trying the suspend so this doesn't happen.
		startFwupdFn, err := setup.DisableServiceIfExists(ctx, "fwupd")
		if err != nil {
			s.Fatal("Failed to stop fwupd: ", err)
		}
		if startFwupdFn != nil {
			defer startFwupdFn(ctx)
		}
	}

	stopEvtest, err := startEvTestLogging(ctx, s)
	if err != nil {
		s.Fatal("Couldn't startEvTestLogging")
	}
	defer stopEvtest()

	hasSubstateRequirements, reqsOutDir, err := setupSubstateRequirements(ctx, s)
	if err != nil {
		s.Fatalf("Couldn't create %s for substate_requirements: %v", reqsOutDir, err)
	}

	for i := 1; i <= params.Iterations; i++ {
		testing.ContextLogf(ctx, "Suspend %d of %d", i, params.Iterations)
		_, err := suspend.ForDurationWithKernelFreezeTimeout(ctx, 10*time.Second, 8*time.Second)

		if hasSubstateRequirements {
			if err := saveSubstateRequirements(ctx, s, reqsOutDir, i); err != nil {
				s.Fatal("Couldn't save subsate_requirements: ", err)
			}
		}

		if err != nil {
			s.Fatal("Failed to suspend: ", err)
		}
	}
}
