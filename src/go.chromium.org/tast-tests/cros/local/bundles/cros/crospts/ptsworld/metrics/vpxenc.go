// Copyright 2023 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package metrics

const vpxencTitle = "VP9 libvpx Encoding"

// vpxencCrosboltNameTable maps the PTS description to crosbolt name.
var vpxencCrosboltNameTable = map[string]string{
	"Speed: Speed 5 - Input: Bosphorus 1080p": "Speed_5_Input_bosphorus_1080p",
}
