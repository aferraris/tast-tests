// Copyright 2023 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package metrics

const rnNoiseTitle = "RNNoise"

// rnNoiseCrosboltNameTable maps the PTS description to crosbolt name.
var rnNoiseCrosboltNameTable = map[string]string{
	"": "Suppression",
}
