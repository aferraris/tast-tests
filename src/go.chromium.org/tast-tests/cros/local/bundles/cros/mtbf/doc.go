// Copyright 2022 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package mtbf contains local Tast tests that exercise various tests to conduct the MTBF (Mean Time Between Failure) value.
package mtbf
