// Copyright 2021 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package setup

import (
	"context"
	"time"

	"go.chromium.org/tast-tests/cros/local/arc"
	"go.chromium.org/tast-tests/cros/local/arc/optin"
	"go.chromium.org/tast-tests/cros/local/chrome"
	"go.chromium.org/tast-tests/cros/local/chrome/lacros/lacrosfixt"
	"go.chromium.org/tast/core/testing"
)

const (
	// userVar and passwdVar are runtime variable names for user login credentials.
	userVar   = "benchmark.username"
	passwdVar = "benchmark.password"

	// BenchmarkARCFixture is a fixture name that will be registered to tast.
	// The fixture brings up Chrome and ARC with PlayStore.
	BenchmarkARCFixture = "benchmarkARCFixture"
	// BenchmarkChromeFixture is a fixture name that will be registered to tast.
	// It brings up Chrome.
	BenchmarkChromeFixture = "benchmarkChromeFixture"
	// BenchmarkLacrosFixture is a fixture name that will be registered to tast.
	// It brings up Chrome with Lacros enabled.
	BenchmarkLacrosFixture = "benchmarkLacrosFixture"
)

func benchmarkARCFixtureOptions(ctx context.Context, s *testing.FixtState) ([]chrome.Option, error) {
	userID, ok := s.Var(userVar)
	if !ok {
		s.Fatalf("Runtime variable %s is not provided", userVar)
	}
	userPasswd, ok := s.Var(passwdVar)
	if !ok {
		s.Fatalf("Runtime variable %s is not provided", passwdVar)
	}

	return []chrome.Option{
		chrome.KeepState(),
		chrome.ARCSupported(),
		chrome.GAIALogin(chrome.Creds{User: userID, Pass: userPasswd})}, nil
}

func init() {
	fixtureConfig := arc.DefaultBootedFixtureConfig()
	fixtureConfig.PlayStoreOptin = true
	fixtureConfig.FOpts = benchmarkARCFixtureOptions
	testing.AddFixture(&testing.Fixture{
		Name:     BenchmarkARCFixture,
		Desc:     "The fixture starts chrome with ARC supported",
		Contacts: []string{"xliu@cienet.com"},
		Impl:     arc.NewArcBootedFixture(fixtureConfig),
		// Add two minutes to setup time to allow extra Play Store UI operations.
		SetUpTimeout:   chrome.GAIALoginTimeout + optin.OptinTimeout + arc.BootTimeout + 2*time.Minute,
		ResetTimeout:   chrome.ResetTimeout,
		PreTestTimeout: arc.PreTestTimeout,
		// Provide a longer enough PostTestTimeout value to fixture when ARC will try to dump ARCVM message.
		// Or there might be error of "context deadline exceeded".
		PostTestTimeout: 5 * time.Second,
		TearDownTimeout: chrome.ResetTimeout,
		Vars:            []string{userVar, passwdVar},
	})
	testing.AddFixture(&testing.Fixture{
		Name:     BenchmarkChromeFixture,
		Desc:     "The fixture starts chrome with GAIA login and ARC Supported",
		Contacts: []string{"xliu@cienet.com"},
		// Although ARCSupported is provided as an option to bring up the ARC on DUT, we will not
		// use ARC in the test so we don't need set up ARC/ADB. Use LoggedIn Fixture.
		Impl:            chrome.NewLoggedInFixture(benchmarkARCFixtureOptions),
		SetUpTimeout:    chrome.GAIALoginTimeout,
		ResetTimeout:    chrome.ResetTimeout,
		TearDownTimeout: chrome.ResetTimeout,
		Vars:            []string{userVar, passwdVar},
	})
	testing.AddFixture(&testing.Fixture{
		Name:     BenchmarkLacrosFixture,
		Desc:     "The fixture starts chrome with GAIA login and ARC Supported and Lacros enabled",
		Contacts: []string{"xliu@cienet.com", "jason.hsiao@cienet.com"},
		// Although ARCSupported is provided as an option to bring up the ARC on DUT, we will not
		// use ARC in the test so we don't need set up ARC/ADB. Use LoggedIn Fixture.
		Impl: chrome.NewLoggedInFixture(func(ctx context.Context, s *testing.FixtState) ([]chrome.Option, error) {
			opts, err := benchmarkARCFixtureOptions(ctx, s)
			if err != nil {
				s.Fatal("Failed to get bench mark ARC fixture options: ", err)
			}
			return lacrosfixt.NewConfig(lacrosfixt.ChromeOptions(opts...)).Opts()
		}),
		SetUpTimeout:    chrome.GAIALoginTimeout,
		ResetTimeout:    chrome.ResetTimeout,
		TearDownTimeout: chrome.ResetTimeout,
		Vars:            []string{userVar, passwdVar},
	})
}
