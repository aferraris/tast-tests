// Copyright 2023 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package firmware

import (
	"context"
	"os"

	"github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc"

	"go.chromium.org/tast-tests/cros/common/testexec"
	"go.chromium.org/tast-tests/cros/local/kernel"
	pb "go.chromium.org/tast-tests/cros/services/cros/firmware"

	"go.chromium.org/tast/core/errors"
	"go.chromium.org/tast/core/testing"
)

func init() {
	testing.AddService(&testing.Service{
		Register: func(srv *grpc.Server, s *testing.ServiceState) {
			pb.RegisterKernelServiceServer(srv, &KernelService{s: s})
		},
	})
}

// KernelService implements tast.cros.firmware.KernelService.
type KernelService struct {
	s *testing.ServiceState
}

// GetCgptTable returns structure containing metadata with CGPT partitions.
func (ks *KernelService) GetCgptTable(ctx context.Context, req *pb.GetCgptTableRequest) (resp *pb.GetCgptTableResponse, err error) {
	testing.ContextLog(ctx, "Reading CGPT table for device ", req.BlockDevice)
	rootDevWithoutPart, _ := kernel.SplitRootDevAndPart(ctx, req.BlockDevice)
	partitionTable, err := kernel.GetCgptTable(ctx, rootDevWithoutPart)
	if err != nil {
		return nil, errors.Wrap(err, "failed to read cgpt table")
	}

	return &pb.GetCgptTableResponse{CgptTable: partitionTable}, nil
}

// GetRawHeader returns the raw header of CGPT partition (first 4096 bytes)
func (ks *KernelService) GetRawHeader(ctx context.Context, req *pb.GetRawHeaderRequest) (*pb.GetRawHeaderResponse, error) {
	if err := testexec.CommandContext(ctx, "dd", "if="+req.PartitionPath, "of=/tmp/cgpt-header", "bs=4096", "count=1", "conv=sync").Run(); err != nil {
		return nil, errors.Wrap(err, "failed to read raw header from partition")
	}
	defer os.Remove("/tmp/cgpt-header")
	rawHeader, err := os.ReadFile("/tmp/cgpt-header")
	if err != nil {
		return nil, errors.Wrap(err, "failed to read raw header dump")
	}
	return &pb.GetRawHeaderResponse{
		RawHeader: rawHeader,
	}, nil
}

// WriteRawHeader writes the raw CGPT header into chosen partitionpartition
func (ks *KernelService) WriteRawHeader(ctx context.Context, req *pb.WriteRawHeaderRequest) (*empty.Empty, error) {
	if err := os.WriteFile("/tmp/cgpt-header", req.RawHeader, os.FileMode(0666)); err != nil {
		return nil, errors.Wrap(err, "failed to save raw header into temporary file")
	}
	defer os.Remove("/tmp/cgpt-header")
	if err := testexec.CommandContext(ctx, "dd", "if=/tmp/cgpt-header", "of="+req.PartitionPath, "bs=4096", "count=1", "conv=sync").Run(); err != nil {
		return nil, errors.Wrap(err, "failed to write raw header into partition")
	}
	return &empty.Empty{}, nil
}

// RestoreCgptAttributes restores CGPT partition attributes directly dumped from GetCgptTable
func (ks *KernelService) RestoreCgptAttributes(ctx context.Context, req *pb.RestoreCgptAttributesRequest) (*empty.Empty, error) {
	var rootDevWithPart string
	if req.BlockDevice != "" {
		rootDevWithPart = req.BlockDevice
	} else {
		var err error
		rootDevWithPart, err = kernel.GetCurrentRootDevice(ctx, true)
		if err != nil {
			return nil, errors.Wrap(err, "failed to get root device")
		}
	}
	rootDevWithoutPart, _ := kernel.SplitRootDevAndPart(ctx, rootDevWithPart)

	if err := kernel.RestoreCgptAttributes(ctx, rootDevWithoutPart, req.CgptTable); err != nil {
		return nil, errors.Wrap(err, "failed to restore cgpt attributes")
	}
	return &empty.Empty{}, nil
}

// BackupPartition backs up partition and saves to a file.
func (ks *KernelService) BackupPartition(ctx context.Context, req *pb.Partition) (*pb.PartitionInfo, error) {
	var rootDevWithPart string
	if req.RootDev != "" {
		rootDevWithPart = req.RootDev
	} else {
		var err error
		rootDevWithPart, err = kernel.GetCurrentRootDevice(ctx, true)
		if err != nil {
			return nil, errors.Wrap(err, "failed to get root device")
		}
	}
	rootDevWithoutPart, _ := kernel.SplitRootDevAndPart(ctx, rootDevWithPart)

	table, path, err := kernel.BackupPartition(ctx, rootDevWithoutPart, kernel.PartitionNameCopyToLabel(req.Name, req.Copy))
	if err != nil {
		return nil, errors.Wrap(err, "could not back up partition")
	}

	return &pb.PartitionInfo{
		Name:       req.Name,
		Copy:       req.Copy,
		BackupPath: path,
		RootDev:    rootDevWithPart,
		Table:      table,
	}, nil
}

// RestorePartition restores partition from backup to current boot device.
func (ks *KernelService) RestorePartition(ctx context.Context, req *pb.PartitionInfo) (*empty.Empty, error) {
	rootDevWithoutPart, err := kernel.GetCurrentRootDevice(ctx, false)
	if err != nil {
		return nil, errors.Wrap(err, "couldn't get current rootdev")
	}
	rootDevWithPart := kernel.RootDevPartitionPath(rootDevWithoutPart, int(req.Table.PartitionNumber))

	if err := kernel.RestorePartition(ctx, req.BackupPath, rootDevWithPart); err != nil {
		return nil, errors.Wrap(err, "could not restore partition from backup")
	}

	label := kernel.PartitionNameCopyToLabel(req.Name, req.Copy)
	if err := kernel.RestoreCgptAttributes(ctx, rootDevWithoutPart, map[string]*pb.CgptPartition{label: req.Table}); err != nil {
		return nil, errors.Wrap(err, "failed to retore CGPT attributes")
	}

	return &empty.Empty{}, nil
}

// BackupKernel backs up both partition A and B copies and saves them to a file.
func (ks *KernelService) BackupKernel(ctx context.Context, req *pb.KernelBackup) (_ *pb.KernelBackup, retErr error) {
	var rootDevWithPart string
	if req.RootDev != "" {
		rootDevWithPart = req.RootDev
	} else {
		var err error
		rootDevWithPart, err = kernel.GetCurrentRootDevice(ctx, true)
		if err != nil {
			return nil, errors.Wrap(err, "failed to get root device")
		}
		req.RootDev = rootDevWithPart
	}
	// If all the backup fields are set to false, backup only KERNEL.
	if !req.BackupKernel && !req.BackupMiniOS && !req.BackupRootfs {
		req.BackupKernel = true
	}
	var allBackupPath []string
	defer func() {
		if retErr != nil {
			for _, path := range allBackupPath {
				os.Remove(path)
			}
		}
	}()
	for _, step := range []struct {
		shouldBackup        bool
		backupPartitionName pb.PartitionName
	}{
		{
			shouldBackup:        req.BackupKernel,
			backupPartitionName: pb.PartitionName_KERNEL,
		},
		{
			shouldBackup:        req.BackupMiniOS,
			backupPartitionName: pb.PartitionName_MINIOS,
		},
		{
			shouldBackup:        req.BackupRootfs,
			backupPartitionName: pb.PartitionName_ROOTFS,
		},
	} {
		if !step.shouldBackup {
			continue
		}
		partitionInfoA, err := ks.BackupPartition(ctx, &pb.Partition{
			Name:    step.backupPartitionName,
			Copy:    pb.PartitionCopy_A,
			RootDev: rootDevWithPart,
		})
		if err != nil {
			// If backing up partition fails, unfinished backup file is already deleted.
			return nil, errors.Wrap(err, "failed to back up partition A")
		}
		allBackupPath = append(allBackupPath, partitionInfoA.BackupPath)

		partitionInfoB, err := ks.BackupPartition(ctx, &pb.Partition{
			Name:    step.backupPartitionName,
			Copy:    pb.PartitionCopy_B,
			RootDev: rootDevWithPart,
		})
		if err != nil {
			// If backing up partition fails, unfinished backup file is already deleted.
			return nil, errors.Wrap(err, "failed to back up partition B")
		}
		allBackupPath = append(allBackupPath, partitionInfoB.BackupPath)
		switch step.backupPartitionName {
		case pb.PartitionName_MINIOS:
			req.MiniOSA = partitionInfoA
			req.MiniOSB = partitionInfoB
		case pb.PartitionName_ROOTFS:
			req.RootA = partitionInfoA
			req.RootB = partitionInfoB
		default:
			req.KernA = partitionInfoA
			req.KernB = partitionInfoB
		}
	}
	return req, nil
}

// RestoreKernel restores both partition A and B from back ups.
func (ks *KernelService) RestoreKernel(ctx context.Context, req *pb.KernelBackup) (*empty.Empty, error) {
	var retErr error
	for _, step := range []struct {
		shouldBackup         bool
		backupPartitionInfoA *pb.PartitionInfo
		backupPartitionInfoB *pb.PartitionInfo
	}{
		{
			shouldBackup:         req.BackupKernel,
			backupPartitionInfoA: req.KernA,
			backupPartitionInfoB: req.KernB,
		},
		{
			shouldBackup:         req.BackupMiniOS,
			backupPartitionInfoA: req.MiniOSA,
			backupPartitionInfoB: req.MiniOSB,
		},
		{
			shouldBackup:         req.BackupRootfs,
			backupPartitionInfoA: req.RootA,
			backupPartitionInfoB: req.RootB,
		},
	} {
		if !step.shouldBackup {
			continue
		}
		if _, err := ks.RestorePartition(ctx, step.backupPartitionInfoA); err != nil {
			// If restoring A failed, try to restore B instead of returning immediately.
			retErr = errors.Wrap(err, "failed to restore partition A")
		}

		if _, err := ks.RestorePartition(ctx, step.backupPartitionInfoB); err != nil {
			if retErr != nil {
				// If restoring both A and B fails, report both errors instead of just latest.
				retErr = errors.Wrap(errors.Wrap(err, "failed to restore partition B"), retErr.Error())
			} else {
				retErr = errors.Wrap(err, "failed to restore partition B")
			}
		}
	}
	return &empty.Empty{}, retErr
}

// EnsureBothKernelCopiesBootable makes sure both kernel copies are identical and bootable.
func (ks *KernelService) EnsureBothKernelCopiesBootable(ctx context.Context, req *empty.Empty) (*empty.Empty, error) {
	rootDevWithPart, err := kernel.GetCurrentRootDevice(ctx, true)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get current root device")
	}
	if err := kernel.EnsureBothKernelCopiesBootable(ctx, rootDevWithPart); err != nil {
		return nil, err
	}
	return &empty.Empty{}, nil
}

// PrioritizeKernelCopy ensures DUT boots to expected kernel copy on next reboot (eg. KERN-A or KERN-B).
func (ks *KernelService) PrioritizeKernelCopy(ctx context.Context, req *pb.Partition) (*empty.Empty, error) {
	rootDevWithoutPart, err := kernel.GetCurrentRootDevice(ctx, false)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get current root device")
	}
	if err := kernel.PrioritizeKernelCopy(ctx, rootDevWithoutPart, req.Copy); err != nil {
		return nil, err
	}
	return &empty.Empty{}, nil
}

// GetCurrentCopy returns the current label DUT is using.
func (ks *KernelService) GetCurrentCopy(ctx context.Context, req *pb.Partition) (*pb.Partition, error) {
	rootDevWithPart, err := kernel.GetCurrentRootDevice(ctx, true)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get current root device")
	}

	currPart, err := kernel.GetPartitionTable(ctx, rootDevWithPart)
	if err != nil {
		return nil, errors.Wrap(err, "failed to query current kernel copy")
	}
	testing.ContextLog(ctx, "DUT is currently booted to ", currPart.Label)

	name, err := kernel.GetNameFromLabel(currPart.Label)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get name from label")
	}
	copy := kernel.GetCopyFromLabel(currPart.Label)

	return &pb.Partition{
		Name:    kernel.PartNameToNameEnum[name],
		Copy:    kernel.CopyToCopyEnum[copy],
		RootDev: rootDevWithPart,
	}, nil
}

// VerifyKernelCopy checks that DUT is currently booted to expected kernel copy.
func (ks *KernelService) VerifyKernelCopy(ctx context.Context, req *pb.Partition) (*empty.Empty, error) {
	currRootDevWithPart, err := kernel.GetCurrentRootDevice(ctx, true)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get root device")
	}
	currPart, err := kernel.GetPartitionTable(ctx, currRootDevWithPart)
	if err != nil {
		return nil, errors.Wrap(err, "failed to query current kernel copy")
	}
	testing.ContextLogf(ctx, "DUT is currently booted from %s (label: %q)", currPart.PartitionPath, currPart.Label)

	currCopy := kernel.GetCopyFromLabel(currPart.Label)
	expCopy := kernel.CopyEnumToCopy[req.Copy]
	if currCopy != expCopy {
		return nil, errors.Errorf("expected kernel copy to be %q but was booted to %q", expCopy, currPart.Label)
	}
	return &empty.Empty{}, nil
}

// BackupRootfsVerityHash saves the verity hash for given kernel from the corresponding rootfs partition.
func (ks *KernelService) BackupRootfsVerityHash(ctx context.Context, req *pb.PartitionInfo) (*pb.RootfsVerityHashBackup, error) {
	var rootDevWithPart string
	if req.RootDev != "" {
		rootDevWithPart = req.RootDev
	} else {
		var err error
		rootDevWithPart, err = kernel.GetCurrentRootDevice(ctx, true)
		if err != nil {
			return nil, errors.Wrap(err, "failed to get root device")
		}
	}
	rootDevWithoutPart, _ := kernel.SplitRootDevAndPart(ctx, rootDevWithPart)

	offset, size, backupPath, err := kernel.BackupRootfsVerityHash(ctx, rootDevWithoutPart, req.Copy)
	if err != nil {
		return nil, errors.Wrap(err, "could not back up rootfs verity hash")
	}

	return &pb.RootfsVerityHashBackup{
		Copy:       req.Copy,
		RootDev:    rootDevWithPart,
		Offset:     offset,
		HashSize:   size,
		BackupPath: backupPath,
	}, nil
}

// RestoreRootfsVerityHash sets the verity hash for the current root device from backup.
func (ks *KernelService) RestoreRootfsVerityHash(ctx context.Context, req *pb.RootfsVerityHashBackup) (*empty.Empty, error) {
	rootDevWithoutPart, err := kernel.GetCurrentRootDevice(ctx, false)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get root device")
	}
	if err := kernel.RestoreRootfsVerityHash(ctx, req.Offset, req.BackupPath, rootDevWithoutPart, req.Copy); err != nil {
		return nil, errors.Wrap(err, "could not restore rootfs verity hash")
	}

	return &empty.Empty{}, nil
}

// CorruptRootfsVerityHash corrupts verity hash for given kernel copy on current root device.
func (ks *KernelService) CorruptRootfsVerityHash(ctx context.Context, req *pb.RootfsVerityHashBackup) (*empty.Empty, error) {
	rootDevWithoutPart, err := kernel.GetCurrentRootDevice(ctx, false)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get root device")
	}
	if corruptErr := kernel.CorruptRootfsVerityHash(ctx, req.Offset, req.HashSize, rootDevWithoutPart, req.Copy); corruptErr != nil {
		if restoreErr := kernel.RestoreRootfsVerityHash(ctx, req.Offset, req.BackupPath, rootDevWithoutPart, req.Copy); restoreErr != nil {
			return nil, errors.Wrapf(restoreErr, "could not corrupt rootfs verity hash and failed to restore it back to original hash: %v", corruptErr)
		}
		return nil, errors.Wrap(corruptErr, "could not corrupt rootfs verity hash")
	}

	return &empty.Empty{}, nil
}

// GetKernelVersion uses vbutil_kernel to get the kernel version for a given partition.
func (ks *KernelService) GetKernelVersion(ctx context.Context, req *pb.Partition) (*pb.KernelVersion, error) {
	var rootDevWithPart string
	if req.RootDev != "" {
		rootDevWithPart = req.RootDev
	} else {
		var err error
		rootDevWithPart, err = kernel.GetCurrentRootDevice(ctx, true)
		if err != nil {
			return nil, errors.Wrap(err, "failed to get root device")
		}
	}
	rootDevWithoutPart, _ := kernel.SplitRootDevAndPart(ctx, rootDevWithPart)

	version, err := kernel.GetKernelVersion(ctx, rootDevWithoutPart, req.Copy)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get kernel version")
	}

	return &pb.KernelVersion{
		RootDev: rootDevWithPart,
		Version: version,
		Copy:    req.Copy,
	}, nil
}

// SetKernelVersion uses vbutil_kernel to set the kernel version for a given partition.
// If caller is passing in root device, be sure to clear the rootdev if it's just being set for currently
// booted device and not a particular device. If setting a particular device, verify it exists first.
func (ks *KernelService) SetKernelVersion(ctx context.Context, req *pb.KernelVersion) (*empty.Empty, error) {
	var rootDevWithPart string
	if req.RootDev != "" {
		rootDevWithPart = req.RootDev
	} else {
		var err error
		rootDevWithPart, err = kernel.GetCurrentRootDevice(ctx, true)
		if err != nil {
			return nil, errors.Wrap(err, "failed to get root device")
		}
	}
	rootDevWithoutPart, _ := kernel.SplitRootDevAndPart(ctx, rootDevWithPart)

	if err := kernel.SetKernelVersion(ctx, rootDevWithoutPart, req.Copy, req.Version); err != nil {
		return nil, errors.Wrapf(err, "failed to set kernel version to %q", req.Version)
	}
	return &empty.Empty{}, nil
}

// SetKernelHeaderMagic sets the header magic for the kernel.
func (ks *KernelService) SetKernelHeaderMagic(ctx context.Context, req *pb.KernelHeaderMagicInfo) (*empty.Empty, error) {
	var rootDevWithPart string
	if req.RootDev != "" {
		rootDevWithPart = req.RootDev
	} else {
		var err error
		rootDevWithPart, err = kernel.GetCurrentRootDevice(ctx, true)
		if err != nil {
			return nil, errors.Wrap(err, "failed to get root device")
		}
	}
	rootDevWithoutPart, _ := kernel.SplitRootDevAndPart(ctx, rootDevWithPart)

	label := kernel.PartitionNameCopyToLabel(req.Name, req.Copy)
	if err := kernel.SetKernelHeaderMagic(ctx, rootDevWithoutPart, label, kernel.HeaderMagicEnumToMagic[req.Magic], req.ForceBoot); err != nil {
		return nil, errors.Wrap(err, "failed to set kernel header magic")
	}
	return &empty.Empty{}, nil
}

// GetCurrentRootDevice gets the path to the current root device with part number.
func (ks *KernelService) GetCurrentRootDevice(ctx context.Context, req *empty.Empty) (*pb.Partition, error) {
	rootDevWithPart, err := kernel.GetCurrentRootDevice(ctx, true)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get current root device")
	}
	return &pb.Partition{RootDev: rootDevWithPart}, nil
}
