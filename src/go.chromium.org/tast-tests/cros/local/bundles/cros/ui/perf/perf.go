// Copyright 2022 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package perf contains helper functions to ease perfutil package usage.
package perf

import (
	"context"

	"go.chromium.org/tast-tests/cros/common/chrome/histogram"
	"go.chromium.org/tast-tests/cros/local/perfutil"
	"go.chromium.org/tast/core/testing"
)

// Run is used by perfutil runner to run the performance scenario. It wraps
// s.Run around the scenario function, returning a closure to be used by tests.
func Run(s *testing.State, scenario perfutil.ScenarioFunc) func(ctx context.Context, name string) ([]*histogram.Histogram, error) {
	return func(ctx context.Context, name string) ([]*histogram.Histogram, error) {
		var hists []*histogram.Histogram
		var err error
		s.Run(ctx, name, func(ctx context.Context, s *testing.State) {
			hists, err = scenario(ctx, name)
			if err != nil {
				testing.ContextLog(ctx, "Failed to run the test scenario: ", err)
			}
		})
		return hists, err
	}
}
