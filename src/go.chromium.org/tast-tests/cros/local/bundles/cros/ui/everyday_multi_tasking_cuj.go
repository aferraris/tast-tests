// Copyright 2021 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package ui

import (
	"context"
	"time"

	"go.chromium.org/tast-tests/cros/local/audio"
	"go.chromium.org/tast-tests/cros/local/audio/crastestclient"
	et "go.chromium.org/tast-tests/cros/local/bundles/cros/ui/everydaymultitaskingcuj"
	"go.chromium.org/tast-tests/cros/local/chrome"
	"go.chromium.org/tast-tests/cros/local/chrome/browser"
	"go.chromium.org/tast-tests/cros/local/chrome/cuj"
	"go.chromium.org/tast-tests/cros/local/chrome/cuj/bluetooth"
	"go.chromium.org/tast-tests/cros/local/chrome/display"
	"go.chromium.org/tast-tests/cros/local/ui/cujrecorder"
	"go.chromium.org/tast/core/ctxutil"
	"go.chromium.org/tast/core/testing"
	"go.chromium.org/tast/core/testing/hwdep"
)

type multiTaskingParam struct {
	tier        cuj.Tier
	appName     string
	enableBT    bool // enable the bluetooth or not
	browserType browser.Type
}

func init() {
	testing.AddTest(&testing.Test{
		// TODO (b/242590511): Deprecated after moving all performance cuj test cases to go.chromium.org/tast-tests/cros/local/bundles/cros/spera directory.
		Func:         EverydayMultiTaskingCUJ,
		LacrosStatus: testing.LacrosVariantExists,
		Desc:         "Measures the performance of everyday multi-tasking CUJ",
		Contacts:     []string{"chromeos-perf-reliability-eng@google.com", "cienet-development@googlegroups.com", "chicheny@google.com"},
		BugComponent: "b:1025042", // ChromeOS > EngProd > Platform > SPERA > Automation
		SoftwareDeps: []string{"chrome", "arc"},
		HardwareDeps: hwdep.D(hwdep.InternalDisplay()),
		Vars: []string{
			"ui.cuj_mute",      // Optional. Mute the DUT during the test.
			"ui.cuj_mode",      // Optional. Expecting "tablet" or "clamshell".
			"ui.bt_devicename", // Required for Bluetooth subtests.
			"ui.collectTrace",  // Optional. Expecting "enable" or "disable", default is "disable".
		},
		Data: []string{cujrecorder.SystemTraceConfigFile},
		Params: []testing.Param{
			{
				Name:    "basic_ytmusic",
				Fixture: "loggedInAndKeepStateARCSupported",
				Timeout: 20 * time.Minute,
				Val: multiTaskingParam{
					tier:     cuj.Basic,
					appName:  et.YoutubeMusicAppName,
					enableBT: false,
				},
			}, {
				Name:              "basic_lacros_ytmusic",
				Fixture:           "loggedInAndKeepStateARCSupportedLacros",
				Timeout:           20 * time.Minute,
				ExtraSoftwareDeps: []string{"lacros"},
				Val: multiTaskingParam{
					tier:        cuj.Basic,
					appName:     et.YoutubeMusicAppName,
					enableBT:    false,
					browserType: browser.TypeLacros,
				},
			}, {
				Name:    "basic_ytmusic_bluetooth",
				Fixture: "loggedInAndKeepStateARCSupported",
				Timeout: 20 * time.Minute,
				Val: multiTaskingParam{
					tier:     cuj.Basic,
					appName:  et.YoutubeMusicAppName,
					enableBT: true,
				},
			}, {
				Name:              "basic_lacros_ytmusic_bluetooth",
				Fixture:           "loggedInAndKeepStateARCSupportedLacros",
				Timeout:           20 * time.Minute,
				ExtraSoftwareDeps: []string{"lacros"},
				Val: multiTaskingParam{
					tier:        cuj.Basic,
					appName:     et.YoutubeMusicAppName,
					enableBT:    true,
					browserType: browser.TypeLacros,
				},
			}, {
				Name:    "basic_spotify_bluetooth",
				Fixture: "loggedInAndKeepStateARCSupported",
				Timeout: 20 * time.Minute,
				Val: multiTaskingParam{
					tier:     cuj.Basic,
					appName:  et.SpotifyAppName,
					enableBT: true,
				},
			}, {
				Name:              "basic_lacros_spotify_bluetooth",
				Fixture:           "loggedInAndKeepStateARCSupportedLacros",
				Timeout:           20 * time.Minute,
				ExtraSoftwareDeps: []string{"lacros"},
				Val: multiTaskingParam{
					tier:        cuj.Basic,
					appName:     et.SpotifyAppName,
					enableBT:    true,
					browserType: browser.TypeLacros,
				},
			}, {
				Name:              "plus_helloworld",
				Fixture:           "loggedInAndKeepStateARCSupported",
				Timeout:           15 * time.Minute,
				ExtraSoftwareDeps: []string{"android_p"},
				Val: multiTaskingParam{
					tier:     cuj.Plus,
					appName:  et.HelloWorldAppName,
					enableBT: false,
				},
			}, {
				Name:              "plus_helloworld_vm",
				Fixture:           "loggedInAndKeepStateARCSupported",
				Timeout:           15 * time.Minute,
				ExtraSoftwareDeps: []string{"android_vm"},
				Val: multiTaskingParam{
					tier:     cuj.Plus,
					appName:  et.HelloWorldAppName,
					enableBT: false,
				},
			}, {
				Name:    "plus_ytmusic",
				Fixture: "loggedInAndKeepStateARCSupported",
				Timeout: 30 * time.Minute,
				Val: multiTaskingParam{
					tier:     cuj.Plus,
					appName:  et.YoutubeMusicAppName,
					enableBT: false,
				},
			}, {
				Name:              "plus_lacros_ytmusic",
				Fixture:           "loggedInAndKeepStateARCSupportedLacros",
				Timeout:           30 * time.Minute,
				ExtraSoftwareDeps: []string{"lacros"},
				Val: multiTaskingParam{
					tier:        cuj.Plus,
					appName:     et.YoutubeMusicAppName,
					enableBT:    false,
					browserType: browser.TypeLacros,
				},
			}, {
				Name:    "plus_ytmusic_bluetooth",
				Fixture: "loggedInAndKeepStateARCSupported",
				Timeout: 30 * time.Minute,
				Val: multiTaskingParam{
					tier:     cuj.Plus,
					appName:  et.YoutubeMusicAppName,
					enableBT: true,
				},
			}, {
				Name:              "plus_lacros_ytmusic_bluetooth",
				Fixture:           "loggedInAndKeepStateARCSupportedLacros",
				Timeout:           30 * time.Minute,
				ExtraSoftwareDeps: []string{"lacros"},
				Val: multiTaskingParam{
					tier:        cuj.Plus,
					appName:     et.YoutubeMusicAppName,
					enableBT:    true,
					browserType: browser.TypeLacros,
				},
			}, {
				Name:    "plus_spotify_bluetooth",
				Fixture: "loggedInAndKeepStateARCSupported",
				Timeout: 30 * time.Minute,
				Val: multiTaskingParam{
					tier:     cuj.Plus,
					appName:  et.SpotifyAppName,
					enableBT: true,
				},
			}, {
				Name:              "plus_lacros_spotify_bluetooth",
				Fixture:           "loggedInAndKeepStateARCSupportedLacros",
				Timeout:           30 * time.Minute,
				ExtraSoftwareDeps: []string{"lacros"},
				Val: multiTaskingParam{
					tier:        cuj.Plus,
					appName:     et.SpotifyAppName,
					enableBT:    true,
					browserType: browser.TypeLacros,
				},
			}, {
				Name:    "plus_spotify_quickcheck",
				Fixture: "loggedInAndKeepStateARCSupported",
				Timeout: 15 * time.Minute,
				Val: multiTaskingParam{
					tier:     cuj.Plus,
					appName:  et.SpotifyAppName,
					enableBT: false,
				},
			},
		},
	})
}

func EverydayMultiTaskingCUJ(ctx context.Context, s *testing.State) {
	param := s.Param().(multiTaskingParam)
	tier := param.tier
	app := param.appName
	enableBT := param.enableBT

	cr := s.FixtValue().(chrome.HasChrome).Chrome()
	a := s.FixtValue().(cuj.FixtureData).ARC

	cleanupCtx := ctx
	ctx, cancel := ctxutil.Shorten(ctx, 10*time.Second)
	defer cancel()

	if _, ok := s.Var("ui.cuj_mute"); ok {
		if err := crastestclient.Mute(ctx); err != nil {
			s.Fatal("Failed to mute audio: ", err)
		}
		defer crastestclient.Unmute(cleanupCtx)
	}

	isBtEnabled, err := bluetooth.IsEnabled(ctx)
	if err != nil {
		s.Fatal("Failed to get bluetooth status: ", err)
	}

	if enableBT {
		testing.ContextLog(ctx, "Start to connect bluetooth")
		deviceName := s.RequiredVar("ui.bt_devicename")
		if err := bluetooth.ConnectDevice(ctx, deviceName); err != nil {
			s.Fatal("Failed to connect bluetooth: ", err)
		}
		if !isBtEnabled {
			defer func(ctx context.Context) {
				if err := bluetooth.Disable(ctx); err != nil {
					s.Fatal("Failed to disable bluetooth: ", err)
				}
			}(cleanupCtx)
		}
	} else if isBtEnabled {
		testing.ContextLog(ctx, "Start to disable bluetooth")
		if err := bluetooth.Disable(ctx); err != nil {
			s.Fatal("Failed to disable bluetooth: ", err)
		}
		defer func(ctx context.Context) {
			if err := bluetooth.Enable(ctx); err != nil {
				s.Fatal("Failed to connect bluetooth: ", err)
			}
		}(cleanupCtx)
	}

	// The cras active node will change if the bluetooth has been connected or disconnected.
	// Wait until cras node change completes before doing output volume testing.
	condition := func(cn *audio.CrasNode) bool {
		if !cn.Active {
			return false
		}

		// According to src/third_party/adhd/cras/README.dbus-api,
		// the audio.CrasNode.Type should be "BLUETOOTH" when a BlueTooth device
		// is used as input or output.
		if enableBT {
			return cn.Type == "BLUETOOTH"
		}
		return cn.Type != "BLUETOOTH"
	}
	cras, err := audio.NewCras(ctx)
	if err != nil {
		s.Fatal("Failed to create cras: ", err)
	}
	if err := cras.WaitForDeviceUntil(ctx, condition, 40*time.Second); err != nil {
		s.Fatalf("Failed to wait for cras nodes to be in expected status: %v; please check the DUT log for possible audio device connection issue", err)
	}

	// Spotify login account.
	var account string
	if app == et.SpotifyAppName {
		account = cr.Creds().User
	}

	tconn, err := cr.TestAPIConn(ctx)
	if err != nil {
		s.Fatal("Failed to connect to test API: ", err)
	}

	tabletMode, resetTabletMode, err := cuj.EnableTabletMode(ctx, tconn, s.Var, "ui.cuj_mode")
	if err != nil {
		s.Fatal("Failed to enable tablet mode: ", err)
	}
	defer resetTabletMode(cleanupCtx)

	if tabletMode {
		cleanup, err := display.RotateToLandscape(ctx, tconn)
		if err != nil {
			s.Fatal("Failed to rotate display to landscape: ", err)
		}
		defer cleanup(cleanupCtx)
	}
	traceConfigPath := ""
	if collect, ok := s.Var("ui.collectTrace"); ok && collect == "enable" {
		traceConfigPath = s.DataPath(cujrecorder.SystemTraceConfigFile)
	}
	testRunParams := et.NewRunParams(tier, s.OutDir(), app, account, traceConfigPath, tabletMode, enableBT)
	if err := et.Run(ctx, cr, param.browserType, a, testRunParams); err != nil {
		s.Fatal("Failed to run everyday multi-tasking cuj test: ", err)
	}
}
