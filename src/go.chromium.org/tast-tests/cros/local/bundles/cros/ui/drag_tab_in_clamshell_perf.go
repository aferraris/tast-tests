// Copyright 2021 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package ui

import (
	"context"
	"time"

	"go.chromium.org/tast-tests/cros/common/action"
	uiperf "go.chromium.org/tast-tests/cros/local/bundles/cros/ui/perf"
	"go.chromium.org/tast-tests/cros/local/chrome"
	"go.chromium.org/tast-tests/cros/local/chrome/ash"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto/faillog"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto/mouse"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto/nodewith"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto/role"
	"go.chromium.org/tast-tests/cros/local/perfutil"
	"go.chromium.org/tast-tests/cros/local/power"
	"go.chromium.org/tast-tests/cros/local/ui"
	"go.chromium.org/tast/core/errors"
	"go.chromium.org/tast/core/testing"
	"go.chromium.org/tast/core/testing/hwdep"
)

func init() {
	testing.AddTest(&testing.Test{
		Func:         DragTabInClamshellPerf,
		LacrosStatus: testing.LacrosVariantNeeded,
		Desc:         "Measures the presentation time of dragging a tab in clamshell mode",
		Contacts: []string{
			"cros-sw-perf@google.com",
			"yichenz@chromium.org",
		},
		BugComponent: "b:1045832", // ChromeOS > Software > Performance > TPS
		Attr:         []string{"group:crosbolt", "crosbolt_perbuild"},
		SoftwareDeps: []string{"chrome"},
		HardwareDeps: hwdep.D(hwdep.InternalDisplay()),
		Fixture:      "chromeLoggedIn",
		Timeout:      5 * time.Minute,
	})
}

func DragTabInClamshellPerf(ctx context.Context, s *testing.State) {
	// Ensure display on to record ui performance correctly.
	if err := power.TurnOnDisplay(ctx); err != nil {
		s.Fatal("Failed to turn on display: ", err)
	}

	cr := s.FixtValue().(chrome.HasChrome).Chrome()

	tconn, err := cr.TestAPIConn(ctx)
	if err != nil {
		s.Fatal("Failed to connect to test API: ", err)
	}

	cleanup, err := ash.EnsureTabletModeEnabled(ctx, tconn, false)
	if err != nil {
		s.Fatal("Failed to ensure in clamshell mode: ", err)
	}
	defer cleanup(ctx)

	for i := 0; i < 2; i++ {
		conn, err := cr.NewConn(ctx, ui.PerftestURL)
		if err != nil {
			s.Fatalf("Failed to open %d-th tab: %v", i, err)
		}
		if err := conn.Close(); err != nil {
			s.Fatalf("Failed to close the connection to %d-th tab: %v", i, err)
		}
	}

	defer faillog.DumpUITreeOnError(ctx, s.OutDir(), s.HasError, tconn)

	ac := uiauto.New(tconn)

	ws, err := ash.GetAllWindows(ctx, tconn)
	if err != nil {
		s.Fatal("Failed to obtain the window list: ", err)
	}
	id0 := ws[0].ID
	if err := ash.SetWindowStateAndWait(ctx, tconn, id0, ash.WindowStateNormal); err != nil {
		s.Fatal("Failed to set the window state to normal: ", err)
	}
	if err := ash.WaitWindowFinishAnimating(ctx, tconn, id0); err != nil {
		s.Fatal("Failed to wait for top window animation: ", err)
	}
	w0, err := ash.GetWindow(ctx, tconn, id0)
	if err != nil {
		s.Fatal("Failed to get the window: ", err)
	}
	if w0.State != ash.WindowStateNormal {
		s.Fatalf("Wrong window state: expected Normal, got %s", w0.State)
	}
	bounds := w0.BoundsInRoot
	end := bounds.CenterPoint()

	// Find tabs.
	tabParam := nodewith.Role(role.Tab).ClassName("Tab")
	tabs, err := ac.NodesInfo(ctx, tabParam)
	if err != nil {
		s.Fatal("Failed to find tabs: ", err)
	}
	if len(tabs) != 2 {
		s.Fatalf("Expected 2 tabs, only found %v tab(s)", len(tabs))
	}
	tabRect, err := ac.Location(ctx, tabParam.First())
	if err != nil {
		s.Fatal("Failed to get the location of the tab: ", err)
	}
	start := tabRect.CenterPoint()

	if err := perfutil.RunMultipleAndSave(ctx, s.OutDir(), cr.Browser(), uiperf.Run(s, perfutil.RunAndWaitAll(tconn, func(ctx context.Context) error {
		return uiauto.Combine("drag and move a tab",
			mouse.Drag(tconn, start, end, time.Second),
			ac.Retry(10, checkWindowsNum(ctx, tconn, 2)),
			uiauto.Sleep(time.Second),
			mouse.Drag(tconn, end, start, time.Second),
			ac.Retry(10, checkWindowsNum(ctx, tconn, 1)),
			uiauto.Sleep(time.Second),
		)(ctx)
	},
		"Ash.TabDrag.PresentationTime.ClamshellMode",
		"Ash.TabDrag.PresentationTime.MaxLatency.ClamshellMode")),
		perfutil.StoreLatency,
		perfutil.RunnerOptions{IgnoreFirstRun: true, DropMinMaxValues: true},
	); err != nil {
		s.Fatal("Failed to run or save: ", err)
	}
}

func checkWindowsNum(ctx context.Context, tconn *chrome.TestConn, num int) action.Action {
	return func(ctx context.Context) error {
		ws, err := ash.GetAllWindows(ctx, tconn)
		if err != nil {
			return errors.Wrap(err, "failed to obtain the window list")
		}
		if num != len(ws) {
			return errors.Wrapf(err, "failed to verify the number of windows, got %v, want %v", num, len(ws))
		}
		return nil
	}
}
