// Copyright 2022 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package ui

import (
	"context"
	"time"

	"github.com/golang/protobuf/ptypes/empty"
	"go.chromium.org/tast-tests/cros/local/common"
	"google.golang.org/grpc"
	"google.golang.org/protobuf/types/known/emptypb"

	"go.chromium.org/tast-tests/cros/local/audio"
	"go.chromium.org/tast-tests/cros/local/audio/crastestclient"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto/filesapp"
	"go.chromium.org/tast-tests/cros/local/cryptohome"
	"go.chromium.org/tast-tests/cros/local/input"
	"go.chromium.org/tast-tests/cros/services/cros/ui"
	"go.chromium.org/tast/core/errors"
	"go.chromium.org/tast/core/testing"
)

func init() {
	testing.AddService(&testing.Service{
		Register: func(srv *grpc.Server, s *testing.ServiceState) {
			ui.RegisterAudioServiceServer(srv, &AudioService{
				s:            s,
				sharedObject: common.SharedObjectsForServiceSingleton,
			})
		},
	})
}

// AudioService implements tast.cros.ui.AudioService.
type AudioService struct {
	s            *testing.ServiceState
	sharedObject *common.SharedObjectsForService
}

// New is no longer supported and will throw an error if called.
// Deprecated: Use ChromeService.New() instead.
func (as *AudioService) New(ctx context.Context, e *emptypb.Empty) (*emptypb.Empty, error) {
	return nil, errors.New("the AudioService.New method is no longer supported, use ChromeService.New() instead")
}

// Close is no longer supported and will throw an error if called.
// Deprecated: Use ChromeService.Close() instead.
func (as *AudioService) Close(ctx context.Context, e *emptypb.Empty) (*emptypb.Empty, error) {
	return nil, errors.New("the AudioService.Close method is no longer supported, use ChromeService.Close() instead")
}

// OpenDirectoryAndFile performs launching filesapp and opening particular file
// in given directory.
func (as *AudioService) OpenDirectoryAndFile(ctx context.Context, req *ui.AudioServiceRequest) (*empty.Empty, error) {
	if as.sharedObject.Chrome == nil {
		return nil, errors.New("Chrome not available")
	}

	tconn, err := as.sharedObject.Chrome.TestAPIConn(ctx)
	if err != nil {
		return nil, err
	}

	filesTitlePrefix := "Files - "
	files, err := filesapp.Launch(ctx, tconn)
	if err != nil {
		return nil, errors.Wrap(err, "failed to launch the Files App")
	}

	if err := files.OpenDir(req.DirectoryName, filesTitlePrefix+req.DirectoryName)(ctx); err != nil {
		return nil, errors.Wrapf(err, "failed to open %q folder in files app", req.DirectoryName)
	}

	if req.FileName != "" {
		if err := files.OpenFile(req.FileName)(ctx); err != nil {
			return nil, errors.Wrapf(err, "failed to open the audio file %q", req.FileName)
		}
	}

	return &empty.Empty{}, nil
}

// GenerateTestRawData generates test raw data file.
func (as *AudioService) GenerateTestRawData(ctx context.Context, req *ui.AudioServiceRequest) (*empty.Empty, error) {
	if as.sharedObject.Chrome == nil {
		return nil, errors.New("Chrome not available")
	}

	// Generate sine raw input file that lasts 30 seconds.
	rawFile := audio.TestRawData{
		Path:          req.FilePath,
		BitsPerSample: 16,
		Channels:      2,
		Rate:          48000,
		Frequencies:   []int{440, 440},
		Volume:        0.05,
		Duration:      int(req.DurationInSecs),
	}

	if err := audio.GenerateTestRawData(ctx, rawFile); err != nil {
		return nil, errors.Wrap(err, "failed to generate audio test data")
	}
	return &empty.Empty{}, nil
}

// ConvertRawToWav will convert raw data file to wav file format.
func (as *AudioService) ConvertRawToWav(ctx context.Context, req *ui.AudioServiceRequest) (*empty.Empty, error) {
	if as.sharedObject.Chrome == nil {
		return nil, errors.New("Chrome not available")
	}
	rawData := audio.TestRawData{
		Path:          req.FilePath,
		BitsPerSample: 16,
		Channels:      2,
		Rate:          48000,
	}
	if err := audio.ConvertRawToWav(ctx, rawData, req.FileName); err != nil {
		return nil, errors.Wrap(err, "failed to convert raw to wav")
	}
	return &empty.Empty{}, nil
}

// KeyboardAccel will create keyboard event and performs keyboard
// key press with Accel().
func (as *AudioService) KeyboardAccel(ctx context.Context, req *ui.AudioServiceRequest) (*empty.Empty, error) {
	if as.sharedObject.Chrome == nil {
		return nil, errors.New("Chrome not available")
	}
	kb, err := input.Keyboard(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to find keyboard")
	}
	defer kb.Close(ctx)

	if err := kb.Accel(ctx, req.Expr); err != nil {
		return nil, errors.Wrapf(err, "failed to press %q using keyboard", req.Expr)
	}
	return &empty.Empty{}, nil
}

// AudioCrasSelectedInputDevice will return selected audio device name
// and audio device type.
func (as *AudioService) AudioCrasSelectedInputDevice(ctx context.Context, req *empty.Empty) (*ui.AudioServiceResponse, error) {
	// Get Current active node.
	cras, err := audio.NewCras(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create Cras object")
	}
	inDeviceName, inDeviceType, err := cras.SelectedInputDevice(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get the selected audio device")
	}
	return &ui.AudioServiceResponse{DeviceName: inDeviceName, DeviceType: inDeviceType}, nil
}

// AudioCrasSelectedOutputDevice will return selected audio device name
// and audio device type.
func (as *AudioService) AudioCrasSelectedOutputDevice(ctx context.Context, req *empty.Empty) (*ui.AudioServiceResponse, error) {
	if as.sharedObject.Chrome == nil {
		return nil, errors.New("Chrome not available")
	}
	// Get Current active node.
	cras, err := audio.NewCras(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create Cras object")
	}
	outDeviceName, outDeviceType, err := cras.SelectedOutputDevice(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get the selected audio device")
	}
	return &ui.AudioServiceResponse{DeviceName: outDeviceName, DeviceType: outDeviceType}, nil
}

// VerifyFirstRunningDevice will check for audio routing device status.
func (as *AudioService) VerifyFirstRunningDevice(ctx context.Context, req *ui.AudioServiceRequest) (*empty.Empty, error) {
	if as.sharedObject.Chrome == nil {
		return nil, errors.New("Chrome not available")
	}
	if err := testing.Poll(ctx, func(ctx context.Context) error {
		devName, err := crastestclient.FirstRunningDevice(ctx, audio.OutputStream)
		if err != nil {
			return errors.Wrap(err, "failed to detect running output device")
		}

		if deviceName := req.Expr; deviceName != devName {
			return errors.Wrapf(err, "failed to route the audio through expected audio node: got %q; want %q", devName, deviceName)
		}
		return nil
	}, &testing.PollOptions{Timeout: 10 * time.Second}); err != nil {
		return nil, errors.Wrap(err, "failed to check audio running device")
	}
	return &empty.Empty{}, nil
}

// SetActiveNodeByType will set the provided audio node as Active audio node.
func (as *AudioService) SetActiveNodeByType(ctx context.Context, req *ui.AudioServiceRequest) (*empty.Empty, error) {
	if as.sharedObject.Chrome == nil {
		return nil, errors.New("Chrome not available")
	}
	var cras *audio.Cras
	if err := cras.SetActiveNodeByType(ctx, req.Expr); err != nil {
		return nil, errors.Wrapf(err, "failed to select active device %s", req.Expr)
	}
	return &empty.Empty{}, nil
}

// DownloadsPath returns the path to the Downloads folder of the current user.
func (as *AudioService) DownloadsPath(ctx context.Context, req *empty.Empty) (*ui.AudioServiceResponse, error) {
	if as.sharedObject.Chrome == nil {
		return nil, errors.New("chrome not available")
	}

	downloadsPath, err := cryptohome.DownloadsPath(ctx, as.sharedObject.Chrome.NormalizedUser())
	if err != nil {
		return nil, errors.Wrap(err, "failed to retrieve users Downloads path")
	}
	return &ui.AudioServiceResponse{DownloadsPath: downloadsPath}, nil
}

// SetWBSEnabled will set whether WBS should be enabled in the audio server.
func (as *AudioService) SetWBSEnabled(ctx context.Context, req *ui.AudioServiceRequest) (*empty.Empty, error) {
	if err := crastestclient.SetWBSEnabled(ctx, req.WBSEnabled); err != nil {
		return nil, errors.Wrap(err, "failed to change WBS support on host")
	}
	return &empty.Empty{}, nil
}
