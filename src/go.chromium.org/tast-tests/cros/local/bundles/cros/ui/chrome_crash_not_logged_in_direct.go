// Copyright 2019 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package ui

import (
	"context"

	"go.chromium.org/tast-tests/cros/local/bundles/cros/ui/chromecrash"
	"go.chromium.org/tast-tests/cros/local/chrome"
	"go.chromium.org/tast-tests/cros/local/chrome/browser"
	"go.chromium.org/tast-tests/cros/local/crash"
	"go.chromium.org/tast/core/testing"
)

type chromeCrashNotLoggedInDirectParams struct {
	fileType chromecrash.CrashFileType
	handler  chromecrash.CrashHandler
}

func init() {
	testing.AddTest(&testing.Test{
		Func:         ChromeCrashNotLoggedInDirect,
		LacrosStatus: testing.LacrosVariantUnneeded,
		Desc:         "Checks that Chrome writes crash dumps while not logged in; old version that does not invoke crash_reporter",
		Contacts:     []string{"chromeos-data-eng@google.com", "iby@chromium.org"},
		BugComponent: "b:1032705",
		SoftwareDeps: []string{"chrome"},
		Attr:         []string{"group:mainline"},
		Params: []testing.Param{{
			Name: "breakpad",
			Val: chromeCrashNotLoggedInDirectParams{
				handler:  chromecrash.Breakpad,
				fileType: chromecrash.BreakpadDmp,
			},
			ExtraSoftwareDeps: []string{"breakpad"},
		}, {
			Name: "crashpad",
			Val: chromeCrashNotLoggedInDirectParams{
				handler:  chromecrash.Crashpad,
				fileType: chromecrash.MetaFile,
			},
			ExtraSoftwareDeps: []string{"crashpad"},
			ExtraAttr:         []string{"group:hw_agnostic"},
		}},
	})
}

// ChromeCrashNotLoggedInDirect tests that Chrome crashes that happen during tast
// tests are properly captured (that is, during tast tests which are testing
// something other than the crash system).
//
// The other Chrome crash tests cover cases that we expect to occur on end-user
// machines, by simulating user consent. This test covers the tast case, where
// we bypass consent by telling the crash system that we are in a test
// environment. In particular, breakpad goes through a very different code path
// which doesn't involve crash_reporter at all, and we want that to keep working.
//
// Note: The name is a misnomer; the 'Direct' refers to the old days when both
// breakpad and crashpad bypassed crash_reporter and wrote the crashes directly
// onto disk during this test. Crashpad no longer does that; the test should be
// named "TastMode". TODO(https://crbug.com/1201467): Rename to
// ChromeCrashNotLoggedInTastMode
func ChromeCrashNotLoggedInDirect(ctx context.Context, s *testing.State) {
	params := s.Param().(chromeCrashNotLoggedInDirectParams)
	ct, err := chromecrash.NewCrashTester(ctx, chromecrash.GPUProcess, browser.TypeAsh, params.fileType)
	if err != nil {
		s.Fatal("NewCrashTester failed: ", err)
	}
	defer ct.Close()

	cr, err := chrome.New(ctx, chrome.NoLogin(), chrome.ExtraArgs(chromecrash.GetExtraArgs(params.handler, crash.MockConsent)...))
	if err != nil {
		s.Fatal("Chrome startup failed: ", err)
	}
	defer cr.Close(ctx)

	// We use crash.DevImage() here because this test still uses the testing
	// command-line flags on crash_reporter to bypass metrics consent and such.
	// Those command-line flags only work if the crash-test-in-progress does not
	// exist.
	if err := crash.SetUpCrashTest(ctx, crash.DevImage()); err != nil {
		s.Fatal("SetUpCrashTest failed: ", err)
	}
	defer crash.TearDownCrashTest(ctx)
	if err := ct.AssociateWithChrome(ctx, cr); err != nil {
		s.Fatal("Failed to associate chrome with the crash tester: ", err)
	}

	var files []string
	if files, err = ct.KillAndGetCrashFiles(ctx); err != nil {
		s.Fatal("Couldn't kill Chrome or get dumps: ", err)
	}

	if params.fileType == chromecrash.MetaFile {
		if err := chromecrash.FindCrashFilesIn(crash.LocalCrashDir, files); err != nil {
			s.Errorf("Crash files weren't written to %s after crashing process: %v", crash.LocalCrashDir, err)
		}
	} else {
		if err := chromecrash.FindBreakpadDmpFilesIn(crash.LocalCrashDir, files); err != nil {
			s.Errorf(".dmp files weren't written to %s after crashing process: %v", crash.LocalCrashDir, err)
		}
	}
}
