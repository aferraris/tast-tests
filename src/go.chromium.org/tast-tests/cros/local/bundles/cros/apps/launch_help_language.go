// Copyright 2021 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package apps

import (
	"context"
	"time"

	"go.chromium.org/tast-tests/cros/local/apps"
	"go.chromium.org/tast-tests/cros/local/bundles/cros/apps/fixture"
	"go.chromium.org/tast-tests/cros/local/bundles/cros/apps/pre"
	"go.chromium.org/tast-tests/cros/local/chrome/ash"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto/faillog"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto/nodewith"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto/role"
	"go.chromium.org/tast/core/testing"
	"go.chromium.org/tast/core/testing/hwdep"
)

func init() {
	testing.AddTest(&testing.Test{
		Func:         LaunchHelpLanguage,
		LacrosStatus: testing.LacrosVariantUnneeded,
		Desc:         "Launch Help APP in different system languages",
		Contacts: []string{
			"showoff-eng@google.com",
		},
		BugComponent: "b:690873",
		Attr:         []string{"group:mainline", "group:hw_agnostic"},
		SoftwareDeps: []string{"chrome", "chrome_internal"},
		HardwareDeps: hwdep.D(pre.AppsStableModels),
		Fixture:      fixture.LoggedInJP,
	})
}

func LaunchHelpLanguage(ctx context.Context, s *testing.State) {
	const (
		regionCode       = "jp"
		appName          = "使い方・ヒント"
		helpCategoryName = "新機能"
	)

	tconn := s.FixtValue().(fixture.FixtData).TestAPIConn

	defer faillog.DumpUITreeOnError(ctx, s.OutDir(), s.HasError, tconn)

	if err := apps.Launch(ctx, tconn, apps.Help.ID); err != nil {
		s.Fatalf("Failed to launch %s: %v", apps.Help.Name, err)
	}

	s.Log("Wait for Help shown in shelf")
	if err := ash.WaitForApp(ctx, tconn, apps.Help.ID, time.Minute); err != nil {
		s.Fatal("Failed to check Help in shelf: ", err)
	}

	s.Logf("Wait for Help app rendering in %s language", regionCode)
	ui := uiauto.New(tconn).WithTimeout(30 * time.Second)

	appRootFinder := nodewith.Name(appName).Role(role.RootWebArea)

	welcomeTextFinder := nodewith.Name(helpCategoryName).First().Ancestor(appRootFinder)
	if err := ui.WaitUntilExists(welcomeTextFinder)(ctx); err != nil {
		s.Fatalf("Failed to launch Help in %s language: %v", regionCode, err)
	}
}
