// Copyright 2021 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package apps

import (
	"context"
	"time"

	"go.chromium.org/tast-tests/cros/local/apps"
	"go.chromium.org/tast-tests/cros/local/bundles/cros/apps/fixture"
	"go.chromium.org/tast-tests/cros/local/bundles/cros/apps/pre"
	"go.chromium.org/tast-tests/cros/local/chrome/ash"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto/faillog"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto/nodewith"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto/role"
	"go.chromium.org/tast/core/testing"
	"go.chromium.org/tast/core/testing/hwdep"
)

func init() {
	testing.AddTest(&testing.Test{
		Func:         LaunchGalleryLanguage,
		LacrosStatus: testing.LacrosVariantExists,
		Desc:         "Launch Gallery APP in different system languages",
		Contacts: []string{
			"backlight-swe@google.com",
		},
		BugComponent: "b:562866",
		Attr:         []string{"group:mainline", "informational"},
		SoftwareDeps: []string{"chrome", "chrome_internal"},
		HardwareDeps: hwdep.D(pre.AppsStableModels),
		Params: []testing.Param{
			{
				Fixture: fixture.LoggedInJP,
			},
			{
				Name:              "lacros",
				Fixture:           fixture.LacrosLoggedInJP,
				ExtraSoftwareDeps: []string{"lacros_stable"},
			},
		},
	})
}

func LaunchGalleryLanguage(ctx context.Context, s *testing.State) {
	const (
		regionCode          = "jp"
		appName             = "ギャラリー"
		openImageButtonName = "画像を開く"
	)

	tconn := s.FixtValue().(fixture.FixtData).TestAPIConn

	defer faillog.DumpUITreeOnError(ctx, s.OutDir(), s.HasError, tconn)

	// SWA installation is not guaranteed during startup.
	// Using this wait to check installation finished before starting test.
	s.Log("Wait for Gallery to be installed")
	if err := ash.WaitForChromeAppInstalled(ctx, tconn, apps.Gallery.ID, 2*time.Minute); err != nil {
		s.Fatal("Failed to wait for installed app: ", err)
	}

	if err := apps.Launch(ctx, tconn, apps.Gallery.ID); err != nil {
		s.Fatal("Failed to launch Gallery: ", err)
	}

	s.Log("Wait for Gallery shown in shelf")
	if err := ash.WaitForApp(ctx, tconn, apps.Gallery.ID, time.Minute); err != nil {
		s.Fatal("Failed to check Gallery in shelf: ", err)
	}

	s.Logf("Wait for Gallery app rendering in %s language", regionCode)
	ui := uiauto.New(tconn).WithTimeout(30 * time.Second)

	appRootFinder := nodewith.Name(appName).Role(role.RootWebArea)
	openImageButtonFinder := nodewith.Role(role.Button).Name(openImageButtonName).Ancestor(appRootFinder)
	if err := ui.WaitUntilExists(openImageButtonFinder)(ctx); err != nil {
		s.Fatalf("Failed to launch Gallery in %s language: %v", regionCode, err)
	}
}
