// Copyright 2020 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package apps

import (
	"context"
	"time"

	"go.chromium.org/tast-tests/cros/common/dma"
	"go.chromium.org/tast-tests/cros/common/ui"
	"go.chromium.org/tast-tests/cros/local/bundles/cros/apps/fixture"
	"go.chromium.org/tast-tests/cros/local/bundles/cros/apps/pre"
	"go.chromium.org/tast-tests/cros/local/chrome"
	"go.chromium.org/tast-tests/cros/local/chrome/apps/helpapp"
	"go.chromium.org/tast-tests/cros/local/chrome/ash"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto/faillog"
	"go.chromium.org/tast/core/testing"
	"go.chromium.org/tast/core/testing/hwdep"
)

// testParameters contains all the data needed to run a single test iteration.
type testParameters struct {
	tabletMode       bool
	oobe             bool
	fieldTrialConfig chrome.FieldTrialConfigMode
}

func init() {
	testing.AddTest(&testing.Test{
		Func:         LaunchHelpApp,
		LacrosStatus: testing.LacrosVariantExists,
		Desc:         "Help app should be launched after OOBE",
		Contacts: []string{
			"showoff-eng@google.com",
		},
		BugComponent: "b:690873",
		// TODO(b/303137892): remove after fixing tast test.
		Attr:         []string{"group:mainline", "informational"},
		VarDeps:      []string{ui.GaiaPoolDefaultVarName},
		SoftwareDeps: []string{"chrome", "chrome_internal"},
		Timeout:      chrome.GAIALoginTimeout + time.Minute,
		Params: []testing.Param{
			{
				Name:              "clamshell_oobe_stable_fieldtrial_testing_config_off",
				ExtraHardwareDeps: hwdep.D(pre.AppsStableModels),
				ExtraSoftwareDeps: []string{"gaia"},
				ExtraAttr:         []string{"group:mainline"},
				Val: testParameters{
					tabletMode:       false,
					oobe:             true,
					fieldTrialConfig: chrome.FieldTrialConfigDisable,
				},
			}, {
				Name:              "clamshell_oobe_stable_fieldtrial_testing_config_on",
				ExtraHardwareDeps: hwdep.D(pre.AppsStableModels),
				ExtraSoftwareDeps: []string{"gaia"},
				ExtraAttr:         []string{"group:mainline", "group:chrome_uprev_cbx"},
				Val: testParameters{
					tabletMode:       false,
					oobe:             true,
					fieldTrialConfig: chrome.FieldTrialConfigEnable,
				},
			}, {
				Name:              "clamshell_oobe_unstable",
				ExtraHardwareDeps: hwdep.D(pre.AppsUnstableModels),
				ExtraSoftwareDeps: []string{"gaia"},
				// b:238260020 - disable aged (>1y) unpromoted informational tests
				// ExtraAttr:         []string{"group:mainline", "informational"},
				Val: testParameters{
					tabletMode: true,
					oobe:       true,
				},
			}, {
				Name:              "tablet_oobe_stable",
				ExtraHardwareDeps: hwdep.D(pre.AppsStableModels),
				ExtraSoftwareDeps: []string{"gaia"},
				ExtraAttr:         []string{"group:mainline"},
				Val: testParameters{
					tabletMode: true,
					oobe:       true,
				},
			}, {
				Name:              "tablet_oobe_unstable",
				ExtraHardwareDeps: hwdep.D(pre.AppsUnstableModels),
				ExtraSoftwareDeps: []string{"gaia"},
				// b:238260020 - disable aged (>1y) unpromoted informational tests
				// ExtraAttr:         []string{"group:mainline", "informational"},
				Val: testParameters{
					tabletMode: true,
					oobe:       true,
				},
			}, {
				Name:              "clamshell_logged_in_stable_fieldtrial_testing_config_off",
				ExtraHardwareDeps: hwdep.D(pre.AppsStableModels),
				ExtraAttr:         []string{"group:mainline"},
				Fixture:           fixture.LoggedInFieldTrialConfigDisable,
				Val: testParameters{
					tabletMode: false,
					oobe:       false,
				},
			}, {
				Name:              "clamshell_logged_in_stable_fieldtrial_testing_config_on",
				ExtraHardwareDeps: hwdep.D(pre.AppsStableModels),
				ExtraAttr:         []string{"group:mainline", "group:chrome_uprev_cbx"},
				Fixture:           fixture.LoggedInFieldTrialConfigEnable,
				Val: testParameters{
					tabletMode: false,
					oobe:       false,
				},
			}, {
				Name:              "clamshell_logged_in_unstable",
				ExtraHardwareDeps: hwdep.D(pre.AppsUnstableModels),
				Fixture:           fixture.LoggedIn,
				// b:238260020 - disable aged (>1y) unpromoted informational tests
				// ExtraAttr:         []string{"group:mainline", "informational"},
				Val: testParameters{
					tabletMode: false,
					oobe:       false,
				},
			}, {
				Name:              "tablet_logged_in_stable",
				ExtraHardwareDeps: hwdep.D(pre.AppsStableModels, hwdep.TouchScreen()),
				ExtraAttr:         []string{"group:mainline"},
				Fixture:           fixture.LoggedIn,
				Val: testParameters{
					tabletMode: true,
					oobe:       false,
				},
			}, {
				Name:              "tablet_logged_in_unstable",
				ExtraHardwareDeps: hwdep.D(pre.AppsUnstableModels, hwdep.TouchScreen()),
				Fixture:           fixture.LoggedIn,
				// b:238260020 - disable aged (>1y) unpromoted informational tests
				// ExtraAttr:         []string{"group:mainline", "informational"},
				Val: testParameters{
					tabletMode: true,
					oobe:       false,
				},
			}, {
				Name:              "clamshell_logged_in_stable_lacros_fieldtrial_testing_config_off",
				ExtraHardwareDeps: hwdep.D(pre.AppsStableModels),
				Fixture:           fixture.LacrosLoggedInFieldTrialConfigDisable,
				ExtraSoftwareDeps: []string{"lacros", "lacros_stable"},
				ExtraAttr:         []string{"group:mainline"},
				Val: testParameters{
					tabletMode: false,
					oobe:       false,
				},
			}, {
				Name:              "clamshell_logged_in_stable_lacros_fieldtrial_testing_config_on",
				ExtraHardwareDeps: hwdep.D(pre.AppsStableModels),
				Fixture:           fixture.LacrosLoggedInFieldTrialConfigEnable,
				ExtraSoftwareDeps: []string{"lacros", "lacros_stable"},
				ExtraAttr:         []string{"group:mainline", "group:chrome_uprev_cbx"},
				Val: testParameters{
					tabletMode: false,
					oobe:       false,
				},
			}, {
				Name:              "tablet_logged_in_stable_lacros",
				Fixture:           fixture.LacrosLoggedIn,
				ExtraSoftwareDeps: []string{"lacros", "lacros_stable"},
				ExtraAttr:         []string{"group:mainline"},
				ExtraHardwareDeps: hwdep.D(pre.AppsStableModels, hwdep.TouchScreen()),
				Val: testParameters{
					tabletMode: true,
					oobe:       false,
				},
			},
		}})
}

// LaunchHelpApp verifies launching Showoff after OOBE.
func LaunchHelpApp(ctx context.Context, s *testing.State) {
	params := s.Param().(testParameters)
	if params.oobe {
		helpAppLaunchDuringOOBE(ctx, s, params.tabletMode, params.fieldTrialConfig)
	} else {
		helpAppLaunchAfterLogin(ctx, s, params.tabletMode)
	}
}

// helpAppLaunchDuringOOBE verifies help app launch during OOBE stage. Help app only launches with real user login in clamshell mode.
func helpAppLaunchDuringOOBE(ctx context.Context, s *testing.State, isTabletMode bool, fieldTrialConfig chrome.FieldTrialConfigMode) {
	var uiMode string
	if isTabletMode {
		uiMode = "--force-tablet-mode=touch_view"
	} else {
		uiMode = "--force-tablet-mode=clamshell"
	}

	cr, err := chrome.New(ctx,
		chrome.GAIALoginPool(dma.CredsFromPool(ui.GaiaPoolDefaultVarName)),
		chrome.DontSkipOOBEAfterLogin(),
		chrome.EnableFeatures("HelpAppFirstRun"),
		chrome.ExtraArgs(uiMode),
		chrome.FieldTrialConfig(fieldTrialConfig),
	)

	if err != nil {
		s.Fatal("Failed to start Chrome: ", err)
	}

	tconn, err := cr.TestAPIConn(ctx)
	if err != nil {
		s.Fatal("Failed to connect Test API: ", err)
	}

	defer faillog.DumpUITreeOnError(ctx, s.OutDir(), s.HasError, tconn)

	// Verify HelpApp (aka Explore) launched in Clamshell mode only.
	if err := assertHelpAppLaunched(ctx, s, tconn, cr, !isTabletMode); err != nil {
		s.Fatalf("Failed to verify help app launching during oobe in tablet mode enabled(%v): %v", isTabletMode, err)
	}
}

// helpAppLaunchAfterLogin verifies help app launch after user login. It should be able to launch on devices in both clamshell and tablet mode.
func helpAppLaunchAfterLogin(ctx context.Context, s *testing.State, isTabletMode bool) {
	cr := s.FixtValue().(fixture.FixtData).Chrome
	tconn := s.FixtValue().(fixture.FixtData).TestAPIConn

	defer faillog.DumpUITreeOnError(ctx, s.OutDir(), s.HasError, tconn)

	s.Logf("Ensure tablet mode enabled(%v)", isTabletMode)
	cleanup, err := ash.EnsureTabletModeEnabled(ctx, tconn, isTabletMode)
	if err != nil {
		s.Fatalf("Failed to ensure tablet mode enabled(%v): %v", isTabletMode, err)
	}
	defer cleanup(ctx)

	if err := helpapp.NewContext(cr, tconn).Launch()(ctx); err != nil {
		s.Fatal("Failed to launch help app: ", err)
	}

	if err := assertHelpAppLaunched(ctx, s, tconn, cr, true); err != nil {
		s.Fatal("Failed to verify help app launching after user logged in: ", err)
	}
}

// assertHelpAppLaunched asserts help app to be launched or not
func assertHelpAppLaunched(ctx context.Context, s *testing.State, tconn *chrome.TestConn, cr *chrome.Chrome, isLaunched bool) error {
	helpCtx := helpapp.NewContext(cr, tconn)
	if isLaunched {
		// Verify perk is shown to default consumer user.
		err := helpCtx.WaitForHTMLElementPresent(ctx, "showoff-offers-page")
		if err != nil {
			s.Fatal("Failed to wait for offers page to render: ", err)
		}
	} else {
		isAppLaunched, err := helpCtx.Exists(ctx)
		if err != nil {
			s.Fatal("Failed to verify help app existence: ", err)
		}

		if isAppLaunched {
			s.Fatal("Help app should not be launched after oobe on a managed device")
		}
	}
	return nil
}

// shouldLaunchHelp returns a result to launch help app or not.
func shouldLaunchHelp(isTabletMode, isOOBE bool) bool {
	return !isOOBE || !isTabletMode
}
