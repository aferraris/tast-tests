// Copyright 2021 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package apps

import (
	"context"
	"fmt"

	"go.chromium.org/tast-tests/cros/local/bundles/cros/apps/webstore"
	"go.chromium.org/tast-tests/cros/local/chrome"
	"go.chromium.org/tast/core/testing"
)

type appsParams struct {
	appName    string
	urlAppName string
	urlAppID   string
	tabName    string
}

func init() {
	testing.AddTest(&testing.Test{
		Func:         WebstoreAppInstallUninstall,
		LacrosStatus: testing.LacrosVariantNeeded,
		Desc:         "Chrome webstore app install uninstall",
		Contacts:     []string{"intel.chrome.automation.team@intel.com", "pathan.jilani@intel.com"},
		BugComponent: "b:157291", // ChromeOS > External > Intel
		SoftwareDeps: []string{"chrome"},
		Attr:         []string{"group:mainline", "informational", "group:intel-nda", "group:hw_agnostic"},
		Params: []testing.Param{
			{
				Name: "google_photo",
				Val: appsParams{appName: "Google Photo",
					urlAppName: "save-image-to-google-photos",
					tabName:    "Save Image To Google Photos",
					urlAppID:   "djakijfdccnmmnknpmphdkkjbjfenkne",
				},
				Fixture: "chromeLoggedIn",
			},
			{
				Name: "cut_the_rope",
				Val: appsParams{appName: "Cut The Rope",
					urlAppName: "cut-the-rope-original",
					tabName:    "Cut The Rope Original",
					urlAppID:   "fjiiikojnkdhmnbhcdkieejjjohfpcoo",
				},
				Fixture: "chromeLoggedIn",
			},
		},
	})
}

func WebstoreAppInstallUninstall(ctx context.Context, s *testing.State) {
	testOpt := s.Param().(appsParams)
	cr := s.FixtValue().(chrome.HasChrome).Chrome()
	tconn, err := cr.TestAPIConn(ctx)
	if err != nil {
		s.Fatal("Failed to connect Test API: ", err)
	}

	appURL := fmt.Sprintf("https://chrome.google.com/webstore/detail/%s/%s", testOpt.urlAppName, testOpt.urlAppID)

	// App Install parameters.
	app := webstore.App{Name: testOpt.appName,
		URL:           appURL,
		WebTab:        testOpt.tabName,
		VerifyText:    "Remove from Chrome",
		AddRemoveText: "Add to Chrome",
		ConfirmText:   "Add extension",
	}

	s.Logf("Installing %q app", testOpt.appName)
	if err := webstore.UpgradeWebstoreApp(ctx, cr, tconn, app); err != nil {
		s.Fatal("Failed to install webapp: ", err)
	}

	// App Uninstall parameters.
	app = webstore.App{Name: testOpt.appName,
		URL:           appURL,
		WebTab:        testOpt.tabName,
		VerifyText:    "Add to Chrome",
		AddRemoveText: "Remove from Chrome",
		ConfirmText:   "Remove",
	}

	s.Logf("Uninstalling %q app", testOpt.appName)
	if err := webstore.UpgradeWebstoreApp(ctx, cr, tconn, app); err != nil {
		s.Fatal("Failed to uninstall webapp: ", err)
	}
}
