// Copyright 2023 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package crostini

import (
	"context"
	"time"

	"go.chromium.org/tast-tests/cros/common/dma"
	"go.chromium.org/tast-tests/cros/common/ui"
	"go.chromium.org/tast-tests/cros/local/chrome"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto/quicksettings"
	"go.chromium.org/tast-tests/cros/local/chrome/userutil"
	"go.chromium.org/tast-tests/cros/local/crostini"
	"go.chromium.org/tast-tests/cros/local/vm"
	"go.chromium.org/tast/core/ctxutil"
	"go.chromium.org/tast/core/errors"
	"go.chromium.org/tast/core/testing"
)

func init() {
	testing.AddTest(&testing.Test{
		Func:         TwoUsersInstallRemoveSecond,
		LacrosStatus: testing.LacrosVariantUnneeded,
		Desc:         "Test two users can install crostini parallely and then remove the second",
		Contacts:     []string{"hardikgoyal@google.com", "cryptohome-core@google.com"},
		VarDeps:      []string{ui.GaiaPoolDefaultVarName, "ui.signinProfileTestExtensionManifestKey"},
		Attr:         []string{"group:mainline", "group:crostini_slow", "informational"},
		SoftwareDeps: []string{"chrome", "vm_host"},
		BugComponent: "b:1088399", // ChromeOS > Security > Cryptohome
		Params: []testing.Param{
			// Parameters generated by two_users_install_remove_second_test.go. DO NOT EDIT.
			{
				Name:              "stable",
				ExtraData:         []string{crostini.GetContainerMetadataArtifact("bullseye", false), crostini.GetContainerRootfsArtifact("bullseye", false)},
				ExtraSoftwareDeps: []string{"dlc"},
				ExtraHardwareDeps: crostini.CrostiniOptimalPerf,
				Timeout:           14 * time.Minute,
				Val:               vm.DebianBullseye,
			},
		},
	})
}

func TwoUsersInstallRemoveSecond(ctx context.Context, s *testing.State) {
	debianVersion := s.Param().(vm.ContainerDebianVersion)

	cleanUpCtx := ctx
	ctx, cancel := ctxutil.Shorten(ctx, 40*time.Second)
	defer cancel()

	// Login options for the first user.
	optsUser1 := []chrome.Option{chrome.GAIALoginPool(dma.CredsFromPool(ui.GaiaPoolDefaultVarName)),
		chrome.ExtraArgs("--vmodule=crostini*=1"),
	}
	// First user setup and crostini install.
	// Prepare to install Crostini for the first user.
	var firstCr *chrome.Chrome
	var firstTconn *chrome.TestConn
	var err error
	if firstCr, firstTconn, err = loginChromeAndGetTConn(ctx, optsUser1...); err != nil {
		s.Fatalf("Failed to login Chrome and get test API for user 1: %s", err)
	}

	iOptionsUser1 := crostini.GetInstallerOptions(s, debianVersion, false /*largeContainer*/, firstCr.NormalizedUser())

	// Install Crostini.
	_, _, err = crostini.InstallCrostini(ctx, firstTconn, firstCr, iOptionsUser1)
	if err != nil {
		s.Fatalf("Failed to test Crostini for %s: %s", iOptionsUser1.UserName, err)
	}
	firstCr.Close(cleanUpCtx)
	// Cleanup for the first user.
	defer func() {
		if err := cleanupVM(cleanUpCtx, optsUser1...); err != nil {
			s.Fatalf("Failed to uninstall Crostini for %s: %s", iOptionsUser1.UserName, err)
		}
	}()

	// Login options for the second user.
	optsUser2 := []chrome.Option{
		chrome.KeepState(),
		chrome.ExtraArgs("--vmodule=crostini*=1"),
	}
	var secondCreds chrome.Creds
	var secondCr *chrome.Chrome
	var secondTconn *chrome.TestConn
	// Prepare to install Crostini for the second user.
	if secondCr, secondTconn, err = loginChromeAndGetTConn(ctx, optsUser2...); err != nil {
		s.Fatal("Failed to login Chrome and get test API for testuser: ", err)
	}
	iOptionsUser2 := crostini.GetInstallerOptions(s, debianVersion, false /*largeContainer*/, secondCr.NormalizedUser())
	// Install Crostini.
	_, _, err = crostini.InstallCrostini(ctx, secondTconn, secondCr, iOptionsUser2)
	if err != nil {
		s.Fatalf("Failed to test Crostini for %s: %s", iOptionsUser2.UserName, err)
	}
	// Cleanup for the second user.
	defer func() {
		if err := cleanupVM(cleanUpCtx, optsUser2...); err != nil {
			s.Fatalf("Failed to uninstall Crostini for %s: %s", iOptionsUser2.UserName, err)
		}
	}()

	if err := quicksettings.SignOut(ctx, secondTconn); err != nil {
		s.Fatal("Failed to logout: ", err)
	}
	secondCreds = secondCr.Creds()
	secondCr.Close(cleanUpCtx)
	// Remove second user.
	{
		// chrome.NoLogin() and chrome.KeepState() are needed to show the login
		// screen with a user pod (instead of the OOBE login screen).
		cr, err := chrome.New(
			ctx,
			chrome.NoLogin(),
			chrome.KeepState(),
			chrome.LoadSigninProfileExtension(s.RequiredVar("ui.signinProfileTestExtensionManifestKey")),
		)
		if err != nil {
			s.Fatal("Failed to start chrome: ", err)
		}
		defer cr.Close(cleanUpCtx)
		// Connect to login extension.
		tLoginConn, err := cr.SigninProfileTestAPIConn(ctx)
		if err != nil {
			s.Fatal("Failed to create login test connection with chrome: ", err)
		}

		if err := userutil.RemoveUserOnLoginScreen(ctx, tLoginConn, cr, secondCreds.User); err != nil {
			s.Fatal("Failed to remove user: ", err)
		}
	}

	// Relogin to second user.
	{
		// Login options for the second user for relogin.
		optsReLoginUser2 := []chrome.Option{
			chrome.KeepState(),
			chrome.FakeLogin(secondCreds),
			chrome.ExtraArgs("--vmodule=crostini*=1"),
		}
		cr, err := chrome.New(ctx, optsReLoginUser2...)
		if err != nil {
			s.Fatal("Failed to login again: ", err)
		}
		defer cr.Close(cleanUpCtx)
	}

}

func loginChromeAndGetTConn(ctx context.Context, opts ...chrome.Option) (cr *chrome.Chrome, tconn *chrome.TestConn, err error) {
	if cr, err = chrome.New(ctx, opts...); err != nil {
		return nil, nil, errors.Wrap(err, "failed to login Chrome")
	}
	if tconn, err = cr.TestAPIConn(ctx); err != nil {
		return nil, nil, errors.Wrap(err, "failed to get test API connection")
	}

	return cr, tconn, nil
}

func cleanupVM(ctx context.Context, opts ...chrome.Option) error {
	// Login.
	cr, _, err := loginChromeAndGetTConn(ctx, opts...)
	if err != nil {
		return errors.Wrap(err, "failed to login Chrome")
	}

	// Get the container.
	_, err = vm.GetRunningContainer(ctx, cr.NormalizedUser())
	if err != nil {
		testing.ContextLogf(ctx, "Failed to connect to the container, it might not exist: %s", err)
	} else {
		if err := vm.StopConcierge(ctx); err != nil {
			testing.ContextLogf(ctx, "Failure stopping concierge: %s", err)
		}
	}

	// Unmount the component.
	vm.UnmountComponent(ctx)
	if err := vm.DeleteImages(); err != nil {
		testing.ContextLogf(ctx, "Error deleting images: %q", err)
	}

	if cr != nil {
		if err := cr.Close(ctx); err != nil {
			return errors.Wrap(err, "failure closing chrome")
		}
	}

	return nil
}
