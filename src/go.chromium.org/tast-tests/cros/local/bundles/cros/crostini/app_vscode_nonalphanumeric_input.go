// Copyright 2023 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package crostini

import (
	"context"
	"time"

	"go.chromium.org/tast-tests/cros/local/bundles/cros/crostini/imetestutil"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto/faillog"
	"go.chromium.org/tast-tests/cros/local/crostini"
	"go.chromium.org/tast-tests/cros/local/guestos/apps"
	"go.chromium.org/tast-tests/cros/local/screenshot"
	"go.chromium.org/tast-tests/cros/local/terminalapp"
	"go.chromium.org/tast-tests/cros/local/uidetection"
	"go.chromium.org/tast/core/ctxutil"
	"go.chromium.org/tast/core/testing"
)

func init() {
	testing.AddTest(&testing.Test{
		Func:         AppVSCodeNonalphanumericInput,
		LacrosStatus: testing.LacrosVariantUnneeded,
		Desc:         "Verify non-alphanumeric keys (tab, ctrl+c, ctrl+v, arrows, backspace and enter) work in VSCode",
		Contacts:     []string{"clumptini+oncall@google.com"},
		Attr:         []string{"group:mainline", "group:crostini_app_cq"},
		Vars:         screenshot.ScreenDiffVars,
		SoftwareDeps: []string{"chrome", "vm_host"},
		VarDeps:      uidetection.UIDetectionVars,
		BugComponent: "b:1122570",
		Params: []testing.Param{
			// Parameters generated by params_test.go. DO NOT EDIT.
			{
				Name:              "bookworm_clamshell_stable",
				ExtraSoftwareDeps: []string{"crostini_app", "dlc"},
				ExtraHardwareDeps: crostini.CrostiniAppStable,
				Fixture:           "crostiniBookwormLargeContainerClamshell",
				Timeout:           15 * time.Minute,
			},
		},
	})
}
func AppVSCodeNonalphanumericInput(ctx context.Context, s *testing.State) {
	tconn := s.FixtValue().(crostini.FixtureData).Tconn
	keyboard := s.FixtValue().(crostini.FixtureData).KB
	cont := s.FixtValue().(crostini.FixtureData).Cont

	// Use a shortened context for test operations to reserve time for cleanup.
	cleanupCtx := ctx
	ctx, cancel := ctxutil.Shorten(ctx, 5*time.Second)
	defer cancel()

	// Open Terminal app.
	terminalApp, err := terminalapp.Launch(ctx, tconn)
	if err != nil {
		s.Fatal("Failed to open Terminal app: ", err)
	}
	defer terminalApp.Exit(keyboard)(cleanupCtx)

	// Since defers are executed in a stack, this needs to be the last defer so it doesn't close the window before dumping the tree.
	handler := faillog.DumpUITreeWithScreenshotHandler(cleanupCtx, tconn, "ui_tree")
	s.AttachErrorHandlers(handler, handler)

	ui := uiauto.New(tconn)
	uda := uidetection.NewDefault(tconn)

	if err := apps.InitialiseVSCode(ctx, apps.VSCode, cont, keyboard, tconn); err != nil {
		s.Fatal("Failed to open VSCode app from terminal and initialise settings: ", err)
	}

	// Open the VSCode again, this time, it won't open the Get Started tab.
	if err := apps.LaunchVSCodeForFile(apps.VSCode, uda, ui, terminalApp, keyboard, apps.VSCodeTestFile)(ctx); err != nil {
		s.Fatal("Failed to re-open VSCode for entering test input: ", err)
	}

	// Test basic string editing using non-alphanumeric keys.
	if err := imetestutil.TestInputActionsInEditor(ctx, keyboard, tconn); err != nil {
		s.Fatal("Failed to type string and do basic editing: ", err)
	}

	if err := apps.SaveFileAndCloseVSCode(apps.VSCode, ui, keyboard, apps.VSCodeTestFile)(ctx); err != nil {
		s.Fatal("Failed to save and close VSCode: ", err)
	}
}
