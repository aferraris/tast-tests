// Copyright 2023 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package crostini

import (
	"context"
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"time"

	"golang.org/x/exp/slices"

	"go.chromium.org/tast-tests/cros/common/action"
	"go.chromium.org/tast-tests/cros/common/testexec"
	"go.chromium.org/tast-tests/cros/common/usbdevice"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto/faillog"
	"go.chromium.org/tast-tests/cros/local/crostini"
	"go.chromium.org/tast-tests/cros/local/crostini/ui/settings"
	"go.chromium.org/tast-tests/cros/local/vm"
	"go.chromium.org/tast/core/ctxutil"
	"go.chromium.org/tast/core/errors"
	"go.chromium.org/tast/core/testing"
)

func init() {
	testing.AddTest(&testing.Test{
		Func:         USBShareMassStorage,
		LacrosStatus: testing.LacrosVariantExists,
		Desc:         "Share and unshare a virtual usb mass storage device with Crostini, verify read/write to usb mass storage inside Crostini",
		Contacts:     []string{"clumptini+oncall@google.com"},
		// TODO(b/317944073): Re-enable the test after this bug is fixed.
		// USB mass-storage sharing has not been working correctly since before this test is introduced.
		Attr:         []string{},
		SoftwareDeps: []string{"chrome", "vm_host"},
		BugComponent: "b:1122570",
		Params: []testing.Param{
			// Parameters generated by params_test.go. DO NOT EDIT.
			{
				Name:              "bookworm_stable",
				ExtraSoftwareDeps: []string{"dlc"},
				ExtraHardwareDeps: crostini.CrostiniOptimalPerf,
				Fixture:           "crostiniBookworm",
				Timeout:           7 * time.Minute,
			},
		},
	})
}

func USBShareMassStorage(ctx context.Context, s *testing.State) {
	const (
		outsideFile         = "outside.txt"
		outsideFileContents = "outside"
		insideFile          = "inside.txt"
		insideFileContents  = "inside"
		deviceName          = "Mass Storage Gadget"
		deviceLabel         = "virtual-usb"
		containerHomeDir    = "/home/testuser"
		containerMountDir   = containerHomeDir + "/usb-drive"
		mountDir            = "/media/removable/" + deviceLabel
	)
	cr := s.FixtValue().(crostini.FixtureData).Chrome
	cont := s.FixtValue().(crostini.FixtureData).Cont
	tconn := s.FixtValue().(crostini.FixtureData).Tconn
	deviceID := fmt.Sprintf("%04x:%04x", usbdevice.VendorIDNetchip, usbdevice.ProductIDNetchipMassStorage)

	cleanupCtx := ctx
	ctx, cancel := ctxutil.Shorten(ctx, 5*time.Second)
	defer cancel()

	handler := faillog.DumpUITreeWithScreenshotHandler(cleanupCtx, tconn, "ui_tree")
	s.AttachErrorHandlers(handler, handler)

	_, cleanup, err := setupMassStorage(ctx, deviceLabel)
	if err != nil {
		s.Fatal("Unable to setup the mass storage device: ", err)
	}
	defer cleanup(cleanupCtx)

	if err := waitUntilMountedOutside(ctx, mountDir); err != nil {
		s.Fatal("Timed out waiting for ChromeOS to mount the mass storage device: ", err)
	}

	outsideFilePath := filepath.Join(mountDir, outsideFile)
	if err := os.WriteFile(outsideFilePath, []byte(outsideFileContents), 0644); err != nil {
		s.Fatalf("Failed to create file %q: %s", outsideFilePath, err)
	}
	defer os.Remove(outsideFilePath)

	settings, err := settings.OpenLinuxSettings(ctx, tconn, cr, settings.ManageUSBDevices)
	if err != nil {
		s.Fatal("Failed to find Manage USB devices: ", err)
	}
	defer settings.Close(ctx)
	devices, err := settings.GetUSBDevices(ctx)
	if err != nil {
		s.Fatal("Unable to get devices: ", err)
	}
	if !slices.Contains(devices, deviceName) {
		s.Fatalf("Device %q is not found on the list: %v", deviceName, devices)
	}

	// Assert can share the USB device.
	if err := settings.ShareUSBDevice(ctx, deviceName); err != nil {
		s.Fatal("Unable to share a mass storage device: ", err)
	}
	if err := assertDeviceSharedState(ctx, cont, deviceID, true); err != nil {
		s.Fatal("Mass storage device is not present in the container: ", err)
	}

	unmountInside, err := mountInside(ctx, cont, deviceLabel, containerMountDir)
	if err != nil {
		s.Fatal("Failed to mount mass storage device in the container: ", err)
	}
	defer unmountInside(cleanupCtx)

	// Assert can read the outside file from the usb storage inside of the container.
	if err := cont.CheckFileContent(ctx, filepath.Join(containerMountDir, outsideFile), outsideFileContents); err != nil {
		s.Fatal("The usb storage file contents inside of the container doesn't match: ", err)
	}

	// Assert can write a file to the usb storage inside of the container.
	if err := cont.WriteFile(ctx, filepath.Join(containerMountDir, insideFile), insideFileContents); err != nil {
		s.Fatal("Failed to create a new file inside of the container: ", err)
	}

	if err := unmountInside(ctx); err != nil {
		s.Fatal("Unable to unmount the mass storage device inside of the container: ", err)
	}

	// Assert can unshare the USB device.
	if err := settings.UnshareUSBDevice(ctx, deviceName); err != nil {
		s.Fatal("Unable to unshare a mass storage device: ", err)
	}
	if err := assertDeviceSharedState(ctx, cont, deviceID, false); err != nil {
		s.Fatal("Mass storage device is still present in the container: ", err)
	}

	if err := waitUntilMountedOutside(ctx, mountDir); err != nil {
		s.Fatal("Timed out waiting for ChromeOS to mount the mass storage device: ", err)
	}

	// Assert can read a file written to the usb storage inside of the container after unsharing.
	insideFilePath := filepath.Join(mountDir, insideFile)
	if out, err := os.ReadFile(insideFilePath); err != nil {
		s.Fatalf("Failed to read file %q: %v", insideFilePath, err)
	} else if string(out) != insideFileContents {
		s.Fatalf("The usb storage file contents is unexpected, want: %q, got %q", insideFileContents, string(out))
	}
}

func waitUntilMountedOutside(ctx context.Context, mountDir string) error {
	if err := testing.Poll(ctx, func(ctx context.Context) error {
		if _, stderr, err := testexec.CommandContext(ctx, "mountpoint", mountDir).SeparatedOutput(); err != nil {
			return errors.Wrapf(err, "failed to find a mountpoint: %q", string(stderr))
		}
		return nil
	}, &testing.PollOptions{Timeout: 15 * time.Second, Interval: time.Second}); err != nil {
		return errors.Wrapf(err, "timed out waiting for a mountpoint %q", mountDir)
	}
	return nil
}

func mountInside(ctx context.Context, cont *vm.Container, label, mountDir string) (action.Action, error) {
	// When switching between ChromeOS and Crostini, the device may not be available for some time. Therefore need to poll.
	var device string
	if err := testing.Poll(ctx, func(ctx context.Context) error {
		out, err := cont.Command(ctx, "sudo", "blkid", "--label", label).Output()
		if err != nil {
			return errors.Wrapf(err, "failed to get device path by label: %q", string(out))
		}
		if string(out) == "" {
			return errors.Wrapf(err, "no device found for label %q", label)
		}
		device = string(out)
		return nil
	}, &testing.PollOptions{Timeout: 15 * time.Second, Interval: time.Second}); err != nil {
		return nil, errors.Wrapf(err, "failed to identify the device for label %q", label)
	}
	if err := cont.Command(ctx, "mkdir", "-p", mountDir).Run(testexec.DumpLogOnError); err != nil {
		return nil, errors.Wrap(err, "failed to create a mount dir")
	}
	if err := cont.Command(ctx, "fuse2fs", device, mountDir, "-o", "fakeroot").Run(testexec.DumpLogOnError); err != nil {
		return nil, errors.Wrap(err, "failed to mount mass storage device inside of the container")
	}
	// Mounting could take some time. Need to verify it is done before proceeding.
	if err := testing.Poll(ctx, func(ctx context.Context) error {
		out, err := cont.Command(ctx, "mount").Output()
		if err != nil {
			return errors.Wrapf(err, "failed to list mount points: %q", string(out))
		}
		if !strings.Contains(string(out), mountDir) {
			return errors.Wrapf(err, "%s is not mounted on %s yet", device, mountDir)
		}
		return nil
	}, &testing.PollOptions{Timeout: 15 * time.Second, Interval: time.Second}); err != nil {
		return nil, errors.Wrapf(err, "%s is not mounted on %s", device, mountDir)
	}
	isUnmounted := false
	cleanup := func(ctx context.Context) error {
		if isUnmounted {
			return nil
		}
		if err := cont.Command(ctx, "fusermount", "-u", mountDir).Run(testexec.DumpLogOnError); err != nil {
			return errors.Wrap(err, "failed to unmount the mass storage device inside of the container")
		}
		if err := cont.RemoveAll(ctx, mountDir); err != nil {
			return errors.Wrap(err, "failed to remove mount dir inside of the container")
		}
		isUnmounted = true
		return nil
	}
	return cleanup, nil
}

func setupMassStorage(ctx context.Context, label string) (string, action.Action, error) {
	usbMassStorage := usbdevice.NewUSBMassStorage()
	if err := usbMassStorage.Init(ctx, 16 /*sizeInMb*/); err != nil {
		return "", nil, errors.Wrap(err, "failed to initialise the mass storage device")
	}
	if err := usbMassStorage.PlugIn(ctx, false /*readOnly*/); err != nil {
		return "", nil, errors.Wrap(err, "failed to plug in the mass storage device")
	}
	if err := usbMassStorage.FormatFileSystem(ctx, "mkfs.ext4", "-L", label); err != nil {
		return "", nil, errors.Wrap(err, "failed to format the mass storage device")
	}
	mountPoint := usbMassStorage.DevicePath()
	return mountPoint, usbMassStorage.CleanUp, nil
}

func assertDeviceSharedState(ctx context.Context, cont *vm.Container, deviceID string, expectedSharedState bool) error {
	// When switching between ChromeOS and Crostini, the device may not be available for some time. Therefore need to poll.
	if err := testing.Poll(ctx, func(ctx context.Context) error {
		out, err := cont.Command(ctx, "lsusb").Output()
		if err != nil {
			errors.Wrapf(err, "failed to run lsusb in the container: %q", string(out))
		}
		if strings.Contains(string(out), deviceID) != expectedSharedState {
			errors.Errorf("device %q shared state in lsusb output does not match the expectation, want %t, got %t, lsusb output: %q", deviceID, expectedSharedState, !expectedSharedState, string(out))
		}
		return nil
	}, &testing.PollOptions{Timeout: 15 * time.Second, Interval: time.Second}); err != nil {
		return errors.Wrapf(err, "failed to assert device shared state is %t", expectedSharedState)
	}
	return nil
}
