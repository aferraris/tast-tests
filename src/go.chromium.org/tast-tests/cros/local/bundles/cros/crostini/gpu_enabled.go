// Copyright 2019 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package crostini

import (
	"context"
	"strings"
	"time"

	"go.chromium.org/tast-tests/cros/common/testexec"
	"go.chromium.org/tast-tests/cros/local/crostini"
	"go.chromium.org/tast/core/shutil"
	"go.chromium.org/tast/core/testing"
)

func init() {
	testing.AddTest(&testing.Test{
		Func:         GPUEnabled,
		LacrosStatus: testing.LacrosVariantUnneeded,
		Desc:         "Tests that Crostini starts with the correct GPU device depending on whether the GPU flag is set or not",
		Contacts:     []string{"clumptini+oncall@google.com"},
		Attr:         []string{"group:mainline"},
		SoftwareDeps: []string{"chrome", "vm_host", "crosvm_gpu"},
		BugComponent: "b:1122570",
		Params: []testing.Param{
			// TODO(b/304170307): Mali-G57 crashes on bookworm.
			{
				Name:              "sw_bullseye_stable",
				ExtraSoftwareDeps: []string{"crosvm_no_gpu", "dlc"},
				ExtraHardwareDeps: crostini.CrostiniOptimalPerf,
				Fixture:           "crostiniBullseye",
				Timeout:           7 * time.Minute,
				Val:               "llvmpipe",
			}, {
				Name:              "sw_bookworm_stable",
				ExtraAttr:         []string{"informational"},
				ExtraSoftwareDeps: []string{"crosvm_no_gpu", "dlc"},
				ExtraHardwareDeps: crostini.CrostiniOptimalPerf,
				Fixture:           "crostiniBookworm",
				Timeout:           7 * time.Minute,
				Val:               "llvmpipe",
			}, {
				Name:              "gpu_bullseye_stable",
				ExtraSoftwareDeps: []string{"crosvm_gpu", "dlc"},
				ExtraHardwareDeps: crostini.CrostiniOptimalPerf,
				Fixture:           "crostiniBullseye",
				Timeout:           7 * time.Minute,
				Val:               "virgl",
			}, {
				Name:              "gpu_bookworm_stable",
				ExtraAttr:         []string{"informational"},
				ExtraSoftwareDeps: []string{"crosvm_gpu", "dlc"},
				ExtraHardwareDeps: crostini.CrostiniOptimalPerf,
				Fixture:           "crostiniBookworm",
				Timeout:           7 * time.Minute,
				Val:               "virgl",
			},
		},
	})
}

func GPUEnabled(ctx context.Context, s *testing.State) {
	cont := s.FixtValue().(crostini.FixtureData).Cont
	expectedDevice := s.Param().(string)

	cmd := cont.Command(ctx, "sh", "-c", "glxinfo -B | grep Device:")
	if out, err := cmd.Output(testexec.DumpLogOnError); err != nil {
		s.Fatalf("Failed to run %q: %v", shutil.EscapeSlice(cmd.Args), err)
	} else {
		output := string(out)
		if !strings.Contains(output, expectedDevice) {
			s.Fatalf("Failed to verify GPU device: got %q; want %q", output, expectedDevice)
		}
		s.Logf("GPU is %q", output)
	}
}
