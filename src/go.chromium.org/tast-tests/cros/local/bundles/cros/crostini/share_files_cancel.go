// Copyright 2020 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package crostini

import (
	"context"
	"time"

	"go.chromium.org/tast-tests/cros/local/chrome/uiauto"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto/faillog"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto/filesapp"
	"go.chromium.org/tast-tests/cros/local/crostini"
	"go.chromium.org/tast-tests/cros/local/crostini/ui/sharedfolders"
	"go.chromium.org/tast/core/ctxutil"
	"go.chromium.org/tast/core/testing"
)

func init() {
	testing.AddTest(&testing.Test{
		Func:         ShareFilesCancel,
		LacrosStatus: testing.LacrosVariantUnneeded,
		Desc:         "Test clicking Cancel on share confirm dialog",
		Contacts:     []string{"clumptini+oncall@google.com"},
		Attr:         []string{"group:mainline"},
		SoftwareDeps: []string{"chrome", "vm_host"},
		BugComponent: "b:1122570",
		Params: []testing.Param{
			// Parameters generated by params_test.go. DO NOT EDIT.
			{
				Name:              "bookworm_stable",
				ExtraSoftwareDeps: []string{"dlc"},
				ExtraHardwareDeps: crostini.CrostiniOptimalPerf,
				Fixture:           "crostiniBookworm",
				Timeout:           7 * time.Minute,
			},
		},
	})
}
func ShareFilesCancel(ctx context.Context, s *testing.State) {
	tconn := s.FixtValue().(crostini.FixtureData).Tconn
	cont := s.FixtValue().(crostini.FixtureData).Cont
	cr := s.FixtValue().(crostini.FixtureData).Chrome

	// Use a shortened context for test operations to reserve time for cleanup.
	cleanupCtx := ctx
	ctx, cancel := ctxutil.Shorten(ctx, 5*time.Second)
	defer cancel()

	handler := faillog.DumpUITreeWithScreenshotHandler(cleanupCtx, tconn, "ui_tree")
	s.AttachErrorHandlers(handler, handler)

	// Open Files app.
	filesApp, err := filesapp.Launch(ctx, tconn)
	if err != nil {
		s.Fatal("Failed to open Files app: ", err)
	}

	sharedFolders := sharedfolders.NewSharedFolders(tconn)

	// Clean up shared folders in the end.
	defer func() {
		if err := sharedFolders.UnshareAll(cont, cr)(cleanupCtx); err != nil {
			s.Error("Failed to unshare all folders: ", err)
		}
	}()

	ui := uiauto.New(tconn)
	// Share My files.
	if err := uiauto.Combine("click Share with Linux on My files and click button Cancel on the confirmation dialog",
		sharedFolders.ShareMyFiles(ctx, filesApp),
		ui.LeftClick(sharedfolders.ShareConfirmDialog.CancelButton),
		ui.WithTimeout(5*time.Second).WaitUntilGone(sharedfolders.ShareToastNotification.Toast),
		sharedFolders.CheckNoSharedFolders(cont, cr))(ctx); err != nil {
		s.Fatal("Failed to test cancel share My files: ", err)
	}
}
