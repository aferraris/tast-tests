// Copyright 2020 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package crostini

import (
	"context"
	"time"

	"go.chromium.org/tast-tests/cros/local/chrome/uiauto"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto/faillog"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto/filesapp"
	"go.chromium.org/tast-tests/cros/local/crostini"
	"go.chromium.org/tast/core/ctxutil"
	"go.chromium.org/tast/core/testing"
)

func init() {
	testing.AddTest(&testing.Test{
		Func:         FilesAppWatch,
		LacrosStatus: testing.LacrosVariantUnneeded,
		Desc:         "Checks crostini FilesApp watch",
		Contacts:     []string{"clumptini+oncall@google.com"},
		Attr:         []string{"group:mainline"},
		Vars:         []string{"keepState"},
		SoftwareDeps: []string{"chrome", "vm_host"},
		BugComponent: "b:1122570",
		Params: []testing.Param{
			// Parameters generated by params_test.go. DO NOT EDIT.
			{
				Name:              "bookworm_stable",
				ExtraSoftwareDeps: []string{"dlc"},
				ExtraHardwareDeps: crostini.CrostiniOptimalPerf,
				Fixture:           "crostiniBookworm",
				Timeout:           7 * time.Minute,
			},
		},
	})
}

func FilesAppWatch(ctx context.Context, s *testing.State) {
	pre := s.FixtValue().(crostini.FixtureData)
	tconn := pre.Tconn
	cont := pre.Cont

	const (
		testFileName1   = "FilesAppWatch1.txt"
		testFileName2   = "FilesAppWatch2.txt"
		testFileContent = "FilesAppWatch"
	)

	cleanupCtx := ctx
	ctx, cancel := ctxutil.Shorten(ctx, 5*time.Second)
	defer cancel()

	cleanupFile := func(ctx context.Context, filename string) {
		if err := cont.RemoveAll(ctx, filename); err != nil {
			s.Logf("Failed to remove file %s: %s", filename, err)
		}
	}

	if err := cont.WriteFile(ctx, testFileName1, testFileContent); err != nil {
		s.Fatal("Create file failed: ", err)
	}
	defer cleanupFile(cleanupCtx, testFileName1)

	// Launch the files application
	files, err := filesapp.Launch(ctx, tconn)
	if err != nil {
		s.Fatal("Launching the Files App failed: ", err)
	}
	defer files.Close(cleanupCtx)

	handler := faillog.DumpUITreeWithScreenshotHandler(cleanupCtx, tconn, "ui_tree")
	s.AttachErrorHandlers(handler, handler)
	// Validate file1.txt is shown in 'Linux files'.
	if err := uiauto.Combine("find file1.txt",
		files.OpenDir("Linux files", "Files - Linux files"),
		files.WithTimeout(10*time.Second).WaitForFile(testFileName1))(ctx); err != nil {
		s.Fatal("Failed to find file1.txt created in the container in Linux files: ", err)
	}
	// Create file2.txt in container and check that FilesApp refreshes.
	if err := cont.WriteFile(ctx, testFileName2, testFileContent); err != nil {
		s.Fatal("Create file failed: ", err)
	}
	defer cleanupFile(cleanupCtx, testFileName2)
	if err := files.WithTimeout(10 * time.Second).WaitForFile(testFileName2)(ctx); err != nil {
		s.Fatal("Waiting for file2.txt failed: ", err)
	}
	// Select back to Downloads to remove the linux watcher.
	if err := files.OpenDownloads()(ctx); err != nil {
		s.Fatal("Failed to open Downloads to remove Linux watcher: ", err)
	}
}
