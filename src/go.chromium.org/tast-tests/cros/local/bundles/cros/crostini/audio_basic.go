// Copyright 2019 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package crostini

import (
	"context"
	"regexp"
	"strings"
	"time"

	"go.chromium.org/tast-tests/cros/common/testexec"
	"go.chromium.org/tast-tests/cros/local/crostini"
	"go.chromium.org/tast/core/testing"
)

func init() {
	testing.AddTest(&testing.Test{
		Func:         AudioBasic,
		LacrosStatus: testing.LacrosVariantUnneeded,
		Desc:         "Runs a basic test on the container's audio using a pre-built crostini image",
		Contacts:     []string{"chromeos-audio-bugs@google.com", "clumptini+oncall@google.com", "paulhsia@chromium.org"},
		Attr:         []string{"group:mainline", "group:audio", "group:sw_gates_virt", "sw_gates_virt_enabled"},
		SoftwareDeps: []string{"chrome", "vm_host"},
		BugComponent: "b:776546",
		Params: []testing.Param{
			// Parameters generated by params_test.go. DO NOT EDIT.
			{
				Name:              "bullseye_stable",
				ExtraSoftwareDeps: []string{"dlc"},
				ExtraHardwareDeps: crostini.CrostiniOptimalPerf,
				Fixture:           "crostiniBullseye",
				Timeout:           7 * time.Minute,
			}, {
				Name:              "bullseye_lowperf",
				ExtraSoftwareDeps: []string{"dlc"},
				ExtraHardwareDeps: crostini.CrostiniLowPerf,
				Fixture:           "crostiniBullseyeWithoutArc",
				Timeout:           7 * time.Minute,
			}, {
				Name:              "bookworm_stable",
				ExtraSoftwareDeps: []string{"dlc"},
				ExtraHardwareDeps: crostini.CrostiniOptimalPerf,
				Fixture:           "crostiniBookworm",
				Timeout:           7 * time.Minute,
			}, {
				Name:              "bookworm_lowperf",
				ExtraSoftwareDeps: []string{"dlc"},
				ExtraHardwareDeps: crostini.CrostiniLowPerf,
				Fixture:           "crostiniBookwormWithoutArc",
				Timeout:           7 * time.Minute,
			},
		},
	})
}

func AudioBasic(ctx context.Context, s *testing.State) {
	cont := s.FixtValue().(crostini.FixtureData).Cont

	s.Log("List ALSA output devices")
	if err := cont.Command(ctx, "aplay", "-l").Run(testexec.DumpLogOnError); err != nil {
		s.Fatal("Failed to list ALSA output devices: ", err)
	}

	s.Log("Play zeros with ALSA API")
	if err := cont.Command(
		ctx, "aplay", "-r", "48000", "-c", "2", "-d", "3", "-f", "dat", "/dev/zero",
	).Run(testexec.DumpLogOnError); err != nil {
		s.Fatal("Failed to playback with ALSA API: ", err)
	}

	// isSIGINT checks if the err is triggered by SIGINT.
	isSIGINT := func(err error) bool {
		return strings.Contains(err.Error(), "exit status 124")
	}

	s.Log("Play zeros with PulseAudio API")
	if err := cont.Command(
		ctx, "timeout", "-s", "SIGINT", "3s", "paplay", "--raw", "--rate=48000",
		"--channels=2", "/dev/zero",
	).Run(testexec.DumpLogOnError); err != nil {
		// SIGINT error is expected after timeout -s SIGINT.
		if !isSIGINT(err) {
			s.Fatal("Failed to playback with PulseAudio API: ", err)
		}
	}

	pipeWireSinksPattern := regexp.MustCompile(
		"[0-9]+\talsa-sink\tPipeWire\ts16le 2ch 48000Hz\t(IDLE|SUSPENDED)\n")
	pulseAudioSinksPattern := regexp.MustCompile(
		"1\talsa_output.hw_0_0\tmodule-alsa-sink.c\ts16le 2ch (44100|48000)Hz\t(IDLE|SUSPENDED)\n")

	if out, err := cont.Command(
		ctx, "pactl", "list", "sinks", "short",
	).Output(testexec.DumpLogOnError); err != nil {
		s.Fatal("Failed to list audio sinks using pactl: ", err)
	} else if res := (pulseAudioSinksPattern.Match(out) || pipeWireSinksPattern.Match(out)); !res {
		s.Fatal("Failed to load audio sinks to PulseAudio or PipeWire:", string(out))
	}

	s.Log("List ALSA input devices")
	if err := cont.Command(ctx, "arecord", "-l").Run(testexec.DumpLogOnError); err != nil {
		s.Fatal("Failed to list ALSA input devices: ", err)
	}

	s.Log("Capture with ALSA API")
	if err := cont.Command(
		ctx, "arecord", "-r", "48000", "-c", "2", "-d", "3", "-f", "dat", "/dev/null",
	).Run(testexec.DumpLogOnError); err != nil {
		s.Fatal("Failed to capture with ALSA API: ", err)
	}

	s.Log("Capture with PulseAudio API")
	if err := cont.Command(
		ctx, "timeout", "-s", "SIGINT", "3s", "paplay", "--raw", "--rate=48000",
		"--channels=2", "/dev/null",
	).Run(testexec.DumpLogOnError); err != nil {
		if !isSIGINT(err) {
			s.Fatal("Failed to capture with PulseAudio API: ", err)
		}
	}

	pipeWireSourcesPattern := regexp.MustCompile(
		"[0-9]+\talsa-source\tPipeWire\ts16le 2ch 48000Hz\t(IDLE|SUSPENDED)\n")
	pulseAudioSourcesPattern := regexp.MustCompile(
		"[0-9]+\talsa_input.hw_0_0\tmodule-alsa-source.c\ts16le 2ch (44100|48000)Hz\t(IDLE|SUSPENDED)\n")

	if out, err := cont.Command(
		ctx, "pactl", "list", "sources", "short",
	).Output(testexec.DumpLogOnError); err != nil {
		s.Fatal("Failed to list audio sources using pactl: ", err)
	} else if res := (pulseAudioSourcesPattern.Match(out) || pipeWireSourcesPattern.Match(out)); !res {
		s.Fatal("Failed to load audio sources to PulseAudio or PipeWire:", string(out))
	}
}
