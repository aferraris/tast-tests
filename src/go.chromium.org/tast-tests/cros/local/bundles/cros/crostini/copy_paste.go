// Copyright 2023 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package crostini

import (
	"context"
	"time"

	"go.chromium.org/tast-tests/cros/local/chrome/uiauto/faillog"
	"go.chromium.org/tast-tests/cros/local/crostini"
	"go.chromium.org/tast-tests/cros/local/guestos"
	"go.chromium.org/tast/core/ctxutil"
	"go.chromium.org/tast/core/testing"
)

func init() {

	testing.AddTest(&testing.Test{
		Func:         CopyPaste,
		LacrosStatus: testing.LacrosVariantUnneeded,
		Desc:         "Test copy paste functionality",
		Contacts:     []string{"clumptini+oncall@google.com"},
		Attr:         []string{"group:mainline"},
		Data:         []string{guestos.CopyApplet, guestos.PasteApplet},
		SoftwareDeps: []string{"chrome", "vm_host"},
		BugComponent: "b:1122570",
		Params: []testing.Param{
			// Parameters generated by copy_paste_test.go. DO NOT EDIT.
			{
				Name:              "wayland_to_wayland_bullseye_stable",
				ExtraSoftwareDeps: []string{"dlc"},
				ExtraHardwareDeps: crostini.CrostiniOptimalPerf,
				Fixture:           "crostiniBullseye",
				Timeout:           7 * time.Minute,
				Val: guestos.CopyPasteConfig{
					Copy:  guestos.WaylandCopyConfig,
					Paste: guestos.WaylandPasteConfig,
				},
			}, {
				Name:              "wayland_to_wayland_bookworm_stable",
				ExtraSoftwareDeps: []string{"dlc"},
				ExtraHardwareDeps: crostini.CrostiniOptimalPerf,
				Fixture:           "crostiniBookworm",
				Timeout:           7 * time.Minute,
				Val: guestos.CopyPasteConfig{
					Copy:  guestos.WaylandCopyConfig,
					Paste: guestos.WaylandPasteConfig,
				},
			}, {
				Name:              "wayland_to_x11_bullseye_stable",
				ExtraSoftwareDeps: []string{"dlc"},
				ExtraHardwareDeps: crostini.CrostiniOptimalPerf,
				Fixture:           "crostiniBullseye",
				Timeout:           7 * time.Minute,
				Val: guestos.CopyPasteConfig{
					Copy:  guestos.WaylandCopyConfig,
					Paste: guestos.X11PasteConfig,
				},
			}, {
				Name:              "wayland_to_x11_bookworm_stable",
				ExtraSoftwareDeps: []string{"dlc"},
				ExtraHardwareDeps: crostini.CrostiniOptimalPerf,
				Fixture:           "crostiniBookworm",
				Timeout:           7 * time.Minute,
				Val: guestos.CopyPasteConfig{
					Copy:  guestos.WaylandCopyConfig,
					Paste: guestos.X11PasteConfig,
				},
			}, {
				Name:              "x11_to_wayland_bullseye_stable",
				ExtraSoftwareDeps: []string{"dlc"},
				ExtraHardwareDeps: crostini.CrostiniOptimalPerf,
				Fixture:           "crostiniBullseye",
				Timeout:           7 * time.Minute,
				Val: guestos.CopyPasteConfig{
					Copy:  guestos.X11CopyConfig,
					Paste: guestos.WaylandPasteConfig,
				},
			}, {
				Name:              "x11_to_wayland_bookworm_stable",
				ExtraSoftwareDeps: []string{"dlc"},
				ExtraHardwareDeps: crostini.CrostiniOptimalPerf,
				Fixture:           "crostiniBookworm",
				Timeout:           7 * time.Minute,
				Val: guestos.CopyPasteConfig{
					Copy:  guestos.X11CopyConfig,
					Paste: guestos.WaylandPasteConfig,
				},
			}, {
				Name:              "x11_to_x11_bullseye_stable",
				ExtraSoftwareDeps: []string{"dlc"},
				ExtraHardwareDeps: crostini.CrostiniOptimalPerf,
				Fixture:           "crostiniBullseye",
				Timeout:           7 * time.Minute,
				Val: guestos.CopyPasteConfig{
					Copy:  guestos.X11CopyConfig,
					Paste: guestos.X11PasteConfig,
				},
			}, {
				Name:              "x11_to_x11_bookworm_stable",
				ExtraSoftwareDeps: []string{"dlc"},
				ExtraHardwareDeps: crostini.CrostiniOptimalPerf,
				Fixture:           "crostiniBookworm",
				Timeout:           7 * time.Minute,
				Val: guestos.CopyPasteConfig{
					Copy:  guestos.X11CopyConfig,
					Paste: guestos.X11PasteConfig,
				},
			},
		},
	})
}

func CopyPaste(ctx context.Context, s *testing.State) {
	pre := s.FixtValue().(crostini.FixtureData)
	param := s.Param().(guestos.CopyPasteConfig)
	tconn := pre.Tconn
	cont := pre.Cont
	kb := pre.KB

	// Use a shortened context for test operations to reserve time for cleanup.
	cleanupCtx := ctx
	ctx, cancel := ctxutil.Shorten(ctx, 5*time.Second)
	defer cancel()

	handler := faillog.DumpUITreeWithScreenshotHandler(cleanupCtx, tconn, "ui_tree")
	s.AttachErrorHandlers(handler, handler)

	if err := guestos.CopyPaste(ctx, param, s.DataPath, tconn, kb, cont); err != nil {
		s.Fatal("Copy and paste test failed: ", err)
	}
}
