// Copyright 2023 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package crostini

import (
	"context"
	"time"

	"go.chromium.org/tast-tests/cros/local/bundles/cros/crostini/imetestutil"
	"go.chromium.org/tast-tests/cros/local/chrome"
	"go.chromium.org/tast-tests/cros/local/chrome/ime"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto/faillog"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto/nodewith"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto/role"
	"go.chromium.org/tast-tests/cros/local/crostini"
	"go.chromium.org/tast-tests/cros/local/input"
	"go.chromium.org/tast-tests/cros/local/terminalapp"
	"go.chromium.org/tast-tests/cros/local/uidetection"
	"go.chromium.org/tast-tests/cros/local/vm"
	"go.chromium.org/tast/core/ctxutil"
	"go.chromium.org/tast/core/errors"
	"go.chromium.org/tast/core/testing"
)

func init() {
	testing.AddTest(&testing.Test{
		Func:         AppGeditIME,
		LacrosStatus: testing.LacrosVariantUnneeded,
		Desc:         "Test IME inputs in Gedit App",
		Contacts: []string{
			"essential-inputs-gardener-oncall@google.com",
			"essential-inputs-team@google.com",
			"clumptini@google.com",
			"shend@google.com",
		},
		Attr:         []string{"group:mainline", "informational"},
		SoftwareDeps: []string{"chrome", "vm_host"},
		BugComponent: "b:95887", // ChromeOS > Software > Essential Inputs
		Params: []testing.Param{
			// Parameters generated by params_test.go. DO NOT EDIT.
			{
				Name:              "bookworm_clamshell_arabic_stable",
				ExtraSoftwareDeps: []string{"crostini_app", "dlc"},
				ExtraHardwareDeps: crostini.CrostiniAppStable,
				Fixture:           "crostiniBookwormLargeContainerClamshell",
				Timeout:           15 * time.Minute,
				Val:               "arabic",
			}, {
				Name:              "bookworm_clamshell_english_stable",
				ExtraSoftwareDeps: []string{"crostini_app", "dlc"},
				ExtraHardwareDeps: crostini.CrostiniAppStable,
				Fixture:           "crostiniBookwormLargeContainerClamshell",
				Timeout:           15 * time.Minute,
				Val:               "english",
			}, {
				Name:              "bookworm_clamshell_japanese_stable",
				ExtraSoftwareDeps: []string{"crostini_app", "dlc"},
				ExtraHardwareDeps: crostini.CrostiniAppStable,
				Fixture:           "crostiniBookwormLargeContainerClamshell",
				Timeout:           15 * time.Minute,
				Val:               "japanese",
			},
		},
	})
}

func AppGeditIME(ctx context.Context, s *testing.State) {
	tconn := s.FixtValue().(crostini.FixtureData).Tconn
	cont := s.FixtValue().(crostini.FixtureData).Cont
	keyboard := s.FixtValue().(crostini.FixtureData).KB

	// Reserve time for clean-up tasks.
	cleanupCtx := ctx
	ctx, cancel := ctxutil.Shorten(ctx, 15*time.Second)
	defer cancel()

	// Open Terminal app.
	terminalApp, err := terminalapp.Launch(ctx, tconn)
	if err != nil {
		s.Fatal("Failed to open Terminal app: ", err)
	}
	defer terminalApp.Exit(keyboard)(cleanupCtx)

	handler := faillog.DumpUITreeWithScreenshotHandler(cleanupCtx, tconn, "ui_tree")
	s.AttachErrorHandlers(handler, handler)

	// Switch back to default IME.
	defer imetestutil.ResetToDefaultIME(cleanupCtx, tconn)

	imeName := s.Param().(string)
	imeData := imetestutil.IMETestCases[imeName]
	if err := testUseIMEInGeditFile(ctx, terminalApp, keyboard, tconn, cont, imeData); err != nil {
		s.Fatal("Failed to use IME in Gedit file: ", err)
	}
}

func testUseIMEInGeditFile(ctx context.Context, terminalApp *terminalapp.TerminalApp, keyboard *input.KeyboardEventWriter, tconn *chrome.TestConn, cont *vm.Container, imeData imetestutil.IMETestData) error {
	const (
		testFile = "test.txt"
	)

	ui := uiauto.New(tconn)
	uda := uidetection.NewDefault(tconn)
	appWindow := nodewith.NameStartingWith(testFile).Role(role.Window).First()
	inputMethod := imeData.InputMethod
	expectedText := imeData.ExpectedText

	if err := uiauto.Combine("open file with Gedit via the terminal",
		// Launch Gedit.
		terminalApp.RunCommand(keyboard, "gedit "+testFile),
		// Sometimes the first character gets lost if input is entered immediately.
		// Wait until the menu exists, indicating the window is launched.
		uda.WaitUntilExists(uidetection.Word("Open").WithinA11yNode(appWindow)),
		ui.LeftClick(appWindow),
		inputMethod.InstallAndActivate(tconn),
		inputMethod.WaitUntilActivated(tconn),
		imeData.EnterTestStringActionPK(keyboard, ui),
	)(ctx); err != nil {
		return errors.Wrap(err, "failed to enter test string")
	}

	if imeData.InputMethod == ime.Japanese {
		if err := imetestutil.TestJapaneseCandidatesBoxInEditor(ctx, ui, uda, keyboard); err != nil {
			return err
		}
		expectedText += "\n" + imetestutil.JapaneseCandidatesBoxTestString
	}

	if err := uiauto.Combine("save file",
		// Press ctrl+S to save the file.
		keyboard.AccelAction("ctrl+S"),
		// Take screenshot.
		crostini.TakeAppScreenshot("gedit"),
		// Press ctrl+W twice to exit window.
		keyboard.AccelAction("ctrl+W"),
		keyboard.AccelAction("ctrl+W"),
		// Check window close.
		ui.WaitUntilGone(appWindow),
	)(ctx); err != nil {
		return err
	}
	// Check the content of the test file.
	if err := cont.CheckFileContent(ctx, testFile, expectedText+"\n"); err != nil {
		return errors.Wrap(err, "failed to verify the content of the test file")
	}

	return nil
}
