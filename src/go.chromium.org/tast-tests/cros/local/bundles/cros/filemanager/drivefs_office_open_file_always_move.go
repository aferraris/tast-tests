// Copyright 2023 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package filemanager

import (
	"context"
	"path/filepath"
	"time"

	"go.chromium.org/tast-tests/cros/local/chrome/uiauto/faillog"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto/filesapp"
	"go.chromium.org/tast-tests/cros/local/drivefs"
	"go.chromium.org/tast-tests/cros/local/filesconsts"
	"go.chromium.org/tast-tests/cros/local/onedrive"
	"go.chromium.org/tast/core/ctxutil"
	"go.chromium.org/tast/core/testing"
)

func init() {
	testing.AddTest(&testing.Test{
		Func:           DrivefsOfficeOpenFileAlwaysMove,
		LifeCycleStage: testing.LifeCycleOwnerMonitored,
		LacrosStatus:   testing.LacrosVariantExists,
		Desc:           "Verifies that docx, xlsx and pptx open in Google Drive and we can see 'always move' checkbox for 2nd time",
		BugComponent:   "b:1199143",
		Timeout:        5 * time.Minute,
		Contacts: []string{
			"chromeos-files-syd@google.com",
			"lucmult@chromium.org",
			"wenbojie@chromium.org",
		},
		SoftwareDeps: []string{
			"chrome",
			"chrome_internal",
			"drivefs",
			"gaia",
		},
		Attr: []string{
			"group:mainline",
			"group:hw_agnostic",
			"informational",
		},
		Params: []testing.Param{{
			Fixture: "onedriveAndGoogleDrive",
		}, {
			Name:              "lacros",
			Fixture:           "onedriveAndGoogleDriveLacros",
			ExtraSoftwareDeps: []string{"lacros"},
		}},
		SearchFlags: []*testing.StringPair{{
			Key:   "feature_id",
			Value: "screenplay-7f884cb5-976c-4e75-b2de-ea1306e4f3ee",
		}},
	})
}

// DrivefsOfficeOpenFileAlwaysMove tests user opening the 3 file types:
// 1. docx: First file, goes through the setup flow.
// 2. pptx: Opens and  checks the "Don't ask again" in the move file confirmation dialog.
// 3. xlsx: Opens without the confirmation dialog.
func DrivefsOfficeOpenFileAlwaysMove(ctx context.Context, s *testing.State) {
	data := s.FixtValue().(*onedrive.FixtureData)
	cr := data.Chrome
	tconn := data.TestAPIConn
	targetBaseName := filepath.Base(data.TargetFolder)
	driveFsClient := data.DriveFs

	for i, subTest := range data.GeneratedFiles {
		fileName := subTest.FileName
		fileType := subTest.FileType
		f := func(ctx context.Context, s *testing.State) {
			cleanupCtx := ctx
			ctx, cancel := ctxutil.Shorten(ctx, 5*time.Second)
			defer cancel()

			files, err := filesapp.Launch(ctx, tconn)
			if err != nil {
				s.Fatal("Failed to launch Files app: ", err)
			}
			defer files.Close(cleanupCtx)
			defer faillog.DumpUITreeWithScreenshotOnError(cleanupCtx, s.OutDir(), s.HasError, cr, "ui_"+fileType)
			defer driveFsClient.SaveLogsOnError(cleanupCtx, s.HasError)

			cloudUpload, err := files.OpenOfficeFile(ctx, targetBaseName, fileName, filesconsts.DriveFs)
			if err != nil {
				s.Fatal("Failed to open office file: ", err)
			}

			if err := cloudUpload.RunGoogleDriveSetupFlow()(ctx); err != nil {
				s.Fatal("Failed to run the setup dialog steps: ", err)
			}

			// Move/copy confirmation dialog.
			// 1st file: It doesn't show the "Don't ask again" option.
			// 2nd file: We want to check the "Don't ask again".
			// 3rd file: The dialog shouldn't show.
			if i < 2 {
				alwaysMove := false // 1st file.
				if i == 1 {
					alwaysMove = true // 2nd file.
				}

				if err := cloudUpload.WaitUploadConfirmationDialogAndClickToUpload(alwaysMove)(ctx); err != nil {
					s.Fatal("Failed confirming to upload to cloud: ", err)
				}
			}

			if err := drivefs.WaitForDSSWindowAndClose(tconn, fileName)(ctx); err != nil {
				s.Fatalf("Failed waiting file to open in Google Drive: %q: %v", fileName, err)
			}

			if err := drivefs.VerifySourceDestinationMD5SumMatch(driveFsClient, subTest.SrcFile, fileName)(ctx); err != nil {
				s.Fatal("Google Drive upload didn't match: ", err)
			}
		}

		if !s.Run(ctx, fileType, f) {
			s.Errorf("Failed to run test in %q", fileType)
		}
	}
}
