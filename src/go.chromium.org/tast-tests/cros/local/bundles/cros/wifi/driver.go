// Copyright 2019 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package wifi

import (
	"context"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"time"

	tdreq "go.chromium.org/tast-tests/cros/common/testdevicerequirements"
	"go.chromium.org/tast-tests/cros/local/shill"
	"go.chromium.org/tast-tests/cros/local/sysutil"
	"go.chromium.org/tast/core/lsbrelease"
	"go.chromium.org/tast/core/testing"
	"go.chromium.org/tast/core/testing/wlan"
)

func init() {
	testing.AddTest(&testing.Test{
		Func: Driver,
		Desc: "Ensure wireless devices have the expected associated kernel driver",
		Contacts: []string{
			"chromeos-wifi-champs@google.com", // WiFi oncall rotation
		},
		BugComponent: "b:893827", // ChromeOS > Platform > Connectivity > WiFi
		// Run on both Tast CQ and suite:wifi_matfunc.
		Attr:         []string{"group:mainline", "group:wificell", "wificell_func"},
		SoftwareDeps: []string{"wifi", "no_kernel_upstream"},
		// List of requirements this test satisfies.
		Requirements:    []string{tdreq.WiFiDrvSupportCrOS, tdreq.WiFiProcPassFW, tdreq.WiFiProcPassAVL, tdreq.WiFiProcPassAVLBeforeUpdates, tdreq.WiFiProcPassMatfunc, tdreq.WiFiProcPassMatfuncBeforeUpdates},
		VariantCategory: `{"name": "WifiBtChipset_Soc_Kernel"}`,
	})
}

// defaultRevision constifies key for default kernel version
const defaultRevision = "default"

// Reven's WiFi driver is upstream iwlwifi instead of specific
// drivers for other ChromiumOS boards. The path of the WiFi
// module should be separated from other ChromiumOS boards.
var expectedFlexWLANDriver = map[wlan.DeviceID]map[string]string{
	wlan.Intel8265: {
		defaultRevision: "wireless/intel/iwlwifi/iwlwifi.ko",
	},
	wlan.Intel9000: {
		defaultRevision: "wireless/intel/iwlwifi/iwlwifi.ko",
	},
	wlan.IntelAX201: {
		defaultRevision: "wireless/intel/iwlwifi/iwlwifi.ko",
	},
}

var expectedWLANDriver = map[wlan.DeviceID]map[string]string{
	wlan.Intel7260: {
		defaultRevision: "wireless/iwl7000/iwlwifi/iwlwifi.ko",
		"3.14":          "wireless-3.8/iwl7000/iwlwifi/iwlwifi.ko",
	},
	wlan.Intel7265: {
		defaultRevision: "wireless/iwl7000/iwlwifi/iwlwifi.ko",
		"3.14":          "wireless-3.8/iwl7000/iwlwifi/iwlwifi.ko",
	},
	wlan.Intel9000: {
		defaultRevision: "wireless/iwl7000/iwlwifi/iwlwifi.ko",
	},
	wlan.Intel9260: {
		defaultRevision: "wireless/iwl7000/iwlwifi/iwlwifi.ko",
	},
	wlan.Intel22260: {
		defaultRevision: "wireless/iwl7000/iwlwifi/iwlwifi.ko",
	},
	wlan.Intel22560: {
		defaultRevision: "wireless/iwl7000/iwlwifi/iwlwifi.ko",
	},
	wlan.IntelAX203: {
		defaultRevision: "wireless/iwl7000/iwlwifi/iwlwifi.ko",
	},
	wlan.IntelAX211: {
		defaultRevision: "wireless/iwl7000/iwlwifi/iwlwifi.ko",
	},
	wlan.IntelBE200: {
		defaultRevision: "wireless/iwl7000/iwlwifi/iwlwifi.ko",
	},
	wlan.QualcommAtherosQCA6174: {
		defaultRevision: "wireless/ath/ath10k/ath10k_pci.ko",
		"4.4":           "wireless/ar10k/ath/ath10k/ath10k_pci.ko",
	},
	wlan.QualcommAtherosQCA6174SDIO: {
		defaultRevision: "wireless/ath/ath10k/ath10k_sdio.ko",
	},
	wlan.QualcommWCN3990: {
		defaultRevision: "wireless/ath/ath10k/ath10k_snoc.ko",
	},
	wlan.QualcommWCN6750: {
		defaultRevision: "wireless/ath/ath11k/ath11k_ahb.ko",
	},
	wlan.QualcommWCN6855: {
		defaultRevision: "wireless/ath/ath11k/ath11k_pci.ko",
	},
	wlan.Marvell88w8897SDIO: {
		defaultRevision: "wireless/marvell/mwifiex/mwifiex_sdio.ko",
		"3.8":           "wireless/mwifiex/mwifiex_sdio.ko",
		"3.10":          "wireless-3.8/mwifiex/mwifiex_sdio.ko",
		"3.14":          "wireless-3.8/mwifiex/mwifiex_sdio.ko",
		"3.18":          "wireless/mwifiex/mwifiex_sdio.ko",
	},
	wlan.BroadcomBCM4354SDIO: {
		"3.8":  "wireless/brcm80211/brcmfmac/brcmfmac.ko",
		"3.14": "wireless-3.8/brcm80211/brcmfmac/brcmfmac.ko",
		"4.14": "wireless/broadcom/brcm80211/brcmfmac/brcmfmac.ko",
		"4.19": "wireless/broadcom/brcm80211/brcmfmac/brcmfmac.ko",
	},
	wlan.BroadcomBCM4356PCIE: {
		"3.10": "wireless-3.8/brcm80211/brcmfmac/brcmfmac.ko",
		"4.14": "wireless/broadcom/brcm80211/brcmfmac/brcmfmac.ko",
		"4.19": "wireless/broadcom/brcm80211/brcmfmac/brcmfmac.ko",
	},
	wlan.Marvell88w8997PCIE: {
		defaultRevision: "wireless/marvell/mwifiex/mwifiex_pcie.ko",
	},
	wlan.Realtek8822CPCIE: {
		defaultRevision: "wireless/realtek/rtw88/rtw88_8822ce.ko",
	},
	wlan.Realtek8852APCIE: {
		defaultRevision: "wireless/realtek/rtw89/rtw89_8852ae.ko",
	},
	wlan.Realtek8852CPCIE: {
		defaultRevision: "wireless/realtek/rtw89/rtw89_8852ce.ko",
	},
	wlan.MediaTekMT7921PCIE: {
		defaultRevision: "wireless/mediatek/mt76/mt7921/mt7921e.ko",
	},
	wlan.MediaTekMT7921SDIO: {
		defaultRevision: "wireless/mediatek/mt76/mt7921/mt7921s.ko",
	},
	wlan.MediaTekMT7922PCIE: {
		defaultRevision: "wireless/mediatek/mt76/mt7921/mt7921e.ko",
	},
}

func Driver(ctx context.Context, s *testing.State) {
	const (
		intelVendorNum   = "0x8086"
		support160MHz    = '0'
		supportOnly80MHz = '2'
		flexBoard        = "reven"
	)

	logBandwidthSupport := func(ctx context.Context, dev *wlan.DevInfo) {
		if dev.Vendor != intelVendorNum {
			return
		}
		if len(dev.Subsystem) < 4 {
			return
		}
		if dev.Subsystem[3] == support160MHz {
			testing.ContextLog(ctx, "Bandwidth Support: Supports 160 MHz Bandwidth")
		} else if dev.Subsystem[3] == supportOnly80MHz {
			testing.ContextLog(ctx, "Bandwidth Support: Supports only 80 MHz Bandwidth")
		} else {
			testing.ContextLog(ctx, "Bandwidth Support: Doesn't support (80 MHz , 160 MHz) Bandwidth")
		}
	}

	manager, err := shill.NewManager(ctx)
	if err != nil {
		s.Fatal("Failed creating shill manager proxy: ", err)
	}

	netIf, err := shill.WifiInterface(ctx, manager, 5*time.Second)
	if err != nil {
		s.Fatal(err, "Could not get a WiFi interface: ", err)
	}

	// TODO(oka): Original test skips if "wifi" is not initialized (USE="-wifi").
	// Consider if we should do it.
	// https://chromium-review.googlesource.com/c/chromiumos/third_party/autotest/+/890121
	devInfo, err := wlan.DeviceInfo()
	if err != nil {
		s.Fatal("Failed to get device name: ", err)
	}

	// If the device is Intel, check if it supports
	// 160 MHz / 80 MHz wide channels and log this information.
	logBandwidthSupport(ctx, devInfo)

	var wlanDriverList map[wlan.DeviceID]map[string]string

	lsb, err := lsbrelease.Load()
	if err != nil {
		s.Fatal("Failed to parses a text file in the /etc/lsb-release: ", err)
	}

	if board, ok := lsb[lsbrelease.Board]; !ok {
		s.Errorf("Failed to find %s in /etc/lsb-release", lsbrelease.Board)
	} else if strings.HasPrefix(board, flexBoard) {
		wlanDriverList = expectedFlexWLANDriver
	} else {
		wlanDriverList = expectedWLANDriver
	}

	if _, ok := wlanDriverList[devInfo.ID]; !ok {
		s.Fatal("Unexpected device ", devInfo.Name)
	}

	u, err := sysutil.Uname()
	if err != nil {
		s.Fatal("Failed to get uname: ", err)
	}
	baseRevision := strings.Join(strings.Split(u.Release, ".")[:2], ".")
	expectedPath, ok := wlanDriverList[devInfo.ID][baseRevision]
	if !ok {
		s.Log("Base revision not found in driver map, checking default")
		expectedPath, ok = wlanDriverList[devInfo.ID][defaultRevision]
		if !ok {
			s.Fatalf("Unexpected base revision %v for device %v", baseRevision, devInfo.Name)
		}
	}

	netDriversRoot := filepath.Join("/lib/modules", u.Release, "kernel/drivers/net")
	expectedModulePath := filepath.Join(netDriversRoot, expectedPath)

	if info, err := os.Stat(filepath.Dir(expectedModulePath)); err != nil {
		s.Error("Failed to stat module dir: ", err)
	} else if !info.IsDir() {
		s.Errorf("%v is not a directory", filepath.Dir(expectedModulePath))
	}

	if match, _ := filepath.Glob(expectedModulePath + "*"); match == nil {
		s.Errorf("Failed to locate module matching %v*", expectedModulePath)
	}

	moduleDir := filepath.Join("/sys/class/net", netIf, "device/driver/")
	dirs, err := ioutil.ReadDir(moduleDir)
	if err != nil {
		s.Fatal("Failed to list module path: ", err)
	}
	var path string
	for _, dir := range dirs {
		// Most of the devices link module under device/driver/module.
		if dir.Name() == "module" {
			modulePath := filepath.Join(moduleDir, "module")
			path, err = os.Readlink(modulePath)
			if err != nil {
				s.Fatal("Failed to readlink module path: ", err)
			}
			break
		}
		// Some SDIO devices may keep module link in device/driver/mmc?:????:?/driver.
		if match, _ := filepath.Match("mmc*", dir.Name()); match {
			modulePath := filepath.Join(moduleDir, dir.Name(), "driver")
			path, err = os.Readlink(modulePath)
			if err != nil {
				s.Fatal("Failed to readlink module path: ", err)
			}
			break
		}
	}
	if path == "" {
		s.Fatal("Failed to locate module path: ", err)
	}
	moduleName := filepath.Base(path)

	if got, want := moduleName+".ko", filepath.Base(expectedPath); got != want {
		s.Errorf("Module name is %s, want %s", got, want)
	}
}
