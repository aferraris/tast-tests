// Copyright 2022 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package wifi

import (
	"context"
	"time"

	"go.chromium.org/tast-tests/cros/local/chrome"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto/faillog"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto/quicksettings"
	"go.chromium.org/tast-tests/cros/local/shill"
	"go.chromium.org/tast/core/ctxutil"
	"go.chromium.org/tast/core/testing"
)

func init() {
	testing.AddTest(&testing.Test{
		Func:           ToggleWifiFromNetworkQuickSettings,
		LacrosStatus:   testing.LacrosVariantUnneeded,
		LifeCycleStage: testing.LifeCycleOwnerMonitored,
		Desc:           "Checks that WiFi can be enabled and disabled from within the Network Quick Settings",
		Contacts: []string{
			"cros-connectivity@google.com",
			"tjohnsonkanu@google.com",
		},
		BugComponent: "b:1131912", // ChromeOS > Software > System Services > Connectivity > WiFi
		Attr:         []string{"group:mainline", "informational"},
		SoftwareDeps: []string{"chrome"},
		Fixture:      "chromeLoggedIn",
		Timeout:      4 * time.Minute,
	})
}

// ToggleWifiFromNetworkQuickSettings tests that a user can successfully
// toggle the WiFi state using the toggle in the detailed Network view
// within the Quick Settings.
func ToggleWifiFromNetworkQuickSettings(ctx context.Context, s *testing.State) {
	cr := s.FixtValue().(chrome.HasChrome).Chrome()

	tconn, err := cr.TestAPIConn(ctx)
	if err != nil {
		s.Fatal("Failed to create Test API connection: ", err)
	}

	// Reserve one minute for cleanup.
	cleanupCtx := ctx
	ctx, cancel := ctxutil.Shorten(ctx, 1*time.Minute)
	defer cancel()
	defer faillog.DumpUITreeOnError(cleanupCtx, s.OutDir(), s.HasError, tconn)

	// Disable Wifi in shill.
	wifiManager, err := shill.NewWifiManager(ctx, nil)
	if err != nil {
		s.Fatal("Failed to create shill Wi-Fi manager: ", err)
	}
	if err := wifiManager.Enable(ctx, false); err != nil {
		s.Fatal("Failed to disable Wi-Fi: ", err)
	}
	// Restore WiFi after test.
	defer func(ctx context.Context) {
		if err := wifiManager.Enable(ctx, true); err != nil {
			s.Fatal("Failed to enable Wi-Fi: ", err)
		}
	}(cleanupCtx)

	if err := quicksettings.NavigateToNetworkDetailedView(ctx, tconn); err != nil {
		s.Fatal("Failed to navigate to the detailed Network view: ", err)
	}

	wifiToggleButton, err := quicksettings.NetworkDetailedViewWifiToggleButton(ctx, tconn)
	if err != nil {
		s.Fatal("Failed to get Wi-Fi toggle button: ", err)
	}

	ui := uiauto.New(tconn)

	state := true
	const iterations = 20
	for i := 0; i < iterations; i++ {
		s.Logf("Toggling WiFi (iteration %d of %d)", i+1, iterations)

		if err := ui.LeftClick(wifiToggleButton)(ctx); err != nil {
			s.Fatal("Failed to click the WiFi toggle: ", err)
		}

		exp, err := wifiManager.IsWifiEnabled(ctx)
		if err != nil {
			s.Fatal("Failed to toggle WiFi state: ", err)
		}

		if state != exp {
			s.Errorf("WiFi has a different status than expected: got %t, want %t", state, exp)
		}

		state = !state
	}
}
