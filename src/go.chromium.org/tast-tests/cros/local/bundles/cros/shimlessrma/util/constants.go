// Copyright 2023 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package util contains RMAResources which manges Shimless RMA resources.
package util

// ShimlessRmaEnabledModelsCritical are models with Shimless RMA support and stable in staging.
var ShimlessRmaEnabledModelsCritical = []string{
	// octopus
	"fleex",
	"vortininja",
	"sparky",
	"blorb",
	"bluebird",
	"dorp",
	"orbatrix",
	"meep",
	"garg360",
	"foob360",
	"bobba",
	"bloog",
	"grabbiter",
	"dood",
	"phaser",
	"blooglet",
	"vorticon",
	"casta",
	"bobba360",
	"sparky360",
	"phaser360",
	"fleex",
	"laser14",
	"blooguard",
	"mimrock",
	"droid",
	"lick",
	"foob",
	"garg",
	"garfour",
	// grunt
	"kasumi",
	"kasumi360",
	"liara",
	"barla",
	"aleena",
	"treeya",
	"treeya360",
	"careena",
	// coral
	"astronaut",
	"babymega",
	"babytiger",
	"blacktip",
	"blue",
	"bruce",
	"epaulette",
	"lava",
	"nasher",
	"nasher360",
	"rabbid",
	"robo",
	"robo360",
	"santa",
	"porbeagle",
	"whitetip",
	// jacuzzi
	"juniper",
	"kappa",
	"damu",
	"burnet",
	// corsola
	"magneton",
	"rusty",
	"steelix",
	// dedede
	"bookem",
	"boten",
	"maglia",
	"maglith",
	"magolor",
	// nissa
	"craask",
	"craaskbowl",
	"craaskvin",
	"pujjo",
	"pujjoteen"}

// ShimlessRmaEnabledModelsStaging are models with Shimless RMA support and pending to be enlisted to critical model list.
var ShimlessRmaEnabledModelsStaging = []string{}
