// Copyright 2023 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package fixture helps to trigger the shimless flow and install the
// diagnostic IWA for multiple tests.
package fixture
