// Copyright 2022 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package shimlessrma contains local Tast tests that exercise shimlessrma.
package shimlessrma

// json state data as created by rmad service

// WelcomeCancel, WelcomeNextCancel
// Empty State

// SelectComponentsNoneNextCancel
// {"state_history":[1,2]}
