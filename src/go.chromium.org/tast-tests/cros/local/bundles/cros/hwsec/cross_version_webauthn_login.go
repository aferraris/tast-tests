// Copyright 2023 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package hwsec

import (
	"context"
	"time"

	uda "go.chromium.org/chromiumos/system_api/user_data_auth_proto"
	"go.chromium.org/tast-tests/cros/common/hwsec"
	"go.chromium.org/tast-tests/cros/common/u2fd"
	"go.chromium.org/tast-tests/cros/local/bundles/cros/hwsec/fixture"
	"go.chromium.org/tast-tests/cros/local/bundles/cros/hwsec/util"
	"go.chromium.org/tast-tests/cros/local/chrome"
	"go.chromium.org/tast-tests/cros/local/chrome/browser"
	"go.chromium.org/tast-tests/cros/local/chrome/browser/browserfixt"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto/nodewith"
	hwseclocal "go.chromium.org/tast-tests/cros/local/hwsec"
	"go.chromium.org/tast-tests/cros/local/input"
	localu2fd "go.chromium.org/tast-tests/cros/local/u2fd"
	"go.chromium.org/tast/core/ctxutil"
	"go.chromium.org/tast/core/errors"
	"go.chromium.org/tast/core/testing"
)

func init() {
	testing.AddTest(&testing.Test{
		Func:         CrossVersionWebauthnLogin,
		LacrosStatus: testing.LacrosVariantUnneeded,
		Desc:         "Verifies WebAuthn login functionality across the version",
		Contacts: []string{
			"cros-hwsec@google.com",
			"chingkang@google.com",
		},
		// ChromeOS > Platform > System > Hardware Security > HwSec AP
		BugComponent: "b:1188704",
		Attr:         []string{"group:hw_agnostic"},
		SoftwareDeps: []string{"chrome", "tpm2_simulator"},
		Params: []testing.Param{
			// Parameters generated by cross_version_test.go. DO NOT EDIT.
			{
				Name:      "current",
				Fixture:   "crossVersion.current",
				ExtraAttr: []string{"group:mainline", "informational"},
			}, {
				Name:              "ti50_r112",
				Fixture:           "crossVersion.ti50_r112",
				ExtraAttr:         []string{"group:mainline", "informational"},
				ExtraSoftwareDeps: []string{"no_tpm_dynamic", "gsc"},
			}, {
				Name:              "ti50_r113",
				Fixture:           "crossVersion.ti50_r113",
				ExtraAttr:         []string{"group:mainline", "informational"},
				ExtraSoftwareDeps: []string{"no_tpm_dynamic", "gsc"},
			}, {
				Name:              "ti50_r114",
				Fixture:           "crossVersion.ti50_r114",
				ExtraAttr:         []string{"group:mainline", "informational"},
				ExtraSoftwareDeps: []string{"no_tpm_dynamic", "gsc"},
			}, {
				Name:              "ti50_r115",
				Fixture:           "crossVersion.ti50_r115",
				ExtraAttr:         []string{"group:mainline", "informational"},
				ExtraSoftwareDeps: []string{"no_tpm_dynamic", "gsc"},
			}, {
				Name:              "ti50_r116",
				Fixture:           "crossVersion.ti50_r116",
				ExtraAttr:         []string{"group:mainline", "informational"},
				ExtraSoftwareDeps: []string{"no_tpm_dynamic", "gsc"},
			}, {
				Name:              "ti50_r117",
				Fixture:           "crossVersion.ti50_r117",
				ExtraAttr:         []string{"group:mainline", "informational"},
				ExtraSoftwareDeps: []string{"no_tpm_dynamic", "gsc"},
			}, {
				Name:              "ti50_r118",
				Fixture:           "crossVersion.ti50_r118",
				ExtraAttr:         []string{"group:mainline", "informational"},
				ExtraSoftwareDeps: []string{"no_tpm_dynamic", "gsc"},
			}, {
				Name:              "ti50_r119",
				Fixture:           "crossVersion.ti50_r119",
				ExtraAttr:         []string{"group:mainline", "informational"},
				ExtraSoftwareDeps: []string{"no_tpm_dynamic", "gsc"},
			}, {
				Name:              "tpm2_r112",
				Fixture:           "crossVersion.tpm2_r112",
				ExtraAttr:         []string{"group:mainline", "informational"},
				ExtraSoftwareDeps: []string{"no_tpm_dynamic", "no_gsc"},
			}, {
				Name:              "tpm2_r113",
				Fixture:           "crossVersion.tpm2_r113",
				ExtraAttr:         []string{"group:mainline", "informational"},
				ExtraSoftwareDeps: []string{"no_tpm_dynamic", "no_gsc"},
			}, {
				Name:              "tpm2_r114",
				Fixture:           "crossVersion.tpm2_r114",
				ExtraAttr:         []string{"group:mainline", "informational"},
				ExtraSoftwareDeps: []string{"no_tpm_dynamic", "no_gsc"},
			}, {
				Name:              "tpm2_r115",
				Fixture:           "crossVersion.tpm2_r115",
				ExtraAttr:         []string{"group:mainline", "informational"},
				ExtraSoftwareDeps: []string{"no_tpm_dynamic", "no_gsc"},
			}, {
				Name:              "tpm2_r116",
				Fixture:           "crossVersion.tpm2_r116",
				ExtraAttr:         []string{"group:mainline", "informational"},
				ExtraSoftwareDeps: []string{"no_tpm_dynamic", "no_gsc"},
			}, {
				Name:              "tpm2_r117",
				Fixture:           "crossVersion.tpm2_r117",
				ExtraAttr:         []string{"group:mainline", "informational"},
				ExtraSoftwareDeps: []string{"no_tpm_dynamic", "no_gsc"},
			}, {
				Name:              "tpm2_r118",
				Fixture:           "crossVersion.tpm2_r118",
				ExtraAttr:         []string{"group:mainline", "informational"},
				ExtraSoftwareDeps: []string{"no_tpm_dynamic", "no_gsc"},
			}, {
				Name:              "tpm2_r119",
				Fixture:           "crossVersion.tpm2_r119",
				ExtraAttr:         []string{"group:mainline", "informational"},
				ExtraSoftwareDeps: []string{"no_tpm_dynamic", "no_gsc"},
			}, {
				Name:              "tpm_dynamic_r112",
				Fixture:           "crossVersion.tpm_dynamic_r112",
				ExtraAttr:         []string{"group:mainline", "informational"},
				ExtraSoftwareDeps: []string{"tpm_dynamic", "no_gsc"},
			}, {
				Name:              "tpm_dynamic_r113",
				Fixture:           "crossVersion.tpm_dynamic_r113",
				ExtraAttr:         []string{"group:mainline", "informational"},
				ExtraSoftwareDeps: []string{"tpm_dynamic", "no_gsc"},
			}, {
				Name:              "tpm_dynamic_r114",
				Fixture:           "crossVersion.tpm_dynamic_r114",
				ExtraAttr:         []string{"group:mainline", "informational"},
				ExtraSoftwareDeps: []string{"tpm_dynamic", "no_gsc"},
			}, {
				Name:              "tpm_dynamic_r115",
				Fixture:           "crossVersion.tpm_dynamic_r115",
				ExtraAttr:         []string{"group:mainline", "informational"},
				ExtraSoftwareDeps: []string{"tpm_dynamic", "no_gsc"},
			}, {
				Name:              "tpm_dynamic_r116",
				Fixture:           "crossVersion.tpm_dynamic_r116",
				ExtraAttr:         []string{"group:mainline", "informational"},
				ExtraSoftwareDeps: []string{"tpm_dynamic", "no_gsc"},
			}, {
				Name:              "tpm_dynamic_r117",
				Fixture:           "crossVersion.tpm_dynamic_r117",
				ExtraAttr:         []string{"group:mainline", "informational"},
				ExtraSoftwareDeps: []string{"tpm_dynamic", "no_gsc"},
			}, {
				Name:              "tpm_dynamic_r118",
				Fixture:           "crossVersion.tpm_dynamic_r118",
				ExtraAttr:         []string{"group:mainline", "informational"},
				ExtraSoftwareDeps: []string{"tpm_dynamic", "no_gsc"},
			}, {
				Name:              "tpm_dynamic_r119",
				Fixture:           "crossVersion.tpm_dynamic_r119",
				ExtraAttr:         []string{"group:mainline", "informational"},
				ExtraSoftwareDeps: []string{"tpm_dynamic", "no_gsc"},
			},
		},
	})
}

// testWebauthnLogin verifies the WebAuthn GetAssertion operation succeeds.
func testWebauthnLogin(ctx context.Context, config *util.CrossVersionLoginConfig, webauthnURL string) error {
	// Reserve ten seconds for cleanup.
	cleanupCtx := ctx
	ctx, cancel := ctxutil.Shorten(ctx, 10*time.Second)
	defer cancel()

	authConfig := config.AuthConfig
	username := authConfig.Username

	// The smart card authentication is not currently supported in this subtest,
	// as it'd require loading fake smart card middleware extensions in Chrome.
	if authConfig.AuthType != hwsec.PassAuth {
		return nil
	}

	// Check password login.
	opts := []chrome.Option{
		chrome.FakeLogin(chrome.Creds{User: username, Pass: authConfig.Password}),
		chrome.KeepState(),
	}
	cr, err := chrome.New(ctx, opts...)
	if err != nil {
		return errors.Wrap(err, "failed to log in with password")
	}

	u2fDaemon, err := localu2fd.NewU2fDaemon(ctx)
	if err != nil {
		return errors.Wrap(err, "failed to connect to u2fd")
	}
	if err = u2fDaemon.WaitUntilInitialized(ctx); err != nil {
		return errors.Wrap(err, "failed to wait until u2fd is initialized")
	}

	conn, _, closeBrowser, err := browserfixt.SetUpWithURL(ctx, cr, browser.TypeAsh, webauthnURL)
	if err != nil {
		return errors.Wrap(err, "failed to open the browser")
	}
	defer closeBrowser(cleanupCtx)
	defer conn.Close()

	tconn, err := cr.TestAPIConn(ctx)
	if err != nil {
		return errors.Wrap(err, "failed to get test API connection")
	}

	keyboard, err := input.VirtualKeyboard(ctx)
	if err != nil {
		return errors.Wrap(err, "failed to get keyboard")
	}
	defer keyboard.Close(ctx)

	// TODO(b/329367144): Below is a dirty hack: since PINs created by the CLI aren't compatible with
	// those created by Chrome, we can't enroll/test WebAuthn credentials using PIN auth. In addition,
	// WebAuthn UI doesn't support choosing password over PIN if PIN is available. Therefore, a hacky
	// workaround is that we lock-out PIN first, so that the WebAuthn auth routine below can use
	// password for WebAuthn UI (as PIN isn't available).
	cmdRunner := hwseclocal.NewCmdRunner()
	helper, err := hwseclocal.NewHelper(cmdRunner)
	if err != nil {
		return errors.Wrap(err, "failed to create hwsec local helper")
	}
	cryptohome := helper.CryptohomeClient()
	if err := cryptohome.WithAuthSession(ctx, username, false, uda.AuthIntent_AUTH_INTENT_DECRYPT, func(authID string) error {
		for _, key := range config.ExtraVaultKeys {
			if key.LowEntropy {
				for i := 0; i < 5; i++ {
					_, err := cryptohome.AuthenticatePinAuthFactor(ctx, authID, key.KeyLabel, key.Password+"-wrong")
					if err == nil {
						return errors.Wrap(err, "authentication with wrong PIN succeeded unexpectedly")
					}
				}
			}
		}
		return nil
	}); err != nil {
		return err
	}

	authCallback := func(ctx context.Context, ui *uiauto.Context) error {
		// Check if the UI is correct.
		if err := ui.Exists(nodewith.ClassName("LoginPasswordView"))(ctx); err != nil {
			return errors.Wrap(err, "failed to find the password input field")
		}
		// Type password into ChromeOS WebAuthn dialog.
		if err := keyboard.Type(ctx, authConfig.Password+"\n"); err != nil {
			return errors.Wrap(err, "failed to type password into ChromeOS auth dialog")
		}
		return nil
	}

	assertionConfig := u2fd.WebAuthnAssertionConfig{
		Keys: []u2fd.WebAuthnCredential{*config.WebAuthnCred},
		Uv:   "preferred",
	}
	if err := localu2fd.GetAssertionInLocalSite(ctx, conn, tconn, assertionConfig, authCallback); err != nil {
		return errors.Wrap(err, "failed to perform WebAuthn GetAssertion")
	}

	// TODO(b/237120336): Check cryptohome was not recreated, by reading some file
	// that was previously put into the snapshot.
	if err := cr.Close(ctx); err != nil {
		return errors.Wrap(err, "failed to log out after password login")
	}
	// TODO(b/237120336): Check PIN WebAuthn as well.

	return nil
}

func CrossVersionWebauthnLogin(ctx context.Context, s *testing.State) {
	fixtureData := s.FixtValue().(*fixture.CrossVersionLoginFixture)
	for _, config := range fixtureData.ConfigList {
		if config.VaultFSType == util.NoVaultFS {
			continue
		}
		if err := testWebauthnLogin(ctx, &config, fixtureData.WebAuthnURL); err != nil {
			s.Fatal("Failed to test chrome login with USS migration: ", err)
		}
	}
}
