// Copyright 2022 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package login

import (
	"context"
	"time"

	"go.chromium.org/tast-tests/cros/common/ui"
	"go.chromium.org/tast-tests/cros/local/bundles/cros/login/signinutil"
	"go.chromium.org/tast-tests/cros/local/chrome"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto/faillog"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto/nodewith"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto/restriction"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto/role"
	"go.chromium.org/tast-tests/cros/local/chrome/userutil"
	"go.chromium.org/tast-tests/cros/local/input"
	"go.chromium.org/tast/core/ctxutil"
	"go.chromium.org/tast/core/errors"
	"go.chromium.org/tast/core/testing"
)

func init() {
	testing.AddTest(&testing.Test{
		Func:         RestrictSigninAddUserFormat,
		Desc:         "Check that 'Manage other people' validates user email format",
		LacrosStatus: testing.LacrosVariantUnneeded,
		Contacts: []string{
			"cros-lurs@google.com",
			"rrsilva@google.com",
			"chromeos-sw-engprod@google.com",
		},
		BugComponent: "b:1207311", // ChromeOS > Software > Commercial (Enterprise) > Identity > LURS
		Attr:         []string{"group:golden_tier", "group:medium_low_tier", "group:hardware", "group:complementary", "group:hw_agnostic"},
		SoftwareDeps: []string{"chrome"},
		VarDeps:      []string{"ui.signinProfileTestExtensionManifestKey", ui.GaiaPoolDefaultVarName},
		Timeout:      chrome.LoginTimeout + 3*time.Minute,
	})
}

var addUserDialog = nodewith.Role(role.Dialog)

func RestrictSigninAddUserFormat(ctx context.Context, s *testing.State) {
	const (
		deviceOwner    = "fake-device-owner@gmail.com"
		devicePassword = "password"
	)

	cleanUpCtx := ctx
	// 30 seconds should be enough for all clean up operations.
	ctx, cancel := ctxutil.Shorten(ctx, 30*time.Second)
	defer cancel()

	// For the device owner we wait until their ownership has been established.
	if err := userutil.CreateDeviceOwner(ctx, deviceOwner, devicePassword); err != nil {
		s.Fatal("Failed to create device owner: ", err)
	}

	cr, err := userutil.Login(ctx, deviceOwner, devicePassword)
	if err != nil {
		s.Fatal("Failed to log in as device owner: ", err)
	}
	if err := userutil.WaitForOwnership(ctx, cr); err != nil {
		s.Fatal("User did not become device owner: ", err)
	}

	tconn, err := cr.TestAPIConn(ctx)
	if err != nil {
		s.Fatal("Creating login test API connection failed: ", err)
	}

	settings, err := signinutil.OpenManageOtherPeople(ctx, cr, tconn)
	if err != nil {
		s.Fatal("Failed to open 'Manage other people' section in OS Settings: ", err)
	}
	defer cr.Close(cleanUpCtx)
	if settings != nil {
		defer settings.Close(cleanUpCtx)
	}

	// Set up keyboard.
	kb, err := input.Keyboard(ctx)
	if err != nil {
		s.Fatal("Failed to get keyboard: ", err)
	}
	defer kb.Close(cleanUpCtx)

	ui := uiauto.New(tconn)

	if err := uiauto.Combine("enable limit sign-in",
		ui.LeftClick(nodewith.Name(signinutil.LimitSignInOption).Role(role.ToggleButton)),
		ui.WaitUntilExists(nodewith.NameStartingWith(signinutil.GetUsernameFromEmail(deviceOwner)).NameContaining("owner").Role(role.StaticText)),
	)(ctx); err != nil {
		s.Fatal("Failed to enable limit sign-in: ", err)
	}

	for _, tc := range []struct {
		email string
		// Expected email to be added. If unspecified, use "email" field.
		addedEmail string
		isValid    bool
	}{
		// Valid email addresses:
		{email: "test@gmail.com", isValid: true},
		{email: "1234567890@gmail.com", isValid: true},
		{email: "test987", addedEmail: "test987@gmail.com", isValid: true},
		{email: "firstname.lastname@gmail.com", addedEmail: "firstnamelastname@gmail.com", isValid: true},
		{email: "test.email.with+symbol@example.com", isValid: true},
		{email: "test@subdomain.example.com", isValid: true},
		{email: "email@example-one.com", isValid: true},
		{email: "_______@example.com", isValid: true},
		{email: "*@gmail.com", isValid: true},
		// Invalid email addresses:
		{email: "", isValid: false},
		{email: "test@gmail", isValid: false},
		{email: "test..123@gmail", isValid: false},
		{email: "@gmail.com", isValid: false},
	} {
		s.Run(ctx, tc.email, func(ctx context.Context, s *testing.State) {
			cleanUpCtx := ctx
			ctx, cancel := ctxutil.Shorten(ctx, 15*time.Second)
			defer cancel()

			defer func(ctx context.Context, s *testing.State, tconn *chrome.TestConn) {
				if err := cancelDialog(ctx, tconn); err != nil {
					s.Fatal("Failed to cancel the dialog: ", err)
				}
			}(cleanUpCtx, s, tconn)
			defer faillog.DumpUITreeWithScreenshotOnError(cleanUpCtx, s.OutDir(), s.HasError, cr, "ui_tree_"+tc.email)

			if tc.addedEmail == "" {
				tc.addedEmail = tc.email
			}

			result, err := maybeAddUser(ctx, tconn, kb, tc.email, tc.addedEmail)
			if err != nil {
				s.Fatal("Failed to add user: ", err)
			}

			if result != tc.isValid {
				s.Errorf("Failed to confirm if email format is valid: want %t; got %t", tc.isValid, result)
			}
		})
	}
}

// maybeAddUser opens the Add user dialog, enters provided email and tries to
// add it to 'restricted users' list.
// If 'Add' button in the dialog is not active - returns false. This means that
// the provided email was invalid.
// Otherwise - successfully closes the dialog and checks that the user with
// provided addedEmail was added to the 'restricted users' list.
func maybeAddUser(ctx context.Context, tconn *chrome.TestConn, kb *input.KeyboardEventWriter,
	email, addedEmail string) (bool, error) {

	addUserButton := nodewith.Name("Add user").Role(role.Link)
	addUserEmailField := nodewith.Role(role.TextField).Name("Email address")
	addButton := nodewith.Name("Add").Focusable()

	ui := uiauto.New(tconn)

	if err := uiauto.Combine("start adding user",
		ui.WaitUntilExists(addUserButton),
		ui.LeftClick(addUserButton),
		ui.WaitUntilExists(addUserDialog),
		ui.WaitUntilExists(addUserEmailField),
		ui.LeftClick(addUserEmailField),
		kb.TypeAction(email),
		ui.WaitUntilExists(addButton),
	)(ctx); err != nil {
		return false, err
	}

	addButtonInfo, err := ui.Info(ctx, addButton)
	if err != nil {
		return false, errors.Wrap(err, "failed to find info for addButton")
	}

	if addButtonInfo.Restriction == restriction.Disabled {
		return false, nil
	}

	// Try to submit with 'Add' button.
	if err := ui.LeftClickUntil(addButton, ui.WaitUntilGone(addUserDialog))(ctx); err != nil {
		return false, errors.Wrap(err, "failed to add the user")
	}

	if err := ui.WaitUntilExists(nodewith.Name(addedEmail).Role(role.StaticText))(ctx); err != nil {
		return false, errors.Wrap(err, "failed to make sure the user was added")
	}

	return true, nil
}

// cancelDialog closes the Add user dialog if it's present.
func cancelDialog(ctx context.Context, tconn *chrome.TestConn) error {
	cancelButton := nodewith.Name("Cancel").Focusable()
	ui := uiauto.New(tconn)

	if err := ui.Exists(addUserDialog)(ctx); err != nil {
		return nil
	}

	if err := uiauto.Combine("cancel the dialog",
		ui.WaitUntilExists(cancelButton),
		ui.LeftClick(cancelButton),
		ui.WaitUntilGone(addUserDialog),
	)(ctx); err != nil {
		return errors.Wrap(err, "failed to cancel the dialog")
	}

	return nil
}
