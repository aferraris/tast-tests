// Copyright 2022 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package login

import (
	"context"
	"time"

	"go.chromium.org/tast-tests/cros/local/bundles/cros/login/signinutil"
	"go.chromium.org/tast-tests/cros/local/chrome"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto/faillog"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto/nodewith"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto/role"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto/state"
	"go.chromium.org/tast-tests/cros/local/chrome/userutil"
	"go.chromium.org/tast-tests/cros/local/session"
	"go.chromium.org/tast/core/ctxutil"
	"go.chromium.org/tast/core/errors"
	"go.chromium.org/tast/core/testing"
)

func init() {
	testing.AddTest(&testing.Test{
		Func:         RestrictSignin,
		Desc:         "Checks if device owner can restrict signin",
		LacrosStatus: testing.LacrosVariantUnneeded,
		Contacts: []string{
			"cros-lurs@google.com",
			"rrsilva@google.com",
			"chromeos-sw-engprod@google.com",
		},
		BugComponent: "b:1207311", // ChromeOS > Software > Commercial (Enterprise) > Identity > LURS
		Attr:         []string{"group:mainline", "informational", "group:hw_agnostic"},
		SoftwareDeps: []string{"chrome"},
		VarDeps:      []string{"ui.signinProfileTestExtensionManifestKey"},
		Timeout:      chrome.LoginTimeout + time.Minute,
	})
}

func RestrictSignin(ctx context.Context, s *testing.State) {
	const (
		deviceOwner    = "device-owner@gmail.com"
		devicePassword = "password"
	)

	cleanUpCtx := ctx
	// 30 seconds should be enough for all clean up operations.
	ctx, cancel := ctxutil.Shorten(ctx, 30*time.Second)
	defer cancel()

	// For the device owner we wait until their ownership has been established.
	if err := userutil.CreateDeviceOwner(ctx, deviceOwner, devicePassword); err != nil {
		s.Fatal("Failed to create device owner: ", err)
	}

	// Check that 'Add person' is enabled by default.
	func() {
		cr, err := chrome.New(
			ctx,
			chrome.NoLogin(),
			chrome.KeepState(),
			chrome.LoadSigninProfileExtension(s.RequiredVar("ui.signinProfileTestExtensionManifestKey")),
		)
		if err != nil {
			s.Fatal("Failed to start Chrome: ", err)
		}
		defer cr.Close(cleanUpCtx)

		tconn, err := cr.SigninProfileTestAPIConn(ctx)
		if err != nil {
			s.Fatal("Creating login test API connection failed: ", err)
		}
		defer faillog.DumpUITreeOnError(cleanUpCtx, s.OutDir(), s.HasError, tconn)

		// Make sure the button is enabled.
		addPersonEnabled, err := getAddPersonFocusableState(ctx, tconn)
		if err != nil {
			s.Fatal("Failed to find Add person button info: ", err)
		}
		if !addPersonEnabled {
			s.Fatal("Failed to make sure Add person button is enabled")
		}
	}()

	// Select 'Limit sign-in' in Settings.
	func() {
		cr, err := userutil.Login(ctx, deviceOwner, devicePassword)
		if err != nil {
			s.Fatal("Failed to log in as device owner: ", err)
		}
		if err := userutil.WaitForOwnership(ctx, cr); err != nil {
			s.Fatal("User did not become device owner: ", err)
		}

		tconn, err := cr.TestAPIConn(ctx)
		if err != nil {
			s.Fatal("Creating login test API connection failed: ", err)
		}
		defer faillog.DumpUITreeOnError(cleanUpCtx, s.OutDir(), s.HasError, tconn)

		settings, err := signinutil.OpenManageOtherPeople(ctx, cr, tconn)
		if err != nil {
			s.Fatal("Failed to open Manage other people: ", err)
		}
		defer cr.Close(cleanUpCtx)
		if settings != nil {
			defer settings.Close(cleanUpCtx)
		}

		// Waiting for PropertyChangeComplete signal for the ShowUserNames setting
		// to confirm it has been written to disk.
		sessionManager, err := session.NewSessionManager(ctx)
		if err != nil {
			s.Fatal("Failed to create session_manager binding: ", err)
		}
		settingsWatcher, err := sessionManager.WatchPropertyChangeComplete(ctx)
		if err != nil {
			s.Fatal("Failed to start watching PropertyChangeComplete signal: ", err)
		}
		defer settingsWatcher.Close(cleanUpCtx)

		ui := uiauto.New(tconn)

		if err := uiauto.Combine("limit sign-in to existing users only",
			ui.LeftClick(nodewith.Name(signinutil.LimitSignInOption).Role(role.ToggleButton)),
			ui.WaitUntilExists(nodewith.NameStartingWith(signinutil.GetUsernameFromEmail(deviceOwner)).NameContaining("owner").Role(role.StaticText)),
		)(ctx); err != nil {
			s.Fatal("Failed to limit sign-in: ", err)
		}

		select {
		case <-settingsWatcher.Signals:
		case <-ctx.Done():
			s.Fatal("Timed out waiting for PropertyChangeComplete signal: ", ctx.Err())
		}

		setting, err := session.RetrieveSettings(ctx, sessionManager)

		if err != nil {
			s.Fatal("Failed to retrieve settings: ", err)
		}

		if setting.AllowNewUsers.GetAllowNewUsers() {
			s.Fatal("Failed to toggle AllowNewUsers value")
		}
	}()

	// Check that 'Add person' is disabled.
	func() {
		cr, err := chrome.New(
			ctx,
			chrome.NoLogin(),
			chrome.KeepState(),
			chrome.LoadSigninProfileExtension(s.RequiredVar("ui.signinProfileTestExtensionManifestKey")),
		)
		if err != nil {
			s.Fatal("Failed to start Chrome: ", err)
		}
		defer cr.Close(cleanUpCtx)

		tconn, err := cr.SigninProfileTestAPIConn(ctx)
		if err != nil {
			s.Fatal("Creating login test API connection failed: ", err)
		}
		defer faillog.DumpUITreeOnError(cleanUpCtx, s.OutDir(), s.HasError, tconn)

		addPersonEnabled, err := getAddPersonFocusableState(ctx, tconn)
		if err != nil {
			s.Fatal("Failed to find Add person button info: ", err)
		}
		if addPersonEnabled {
			s.Fatal("Failed to make sure Add person button is disabled")
		}
	}()
}

// getAddPersonFocusableState finds "Add person" button and returns whether it's focusable/enabled.
func getAddPersonFocusableState(ctx context.Context, tconn *chrome.TestConn) (bool, error) {
	ui := uiauto.New(tconn)

	// Find 'Add person' button. Make sure the button is enabled.
	addPersonButton := nodewith.Name("Add Person").Role(role.Button).HasClass("LoginShelfButton")
	addPersonInfo, err := ui.Info(ctx, addPersonButton)
	if err != nil {
		return false, errors.Wrap(err, "failed to find Add person button info")
	}
	return addPersonInfo.State[state.Focusable], nil
}
