// Copyright 2021 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package login

import (
	"context"
	"time"

	"go.chromium.org/tast-tests/cros/local/chrome"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto/faillog"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto/lockscreen"
	"go.chromium.org/tast-tests/cros/local/input"
	"go.chromium.org/tast-tests/cros/local/network"
	"go.chromium.org/tast/core/ctxutil"
	"go.chromium.org/tast/core/errors"
	"go.chromium.org/tast/core/testing"
)

func init() {
	testing.AddTest(&testing.Test{
		Func:         Offline,
		LacrosStatus: testing.LacrosVariantUnneeded,
		Desc:         "Test that user can sign in when device offline ",
		Contacts: []string{
			"cros-lurs@google.com",
			"bohdanty@google.com",
			"rrsilva@google.com",
			"chromeos-sw-engprod@google.com",
			"cros-oac@google.com",
		},
		BugComponent: "b:1207311", // ChromeOS > Software > Commercial (Enterprise) > Identity > LURS
		SoftwareDeps: []string{"chrome", "chrome_internal", "non_meets_device"},
		Attr:         []string{"group:mainline", "group:hw_agnostic"},
		VarDeps:      []string{"ui.signinProfileTestExtensionManifestKey"},
		Timeout:      2*chrome.LoginTimeout + time.Minute,
		SearchFlags: []*testing.StringPair{{
			Key: "feature_id",
			// Offline Authentication.
			Value: "screenplay-2ea06509-cb85-4122-9145-900e47707e94",
		}},
	})
}

func Offline(ctx context.Context, s *testing.State) {
	cleanUpCtx := ctx
	ctx, cancel := ctxutil.Shorten(ctx, 30*time.Second)
	defer cancel()

	var creds chrome.Creds
	cr, err := chrome.New(ctx)
	if err != nil {
		s.Fatal("Chrome login failed: ", err)
	}
	creds = cr.Creds()
	if err = cr.Close(ctx); err != nil {
		s.Fatal("Failed to close Chrome instance: ", err)
	}

	loginAgain := func(ctx context.Context) error {
		cr, err := chrome.New(ctx,
			chrome.ExtraArgs("--skip-force-online-signin-for-testing"),
			chrome.NoLogin(),
			chrome.KeepState(),
			chrome.LoadSigninProfileExtension(s.RequiredVar("ui.signinProfileTestExtensionManifestKey")))

		if err != nil {
			return errors.Wrap(err, "chrome start failed")
		}
		defer cr.Close(cleanUpCtx)

		tconn, err := cr.SigninProfileTestAPIConn(ctx)

		if err != nil {
			return errors.Wrap(err, "getting test Signin Profile API connection failed")
		}

		hasErrorVar := true
		hasError := func() bool { return hasErrorVar }
		defer faillog.DumpUITreeOnError(cleanUpCtx, s.OutDir(), hasError, tconn)

		if err := lockscreen.WaitForPasswordField(ctx, tconn, creds.User, 10*time.Second); err != nil {
			return errors.Wrap(err, "failed to wait for the password field")
		}
		keyboard, err := input.VirtualKeyboard(ctx)
		if err != nil {
			return errors.Wrap(err, "failed to get virtual keyboard")
		}
		defer keyboard.Close(cleanUpCtx)

		// Enter wrong password
		if err = lockscreen.EnterPassword(ctx, tconn, creds.User, creds.Pass+"fake", keyboard); err != nil {
			return errors.Wrap(err, "failed to enter password")
		}

		if err := lockscreen.WaitForAuthError(ctx, tconn, 10*time.Second); err != nil {
			return errors.Wrap(err, "failed to wait for auth error")
		}

		// Enter correct password
		if err = lockscreen.EnterPassword(ctx, tconn, creds.User, creds.Pass, keyboard); err != nil {
			return errors.Wrap(err, "failed to enter password")
		}
		if err := lockscreen.WaitForLoggedIn(ctx, tconn, chrome.LoginTimeout); err != nil {
			s.Fatal("Failed to login: ", err)
		}

		hasErrorVar = false
		return nil
	}

	// Run login in offline mode.
	if err := network.ExecFuncOnChromeOffline(ctx, loginAgain); err != nil {
		s.Fatal("Failed to login offline: ", err)
	}
}
