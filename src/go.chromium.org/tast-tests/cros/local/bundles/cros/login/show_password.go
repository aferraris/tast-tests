// Copyright 2022 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package login

import (
	"context"
	"strings"
	"time"

	"go.chromium.org/tast-tests/cros/local/chrome"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto/faillog"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto/lockscreen"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto/ossettings"
	"go.chromium.org/tast-tests/cros/local/chrome/userutil"
	"go.chromium.org/tast-tests/cros/local/input"
	"go.chromium.org/tast/core/ctxutil"
	"go.chromium.org/tast/core/errors"
	"go.chromium.org/tast/core/testing"
)

type testParams struct {
	EnablePIN  bool
	Autosubmit bool
}

const hiddenPwdChar = "•"

func init() {
	testing.AddTest(&testing.Test{
		Func: ShowPassword,
		Desc: "Test Show/Hide password functionality on Password field and \"PIN or password\" field of login screen",
		Contacts: []string{
			"cros-lurs@google.com",
			"rrsilva@google.com",
			"chromeos-sw-engprod@google.com",
		},
		BugComponent: "b:1207311", // ChromeOS > Software > Commercial (Enterprise) > Identity > LURS
		SoftwareDeps: []string{"chrome"},
		Attr:         []string{"group:golden_tier", "group:medium_low_tier", "group:hardware", "group:complementary", "group:hw_agnostic"},
		VarDeps:      []string{"ui.signinProfileTestExtensionManifestKey"},
		Params: []testing.Param{{
			Val: testParams{false, false},
		}, {
			Name: "pin",
			Val:  testParams{true, false},
		}, {
			Name: "switch_to_password",
			Val:  testParams{true, true},
		}},
		// We need two LoginTimeouts because we log on once to create a user and then reboot (reboot is almost as expensive as login). We then perform some fast UI operations.
		Timeout:      2*chrome.LoginTimeout + 2*time.Minute,
		LacrosStatus: testing.LacrosVariantUnneeded,
	})
}

// ShowPassword tests viewing PIN / Password on login screen using the "Show password" button and that it goes hidden using the "Hide password" button.
func ShowPassword(ctx context.Context, s *testing.State) {
	cleanUpCtx := ctx
	ctx, cancel := ctxutil.Shorten(ctx, time.Minute)
	defer cancel()

	const PIN = "123456789012"
	enablePIN := s.Param().(testParams).EnablePIN
	autosubmit := s.Param().(testParams).Autosubmit
	var creds chrome.Creds

	defer userutil.ResetUsers(cleanUpCtx)
	// Log in and log out to create a user pod on the login screen.
	func() {
		cr, err := chrome.New(ctx)
		if err != nil {
			s.Fatal("Chrome login failed: ", err)
		}
		defer cr.Close(cleanUpCtx)
		creds = cr.Creds()

		tconn, err := cr.TestAPIConn(ctx)
		if err != nil {
			s.Fatal("Getting test API connection failed: ", err)
		}
		defer faillog.DumpUITreeOnError(cleanUpCtx, s.OutDir(), s.HasError, tconn)

		if enablePIN {
			// Set up PIN through a connection to the Settings page.
			settings, err := ossettings.Launch(ctx, tconn)
			if err != nil {
				s.Fatal("Failed to launch Settings app: ", err)
			}
			s.Log("Performing PIN set up")
			if err := settings.EnablePINUnlock(cr, creds.Pass, PIN, autosubmit)(ctx); err != nil {
				s.Fatal("Failed to enable PIN unlock: ", err)
			}
		}
	}()

	// chrome.NoLogin() and chrome.KeepState() are needed to show the login screen with a user pod (instead of the OOBE login screen).
	cr, err := chrome.New(ctx,
		chrome.ExtraArgs("--skip-force-online-signin-for-testing"),
		chrome.NoLogin(),
		chrome.KeepState(),
		chrome.LoadSigninProfileExtension(s.RequiredVar("ui.signinProfileTestExtensionManifestKey")),
	)
	if err != nil {
		s.Fatal("Failed to start Chrome: ", err)
	}
	defer cr.Close(cleanUpCtx)

	tconn, err := cr.SigninProfileTestAPIConn(ctx)
	if err != nil {
		s.Fatal("Creating login test API connection failed: ", err)
	}
	defer faillog.DumpUITreeOnError(cleanUpCtx, s.OutDir(), s.HasError, tconn)

	// Wait for the login screen to be ready for PIN / Password entry.
	if err := lockscreen.WaitForPasswordEntry(ctx, tconn, 30*time.Second); err != nil {
		s.Fatal("Failed waiting for the login screen to be ready for PIN / Password entry: ", err)
	}

	// Clicking the "Switch to password" button to view the Password field when PIN autosubmit is enabled
	if autosubmit {
		if err := lockscreen.SwitchToPassword(ctx, tconn); err != nil {
			s.Fatal("Failed to click the Switch to password button: ", err)
		}
	}

	// Test the working of "Show password" and "Hide password" button on login screen.
	if enablePIN && !autosubmit {
		if err := showAndHidePassword(ctx, tconn, creds.User, PIN, true); err != nil {
			s.Fatal("Failed to Show/Hide PIN on login screen: ", err)
		}
	} else {
		if err := showAndHidePassword(ctx, tconn, creds.User, creds.Pass, false); err != nil {
			s.Fatal("Failed to Show/Hide Password on login screen: ", err)
		}
	}
}

// showAndHidePassword tests the working of "Show password" button and "Hide password" button on Password field and "PIN or password" field.
func showAndHidePassword(ctx context.Context, tconn *chrome.TestConn, username, password string, pin bool) error {
	hiddenPwd := strings.Repeat(hiddenPwdChar, len(password))

	kb, err := input.Keyboard(ctx)
	if err != nil {
		return errors.Wrap(err, "failed to get keyboard")
	}
	defer kb.Close(ctx)

	if pin {
		// Enter the PIN on login screen when PIN is enabled.
		testing.ContextLog(ctx, "Entering PIN on \"PIN or password\" field of login screen")
		if err := lockscreen.EnterPIN(ctx, tconn, kb, password); err != nil {
			return errors.Wrap(err, "failed to enter in PIN")
		}
	} else {
		// Enter password on login screen.

		testing.ContextLog(ctx, "Entering password on login screen")
		if err := lockscreen.TypePassword(ctx, tconn, username, password, kb); err != nil {
			return errors.Wrap(err, "failed to type password")
		}
	}

	// Click the "Show password" button and verify that the viewed PIN / Password matches the user entered value.
	if err := lockscreen.ShowPassword(ctx, tconn); err != nil {
		return errors.Wrap(err, "failed to click the Show password button")
	}
	passwordField, err := lockscreen.UserPassword(ctx, tconn, username, pin)
	if err != nil {
		return errors.New("failed to read PIN / Password")
	}
	if passwordField.Value != password {
		return errors.Errorf("the revealed PIN / Password %q is not matching with the user entered value %q",
			passwordField.Value, password)
	}

	// Verify that the PIN / Password goes hidden after clicking the "Hide password" button.
	if err := lockscreen.HidePassword(ctx, tconn); err != nil {
		return errors.Wrap(err, "failed to click the Hide password button")
	}
	passwordVal, err := lockscreen.UserPassword(ctx, tconn, username, pin)
	if err != nil {
		return errors.New("failed to read PIN / Password that is hidden")
	}
	if passwordVal.Value != hiddenPwd {
		return errors.New("PIN / Password is not hidden after clicking the Hide password button")
	}

	// Verify that when the user clicks the "Show password" button, the viewed PIN / Password goes hidden automatically after 5s timeout.
	if err := waitForPasswordHidden(ctx, tconn); err != nil {
		return errors.Wrap(err, "PIN / Password is not hidden after the timeout")
	}
	return nil
}

// waitForPasswordHidden checks that the PIN / Password is auto hidden 5s after "Show password" button is pressed.
func waitForPasswordHidden(ctx context.Context, tconn *chrome.TestConn) error {
	if err := lockscreen.ShowPassword(ctx, tconn); err != nil {
		return errors.Wrap(err, "failed to click the Show password button")
	}
	ui := uiauto.New(tconn)
	if err := ui.WithTimeout(6 * time.Second).WaitUntilGone(lockscreen.HidePasswordButton)(ctx); err != nil {
		return err
	}
	if err := ui.Exists(lockscreen.ShowPasswordButton)(ctx); err != nil {
		return errors.New("failed to find the Show password button")
	}
	return nil
}
