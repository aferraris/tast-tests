// Copyright 2022 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package login

import (
	"context"
	"time"

	"go.chromium.org/tast-tests/cros/local/chrome"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto/faillog"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto/lockscreen"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto/nodewith"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto/quicksettings"
	"go.chromium.org/tast-tests/cros/local/chrome/userutil"
	"go.chromium.org/tast-tests/cros/local/input"
	"go.chromium.org/tast-tests/cros/local/session"
	"go.chromium.org/tast/core/ctxutil"
	"go.chromium.org/tast/core/errors"
	"go.chromium.org/tast/core/testing"
)

func init() {
	testing.AddTest(&testing.Test{
		Func:         SignOutAll,
		LacrosStatus: testing.LacrosVariantUnneeded,
		Desc:         "Verify that the sign in page shows after signing out multi-users by clicking button from uber tray",
		Contacts: []string{
			"cros-lurs@google.com",
			"awendy@google.com",
			"chromeos-sw-engprod@google.com",
		},
		BugComponent: "b:1207311", // ChromeOS > Software > Commercial (Enterprise) > Identity > LURS
		Attr:         []string{"group:golden_tier", "group:medium_low_tier", "group:hardware", "group:complementary", "group:hw_agnostic"},
		SoftwareDeps: []string{"chrome"},
		Vars:         []string{"ui.signinProfileTestExtensionManifestKey"},
		Timeout:      3*chrome.LoginTimeout + 3*time.Minute,
	})
}

// SignOutAll verifies whether the login page is displayed after logging out of multiple users.
func SignOutAll(ctx context.Context, s *testing.State) {
	creds := []chrome.Creds{
		{User: "testuser1@gmail.com", Pass: "test pass 1"},
		{User: "testuser2@gmail.com", Pass: "test pass 2"},
		{User: "testuser3@gmail.com", Pass: "test pass 3"},
	}

	if err := userutil.CreateUser(ctx, creds[0].User, creds[0].Pass); err != nil {
		s.Fatal("Failed to create new user 1: ", err)
	}
	if err := userutil.CreateUser(ctx, creds[1].User, creds[1].Pass, chrome.KeepState()); err != nil {
		s.Fatal("Failed to create new user 2: ", err)
	}
	if err := userutil.CreateUser(ctx, creds[2].User, creds[2].Pass, chrome.KeepState()); err != nil {
		s.Fatal("Failed to create new user 3: ", err)
	}

	cleanupCtx := ctx
	ctx, cancel := ctxutil.Shorten(ctx, 5*time.Second)
	defer cancel()

	cr, err := chrome.New(ctx,
		chrome.ExtraArgs("--skip-force-online-signin-for-testing"),
		chrome.FakeLogin(creds[len(creds)-1]),
		chrome.KeepState(),
	)
	if err != nil {
		s.Fatal("Failed to login last user: ", err)
	}
	defer cr.Close(cleanupCtx)

	tconn, err := cr.TestAPIConn(ctx)
	if err != nil {
		s.Fatal("Failed to get Test API connection: ", err)
	}

	defer faillog.DumpUITreeOnError(ctx, s.OutDir(), s.HasError, tconn)

	currentUser := creds[len(creds)-1].User
	if err := signInAll(ctx, tconn, creds[:len(creds)-1], currentUser); err != nil {
		s.Fatal("Failed to sign in all user: ", err)
	}

	if err := signOutAllAndWait(ctx, tconn); err != nil {
		s.Fatal("Failed to sign out all user: ", err)
	}

	if cr, err = chrome.New(ctx,
		chrome.ExtraArgs("--skip-force-online-signin-for-testing"),
		chrome.NoLogin(),
		chrome.KeepState(),
		chrome.LoadSigninProfileExtension(s.RequiredVar("ui.signinProfileTestExtensionManifestKey")),
	); err != nil {
		s.Fatal("Failed to start Chrome: ", err)
	}
	defer cr.Close(cleanupCtx)

	if tconn, err = cr.SigninProfileTestAPIConn(ctx); err != nil {
		s.Fatal("Failed to re-establish test API connection: ", err)
	}

	if err := signedOut(ctx, tconn, creds); err != nil {
		s.Fatal("Failed to verify user signed out: ", err)
	}
}

// signInAll executes multiple sign-in via quick settings.
// aboutToSignIn are users who haven't signed-in.
// currentUser is the last signed-in user.
func signInAll(ctx context.Context, tconn *chrome.TestConn, aboutToSignIn []chrome.Creds, currentUser string) error {
	var (
		ui            = uiauto.New(tconn)
		powerButton   = nodewith.HasClass("PowerButtonContainer")
		signInAnother = nodewith.NameStartingWith("Sign in another user").HasClass("AddUserButton")
		multiSignIn   = nodewith.Name("Multiple sign-in").HasClass("Label")
	)

	kb, err := input.Keyboard(ctx)
	if err != nil {
		return errors.Wrap(err, "failed to create the keyboard")
	}
	defer kb.Close(ctx)

	// Proceed to do sign-in only on the users who haven't signed-in.
	for _, cred := range aboutToSignIn {
		if err := quicksettings.Show(ctx, tconn); err != nil {
			return errors.Wrap(err, "failed to show quick settings")
		}

		// The current session must be the last signed-in user.
		userIcon := nodewith.NameContaining(currentUser).HasClass("MenuItemView")
		if err := uiauto.Combine("sign in another user",
			ui.LeftClick(powerButton),
			ui.LeftClick(userIcon),
			ui.LeftClick(signInAnother),
			// A prompt of "Sign in another user" will show when signing in the first non-owner user and won't pop-up again after dismissing it.
			uiauto.IfSuccessThen(ui.WithTimeout(5*time.Second).WaitUntilExists(multiSignIn), kb.AccelAction("Enter")),
		)(ctx); err != nil {
			return err
		}

		if _, err := lockscreen.WaitState(ctx, tconn, func(st lockscreen.State) bool { return st.ReadyForPassword }, 10*time.Second); err != nil {
			return errors.Wrap(err, "failed to wait for login screen")
		}

		if err := ui.LeftClick(nodewith.NameContaining(cred.User).HasClass("LoginUserView::TapButton"))(ctx); err != nil {
			return errors.Wrap(err, "failed to select the account")
		}

		pwdInputField := nodewith.Name("Password for " + cred.User).HasClass("LoginPasswordView::LoginTextfield")
		if err := uiauto.Combine("input password and sign in",
			ui.WithTimeout(30*time.Second).LeftClickUntil(pwdInputField, ui.WithTimeout(5*time.Second).WaitUntilExists(pwdInputField.Focused())),
			kb.TypeAction(cred.Pass),
			kb.AccelAction("Enter"),
			ui.WaitUntilGone(pwdInputField),
		)(ctx); err != nil {
			return err
		}

		// Update the last signed-in user.
		currentUser = cred.User
	}

	return nil
}

// signOutAllAndWait sign out all user and wait for session stopped.
func signOutAllAndWait(ctx context.Context, tconn *chrome.TestConn) error {
	if err := quicksettings.Show(ctx, tconn); err != nil {
		return errors.Wrap(err, "failed to click Uber tray")
	}

	sm, err := session.NewSessionManager(ctx)
	if err != nil {
		return errors.Wrap(err, "failed to connect to session manager")
	}

	const state = "stopped"
	sw, err := sm.WatchSessionStateChanged(ctx, state)
	if err != nil {
		return errors.Wrap(err, "failed to watch for D-Bus signals")
	}
	defer sw.Close(ctx)

	ui := uiauto.New(tconn)

	powerButton := nodewith.HasClass("PowerButtonContainer")

	if err := ui.LeftClick(powerButton)(ctx); err != nil {
		return errors.Wrap(err, "failed to click on the powerButton")
	}

	signOutButton := nodewith.Name("Sign out")

	if err := uiauto.New(tconn).MouseMoveTo(signOutButton, 0)(ctx); err != nil {
		return errors.Wrap(err, "failed to move to the location of sign out button")
	}

	// TestConn will be closed once we logged out, which causes connection error if we click the button by `uiauto.Context`,
	// therefore, we avoid using `tconn` here and use input.Mouse instead.
	mouse, err := input.Mouse(ctx)
	if err != nil {
		return errors.Wrap(err, "failed to create mouse")
	}
	defer mouse.Close(ctx)

	if err := mouse.Click(); err != nil {
		return errors.Wrap(err, "failed to click sign out button")
	}

	testing.ContextLogf(ctx, "Waiting for SessionStateChanged %q D-Bus signal from session_manager", state)
	select {
	case <-sw.Signals:
		testing.ContextLog(ctx, "Got SessionStateChanged signal")
	case <-ctx.Done():
		return errors.Wrap(ctx.Err(), "didn't get SessionStateChanged signal")
	}
	return nil
}

// signedOut verifies all users are signed out.
func signedOut(ctx context.Context, tconn *chrome.TestConn, creds []chrome.Creds) error {
	ui := uiauto.New(tconn)

	for _, user := range creds {
		if err := uiauto.Combine("check the password field existed",
			ui.LeftClick(nodewith.NameContaining(user.User).HasClass("LoginUserView::TapButton")),
			ui.WaitUntilExists(nodewith.Name("Password for "+user.User).HasClass("LoginPasswordView::LoginTextfield")),
		)(ctx); err != nil {
			return err
		}
	}
	return nil
}
