// Copyright 2023 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package shortcutcustomization contains local Tast tests that verifies
// shortcut customization app.
package shortcutcustomization
