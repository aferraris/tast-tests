// Copyright 2020 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package policy

import (
	"context"
	"time"

	"go.chromium.org/tast-tests/cros/common/fixture"
	"go.chromium.org/tast-tests/cros/common/pci"
	"go.chromium.org/tast-tests/cros/common/policy"
	"go.chromium.org/tast-tests/cros/common/policy/fakedms"
	"go.chromium.org/tast-tests/cros/local/chrome"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto/faillog"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto/lockscreen"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto/nodewith"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto/quicksettings"
	"go.chromium.org/tast-tests/cros/local/input"
	"go.chromium.org/tast-tests/cros/local/policyutil"
	"go.chromium.org/tast-tests/cros/local/policyutil/fixtures"
	"go.chromium.org/tast/core/testing"
)

func init() {
	testing.AddTest(&testing.Test{
		Func:         AllowScreenLock,
		LacrosStatus: testing.LacrosVariantUnneeded,
		Desc:         "Behavior of AllowScreenLock policy, checking whether the screen can be locked after setting the policy",
		Contacts: []string{
			"cros-lurs@google.com",
			"antrim@google.com",
			"emaxx@google.com",
		},
		BugComponent: "b:1277523",
		SoftwareDeps: []string{"chrome"},
		Attr:         []string{"group:golden_tier", "group:hw_agnostic"},
		Fixture:      fixture.ChromePolicyLoggedInLockscreen,
		SearchFlags: []*testing.StringPair{
			pci.SearchFlag(&policy.AllowScreenLock{}, pci.VerifiedFunctionalityUI),
		},
	})
}

// AllowScreenLock tests the AllowScreenLock policy.
func AllowScreenLock(ctx context.Context, s *testing.State) {
	cr := s.FixtValue().(chrome.HasChrome).Chrome()
	fdms := s.FixtValue().(fakedms.HasFakeDMS).FakeDMS()

	// Open a keyboard device.
	kb, err := input.Keyboard(ctx)
	if err != nil {
		s.Fatal("Failed to open keyboard device: ", err)
	}
	defer kb.Close(ctx)

	for _, param := range []struct {
		name       string
		wantLocked bool                    // wantLocked is the wanted lock screen locked state after trying to lock the screen.
		value      *policy.AllowScreenLock // value is the value of the policy.
	}{
		{
			name:       "allow",
			wantLocked: true,
			value:      &policy.AllowScreenLock{Val: true},
		},
		{
			name:       "deny",
			wantLocked: false,
			value:      &policy.AllowScreenLock{Val: false},
		},
		{
			name:       "unset",
			wantLocked: true,
			value:      &policy.AllowScreenLock{Stat: policy.StatusUnset},
		},
	} {
		s.Run(ctx, param.name, func(ctx context.Context, s *testing.State) {
			defer faillog.DumpUITreeWithScreenshotOnError(ctx, s.OutDir(), s.HasError, cr, "ui_tree_"+param.name)

			// Perform cleanup.
			if err := policyutil.ResetChrome(ctx, fdms, cr); err != nil {
				s.Fatal("Failed to clean up: ", err)
			}

			// Update policies.
			if err := policyutil.ServeAndVerify(ctx, fdms, cr, []policy.Policy{param.value}); err != nil {
				s.Fatal("Failed to update policies: ", err)
			}

			// Connect to Test API.
			tconn, err := cr.TestAPIConn(ctx)
			if err != nil {
				s.Fatal("Failed to connect to test API: ", err)
			}

			// Try to lock the screen with keyboard shortcut.
			s.Log("Trying to lock the screen")
			if err := kb.Accel(ctx, "Search+L"); err != nil {
				s.Fatal("Failed to write events: ", err)
			}
			ui := uiauto.New(tconn)
			lockScreenSubmitNode := nodewith.Name("Submit").ClassName("ArrowButtonView")
			if param.wantLocked {
				if err := ui.WaitUntilExists(lockScreenSubmitNode)(ctx); err != nil {
					s.Error("Failed to find the lock screen submit button: ", err)
				}
			} else {
				if err := ui.EnsureGoneFor(lockScreenSubmitNode, 15*time.Second)(ctx); err != nil {
					s.Error("Lock screen appeared but it shouldn't: ", err)
				}
			}

			// Check if the logout button is shown.
			// If the lock screen is disabled, the button is not there.
			// If enabled, the screen is locked with the hotkey, but the system tray can be
			// opened on the lock screen as well, but the button should not be there as the
			// screen is already locked.
			if err := quicksettings.Show(ctx, tconn); err != nil {
				s.Fatal("Failed to open the system tray: ", err)
			}

			// Check if the power menu button is shown.
			if err := ui.WaitUntilExists(quicksettings.PowerMenuButton)(ctx); err != nil {
				s.Error("Failed to check the power menu button: ", err)
			}

			// Open the power menu button menu with a click on the button.
			if err := ui.LeftClick(quicksettings.PowerMenuButton)(ctx); err != nil {
				s.Error("Failed to find and click power menu button: ", err)
			}

			// Check the power menu button items the Lock shouldn't be there
			if err := uiauto.Combine("Check the power button items",
				ui.WaitUntilGone(nodewith.Name("Lock").ClassName("MenuItemView")),
				ui.WaitUntilExists(nodewith.Name("Restart").ClassName("MenuItemView")),
				ui.WaitUntilExists(nodewith.Name("Shut down").ClassName("MenuItemView")),
				ui.WaitUntilExists(nodewith.Name("Sign out").ClassName("MenuItemView")),
			)(ctx); err != nil {
				s.Error("Failed to check the power menu button items: ", err)
			}

			// Close the power menu button menu with a click on the button again.
			if err := ui.LeftClick(quicksettings.PowerMenuButton)(ctx); err != nil {
				s.Error("Failed to find and click power menu button: ", err)
			}

			// Check the power menu button menu disappeared and the menu items are
			// not available anymore.
			if err := uiauto.Combine("Check power button menu items are no available",
				ui.WaitUntilGone(nodewith.Name("Lock").ClassName("MenuItemView")),
				ui.WaitUntilGone(nodewith.Name("Restart").ClassName("MenuItemView")),
				ui.WaitUntilGone(nodewith.Name("Shut down").ClassName("MenuItemView")),
				ui.WaitUntilGone(nodewith.Name("Sign out").ClassName("MenuItemView")),
			)(ctx); err != nil {
				s.Error("Failed to check the power menu button items are still visible: ", err)
			}

			// Hide the quicksettings.
			if err := quicksettings.Hide(ctx, tconn); err != nil {
				s.Error("Failed to close the system tray: ", err)
			}

			// Check the hide was successful and the power menu button is not available.
			if err := ui.WaitUntilGone(nodewith.Name("Power menu").ClassName("Button"))(ctx); err != nil {
				s.Error("Failed system tray still visible: ", err)
			}

			// Wait until the lock state is the wanted value or until timeout.
			state, err := lockscreen.WaitState(ctx, tconn, func(st lockscreen.State) bool { return st.Locked == param.wantLocked }, 10*time.Second)
			if err != nil {
				s.Errorf("Failed to wait for screen lock state %t: %v  (last status %+v)", param.wantLocked, err, state)
			}

			// Unlock the screen if needed.
			if state.Locked {
				s.Log("Unlocking the screen with password")

				if err := lockscreen.UnlockWithPassword(ctx, tconn, fixtures.Username, fixtures.Password, kb, 10*time.Second, 30*time.Second); err != nil {
					s.Fatal("Failed to unlock the screen: ", err)
				}
			}
		})
	}
}
