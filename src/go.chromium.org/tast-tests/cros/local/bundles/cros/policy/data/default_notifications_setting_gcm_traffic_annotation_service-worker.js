// Copyright 2023 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// This file only exists to be registered as worker script by
// default_notifications_setting_gcm_traffic_annotation.html as is required
// for push notifications. Other than that it has no function.