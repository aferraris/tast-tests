// Copyright 2022 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package policy

import (
	"context"
	"time"

	"go.chromium.org/tast-tests/cros/common/fixture"
	"go.chromium.org/tast-tests/cros/common/pci"
	"go.chromium.org/tast-tests/cros/common/policy"
	"go.chromium.org/tast-tests/cros/common/policy/fakedms"
	"go.chromium.org/tast-tests/cros/local/chrome"
	"go.chromium.org/tast-tests/cros/local/chrome/browser"
	"go.chromium.org/tast-tests/cros/local/chrome/browser/browserfixt"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto/faillog"
	"go.chromium.org/tast-tests/cros/local/policyutil"
	"go.chromium.org/tast-tests/cros/local/policyutil/safesearch"
	"go.chromium.org/tast/core/ctxutil"
	"go.chromium.org/tast/core/testing"
)

func init() {
	testing.AddTest(&testing.Test{
		Func:         ForceYouTubeSafetyMode,
		LacrosStatus: testing.LacrosVariantExists,
		Desc:         "Test the behavior of deprecated ForceYouTubeSafetyMode policy: check if YouTube safe search is enabled based on the value of the policy",
		Contacts: []string{
			"cros-engprod-muc@google.com",
		},
		BugComponent: "b:1263917",
		SoftwareDeps: []string{"chrome"},
		Attr:         []string{"group:golden_tier", "group:hw_agnostic"},
		Params: []testing.Param{{
			Fixture: fixture.ChromePolicyLoggedIn,
			Val:     browser.TypeAsh,
		}, {
			Name:              "lacros",
			ExtraSoftwareDeps: []string{"lacros"},
			Fixture:           fixture.LacrosPolicyLoggedIn,
			Val:               browser.TypeLacros,
		}},
		// Loading two YouTube videos on slower devices can take a while (we observed subtests that took up to 40 seconds), thus give every subtest 1 minute to run.
		Timeout: 7 * time.Minute,
		SearchFlags: []*testing.StringPair{
			pci.SearchFlag(&policy.ForceYouTubeSafetyMode{}, pci.VerifiedFunctionalityJS),
			pci.SearchFlag(&policy.ForceYouTubeRestrict{}, pci.VerifiedFunctionalityJS),
		},
	})
}

func ForceYouTubeSafetyMode(ctx context.Context, s *testing.State) {
	cr := s.FixtValue().(chrome.HasChrome).Chrome()
	fdms := s.FixtValue().(fakedms.HasFakeDMS).FakeDMS()

	// Reserve ten seconds for cleanup.
	cleanupCtx := ctx
	ctx, cancel := ctxutil.Shorten(ctx, 10*time.Second)
	defer cancel()

	for _, param := range []struct {
		name                    string
		strongContentRestricted bool
		mildContentRestricted   bool
		value                   []policy.Policy
	}{
		{
			name:                    "enabled",
			strongContentRestricted: true,
			mildContentRestricted:   false,
			value:                   []policy.Policy{&policy.ForceYouTubeSafetyMode{Val: true}},
		},
		{
			name:                    "enabled_overwritten_by_ForceYouTubeRestrict_disabled",
			strongContentRestricted: false,
			mildContentRestricted:   false,
			value: []policy.Policy{
				&policy.ForceYouTubeSafetyMode{Val: true},
				&policy.ForceYouTubeRestrict{Val: safesearch.ForceYouTubeRestrictDisabled}},
		},
		{
			name:                    "enabled_overwritten_by_ForceYouTubeRestrict_strict",
			strongContentRestricted: true,
			mildContentRestricted:   true,
			value: []policy.Policy{
				&policy.ForceYouTubeSafetyMode{Val: true},
				&policy.ForceYouTubeRestrict{Val: safesearch.ForceYouTubeRestrictStrict}},
		},
		{
			name:                    "disabled",
			strongContentRestricted: false,
			mildContentRestricted:   false,
			value:                   []policy.Policy{&policy.ForceYouTubeSafetyMode{Val: false}},
		},
		{
			name:                    "disabled_overwritten_by_ForceYouTubeRestrict_moderate",
			strongContentRestricted: true,
			mildContentRestricted:   false,
			value: []policy.Policy{
				&policy.ForceYouTubeSafetyMode{Val: false},
				&policy.ForceYouTubeRestrict{Val: safesearch.ForceYouTubeRestrictModerate}},
		},
		{
			name:                    "disabled_overwritten_by_ForceYouTubeRestrict_strict",
			strongContentRestricted: true,
			mildContentRestricted:   true,
			value: []policy.Policy{
				&policy.ForceYouTubeSafetyMode{Val: false},
				&policy.ForceYouTubeRestrict{Val: safesearch.ForceYouTubeRestrictStrict}},
		},
		{
			name:                    "unset",
			strongContentRestricted: false,
			mildContentRestricted:   false,
			value:                   []policy.Policy{&policy.ForceYouTubeSafetyMode{Stat: policy.StatusUnset}},
		},
	} {
		s.Run(ctx, param.name, func(ctx context.Context, s *testing.State) {
			if err := policyutil.ResetChrome(ctx, fdms, cr); err != nil {
				s.Fatal("Failed to clean up: ", err)
			}

			if err := policyutil.ServeAndVerify(ctx, fdms, cr, param.value); err != nil {
				s.Fatal("Failed to update policies: ", err)
			}

			br, closeBrowser, err := browserfixt.SetUp(ctx, cr, s.Param().(browser.Type))
			if err != nil {
				s.Fatal("Failed to setup chrome: ", err)
			}
			defer closeBrowser(cleanupCtx)

			defer faillog.DumpUITreeWithScreenshotOnError(cleanupCtx, s.OutDir(), s.HasError, cr, "ui_tree_"+param.name)

			if err := safesearch.TestYouTubeRestrictedMode(ctx, br, param.strongContentRestricted, param.mildContentRestricted); err != nil {
				s.Error("Failed to verify YouTube content restriction: ", err)
			}
		})
	}
}
