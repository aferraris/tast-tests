// Copyright 2021 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package policy contains functionality to return policy values for
// tests that excersice dlp restrictions.
package policy

import (
	"go.chromium.org/tast-tests/cros/common/policy"
)

// PopulateClipboardBlockAllURLsPolicy returns a clipboard policy blocking clipboard from source to all destination urls.
func PopulateClipboardBlockAllURLsPolicy(source string) []policy.Policy {
	return []policy.Policy{&policy.DataLeakPreventionRulesList{
		Val: []*policy.DataLeakPreventionRulesListValue{
			{
				Name:        "Disable copy and paste of confidential content in any destination",
				Description: "User should not be able to copy and paste confidential content in any destination",
				Sources: &policy.DataLeakPreventionRulesListValueSources{
					Urls: []string{
						source,
					},
				},
				Destinations: &policy.DataLeakPreventionRulesListValueDestinations{
					Urls: []string{
						"*",
					},
				},
				Restrictions: []*policy.DataLeakPreventionRulesListValueRestrictions{
					{
						Class: "CLIPBOARD",
						Level: "BLOCK",
					},
				},
			},
		},
	},
	}
}

// PopulateClipboardWarnPolicy returns a clipboard dlp policy warning when clipboard content is copied and pasted from source to destination.
func PopulateClipboardWarnPolicy(source, destination string) []policy.Policy {
	return []policy.Policy{&policy.DataLeakPreventionRulesList{
		Val: []*policy.DataLeakPreventionRulesListValue{
			{
				Name:        "Warn about copy and paste of confidential content in restricted destination",
				Description: "User should be warned when coping and pasting confidential content in restricted destination",
				Sources: &policy.DataLeakPreventionRulesListValueSources{
					Urls: []string{
						source,
					},
				},
				Destinations: &policy.DataLeakPreventionRulesListValueDestinations{
					Urls: []string{
						destination,
					},
				},
				Restrictions: []*policy.DataLeakPreventionRulesListValueRestrictions{
					{
						Class: "CLIPBOARD",
						Level: "WARN",
					},
				},
			},
		},
	},
	}
}

// PopulateClipboardBlockPolicy returns a clipboard dlp policy blocking when clipboard content is copied and pasted from source to destination.
func PopulateClipboardBlockPolicy(source, destination string) []policy.Policy {
	return []policy.Policy{&policy.DataLeakPreventionRulesList{
		Val: []*policy.DataLeakPreventionRulesListValue{
			{
				Name:        "Disable copy and paste of confidential content in restricted destination",
				Description: "User should not be able to copy and paste confidential content in restricted destination",
				Sources: &policy.DataLeakPreventionRulesListValueSources{
					Urls: []string{
						source,
					},
				},
				Destinations: &policy.DataLeakPreventionRulesListValueDestinations{
					Urls: []string{
						destination,
					},
				},
				Restrictions: []*policy.DataLeakPreventionRulesListValueRestrictions{
					{
						Class: "CLIPBOARD",
						Level: "BLOCK",
					},
				},
			},
		},
	},
	}
}

// PopulateClipboardBlockArcPolicy returns a clipboard dlp policy blocking clipboard from source to ARC component.
func PopulateClipboardBlockArcPolicy(source string) []policy.Policy {
	return []policy.Policy{&policy.DataLeakPreventionRulesList{
		Val: []*policy.DataLeakPreventionRulesListValue{
			{
				Name:        "Disable copy and paste of confidential content from site to ARC",
				Description: "User should not be able to copy and paste confidential content from site to ARC",
				Sources: &policy.DataLeakPreventionRulesListValueSources{
					Urls: []string{
						source,
					},
				},
				Destinations: &policy.DataLeakPreventionRulesListValueDestinations{
					Components: []string{
						"ARC",
					},
				},
				Restrictions: []*policy.DataLeakPreventionRulesListValueRestrictions{
					{
						Class: "CLIPBOARD",
						Level: "BLOCK",
					},
				},
			},
		},
	},
	}
}
