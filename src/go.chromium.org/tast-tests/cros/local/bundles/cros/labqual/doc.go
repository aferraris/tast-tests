// Copyright 2023 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package labqual contains local functions to support labqual tests.
// Reviews for this package can be sent to chrome-fleet-infra-sw-reviewers@ (before tast-owners@ review).
package labqual