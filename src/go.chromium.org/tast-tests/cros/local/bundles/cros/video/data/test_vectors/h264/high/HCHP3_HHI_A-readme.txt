Stream Name:             HCHP3_HHI_A
Stream Source:           HHI
Profile:		 High
Video Sequence:          Rugby
Video Resolution:        SD (720x576)
Video Frames:            250
Frame/Field Coding:      Adaptive Frame/Field
Entropy Coding:          CABAC
Slice Types:             I,P,B
Structure:               Hierarchical, GOP Size 16
Slices per Picture:      1  
Ref Pic List Reorder:    Yes
Adapt Ref Pic Mark:      Yes
Constrained Intra Pred:  Off (Unconstrained)  
Weighted Pred (P):       Off  
Weighted Bipred (B):     Off  
Direct Prediction:       Spatial
Direct 8x8 Inference:    On  
Loop Filter:             On  
Number Slice Groups:     1
Long-term Ref Frames:    No
Arbitrary Slice Order:   No
Redundant Slices:        No
