Files in the folder
Freh11_Readme.txt         This file
Freh11.264                Bitstream 
Freh11_dec.YUV            Reconstructed YUV file
Freh11.trc                Decoder trace file
----------------------------------------------------------------------
Encoder configuration:
Stream Name:             Freh11
Stream Source:           Broadcom
Video Sequence:          Foreman  
Video Resolution:        CIF  
Video Frames:            100
Profile:                 High 
Frame/Field Coding:      Field Coded
Entropy Coding:          CABAC
GOP Structure:           I B B P B B P...
Slices per Picture:      1  
Conformance Point:       JVT-L047d11  
POC Type:                0  
Number Ref Frames:       2  
IDR:                     First frame  
Ref Pic List Reorder:    No  
Adapt Ref Pic Mark:      No
Constrained Intra Pred:  No
Weighted Pred (P):       No
Weighted Bipred (B):     No
Direct Prediction:       Temporal
Direct 8x8 Inference:    On
Loop Filter:             On
SEI:                     No  
VUI:                     No  
Number Slice Groups:     1
Long-term Ref Frames:    No
MMCO:                    No
Arbitrary Slice Order:   No
Redundant Slices:        No
PCM Coded MB:		 No
Quantization Matrices:   Default
