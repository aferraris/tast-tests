Stream Name:             HVLCPFF0_Sony_B  
Stream Source:           Sony  
Video Sequence:          Mobile  
Video Resolution:        720x480  
Video Frames:            19 
Frame/Field Coding:      PAFF  
Entropy Coding:          CAVLC  
Slice Types:             IBBP  
I Period:                5  
Slices per Picture:      1  
Conformance Point:       
POC Type:                0  
Number Ref Frames:       5  
IDR:                     First only  
Ref Pic List Reorder:    No  
Adapt Ref Pic Mark:      Off  
Constrained Intra Pred:  Off (Unconstrained)  
Weighted Pred (P):       Off  
Weighted Bipred (B):     Off  
Direct Prediction:       Spatial  
Direct 8x8 Inference:    On  
Loop Filter:             Off 
SEI:                     No  
VUI:                     No  
Number Slice Groups:     1
Long-term Ref Frames:    No
MMCO:                    No
Arbitrary Slice Order:   No
Redundant Slices:        No
8x8 Transform:           8x8,4x4 adaptive
QMatrix:                 default matrix at SPS
