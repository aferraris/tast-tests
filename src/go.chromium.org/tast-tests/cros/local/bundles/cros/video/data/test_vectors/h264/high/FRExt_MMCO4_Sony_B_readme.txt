Stream Name:  FRExt_MMCO4_Sony_B
Stream Source:  Sony  

Profile         :High Profile
Level           :3.1
Video Sequence:  flower garden
Video Resolution:  352x288
Video Frames:  60
Frame/Field Coding:  Frame
MBAFF: No
Entropy Coding:  CABAC  
Slice Types:  IPB
I Period:  16
Slices per Picture:  1
Conformance Point:  JVT-M050d4

POC Type:  0  
Number Reference Frames:  4
IDR:  First frame  

Transform Mode:Adaptive 8x8/4x4
Sequence Qmatrix:On
Picture Qmatrix:Off

Ref Pic List Reordering:  No
Long-term Ref Frames: Yes
MMCO: Yes

Constrained Intra Prediction:  Off (Unconstrained)  
Weighted Prediction (P):  Off  
Weighted Biprediction (B):  Off  
Direct Prediction:  Spatial  
Direct 8x8 Inference:  Off  
Loop Filter:  On  
Chroma QP Offset:On
SEI:  No  
VUI:  No  
Number Slice Groups: 1
Arbitrary Slice Order: No
Redundant Slices: No
