Bitsteam Name		:FREXT01_JVC_D
Profile			:High Profile
Level			:3.1
Chroma Format		:4:2:0
Bit Depth		:8
Sequence		:Foreman
Width			:352
Height			:288
Num of Frames		:16

Entropy Coding		:CABAC
Coding Type		:MBAFF
IDR Intervals		:1st I picture only
POC type		:0
Slice per Pic		:1
Mixed Slice Type	:OFF

No of B Frms		:2
Num of Ref Frms		:2
Ref Pic List Reordering	:OFF

Direct Type		:Spatial
Direct8x8 Inference Flag:1

Loop Filter		:ON
Constrained Intra Flag	:OFF

Transform Mode		:Adaptive 8x8/4x4
Sequence Qmatrix	:OFF
Picture Qmatrix		:OFF

Weighted Pred(P)	:OFF
Weighted Pred(B)	:OFF
Chroma Qp Offset	:OFF
MMCO			:OFF
VUI			:OFF
SEI			:OFF
HRD Parameters		:OFF

Conformance Point	:AVC 3rd Edition(w6540)
Test Point		:mb_type=0 and CBP=0 at MBAFF




