// Copyright 2021 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package typec

import (
	"context"

	"go.chromium.org/tast-tests/cros/common/testexec"
	"go.chromium.org/tast-tests/cros/local/chrome"
	"go.chromium.org/tast-tests/cros/local/typecutils"
	"go.chromium.org/tast/core/testing"
)

func init() {
	testing.AddTest(&testing.Test{
		Func:         ModeSuspend,
		LacrosStatus: testing.LacrosVariantNeeded,
		Desc:         "Checks USB Type C mode switch behaviour with a Thunderbolt dock during suspend/resume",
		Contacts:     []string{"chromeos-usb-champs@google.com", "pmalani@chromium.org"},
		BugComponent: "b:958036",
		Attr:         []string{"group:typec", "typec_lab"},
		SoftwareDeps: []string{"chrome"},
		Vars:         []string{"ui.signinProfileTestExtensionManifestKey"},
		Data:         []string{"testcert.p12"},
	})
}

// ModeSuspend does the following:
// - Login.
// - Validate that the Thunderbolt dock (and connected display) is enumerated correctly.
// - Enter Suspend (S0ix).
// - Validate that the system entered and exited suspend successfully.
// - Validate that the Thunderbolt dock (and connected display) is still enumerated correctly.
//
// This test requires the following H/W topology to run.
//
//	  DUT ------> Thunderbolt3 (>= Titan Ridge) dock -----> DP monitor.
//	(USB4)
func ModeSuspend(ctx context.Context, s *testing.State) {
	// This check is for test executions which take place on
	// CQ (where TBT peripherals aren't connected).
	port, err := typecutils.CheckPortsForTBTPartner(ctx)
	if err != nil {
		s.Fatal("Failed to determine TBT device from PD identity: ", err)
	}

	if port == -1 {
		s.Fatal("No TBT device connected to DUT")
	}

	// Get to the Chrome login screen.
	loadSigninProfileOption := chrome.LoadSigninProfileExtension(s.RequiredVar("ui.signinProfileTestExtensionManifestKey"))
	cr, err := chrome.New(ctx, chrome.DeferLogin(), loadSigninProfileOption)
	if err != nil {
		s.Fatal("Failed to start Chrome at login screen: ", err)
	}

	testConn, err := cr.SigninProfileTestAPIConn(ctx)
	if err != nil {
		s.Fatal("Failed to get Test API connection: ", err)
	}

	if err := typecutils.EnablePeripheralDataAccess(ctx, s.DataPath("testcert.p12")); err != nil {
		s.Fatal("Failed to enable peripheral data access setting: ", err)
	}

	// Peripherals will only switch to Thunderbolt mode once the user logs in.
	if err := cr.ContinueLogin(ctx); err != nil {
		s.Fatal("Failed to login: ", err)
	}

	if err := typecutils.CheckTBTAndDP(ctx, testConn); err != nil {
		s.Fatal("Failed to verify TBT & DP after login: ", err)
	}

	if err := testexec.CommandContext(ctx, "powerd_dbus_suspend", "--suspend_for_sec=10").Run(); err != nil {
		s.Fatal("Failed to perform system suspend: ", err)
	}

	if err := cr.Reconnect(ctx); err != nil {
		s.Fatal("Failed to reconnect to the Chrome session: ", err)
	}

	testConn, err = cr.SigninProfileTestAPIConn(ctx)
	if err != nil {
		s.Fatal("Failed to re-establish the Test API connection: ", err)
	}

	if err := typecutils.CheckTBTAndDP(ctx, testConn); err != nil {
		s.Fatal("Failed to verify TBT & DP after resume: ", err)
	}
}
