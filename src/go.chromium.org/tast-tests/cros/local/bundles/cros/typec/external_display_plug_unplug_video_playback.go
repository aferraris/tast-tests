// Copyright 2022 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package typec

import (
	"context"
	"net/http"
	"net/http/httptest"
	"regexp"
	"time"

	"go.chromium.org/tast-tests/cros/common/cswitch"
	"go.chromium.org/tast-tests/cros/common/testexec"
	"go.chromium.org/tast-tests/cros/common/usbutils"
	"go.chromium.org/tast-tests/cros/local/audio"
	"go.chromium.org/tast-tests/cros/local/chrome"
	"go.chromium.org/tast-tests/cros/local/input"
	"go.chromium.org/tast-tests/cros/local/typecutils"
	"go.chromium.org/tast/core/ctxutil"
	"go.chromium.org/tast/core/testing"
)

type displayFunctionalities struct {
	displayType string
	cSwitchON   string
}

func init() {
	testing.AddTest(&testing.Test{
		Func:         ExternalDisplayPlugUnplugVideoPlayback,
		LacrosStatus: testing.LacrosVariantUnneeded,
		Desc:         "Verifies HDMI plug/unplug using USB type-C adapter during audio/video playback",
		Contacts:     []string{"intel.chrome.automation.team@intel.com", "ambalavanan.m.m@intel.com"},
		BugComponent: "b:157291", // ChromeOS > External > Intel
		SoftwareDeps: []string{"chrome"},
		Attr:         []string{"group:typec", "group:intel-cswitch-set1"},
		Data:         []string{"bear-320x240.h264.mp4", "video.html", "playback.js"},
		Vars:         []string{"typec.cSwitchPort", "typec.domainIP"},
		Fixture:      "chromeLoggedIn",
		Params: []testing.Param{{
			Name: "hdmi",
			Val: displayFunctionalities{
				displayType: usbutils.TypeCHDMI,
				// The Type-C HDMI is connected to C-Switch in P1 as per the intel_cswitch_set1 suite setup.
				cSwitchON: "1",
			},
			Timeout: 5 * time.Minute,
		}, {
			Name: "dp",
			Val: displayFunctionalities{
				displayType: usbutils.TypeCDP,
				// The Type-C DP is connected to C-Switch in P2 as per the intel_cswitch_set1 suite setup.
				cSwitchON: "2",
			},
			Timeout: 5 * time.Minute,
		}},
	})
}

func ExternalDisplayPlugUnplugVideoPlayback(ctx context.Context, s *testing.State) {
	cr := s.FixtValue().(chrome.HasChrome).Chrome()
	cleanupCtx := ctx
	ctx, cancel := ctxutil.Shorten(ctx, 20*time.Second)
	defer cancel()

	// Test Params.
	testParms := s.Param().(displayFunctionalities)

	// Config file which contains expected values of USB4 parameters.
	const jsonTestConfig = "test_config.json"

	cswitchVar := testParms.cSwitchON
	if cswitchON, ok := s.Var("typec.cSwitchPort"); ok {
		cswitchVar = cswitchON
	}
	// IP address of Tqc server hosting device.
	domainIP := s.RequiredVar("typec.domainIP")

	tconn, err := cr.TestAPIConn(ctx)
	if err != nil {
		s.Fatal("Failed to create test API connection: ", err)
	}

	kb, err := input.Keyboard(ctx)
	if err != nil {
		s.Fatal("Failed to initialize keyboard input: ", err)
	}

	// Create C-Switch session that performs hot plug-unplug on TBT device.
	sessionID, err := cswitch.CreateSession(ctx, domainIP)
	if err != nil {
		s.Fatal("Failed to create session: ", err)
	}

	const cSwitchOFF = "0"
	defer func(ctx context.Context) {
		if err := cswitch.ToggleCSwitchPort(ctx, sessionID, cSwitchOFF, domainIP); err != nil {
			s.Fatal("Failed to disable c-switch port: ", err)
		}
		if err := cswitch.CloseSession(ctx, sessionID, domainIP); err != nil {
			s.Log("Failed to close session: ", err)
		}
	}(cleanupCtx)

	if err := cswitch.ToggleCSwitchPort(ctx, sessionID, cswitchVar, domainIP); err != nil {
		s.Fatal("Failed to enable c-switch port: ", err)
	}
	numberOfDisplay := 1
	spec := usbutils.DisplaySpec{
		NumberOfDisplays: &numberOfDisplay,
		DisplayType:      testParms.displayType,
	}
	if err := usbutils.ExternalDisplayDetectionForLocal(ctx, spec); err != nil {
		s.Fatal("Failed to check for connected external display: ", err)
	}

	srv := httptest.NewServer(http.FileServer(s.DataFileSystem()))
	defer srv.Close()
	url := srv.URL + "/video.html"
	conn, err := cr.NewConn(ctx, url)
	if err != nil {
		s.Fatal("Failed to load video_with_rounded_corners.html: ", err)
	}

	display := typecutils.SwitchWindowToDisplay(ctx, tconn, kb, true)
	if err := display(ctx); err != nil {
		s.Fatal("Failed to switch windows to display: ", err)
	}

	videoFile := "bear-320x240.h264.mp4"
	if err := conn.Call(ctx, nil, "playUntilEnd", videoFile, true); err != nil {
		s.Fatal("Failed to play video: ", err)
	}

	expectedAudioOuputNode := "HDMI"
	cras, err := audio.NewCras(ctx)
	if err != nil {
		s.Fatal("Failed to create Cras object: ", err)
	}

	// Get current audio output device info.
	deviceName, deviceType, err := cras.SelectedOutputDevice(ctx)
	if err != nil {
		s.Fatal("Failed to get the selected audio device: ", err)
	}

	if deviceType != expectedAudioOuputNode {
		if err := cras.SetActiveNodeByType(ctx, expectedAudioOuputNode); err != nil {
			s.Fatalf("Failed to select active device %s: %v", expectedAudioOuputNode, err)
		}
		deviceName, deviceType, err = cras.SelectedOutputDevice(ctx)
		if err != nil {
			s.Fatal("Failed to get the selected audio device: ", err)
		}
		if deviceType != expectedAudioOuputNode {
			s.Fatalf("Failed to set the audio node type: got %q; want %q", deviceType, expectedAudioOuputNode)
		}
	}
	out, err := testexec.CommandContext(ctx, "cras_test_client").Output()
	if err != nil {
		s.Fatal("Failed to exceute cras_test_client command: ", err)
	}
	re := regexp.MustCompile(`yes.*HDMI.*\d\*`)
	if !re.MatchString(string(out)) {
		s.Fatal("Failed to select HDMI as output audio node")
	}

	if err := typecutils.VerifyAudioRoute(ctx, deviceName); err != nil {
		s.Fatalf("Failed to verify audio routing through %q: %v", expectedAudioOuputNode, err)
	}

	if err := cswitch.ToggleCSwitchPort(ctx, sessionID, cSwitchOFF, domainIP); err != nil {
		s.Fatal("Failed to disable c-switch port: ", err)
	}
}
