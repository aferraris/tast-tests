// Copyright 2023 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package metrics contains local Tast tests that check the metrics rust bindings of ChromeOS.
package metrics
