// Copyright 2022 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package nearbyshare

import (
	"context"
	"strconv"

	nearbycommon "go.chromium.org/tast-tests/cros/common/cros/nearbyshare"
	"go.chromium.org/tast-tests/cros/local/chrome"
	"go.chromium.org/tast-tests/cros/local/chrome/nearbyshare"
	"go.chromium.org/tast/core/testing"
)

func init() {
	testing.AddTest(&testing.Test{
		Func:         OnboardingVisibilityPageUI,
		LacrosStatus: testing.LacrosVariantUnneeded,
		Desc:         "Checks that Nearby Share can be enabled from the visibility selection page of onboarding workflow",
		Contacts: []string{
			"chromeos-cross-device-eng@google.com",
			"chromeos-sw-engprod@google.com",
			"pushi@google.com",
		},
		BugComponent: "b:1131838",
		// Use this variable to preserve user accounts on the DUT when running locally,
		// i.e. tast run -var=keepState=true <dut> nearbyshare.OnboardingSinglePageUI
		Vars:         []string{nearbycommon.KeepStateVar},
		Attr:         []string{"group:cross-device", "cross-device_nearbyshare"},
		SoftwareDeps: []string{"chrome"},
	})
}

// OnboardingVisibilityPageUI tests that we can enable Nearby Share in the visibility selection page of onboarding workflow.
func OnboardingVisibilityPageUI(ctx context.Context, s *testing.State) {
	opts := []chrome.Option{
		// Enable the feature flag for nearby one-page onboarding workflow
		// TODO(crbug.com/1265562): Remove after we fully launch the feature
		chrome.EnableFeatures("NearbySharingOnePageOnboarding"),
		chrome.ExtraArgs("--nearby-share-verbose-logging"),
	}
	if val, ok := s.Var(nearbycommon.KeepStateVar); ok {
		b, err := strconv.ParseBool(val)
		if err != nil {
			s.Fatalf("Unable to convert %v var to bool: %v", nearbycommon.KeepStateVar, err)
		}
		if b {
			opts = append(opts, chrome.KeepState())
		}
	}
	cr, err := chrome.New(
		ctx,
		opts...,
	)
	if err != nil {
		s.Fatal("Failed to start Chrome: ", err)
	}

	tconn, err := cr.TestAPIConn(ctx)
	if err != nil {
		s.Fatal("Creating test API connection failed: ", err)
	}

	if err := nearbyshare.EnableNearbyShareInVisibilitySelectionPage(ctx, tconn, cr); err != nil {
		s.Fatal("Failed to enable Nearby Share in visibility selection page of onboarding workflow: ", err)
	}
}
