// Copyright 2024 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package camera

import (
	"context"
	"time"

	"go.chromium.org/tast-tests/cros/local/camera/pnp"
	"go.chromium.org/tast-tests/cros/local/chrome/ash"
	"go.chromium.org/tast-tests/cros/local/chrome/browser/browserfixt"
	"go.chromium.org/tast-tests/cros/local/power"
	powersetup "go.chromium.org/tast-tests/cros/local/power/setup"
	"go.chromium.org/tast/core/ctxutil"
	"go.chromium.org/tast/core/testing"
)

const (
	initTimePNPIdle = 1 * time.Minute
)

type pnpIdleParams struct {
	chromeLogin bool
}

func init() {
	testing.AddTest(&testing.Test{
		Func:         PNPIdle,
		LacrosStatus: testing.LacrosVariantExists,
		Desc:         "Collect power metrics when device is in idle",
		Contacts:     []string{"chromeos-camera-eng@google.com", "esker@chromium.org"},
		BugComponent: "b:167281", // ChromeOS > Platform > Technologies > Camera
		Attr:         []string{"group:crosbolt", "crosbolt_perbuild"},
		SoftwareDeps: []string{"chrome"},
		Timeout:      initTimePNPIdle + pnp.PNPTimeParams.Total + power.RecorderTimeout,
		Params: []testing.Param{{
			Name:    "chrome_login_ash",
			Fixture: pnp.StablePowerAsh,
			Val: pnpIdleParams{
				chromeLogin: true,
			},
		}, {
			Name:    "chrome_login_lacros",
			Fixture: pnp.StablePowerLacros,
			Val: pnpIdleParams{
				chromeLogin: true,
			},
		}, {
			Name:    "no_window_lacros",
			Fixture: pnp.StablePowerLacros,
			Val: pnpIdleParams{
				chromeLogin: false,
			},
		}},
	})
}

func PNPIdle(ctx context.Context, s *testing.State) {
	// Reserve some time for the cleanup, even if it fails due to ctx timeout.
	cleanupCtx := ctx
	ctx, cancel := ctxutil.Shorten(ctx, 10*time.Second)
	defer cancel()

	pnpRoutine := pnp.Routine{}
	defer pnpRoutine.Close(cleanupCtx)
	if err := pnp.Cooldown(ctx); err != nil {
		s.Fatal("Failed to run pnp cooldown routine: ", err)
	}

	testing.ContextLog(ctx, "[Start Work Phase]")
	if s.Param().(pnpIdleParams).chromeLogin {
		browserType := s.FixtValue().(powersetup.PowerUIFixtureData).Bt
		cr := s.FixtValue().(powersetup.PowerUIFixtureData).Cr

		// Open a window with about:blank tab on the target browser.
		conn, _, cleanup, err := browserfixt.SetUpWithURL(ctx, cr, browserType, "about:blank")
		if err != nil {
			s.Fatal("Failed to open a blank new tab: ", err)
		}
		defer cleanup(cleanupCtx)
		defer conn.Close()

		tconn, err := cr.TestAPIConn(ctx)
		if err != nil {
			s.Fatal("Failed to get test API connection: ", err)
		}

		w, err := ash.WaitForAnyWindow(ctx, tconn, ash.BrowserTypeMatch(browserType))
		if err != nil {
			s.Fatal("Failed to open a browser window: ", err)
		}
		if err := ash.SetWindowStateAndWait(ctx, tconn, w.ID, ash.WindowStateMaximized); err != nil {
			s.Fatal("Failed to maximize the browser window: ", err)
		}
	}

	if err := pnp.WarmUp(ctx); err != nil {
		s.Fatal("Failed to run pnp warm up routine: ", err)
	}
	if err := pnpRoutine.MeasurePower(ctx, cleanupCtx, s.OutDir(), s.TestName(), true); err != nil {
		s.Fatal("Failed to run pnp power measuring routine: ", err)
	}
}
