// Copyright 2021 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package camera

import (
	"context"
	"path/filepath"
	"strconv"

	"go.chromium.org/tast-tests/cros/local/gtest"
	"go.chromium.org/tast/core/autocaps"
	"go.chromium.org/tast/core/shutil"
	"go.chromium.org/tast/core/testing"
)

func init() {
	testing.AddTest(&testing.Test{
		Func:         Characteristics,
		Desc:         "Verifies the format of camera characteristics file",
		Contacts:     []string{"chromeos-camera-eng@google.com", "kamesan@chromium.org"},
		BugComponent: "b:167281", // ChromeOS > Platform > Technologies > Camera
		Attr:         []string{"group:mainline", "group:camera-libcamera", "group:cq-medium"},
	})
}

func Characteristics(ctx context.Context, s *testing.State) {
	staticCaps, err := autocaps.Read(autocaps.DefaultCapabilityDir, nil)
	if err != nil {
		s.Fatal("Failed to read static capabilities: ", err)
	}
	hasBuiltinUsbCamera := false
	if c, ok := staticCaps["builtin_usb_camera"]; ok && c == autocaps.Yes {
		hasBuiltinUsbCamera = true
	}

	const gtestExecutableName = "camera_characteristics_test"
	t := gtest.New(gtestExecutableName,
		gtest.ExtraArgs("--skip_if_no_config="+strconv.FormatBool(!hasBuiltinUsbCamera)),
		gtest.Logfile(filepath.Join(s.OutDir(), gtestExecutableName+".log")))
	args, err := t.Args()
	if err != nil {
		s.Fatal("Failed to get GTest execution args: ", err)
	}
	s.Log("Running ", shutil.EscapeSlice(args))
	if _, err := t.Run(ctx); err != nil {
		s.Fatalf("Failed to run %v: %v", gtestExecutableName, err)
	}
}
