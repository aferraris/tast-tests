// Copyright 2021 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package inputs

import (
	"context"
	"strings"
	"time"

	"go.chromium.org/tast-tests/cros/local/chrome/ime"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto/browser/browserui"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto/faillog"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto/imesettings"
	"go.chromium.org/tast-tests/cros/local/chrome/useractions"
	"go.chromium.org/tast-tests/cros/local/input"
	"go.chromium.org/tast-tests/cros/local/inputs/fixture"
	"go.chromium.org/tast-tests/cros/local/inputs/pre"
	"go.chromium.org/tast-tests/cros/local/inputs/testserver"
	"go.chromium.org/tast-tests/cros/local/inputs/util"
	"go.chromium.org/tast/core/ctxutil"
	"go.chromium.org/tast/core/testing"
	"go.chromium.org/tast/core/testing/hwdep"
)

func init() {
	testing.AddTest(&testing.Test{
		Func:         PhysicalKeyboardKoreanTyping,
		LacrosStatus: testing.LacrosVariantExists,
		Desc:         "Checks that physical keyboard can perform basic typing in korean",
		Contacts:     []string{"essential-inputs-gardener-oncall@google.com", "essential-inputs-team@google.com"},
		BugComponent: "b:95887",
		Attr:         []string{"group:mainline", "group:input-tools", "group:hw_agnostic"},
		SoftwareDeps: []string{"inputs_deps", "chrome", "chrome_internal"},
		SearchFlags: util.SearchFlagsWithIMEAndScreenPlay(
			[]ime.InputMethod{ime.Korean},
			[]string{
				"screenplay-dc243599-567a-4a93-babd-6182072afc73",
				"screenplay-f6fa7916-fa0b-4f89-b090-2bcdba6b54e7",
				"screenplay-06f14078-48c8-4fa4-8350-444f06eb4555",
			}),
		HardwareDeps: hwdep.D(pre.InputsStableModels),
		Timeout:      12 * time.Minute,
		Params: []testing.Param{
			{
				Fixture:   fixture.ClamshellNonVK,
				ExtraAttr: []string{"group:input-tools-upstream"},
			},
			{
				Name:              "lacros",
				Fixture:           fixture.LacrosClamshellNonVK,
				ExtraSoftwareDeps: []string{"lacros", "lacros_stable"},
				ExtraAttr:         []string{"informational"},
			},
		},
	})
}

type koreanKeyboardLayout string

const (
	koreanInputType2Set        koreanKeyboardLayout = "2 Set / 두벌식"
	koreanInputType3Set390     koreanKeyboardLayout = "3 Set (390) / 세벌식 (390)"
	koreanInputType3SetFinal   koreanKeyboardLayout = "3 Set (Final) / 세벌식 (최종)"
	koreanInputType3SetNoShift koreanKeyboardLayout = "3 Set (No Shift) / 세벌식 (순아래)"
)

func PhysicalKeyboardKoreanTyping(ctx context.Context, s *testing.State) {
	cr := s.FixtValue().(fixture.FixtData).Chrome
	tconn := s.FixtValue().(fixture.FixtData).TestAPIConn
	uc := s.FixtValue().(fixture.FixtData).UserContext

	cleanupCtx := ctx
	ctx, cancel := ctxutil.Shorten(ctx, 5*time.Second)
	defer cancel()

	ui := uiauto.New(tconn)

	defer faillog.DumpUITreeOnError(cleanupCtx, s.OutDir(), s.HasError, tconn)

	// Add IME for testing.
	if err := ime.Korean.InstallAndActivateUserAction(uc)(ctx); err != nil {
		s.Fatal("Failed to switch to Korean IME")
	}
	uc.SetAttribute(useractions.AttributeInputMethod, ime.Korean.Name)

	keyboard, err := input.Keyboard(ctx)
	if err != nil {
		s.Fatal("Failed to get keyboard: ", err)
	}
	defer keyboard.Close(ctx)

	its, err := testserver.LaunchBrowser(ctx, s.FixtValue().(fixture.FixtData).BrowserType, cr, tconn)
	if err != nil {
		s.Fatal("Failed to launch inputs test server: ", err)
	}
	defer its.CloseAll(cleanupCtx)

	var subtests = []struct {
		testName              string
		keyboardLayout        koreanKeyboardLayout // layout should match the name in IME setting.
		inputASyllableAtATime bool
		inputFunc             uiauto.Action
		expectedText          string
	}{
		{
			// Note: this only works because underlying layout is the US (qwerty) xkb
			// layout. That may change in the future (ref b/199024864).
			testName:              "2 set",
			keyboardLayout:        koreanInputType2Set,
			inputASyllableAtATime: true,
			inputFunc:             keyboard.TypeAction("gks"),
			expectedText:          "한",
		},
		{
			// Note: Options other than 2 set are supported at low priority. In fact,
			// these examples may not be even correct, but these tests will still detect
			// any change in behavior.
			testName:              "3 set 390 (1)",
			keyboardLayout:        koreanInputType3Set390,
			inputASyllableAtATime: true,
			inputFunc:             keyboard.TypeAction("kR"),
			expectedText:          "걔",
		},
		{
			// Note: Options other than 2 set are supported at low priority. In fact,
			// these examples may not be even correct, but these tests will still detect
			// any change in behavior.
			testName:              "3 set 390 (2)",
			keyboardLayout:        koreanInputType3Set390,
			inputASyllableAtATime: true,
			inputFunc:             keyboard.TypeAction("jfs1"),
			expectedText:          "않",
		},
		{
			// Note: Options other than 2 set are supported at low priority. In fact,
			// these examples may not be even correct, but these tests will still detect
			// any change in behavior.
			testName:              "3 set final (1)",
			keyboardLayout:        koreanInputType3SetFinal,
			inputASyllableAtATime: true,
			inputFunc:             keyboard.TypeAction("kG"),
			expectedText:          "걔",
		},
		{
			// Note: Options other than 2 set are supported at low priority. In fact,
			// these examples may not be even correct, but these tests will still detect
			// any change in behavior.
			testName:              "3 set final (2)",
			keyboardLayout:        koreanInputType3SetFinal,
			inputASyllableAtATime: true,
			inputFunc:             keyboard.TypeAction("ifS"),
			expectedText:          "많",
		},
		{
			// Note: Options other than 2 set are supported at low priority. In fact,
			// these examples may not be even correct, but these tests will still detect
			// any change in behavior.
			testName:              "3 set No shift (1)",
			keyboardLayout:        koreanInputType3SetNoShift,
			inputASyllableAtATime: true,
			inputFunc:             keyboard.TypeAction("kR"),
			expectedText:          "개",
		},
		{
			// Note: Options other than 2 set are supported at low priority. In fact,
			// these examples may not be even correct, but these tests will still detect
			// any change in behavior.
			testName:              "3 set No shift (2)",
			keyboardLayout:        koreanInputType3SetNoShift,
			inputASyllableAtATime: true,
			inputFunc:             keyboard.TypeAction("jfs1"),
			expectedText:          "않",
		},
		{
			testName:              "ENTER key to submit",
			keyboardLayout:        koreanInputType2Set,
			inputASyllableAtATime: true,
			inputFunc: uiauto.Combine("type Korean and press enter",
				keyboard.TypeAction("gks"),
				keyboard.AccelAction("Enter"),
			),
			expectedText: "한\n",
		},
		{
			testName:              "one syllable at a time",
			keyboardLayout:        koreanInputType2Set,
			inputASyllableAtATime: true,
			inputFunc: uiauto.Combine("type multiple syllables and delete them",
				keyboard.TypeAction("gksk"),
				uiauto.Repeat(3, keyboard.AccelAction("Backspace")),
			),
			expectedText: "",
		},
		{
			testName:              "multiple syllables at a time",
			keyboardLayout:        koreanInputType2Set,
			inputASyllableAtATime: false,
			inputFunc: uiauto.Combine("type multiple syllables and delete them",
				keyboard.TypeAction("gksk"),
				uiauto.Repeat(3, keyboard.AccelAction("Backspace")),
			),
			expectedText: "ㅎ",
		},
	}

	var inputField = testserver.TextAreaInputField
	for _, subtest := range subtests {
		s.Run(ctx, subtest.testName, func(ctx context.Context, s *testing.State) {
			defer faillog.DumpUITreeWithScreenshotOnError(ctx, s.OutDir(), s.HasError, cr, "ui_tree_"+subtest.testName)

			if err := imesettings.SetKoreanKeyboardLayout(uc, string(subtest.keyboardLayout))(ctx); err != nil {
				s.Fatalf("Failed to set keyboard layout to %q: %v", subtest.keyboardLayout, err)
			}

			if err := imesettings.SetKoreanInputASyllableAtATime(uc, subtest.inputASyllableAtATime)(ctx); err != nil {
				s.Fatalf("Failed to set one syllable at a time to %t: %v", subtest.inputASyllableAtATime, err)
			}

			if err := uiauto.UserAction(
				"Korean PK input",
				its.ValidateInputOnField(inputField, subtest.inputFunc, subtest.expectedText),
				uc, &useractions.UserActionCfg{
					Attributes: map[string]string{
						useractions.AttributeTestScenario: subtest.testName,
						useractions.AttributeInputField:   string(inputField),
						useractions.AttributeFeature:      useractions.FeaturePKTyping,
					},
				},
			)(ctx); err != nil {
				s.Fatalf("Failed to validate keys input in %s: %v", inputField, err)
			}
		})
	}

	testName := "ENTER key on Omnibox"
	s.Run(ctx, testName, func(ctx context.Context, s *testing.State) {
		defer faillog.DumpUITreeWithScreenshotOnError(ctx, s.OutDir(), s.HasError, cr, "ui_tree_"+testName)

		if err := imesettings.SetKoreanKeyboardLayout(uc, string(koreanInputType2Set))(ctx); err != nil {
			s.Fatalf("Failed to set keyboard layout to %q: %v", koreanInputType2Set, err)
		}

		omniboxFinder := browserui.AddressBarFinder
		validateOmniboxAction := uiauto.Combine("verify enter key on omnibox",
			ui.LeftClick(omniboxFinder),
			keyboard.TypeAction("gks"),
			keyboard.AccelAction("Enter"),
			util.WaitForFieldTextToSatisfy(tconn, omniboxFinder, "google URL", func(url string) bool {
				return strings.Contains(url, "google.com") && strings.Contains(url, "한")
			}),
		)

		if err := uiauto.UserAction(
			"Korean PK input",
			validateOmniboxAction,
			uc, &useractions.UserActionCfg{
				Attributes: map[string]string{
					useractions.AttributeTestScenario: testName,
					useractions.AttributeInputField:   "Omnibox",
					useractions.AttributeFeature:      useractions.FeaturePKTyping,
				},
			},
		)(ctx); err != nil {
			s.Fatal("Failed to validate korean PK input in omnibox: ", err)
		}
	})
}
