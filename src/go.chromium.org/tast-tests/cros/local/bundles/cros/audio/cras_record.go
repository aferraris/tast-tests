// Copyright 2019 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package audio

import (
	"context"
	"strconv"
	"strings"
	"time"

	"go.chromium.org/tast-tests/cros/common/testexec"
	"go.chromium.org/tast-tests/cros/local/audio"
	"go.chromium.org/tast-tests/cros/local/audio/crastestclient"
	"go.chromium.org/tast-tests/cros/local/audio/fixture"
	"go.chromium.org/tast/core/testing"
	"go.chromium.org/tast/core/testing/hwdep"
)

// TODO(b/244254621) : remove "sasukette" when b/244254621 is fixed.
// TODO(b/312097873) : remove "brya" when b/309904720 is fixed.
var crasRecordUnstableModels = []string{"sasukette", "brya"}

func init() {
	testing.AddTest(&testing.Test{
		Func:         CrasRecord,
		Desc:         "Verifies CRAS record function works correctly",
		Contacts:     []string{"chromeos-audio-bugs@google.com", "yuhsuan@chromium.org", "cychiang@chromium.org"},
		BugComponent: "b:776546",
		HardwareDeps: hwdep.D(hwdep.Microphone()),
		Fixture:      fixture.UIStopped{Parent: "rebootForAudioDSPFixture"}.Instance(),
		Attr:         []string{"group:mainline"},
		Params: []testing.Param{{
			ExtraHardwareDeps: hwdep.D(hwdep.SkipOnModel(crasRecordUnstableModels...)),
			ExtraAttr:         []string{"group:cq-medium", "group:cq-minimal"},
		}, {
			Name:              "unstable_model",
			ExtraHardwareDeps: hwdep.D(hwdep.Model(crasRecordUnstableModels...)),
			ExtraAttr:         []string{"informational"},
		}},
	})
}

func CrasRecord(ctx context.Context, s *testing.State) {
	const duration = 5 // second

	cras, err := audio.NewCras(ctx)
	if err != nil {
		s.Fatal("Failed to connect to CRAS: ", err)
	}
	if err := cras.SetActiveNodeByType(ctx, "INTERNAL_MIC"); err != nil {
		crastestclient.DumpAudioDiagnostics(ctx, s.OutDir())
		s.Fatal("Failed to set internal mic to active: ", err)
	}

	// Set timeout to duration + 3s, which is the time buffer to complete the normal execution.
	runCtx, cancel := context.WithTimeout(ctx, (duration+3)*time.Second)
	defer cancel()

	// Record function by CRAS.
	command := testexec.CommandContext(
		runCtx, "cras_test_client",
		"--capture_file", "/dev/null",
		"--duration", strconv.Itoa(duration),
		"--num_channels", "2",
		"--rate", "48000")
	command.Start()

	defer func() {
		if err := command.Wait(); err != nil {
			s.Fatal("Record did not finish in time: ", err)
		}
	}()

	devName, err := crastestclient.FirstRunningDevice(ctx, audio.InputStream)
	if err != nil {
		s.Fatal("Failed to detect running input device: ", err)
	}

	s.Log("Input device: ", devName)

	if strings.Contains(devName, "Silent") {
		s.Fatal("Fallback to the silent device")
	}
}
