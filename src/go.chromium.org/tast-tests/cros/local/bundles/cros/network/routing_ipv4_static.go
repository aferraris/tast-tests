// Copyright 2022 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package network

import (
	"context"
	"time"

	"github.com/google/go-cmp/cmp"

	"go.chromium.org/tast-tests/cros/common/shillconst"
	"go.chromium.org/tast-tests/cros/local/network/routing"
	"go.chromium.org/tast-tests/cros/local/network/virtualnet"
	"go.chromium.org/tast-tests/cros/local/shill"
	"go.chromium.org/tast/core/ctxutil"
	"go.chromium.org/tast/core/testing"
)

func init() {
	testing.AddTest(&testing.Test{
		Func:     RoutingIPv4Static,
		Desc:     "Verify the shill behavior and routing semantics when the network does not have DHCP or SLAAC but only static IPv4 config",
		Contacts: []string{"cros-networking@google.com", "jiejiang@google.com"},
		// ChromeOS > Platform > System > Networking > Continuous Maintenance
		BugComponent: "b:1493959",
		Attr:         []string{"group:mainline", "group:network", "network_cq"},
		LacrosStatus: testing.LacrosVariantUnneeded,
		Params: []testing.Param{{
			// Apply static IP when the network is idle.
			Val: false,
		}, {
			// Apply static IP when the network is connecting.
			Name:      "apply_when_connecting",
			Val:       true,
			ExtraAttr: []string{"informational"},
		}},
	})
}

func RoutingIPv4Static(ctx context.Context, s *testing.State) {
	// Use a shortened context for test operations to reserve time for cleanup.
	cleanupCtx := ctx
	ctx, cancel := ctxutil.Shorten(ctx, 10*time.Second)
	defer cancel()

	// This test changes static IP configure, push a test profile to avoid
	// polluting default profile by any chance.
	popFunc, err := shill.LogOutUserAndPushTestProfile(ctx)
	if err != nil {
		s.Fatal("Failed to push test profile: ", err)
	}
	defer popFunc(cleanupCtx)

	disconnectBeforeApply := s.Param().(bool)

	testEnv := routing.NewTestEnv()
	if err := testEnv.SetUp(ctx); err != nil {
		s.Fatal("Failed to set up routing test env: ", err)
	}
	defer func(ctx context.Context) {
		if err := testEnv.TearDown(ctx); err != nil {
			s.Error("Failed to tear down routing test env: ", err)
		}
	}(cleanupCtx)

	// Start a virtualnet with neither IPv4 nor IPv6, and configure static IP on
	// the router side.
	testNetworkOpts := virtualnet.EnvOptions{
		Priority:   routing.HighPriority,
		NameSuffix: routing.TestSuffix,
		EnableDHCP: false,
		RAServer:   false,
	}
	if err := testEnv.CreateNetworkEnvForTest(ctx, testNetworkOpts); err != nil {
		s.Fatal("Failed to create network for test: ", err)
	}

	// The allocated subnet has a /24 prefix.
	ipv4Subnet, err := testEnv.Pool.AllocNextIPv4Subnet()
	if err != nil {
		s.Fatal("Failed to allocate IPv4 subnet for test network: ", err)
	}
	localIPv4Addr := ipv4Subnet.GetAddrEndWith(2)
	routerIPv4Addr := ipv4Subnet.GetAddrEndWith(1)
	if err := testEnv.TestRouter.ConfigureInterface(ctx, testEnv.TestRouter.VethInName, routerIPv4Addr, ipv4Subnet); err != nil {
		s.Fatal("Failed to configure IPv4 inside test router: ", err)
	}

	if disconnectBeforeApply {
		testing.ContextLog(ctx, "Disconnecting the test service")
		if err := testEnv.TestService.Disconnect(ctx); err != nil {
			s.Fatal("Failed to disconnect the test service: ", err)
		}
		if err := testEnv.TestService.WaitForProperty(ctx, shillconst.ServicePropertyState, shillconst.ServiceStateIdle, 5*time.Second); err != nil {
			s.Fatal("Failed to wait for the test service idle: ", err)
		}
	}

	// Configure static IP config on shill service.
	prefixLen := ipv4Subnet.PrefixLen()
	svcStaticIPConfig := map[string]interface{}{
		shillconst.IPConfigPropertyAddress:   localIPv4Addr.String(),
		shillconst.IPConfigPropertyGateway:   routerIPv4Addr.String(),
		shillconst.IPConfigPropertyPrefixlen: prefixLen,
	}
	testing.ContextLogf(ctx, "Configuring %v on the test interface", svcStaticIPConfig)
	if err := testEnv.TestService.SetProperty(ctx, shillconst.ServicePropertyStaticIPConfig, svcStaticIPConfig); err != nil {
		s.Fatal("Failed to configure StaticIPConfig property on the test service: ", err)
	}
	defer func(ctx context.Context) {
		// Reset StaticIPConfig before removing the test interfaces, to avoid
		// installing this address on the physical interfaces. See
		// b/239753191#comment8 for a racing case.
		if err := testEnv.TestService.SetProperty(ctx, shillconst.ServicePropertyStaticIPConfig, map[string]interface{}{}); err != nil {
			testing.ContextLog(ctx, "Failed to reset StaticIPConfig property on the test service: ", err)
		}
	}(cleanupCtx)

	if disconnectBeforeApply {
		testing.ContextLog(ctx, "Connect to the test service")
		if err := testEnv.TestService.Connect(ctx); err != nil {
			s.Fatal("Failed to connect the test service: ", err)
		}
	}

	testing.ContextLog(ctx, "Waiting for test service online")
	if err := testEnv.TestService.WaitForProperty(ctx, shillconst.ServicePropertyState, shillconst.ServiceStateOnline, 5*time.Second); err != nil {
		s.Fatal("Failed to wait for the test service online: ", err)
	}

	testing.ContextLog(ctx, "Waiting for DHCP timeout event for ", routing.DHCPExtraTimeout)
	// GoBigSleepLint: Verify that the DHCP timeout event does not turn the
	// service down. We cannot trigger this event manually so nothing can be done
	// except for sleeping here.
	testing.Sleep(ctx, routing.DHCPExtraTimeout)
	testing.ContextLog(ctx, "DHCP timeout was triggered")

	// Verify the service state is still online.
	// TODO(b/159725895): Ideally the service state should not change after DHCP
	// failure, but actually after the service becomes online at the first time in
	// the above code, the StaticIPConfig will be reset due to the current
	// ethernet_any implementation, so when DHCP failure is triggered, the Network
	// class in shill does not know any IP config on this network so the state
	// will be turned to disconnected at once. After that, this network is no
	// longer attached to ethernet_any and get the StaticIPConfig back and becomes
	// online again. As a result, we need to wait for service online instead of
	// checking its state directly here.
	if err := testEnv.TestService.WaitForProperty(ctx, shillconst.ServicePropertyState, shillconst.ServiceStateOnline, 5*time.Second); err != nil {
		s.Fatal("Failed to wait for the test service online after DHCP expired: ", err)
	}

	// Verify routing setup for test network.
	if errs := testEnv.VerifyTestNetwork(ctx, routing.VerifyOptions{
		IPv4:      true,
		IPv6:      false,
		IsPrimary: true,
		Timeout:   5 * time.Second,
	}); len(errs) != 0 {
		for _, err := range errs {
			s.Error("Failed to verify test network after configuring static IP: ", err)
		}
	}

	// TODO(b/159725895): When the Ethernet service order changes, the first
	// Ethernet service will be reloaded from the ethernet_any profile, this will
	// also overwrite the StaticIPConfig field. This will not affect routing but
	// will affect the IPConfig objects on the Device, so in this test we rewrite
	// this property again.
	if err := testEnv.TestService.SetProperty(ctx, shillconst.ServicePropertyStaticIPConfig, svcStaticIPConfig); err != nil {
		s.Fatal("Failed to configure StaticIPConfig property on the test service: ", err)
	}

	// Verify IPConfigs.
	ipconfigs, err := testEnv.TestService.GetIPConfigs(ctx)
	if err != nil {
		s.Fatal("Failed to get IPConfigs: ", err)
	}
	if len(ipconfigs) != 1 {
		s.Fatal("Expect 1 IPConfig objects, but got ", len(ipconfigs))
	}
	actualIPProps, err := ipconfigs[0].GetIPProperties(ctx)
	if err != nil {
		s.Fatal("Failed to get IPProperties from IPConfig: ", err)
	}
	expectedIPProps := shill.IPProperties{
		Address:     localIPv4Addr.String(),
		Gateway:     routerIPv4Addr.String(),
		Method:      "ipv4",
		PrefixLen:   int32(prefixLen),
		NameServers: []string{},
	}
	if diff := cmp.Diff(actualIPProps, expectedIPProps); diff != "" {
		s.Fatal("Got unexpected IPProperties with diff: ", diff)
	}
}
