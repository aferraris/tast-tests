// Copyright 2021 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package settings

import (
	"context"
	"regexp"
	"time"

	"go.chromium.org/tast-tests/cros/local/chrome"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto/faillog"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto/nodewith"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto/ossettings"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto/role"
	"go.chromium.org/tast/core/ctxutil"
	"go.chromium.org/tast/core/errors"
	"go.chromium.org/tast/core/testing"
)

func init() {
	testing.AddTest(&testing.Test{
		Func:         TermsLinkClickable,
		LacrosStatus: testing.LacrosVariantNeeded,
		Desc:         "Checks the terms of service link is clickable within help page",
		Contacts: []string{
			"cros-settings@google.com",
			"chromeos-sw-engprod@google.com",
		},
		// OS > Systems > Settings
		BugComponent: "b:1246072",
		Attr:         []string{"group:mainline", "informational", "group:hw_agnostic"},
		SoftwareDeps: []string{"chrome"},
		Fixture:      "chromeLoggedIn",
	})
}

// TermsLinkClickable checks the chrome://terms link is clickable within 'About ChromeOS' and chrome://help.
func TermsLinkClickable(ctx context.Context, s *testing.State) {
	cr := s.FixtValue().(chrome.HasChrome).Chrome()

	tconn, err := cr.TestAPIConn(ctx)
	if err != nil {
		s.Fatal("Failed to create test API connection: ", err)
	}

	cleanupCtx := ctx
	ctx, cancel := ctxutil.Shorten(ctx, 5*time.Second)
	defer cancel()

	webPageTest := func(ctx context.Context, s *testing.State) {
		conn, err := cr.NewConn(ctx, "chrome://help")
		if err != nil {
			s.Fatal("Failed to connect to chrome: ", err)
		}
		defer conn.Close()
		defer faillog.DumpUITreeWithScreenshotOnError(cleanupCtx, s.OutDir(), s.HasError, cr, "web_page_dump")

		if err := checkTermsOfService(ctx, cr, tconn, s.OutDir()); err != nil {
			s.Fatal("Failed to click from web page: ", err)
		}
	}

	ossettingsTest := func(ctx context.Context, s *testing.State) {
		settings, err := ossettings.LaunchAtPage(ctx, tconn, ossettings.AboutChromeOS)
		if err != nil {
			s.Fatal("Failed to launch OS settings at `About ChromeOS` page: ", err)
		}
		defer settings.Close(cleanupCtx)
		defer faillog.DumpUITreeWithScreenshotOnError(cleanupCtx, s.OutDir(), s.HasError, cr, "ossettings_dump")

		if err := checkTermsOfService(ctx, cr, tconn, s.OutDir()); err != nil {
			s.Fatal("Failed to click from settings page: ", err)
		}
	}

	for _, subtest := range []struct {
		name string
		f    func(ctx context.Context, s *testing.State)
	}{
		{"check web page", webPageTest},
		{"check os-settings", ossettingsTest},
	} {
		if !s.Run(ctx, subtest.name, subtest.f) {
			s.Errorf("Failed to run subtest %s", subtest.name)
		}
	}
}

func checkTermsOfService(ctx context.Context, cr *chrome.Chrome, tconn *chrome.TestConn, outDir string) error {
	ui := uiauto.New(tconn)

	termsOfServiceLink := nodewith.Name("Terms of Service").Role(role.Link)
	if err := uiauto.Combine("click terms of service link",
		ui.FocusAndWait(termsOfServiceLink),
		ui.WaitUntilExists(termsOfServiceLink),
		ui.LeftClick(termsOfServiceLink),
	)(ctx); err != nil {
		return err
	}

	return verifyContent(ctx, cr, outDir, ui)
}

// Verify that the header of the chrome://terms/ page appears and contains the correct string.
func verifyContent(ctx context.Context, cr *chrome.Chrome, outDir string, ui *uiauto.Context) (err error) {
	cleanupCtx := ctx
	ctx, cancel := ctxutil.Shorten(ctx, 5*time.Second)
	defer cancel()

	url := "chrome://terms/"
	conn, err := cr.NewConnForTarget(ctx, chrome.MatchTargetURL(url))
	if err != nil {
		return errors.Wrapf(err, "failed to connect to window %s", url)
	}
	defer conn.Close()
	defer conn.CloseTarget(cleanupCtx)
	defer faillog.DumpUITreeWithScreenshotOnError(cleanupCtx, outDir, func() bool { return err != nil }, cr, "terms_dump")

	// Verify the content is within the terms page.
	// The terms of service page uses a special whitespace character (NBSP) in
	// "Google Chrome" below. Instead of matching the exact character, match any
	// whitespace character in the regex. For more information, see discussion in
	// b/301003113.
	termsPageHeader := `Google\sChrome and ChromeOS Additional Terms of Service`
	termsPageHeaderFinder := nodewith.NameRegex(regexp.MustCompile("^" + termsPageHeader + "$")).Role(role.Heading)
	if err := ui.WaitUntilExists(termsPageHeaderFinder)(ctx); err != nil {
		return errors.Wrap(err, "unexpected page content")
	}

	return nil
}
