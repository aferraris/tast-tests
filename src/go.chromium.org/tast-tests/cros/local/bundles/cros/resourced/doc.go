// Copyright 2024 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package resourced contains tests that check the functionalities of the resourced daemon.
package resourced
