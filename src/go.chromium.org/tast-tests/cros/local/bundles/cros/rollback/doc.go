// Copyright 2022 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package rollback contains the service for remote enterprise rollback tests
// and local tests related to enterprise rollback.
package rollback
