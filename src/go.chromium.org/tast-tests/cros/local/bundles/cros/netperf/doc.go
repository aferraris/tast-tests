// Copyright 2023 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package netperf contains local Tast tests that monitor the lab network health.
package netperf
