// Copyright 2024 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package cleanupchecks contains functions to check if a DUT is in a clean state.
package cleanupchecks
