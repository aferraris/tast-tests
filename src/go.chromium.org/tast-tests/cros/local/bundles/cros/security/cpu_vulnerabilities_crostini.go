// Copyright 2020 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package security

import (
	"context"
	"path/filepath"
	"strings"
	"time"

	"go.chromium.org/tast-tests/cros/local/crostini"
	"go.chromium.org/tast/core/ctxutil"
	"go.chromium.org/tast/core/testing"
)

func init() {
	testing.AddTest(&testing.Test{
		Func:         CPUVulnerabilitiesCrostini,
		LacrosStatus: testing.LacrosVariantUnneeded,
		Desc:         "Confirm CPU vulnerabilities are mitigated in the guest kernel",
		Contacts: []string{
			"cros-containers-dev@google.com",
			"chromeos-hardening@google.com",
		},
		// ChromeOS > Security > Hardening
		BugComponent: "b:1040049",
		Attr:         []string{"group:mainline", "informational"},
		Vars:         []string{"keepState"},
		SoftwareDeps: []string{"chrome", "vm_host", "cpu_vuln_sysfs", "no_qemu"},
		Params: []testing.Param{
			// Parameters generated by cpu_vulnerabilities_test.go. DO NOT EDIT.
			{
				Name:              "stable",
				ExtraSoftwareDeps: []string{"dlc"},
				ExtraHardwareDeps: crostini.CrostiniOptimalPerf,
				Fixture:           "crostiniBullseye",
				Timeout:           7 * time.Minute,
			}, {
				Name:              "unstable",
				ExtraAttr:         []string{"informational"},
				ExtraSoftwareDeps: []string{"dlc"},
				ExtraHardwareDeps: crostini.CrostiniUnstable,
				Fixture:           "crostiniBullseye",
				Timeout:           7 * time.Minute,
			},
		},
	})
}

func CPUVulnerabilitiesCrostini(ctx context.Context, s *testing.State) {
	cont := s.FixtValue().(crostini.FixtureData).Cont

	// Use a shortened context for test operations to reserve time for cleanup.
	ctx, cancel := ctxutil.Shorten(ctx, 5*time.Second)
	defer cancel()

	vulnDir := "/sys/devices/system/cpu/vulnerabilities/"
	fileList, err := cont.GetFileList(ctx, vulnDir)
	if err != nil {
		s.Fatal("Failed to list vulnerability files: ", err)
	}
	for _, f := range fileList {
		// We are leaving "spec_rstack_overflow" unmitigated on
		// some boards: b/297466181
		if f == "spec_rstack_overflow" {
			continue
		}
		contents, err := cont.ReadFile(ctx, filepath.Join(vulnDir, f))
		if err != nil {
			s.Fatal("Can't read vulnerability file: ", err)
		}
		contents = strings.TrimSpace(contents)
		s.Logf("%s: %s", f, contents)
		if strings.EqualFold(contents, "vulnerable") {
			s.Errorf("File %q has CPU vulnerabilities", f)
		}
	}
}
