// Copyright 2023 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package cellular

import (
	"context"
	"time"

	"go.chromium.org/tast-tests/cros/local/cellular"
	"go.chromium.org/tast-tests/cros/local/chrome"

	"go.chromium.org/tast-tests/cros/local/chrome/uiauto"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto/faillog"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto/lockscreen"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto/ossettings"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto/quicksettings"
	"go.chromium.org/tast/core/ctxutil"
	"go.chromium.org/tast/core/testing"
	"go.chromium.org/tast/core/testing/hwdep"
)

func init() {
	testing.AddTest(&testing.Test{
		Func:           HotspotEnableDisableInLockScreen,
		LacrosStatus:   testing.LacrosVariantUnneeded,
		LifeCycleStage: testing.LifeCycleInDevelopment,
		Desc:           "Test hotspot can be accessed and controled in lock screen through Quick Settings",
		Contacts: []string{
			"alfredyu@cienet.com",
			"chromeos-connectivity-cienet-external@google.com",
		},
		BugComponent: "b:1578688", // ChromeOS > External > Cienet > Manual Test Automation > Test stabilization
		Attr:         []string{"group:cellular", "cellular_unstable", "cellular_sim_active", "cellular_carrier_dependent"},
		HardwareDeps: hwdep.D(hwdep.WifiSAP()),
		SoftwareDeps: []string{"chrome", "hotspot"},
		Fixture:      "cellularHotspotLocal",
		Timeout:      5 * time.Minute,
		Params: []testing.Param{
			// Parameters generated by params_test.go. DO NOT EDIT.

			{
				Name:      "",
				Val:       "",
				ExtraAttr: []string{"cellular_carrier_local"},
			},
			{
				Name:      "tmobile",
				Val:       "tmobile",
				ExtraAttr: []string{"cellular_carrier_tmobile"},
			},
			{
				Name:      "docomo",
				Val:       "docomo",
				ExtraAttr: []string{"cellular_carrier_docomo"},
			},
			{
				Name:      "softbank",
				Val:       "softbank",
				ExtraAttr: []string{"cellular_carrier_softbank"},
			},
		},
	})
}

func HotspotEnableDisableInLockScreen(ctx context.Context, s *testing.State) {
	cleanupCtx := ctx
	ctx, cancel := ctxutil.Shorten(ctx, 10*time.Second)
	defer cancel()

	cr, err := chrome.New(ctx, chrome.EnableFeatures("Hotspot", "TetheringExperimentalFunctionality"))
	if err != nil {
		s.Fatal("Failed to create a new instance of Chrome: ", err)
	}
	defer cr.Close(cleanupCtx)

	helper := s.FixtValue().(*cellular.FixtData).Helper
	if _, err := helper.Connect(ctx); err != nil {
		s.Fatal("Failed to connect cellular service: ", err)
	}

	tconn, err := cr.TestAPIConn(ctx)
	if err != nil {
		s.Fatal("Failed to create Test API connection: ", err)
	}

	hs, err := ossettings.LaunchAtHotspotSubpage(ctx, tconn, cr)
	if err != nil {
		s.Fatal("Failed to open hotspot subpage: ", err)
	}
	defer faillog.DumpUITreeWithScreenshotOnError(cleanupCtx, s.OutDir(), s.HasError, cr, "ui_tree")

	// Toggle on and off hotspot in order to make it appear in Quick Settings.
	if err := hs.ToggleHotspot(ctx, tconn, cr, true); err != nil {
		s.Fatal("Failed to toggle on hotspot: ", err)
	}
	if err := hs.ToggleHotspot(ctx, tconn, cr, false); err != nil {
		s.Fatal("Failed to toggle off hotspot: ", err)
	}

	if hasHotspotTile, err := quicksettings.IsHotspotTileShown(ctx, tconn); err != nil {
		s.Fatal("Failed to show hotspot tile in quick settings: ", err)
	} else if !hasHotspotTile {
		s.Fatal("Failed to show hotspot tile in quick settings:")
	}

	if err := lockscreen.Lock(ctx, tconn); err != nil {
		s.Fatal("Failed to lock the screen on ChromeOS: ", err)
	}

	if err := quicksettings.NavigateToHotspotDetailedView(ctx, tconn); err != nil {
		s.Fatal("Failed to navigate to hotspot detailed view: ", err)
	}

	ui := uiauto.New(tconn)
	if err := uiauto.Combine("Toggle on and off hotspot in Hotspot detailed view",
		ui.EnsureGoneFor(quicksettings.HotspotOnNoDeviceConnectedText, 3*time.Second),
		ui.LeftClick(quicksettings.HotspotDetailedViewToggle),
		ui.WaitUntilExists(quicksettings.HotspotOnNoDeviceConnectedText),
		ui.LeftClick(quicksettings.HotspotDetailedViewToggle),
		ui.WaitUntilGone(quicksettings.HotspotOnNoDeviceConnectedText),
		ui.EnsureGoneFor(quicksettings.HotspotOnNoDeviceConnectedText, 5*time.Second),
	)(ctx); err != nil {
		s.Fatal("Failed to toggle on and off hotspot in Hotspot detailed view: ", err)
	}
}
