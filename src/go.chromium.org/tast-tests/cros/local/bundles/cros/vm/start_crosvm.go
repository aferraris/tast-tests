// Copyright 2018 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package vm

import (
	"context"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"regexp"
	"time"

	"go.chromium.org/tast-tests/cros/local/bundles/cros/vm/dlc"
	"go.chromium.org/tast-tests/cros/local/vm"
	"go.chromium.org/tast/core/testing"
)

func init() {
	testing.AddTest(&testing.Test{
		Func:         StartCrosvm,
		LacrosStatus: testing.LacrosVariantUnneeded,
		Desc:         "Checks that crosvm starts termina and runs commands through stdin",
		Contacts:     []string{"cros-virt-devices-guests@google.com", "keiichiw@google.com"},
		BugComponent: "b:1248538",
		Attr:         []string{"group:mainline", "informational", "group:sw_gates_virt", "sw_gates_virt_enabled"},
		SoftwareDeps: []string{"vm_host", "chrome", "dlc"},
		Fixture:      "vmDLC",
	})
}

// StartCrosvm is used as one of the standard SW gates (go/pe-sw-gates).
// Please ask to crosvm-core@ if you want to modify or delete this test.
func StartCrosvm(ctx context.Context, s *testing.State) {
	data := s.FixtValue().(dlc.FixtData)

	td, err := ioutil.TempDir("", "tast.vm.StartCrosvm.")
	if err != nil {
		s.Fatal("Failed to create temporary directory: ", err)
	}
	defer os.RemoveAll(td)

	ps := vm.NewCrosvmParams(
		data.Kernel,
		vm.Socket(filepath.Join(td, "crosvm_socket")),
		vm.Rootfs(data.Rootfs),
		vm.KernelArgs("init=/bin/bash"),
	)

	cvm, err := vm.NewCrosvm(ctx, ps)
	if err != nil {
		s.Fatal("Failed to start crosvm: ", err)
	}
	defer func() {
		if err := cvm.Close(ctx); err != nil {
			s.Error("Failed to close crosvm: ", err)
		}
	}()

	testing.ContextLog(ctx, "Waiting for VM to boot")
	startCtx, cancel := context.WithTimeout(ctx, 20*time.Second)
	defer cancel()
	line, err := cvm.WaitForOutput(startCtx, regexp.MustCompile("localhost\\b.*#"))
	if err != nil {
		s.Fatal("Didn't get VM prompt: ", err)
	}
	s.Logf("Saw prompt in line %q", line)

	const cmd = "/bin/ls -1 /"
	s.Logf("Running %q and waiting for output", cmd)
	if _, err = io.WriteString(cvm.Stdin(), cmd+"\n"); err != nil {
		s.Fatalf("Failed to write %q command: %v", cmd, err)
	}
	cmdCtx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()
	if line, err = cvm.WaitForOutput(cmdCtx, regexp.MustCompile("^sbin$")); err != nil {
		s.Errorf("Didn't get expected %q output: %v", cmd, err)
	} else {
		s.Logf("Saw line %q", line)
	}
}
