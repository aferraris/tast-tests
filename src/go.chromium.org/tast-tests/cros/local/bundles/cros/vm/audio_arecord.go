// Copyright 2022 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package vm

import (
	"context"
	"fmt"
	"io/ioutil"
	"path/filepath"
	"time"

	"go.chromium.org/tast-tests/cros/local/audio"
	"go.chromium.org/tast-tests/cros/local/bundles/cros/vm/audioutils"
	"go.chromium.org/tast-tests/cros/local/bundles/cros/vm/dlc"
	"go.chromium.org/tast/core/testing"
	"go.chromium.org/tast/core/testing/hwdep"
)

const runAudioArecord string = "run-arecord.sh"

type audioArecordParams struct {
	crosvmArgs               []string
	vhostUserArgs            []string
	expectedCardNames        []string
	expectedDeviceNames      []string
	expectedStreamsPerDevice int
}

func init() {
	testing.AddTest(&testing.Test{
		Func:         AudioArecord,
		LacrosStatus: testing.LacrosVariantUnneeded,
		Desc:         "Check that capture devices are listed correctly",
		Contacts:     []string{"chromeos-audio-bugs@google.com", "normanbt@google.com"},
		BugComponent: "b:1332660",
		Attr:         []string{"group:mainline", "group:audio", "group:cq-medium", "group:sw_gates_virt", "sw_gates_virt_enabled"},
		Data:         []string{runAudioArecord},
		Timeout:      3 * time.Minute,
		SoftwareDeps: []string{"vm_host", "chrome", "dlc"},
		// TODO(b/288063328): Fix frequent "Failed to install DLC" error on these models
		HardwareDeps: hwdep.D(hwdep.SkipOnModel("pujjoteen", "steelix", "rusty", "vorticon", "bobba")),
		Fixture:      "vmDLC",
		Params: []testing.Param{
			{
				Name: "virtio_null_snd",
				Val: audioArecordParams{
					crosvmArgs:               []string{"--virtio-snd", "capture=true,backend=null"},
					expectedCardNames:        []string{"VirtIO SoundCard"},
					expectedDeviceNames:      []string{"VirtIO PCM 0"},
					expectedStreamsPerDevice: 1,
				},
			},
			{
				Name: "virtio_cras_snd",
				Val: audioArecordParams{
					crosvmArgs:               []string{"--virtio-snd", "capture=true,backend=cras,socket_type=legacy"},
					expectedCardNames:        []string{"VirtIO SoundCard"},
					expectedDeviceNames:      []string{"VirtIO PCM 0"},
					expectedStreamsPerDevice: 1,
				},
			},
			{
				Name: "virtio_cras_snd_3_devices_4_streams",
				Val: audioArecordParams{
					crosvmArgs:               []string{"--virtio-snd", "capture=true,backend=cras,socket_type=legacy,num_input_devices=3,num_input_streams=4"},
					expectedCardNames:        []string{"VirtIO SoundCard", "VirtIO SoundCard", "VirtIO SoundCard"},
					expectedDeviceNames:      []string{"VirtIO PCM 0", "VirtIO PCM 1", "VirtIO PCM 2"},
					expectedStreamsPerDevice: 4,
				},
			},
			{
				Name: "virtio_cras_snd_1_device_3_streams",
				Val: audioArecordParams{
					crosvmArgs:               []string{"--virtio-snd", "capture=true,backend=cras,socket_type=legacy,num_input_streams=3"},
					expectedCardNames:        []string{"VirtIO SoundCard"},
					expectedDeviceNames:      []string{"VirtIO PCM 0"},
					expectedStreamsPerDevice: 3,
				},
			},
			{
				Name: "virtio_cras_snd_3_devices_1_stream",
				Val: audioArecordParams{
					crosvmArgs:               []string{"--virtio-snd", "capture=true,backend=cras,socket_type=legacy,num_input_devices=3"},
					expectedCardNames:        []string{"VirtIO SoundCard", "VirtIO SoundCard", "VirtIO SoundCard"},
					expectedDeviceNames:      []string{"VirtIO PCM 0", "VirtIO PCM 1", "VirtIO PCM 2"},
					expectedStreamsPerDevice: 1,
				},
			},
		},
	})
}

// AudioArecord is used as one of the standard SW gates (go/pe-sw-gates).
// Please ask to crosvm-core@ if you want to modify or delete this test.
func AudioArecord(ctx context.Context, s *testing.State) {
	param := s.Param().(audioArecordParams)
	data := s.FixtValue().(dlc.FixtData)

	kernelLogPath := filepath.Join(s.OutDir(), "kernel.log")
	outputLogPath := filepath.Join(s.OutDir(), "output.txt")

	kernelArgs := []string{
		fmt.Sprintf("init=%s", s.DataPath(runAudioArecord)),
		"--",
		outputLogPath,
	}

	config := audioutils.Config{
		CrosvmArgs: param.crosvmArgs,
	}

	// Example of the output from `arecord -l` when using 3 devices, 4 streams
	/*
		**** List of CAPTURE Hardware Devices ****
		card 0: SoundCard [VirtIO SoundCard], device 0: virtio-snd [VirtIO PCM 0]
		  Subdevices: 4/4
		  Subdevice #0: subdevice #0
		  Subdevice #1: subdevice #1
		  Subdevice #2: subdevice #2
		  Subdevice #3: subdevice #3
		card 0: SoundCard [VirtIO SoundCard], device 1: virtio-snd [VirtIO PCM 1]
		  Subdevices: 4/4
		  Subdevice #0: subdevice #0
		  Subdevice #1: subdevice #1
		  Subdevice #2: subdevice #2
		  Subdevice #3: subdevice #3
		card 0: SoundCard [VirtIO SoundCard], device 2: virtio-snd [VirtIO PCM 2]
		  Subdevices: 4/4
		  Subdevice #0: subdevice #0
		  Subdevice #1: subdevice #1
		  Subdevice #2: subdevice #2
		  Subdevice #3: subdevice #3
	*/

	if err := audioutils.RunCrosvm(ctx, data.Kernel, kernelLogPath, kernelArgs, config); err != nil {
		s.Fatal("Failed to run crosvm: ", err)
	}

	output, err := ioutil.ReadFile(outputLogPath)
	if err != nil {
		s.Fatalf("Failed to read output file %q: %v", outputLogPath, err)
	}

	if err := audio.CheckAlsaDeviceList(
		ctx, string(output), param.expectedCardNames,
		param.expectedDeviceNames, param.expectedStreamsPerDevice, true,
	); err != nil {
		s.Errorf("Found difference on arecord -l output, err: %s", err)
	}
}
