// Copyright 2019 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package graphics

import (
	"context"
	"time"

	"go.chromium.org/tast-tests/cros/local/crostini"
	"go.chromium.org/tast-tests/cros/local/graphics/glbench"
	"go.chromium.org/tast/core/testing"
)

// Tast framework requires every subtest's Val have the same reflect type.
// This is a wrapper to wrap interface together.
type config struct {
	config glbench.Config
}

func init() {
	testing.AddTest(&testing.Test{
		Func:         GLBench,
		LacrosStatus: testing.LacrosVariantUnneeded,
		Desc:         "Run glbench (a benchmark that times graphics intensive activities), check results and report its performance",
		BugComponent: "b:995569", // ChromeOS > Platform > Graphics > GPU
		Contacts: []string{
			"chromeos-gfx@google.com",
			"andrescj@chromium.org",
			"pwang@chromium.org",
		},
		SoftwareDeps: []string{"no_qemu"},
		Vars:         []string{"keepState"},
		Params: []testing.Param{
			// TODO(b/307460167): Update this test to use modern Crostini fixtures.
			{
				Name:      "",
				Val:       config{config: &glbench.CrosConfig{}},
				Timeout:   3 * time.Hour,
				ExtraAttr: []string{"group:graphics", "graphics_nightly"},
				Fixture:   "graphicsNoChrome",
			}, {
				Name:      "hasty",
				Val:       config{config: &glbench.CrosConfig{Hasty: true}},
				ExtraAttr: []string{"group:mainline", "informational"},
				Timeout:   5 * time.Minute,
				Fixture:   "graphicsNoChrome",
			}, {
				Name:              "crostini_hasty",
				ExtraAttr:         []string{"group:graphics", "graphics_weekly"},
				ExtraData:         []string{crostini.GetContainerMetadataArtifact("buster", false), crostini.GetContainerRootfsArtifact("buster", false)},
				ExtraSoftwareDeps: []string{"chrome", "crosvm_gpu", "vm_host", "dlc"},
				ExtraHardwareDeps: crostini.CrostiniStable,
				Pre:               crostini.StartedByDlcBuster(),
				Timeout:           5 * time.Minute,
				Val:               config{config: &glbench.CrostiniConfig{Hasty: true}},
			},
		},
	})
}

func GLBench(ctx context.Context, s *testing.State) {
	config := s.Param().(config).config
	value := s.PreValue()
	if value == nil {
		value = s.FixtValue()
	}

	if err := glbench.Run(ctx, s.OutDir(), value, config); err != nil {
		s.Fatal("GLBench fails: ", err)
	}
}
