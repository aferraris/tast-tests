// Copyright 2019 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package graphics

import (
	"context"
	"path/filepath"

	"go.chromium.org/tast-tests/cros/local/gtest"
	"go.chromium.org/tast/core/testing"
)

func init() {
	testing.AddTest(&testing.Test{
		Func:         Dawn,
		LacrosStatus: testing.LacrosVariantUnneeded,
		Desc:         "Verifies that Dawn unit and end-to-end tests run successfully",
		Contacts: []string{
			"chromeos-gfx@google.com",
			"hob@chromium.org",
		},
		BugComponent: "b:960692",
		Params: []testing.Param{{
			Name: "unit_tests",
			Val:  "dawn_unittests",
		}, {
			Name: "end_to_end_tests",
			Val:  "dawn_end2end_tests",
		}},
		SoftwareDeps: []string{"chrome"},
		Fixture:      "gpuWatchDog",
	})
}

func Dawn(ctx context.Context, s *testing.State) {
	exec := filepath.Join("/usr/local/libexec/chrome-binary-tests/", s.Param().(string))
	list, err := gtest.ListTests(ctx, exec)
	if err != nil {
		s.Fatal("Failed to list gtest: ", err)
	}
	logdir := filepath.Join(s.OutDir(), "gtest")
	for _, testcase := range list {
		s.Log("Running ", testcase)
		if _, err := gtest.New(exec,
			gtest.Logfile(filepath.Join(logdir, testcase+".log")),
			gtest.Filter(testcase),
		).Run(ctx); err != nil {
			s.Errorf("%s failed: %v", testcase, err)
		}
	}
}
