// Copyright 2022 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package graphics

import (
	"context"
	"time"

	"go.chromium.org/tast-tests/cros/common/testexec"
	"go.chromium.org/tast/core/testing"
)

func init() {
	testing.AddTest(&testing.Test{
		Func:         LibDRM,
		LacrosStatus: testing.LacrosVariantUnneeded,
		Desc:         "A quick smoke check using the binaries shipping with libdrm",
		Attr:         []string{"group:graphics", "group:mainline", "graphics_nightly"},
		BugComponent: "b:995569", // ChromeOS > Platform > Graphics > GPU
		Contacts: []string{
			"chromeos-gfx@google.com",
			"syedfaaiz@google.com",
		},
		Fixture: "graphicsNoChrome",
		Params: []testing.Param{{
			Name:      "modetest",
			Val:       []string{"modetest"},
			Timeout:   2 * time.Minute,
			ExtraAttr: []string{"group:cq-medium"},
		}, {
			Name:    "proptest",
			Val:     []string{"proptest"},
			Timeout: 2 * time.Minute,
		}},
	})
}

func LibDRM(ctx context.Context, s *testing.State) {
	test := s.Param().([]string)[0]
	err := testexec.CommandContext(ctx, "which", test).Run(testexec.DumpLogOnError)
	if err != nil {
		s.Errorf("Command %v doesn't exist: %v", test, err)
	}
	_, stderr, err := testexec.CommandContext(ctx, test).SeparatedOutput(testexec.DumpLogOnError)
	if err != nil {
		s.Errorf("Failed to run %v: %v", string(stderr), err)
	}
}
