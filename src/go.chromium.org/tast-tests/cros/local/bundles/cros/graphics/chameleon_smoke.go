// Copyright 2022 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package graphics

import (
	"context"
	"path/filepath"
	"strconv"
	"time"

	"go.chromium.org/tast-tests/cros/common/chameleon"
	"go.chromium.org/tast-tests/cros/local/chrome"
	"go.chromium.org/tast-tests/cros/local/graphics"
	"go.chromium.org/tast/core/testing"
)

func init() {
	testing.AddTest(&testing.Test{
		Func:         ChameleonSmoke,
		LacrosStatus: testing.LacrosVariantUnneeded,
		Desc:         "Verifies that a working Chameleon device is connected to at least one valid port",
		Contacts: []string{
			"chromeos-gfx-display@google.com",
			"markyacoub@google.com",
		},
		BugComponent: "b:188154", // ChromeOS > Platform > Graphics > Display
		Attr: []string{
			"group:graphics",
			"graphics_chameleon_igt",
			"graphics_nightly",
		},
		SoftwareDeps: []string{"chrome"},
		VarDeps:      []string{"graphics.chameleon_ip"},
		Fixture:      "gpuWatchHangs",
		Params: []testing.Param{{
			Name: "dp1",
			Val:  "dp1",
		}, {
			Name: "dp2",
			Val:  "dp2",
		}, {
			Name: "hdmi1",
			Val:  "hdmi1",
		}, {
			Name: "hdmi2",
			Val:  "hdmi2",
		}},
		Timeout: chrome.LoginTimeout + time.Minute,
	})
}

func ChameleonSmoke(ctx context.Context, s *testing.State) {
	portStr := s.Param().(string)

	// Log into Chrome to ensure there is something on the screen.
	// Strictly speaking Chrome is not needed for this test.
	cr, err := chrome.New(ctx)
	if err != nil {
		s.Fatal("Failed to connect to Chrome: ", err)
	}
	defer cr.Close(ctx)

	conn, err := cr.NewConn(ctx, "chrome://gpu")
	if err != nil {
		s.Fatal("Failed to load chrome://gpu: ", err)
	}
	defer conn.Close()

	// TODO(b:262981118, ihf|markyacoub): consider creating a fixture for each port
	// so that all Chameleon tests can focus on testing the active ports only
	// and ignore inactive ones.
	cham, err := graphics.ChameleonGetConnection(ctx)

	defer graphics.ChameleonSaveLogsAndOutputOnError(ctx, cham, s.HasError, s.OutDir())
	if err != nil {
		s.Fatal("Failed to get the Chameleond instance: ", err)
	}

	connectedPortMap := graphics.ChameleonGetConnectedPortMap(ctx)

	s.Logf("Inspecting port %s", portStr)
	shouldUsePort, port, err := graphics.ChameleonShouldUsePort(ctx, cham, portStr)
	if err != nil {
		s.Fatalf("Failed to determine if plug can be used for port %d: %s", port, err)
	}

	// Check if the port is part of connectedports - if yes, it should be connected to be passed, if no, it should not be connected to be passed
	isSupposedConnected := connectedPortMap[graphics.ChameleonGetPortname(portStr)]

	if isSupposedConnected && !shouldUsePort {
		s.Fatalf("Port %d (%s) is not plugged in Chameleon but expected to be", port, portStr)
	}

	if !isSupposedConnected && shouldUsePort {
		s.Fatalf("Port %d (%s) is plugged in Chameleon even though it should not be", port, portStr)
	}

	if !isSupposedConnected && !shouldUsePort {
		s.Logf("Port %d (%s) is not plugged in Chameleon as expected", port, portStr)
	} else {

		if err = graphics.ChameleonSetEdid(ctx, cham, port); err != nil {
			s.Fatalf("Failed to set the EDID for a monitor on port %d: %s", port, err)
		}

		if err = graphics.ChameleonPlug(ctx, cham, port); err != nil {
			s.Fatalf("Failed to get stable video input from a physically plugged port %d: %s", port, err)
		}
		defer func(ctx context.Context, port chameleon.PortID) {
			err = cham.Unplug(ctx, port)
			if err != nil {
				s.Fatalf("Failed to unplug a physically plugged port %d: %s ", port, err)
			}
		}(ctx, port)

		// TODO(b:263163784): Replace ChameleonGetScreenshot with screen-util-tools command.
		curChamPath := filepath.Join(s.OutDir(), "cham_"+strconv.Itoa(int(port))+".png")
		if err = graphics.ChameleonGetScreenshot(ctx, cham, port, curChamPath); err != nil {
			s.Errorf("Cannot get Chameleon screenshot on port %d: %s", port, err)
		}

		err = cham.Unplug(ctx, port)
		if err != nil {
			s.Fatalf("Failed to unplug a physically plugged port %d: %s ", port, err)
		}
	}
}
