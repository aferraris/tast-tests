// Copyright 2023 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package rtc contains local Tast tests that exercise Real Time Communication usage.
package rtc
