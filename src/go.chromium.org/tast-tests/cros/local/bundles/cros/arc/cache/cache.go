// Copyright 2020 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package cache

import (
	"context"
	"fmt"
	"io/fs"
	"io/ioutil"
	"os"
	"path/filepath"
	"regexp"
	"sort"
	"strconv"
	"strings"
	"time"

	"go.chromium.org/tast-tests/cros/common/testexec"
	"go.chromium.org/tast-tests/cros/local/arc"
	"go.chromium.org/tast-tests/cros/local/chrome"
	"go.chromium.org/tast/core/ctxutil"
	"go.chromium.org/tast/core/errors"
	"go.chromium.org/tast/core/fsutil"
	"go.chromium.org/tast/core/testing"
)

// PackagesMode represents a flag that determines whether packages_cache.xml
// will be copied within ARC after boot.
type PackagesMode int

const (
	// PackagesCopy forces to set caches and copy packages cache to the preserved destination.
	PackagesCopy PackagesMode = iota
	// PackagesSkipCopy forces to ignore caches and copy packages cache to the preserved destination.
	PackagesSkipCopy
)

// GMSCoreMode represents a flag that determines whether existing GMS Core caches
// would be used or not.
type GMSCoreMode int

const (
	// GMSCoreEnabled requires to use existing GMS Core caches if they are available.
	GMSCoreEnabled GMSCoreMode = iota
	// GMSCoreDisabled requires not to use existing GMS Core caches.
	GMSCoreDisabled
)

// pathCondition represents whether checkPath and checkPathNoMount should expect the path to exist
// or not to exist.
type pathCondition int

const (
	pathMustExist pathCondition = iota
	pathMustNotExist
	pathMustExistAndHasData
)

const (
	// LayoutTxt defines output file name that contains generated file directory layout and
	// file attributes.
	LayoutTxt = "layout.txt"
	// PackagesCacheXML defines the name of packages cache file name.
	PackagesCacheXML = "packages_cache.xml"
	// GeneratedPackagesCacheXML defines the name of pregenerated packages cache file name.
	// Used to rename the cache file retrieved from /system/etc.
	GeneratedPackagesCacheXML = "generated_packages_cache.xml"
	// GMSCoreCacheArchive defines the GMS Core cache tar file name.
	GMSCoreCacheArchive = "gms_core_cache.tar"
	// GMSCoreManifest defines the GMS Core manifest file that lists GMS Core release files
	// with size, modification time in milliseconds, and SHA256.
	// of GMS Core release.
	GMSCoreManifest = "gms_core_manifest"
	// GSFCache defines the GSF cache database file name.
	GSFCache = "gservices_cache.db"
	// TTSStateCache defines the TTS state cache file name.
	TTSStateCache = "tts_state_cache.dat"
	// PregeneratedTTSStateCache defines the name of the pregenerated tts state cache file name.
	// Used to retrieve the tts cache included in the Android image from /system/etc.
	PregeneratedTTSStateCache = "pregen_tts_state_cache.dat"
	// Timeout to wait GMS Core resources.
	gmsCoreWaitTimeout = 4 * time.Minute
	// DexOptCacheArchive is the name of the tar file that includes DexOpt artifacts.
	DexOptCacheArchive = "dex_opt_cache.tar"
)

// OpenSession starts Chrome and ARC with extra arguments.
func OpenSession(ctx context.Context, extraArgs []string, outputDir string) (cr *chrome.Chrome, a *arc.ARC, retErr error) {
	args := arc.DisableSyncFlags()
	args = append(args, extraArgs...)

	// Signs in as chrome.DefaultUser.
	cr, err := chrome.New(ctx,
		chrome.ARCEnabled(),
		chrome.UnRestrictARCCPU(),
		chrome.ExtraArgs(args...))
	if err != nil {
		return nil, nil, errors.Wrap(err, "failed to login to Chrome")
	}

	a, err = arc.New(ctx, outputDir, cr.NormalizedUser())
	if err != nil {
		cr.Close(ctx)
		return nil, nil, errors.Wrap(err, "could not start ARC")
	}
	return cr, a, nil
}

// OpenGmsCoreSession starts Chrome and ARC with GMS Core caches turned on or off, depending on the mode parameter.
// On success, non-nil pointers are returned that must be closed by the calling function.
// However, if there is an error, both pointers will be nil
func OpenGmsCoreSession(ctx context.Context, packagesMode PackagesMode, gmsCoreMode GMSCoreMode, extraArgs []string, outputDir string) (cr *chrome.Chrome, a *arc.ARC, retErr error) {
	args := []string{"--arc-disable-download-provider"}
	switch packagesMode {
	case PackagesSkipCopy:
		args = append(args, "--arc-packages-cache-mode=skip-copy")
	case PackagesCopy:
		args = append(args, "--arc-packages-cache-mode=copy")
	default:
		return nil, nil, errors.Errorf("invalid packagesMode %d passed", packagesMode)
	}
	switch gmsCoreMode {
	case GMSCoreEnabled:
	case GMSCoreDisabled:
		args = append(args, "--arc-disable-gms-core-cache")
	default:
		return nil, nil, errors.Errorf("invalid gmsCoreMode %d passed", gmsCoreMode)
	}

	args = append(args, extraArgs...)

	return OpenSession(ctx, args, outputDir)
}

func waitGmsCoreUpdated(ctx context.Context, a *arc.ARC, gmsRootUnderHome string) error {
	const (
		configPrefs = "shared_prefs/ChimeraConfigService.xml"
	)

	configPrefsPath := filepath.Join(gmsRootUnderHome, configPrefs)
	testing.ContextLogf(ctx, "Waiting GMS Core updated %q", configPrefsPath)

	lastUpdatedRe := regexp.MustCompile(`<long\sname="Chimera.lastUpdateTime"\s+value="(\d+)"\s*/>`)

	var t time.Time

	if err := arc.PollWithReadOnlyAndroidData(ctx, chrome.DefaultUser, func(ctx context.Context) error {
		if _, err := os.Stat(configPrefsPath); err != nil {
			if os.IsNotExist(err) {
				return errors.Wrapf(err, "path %s still does not exist", configPrefsPath)
			}
			return arc.PollBreakIfNotEUCLEANOnVirtioBlkData(ctx, errors.Wrapf(err, "failed to stat %s", configPrefsPath))
		}

		content, err := a.ReadXMLFile(ctx, configPrefsPath)
		if err != nil {
			return testing.PollBreak(errors.Wrapf(err, "failed to read GMS Core prefs %q", configPrefsPath))
		}

		timestamp := lastUpdatedRe.FindStringSubmatch(string(content))
		if timestamp == nil {
			return errors.New("lastUpdateTime not yet set")
		}
		// Epoch time in millis.
		epoch, err := strconv.ParseInt(timestamp[1], 10, 64)
		if err != nil {
			return testing.PollBreak(errors.Wrapf(err, "failed to parse timestamp %q", timestamp[1]))
		}

		t = time.UnixMilli(epoch)
		return nil
	}, &testing.PollOptions{Timeout: gmsCoreWaitTimeout, Interval: time.Second}); err != nil {
		return errors.Wrap(err, "failed to wait GMS Core updated")
	}

	testing.ContextLogf(ctx, "Waited GMS Core updated. timestamp %s", t.String())
	return nil
}

// CopyGmsCoreCaches waits for required caches are ready and copies them to the specified output directory.
func CopyGmsCoreCaches(ctx context.Context, a *arc.ARC, outputDir string) error {
	const (
		gmsRoot      = "/data/user_de/0/com.google.android.gms"
		appChimera   = "app_chimera"
		packagesPath = "/data/system/packages_copy.xml"
		gsfDatabase  = "/data/data/com.google.android.gsf/databases/gservices.db"
	)

	cleanupCtx := ctx
	ctx, cancel := ctxutil.Shorten(ctx, 3*time.Second)
	defer cancel()

	// OpenSession signs in as chrome.DefaultUser.
	androidDataDir, err := arc.AndroidDataDir(ctx, chrome.DefaultUser)
	if err != nil {
		return errors.Wrap(err, "failed to get android-data path")
	}

	gmsRootUnderHome := filepath.Join(androidDataDir, gmsRoot)
	chimeraPath := filepath.Join(gmsRootUnderHome, appChimera)

	if err = waitGmsCoreUpdated(ctx, a, gmsRootUnderHome); err != nil {
		return err
	}

	for _, e := range []struct {
		filename string
		cond     pathCondition
	}{
		{"current_config.fb", pathMustExist},
		{"current_fileapks.pb", pathMustExist},
		{"current_features.fb", pathMustExist},
		{"stored_modulesets.pb", pathMustExist},
		{"current_modules_init.pb", pathMustNotExist},
		{"pending_modules_init.pb", pathMustNotExist},
	} {
		if err := checkPath(ctx, filepath.Join(chimeraPath, e.filename), e.cond); err != nil {
			return err
		}
	}

	version, err := arc.SDKVersion()
	if err != nil {
		return errors.Wrap(err, "failed to get SDK version")
	}

	if err := waitForApksOptimized(ctx, chimeraPath, version); err != nil {
		return err
	}

	if err := waitForPathStabilized(ctx, filepath.Join(chimeraPath, "current_config.fb")); err != nil {
		return err
	}

	targetTar := filepath.Join(outputDir, GMSCoreCacheArchive)

	testing.ContextLogf(ctx, "Compressing GMS Core caches to %q", targetTar)

	cleanupFunc, err := arc.MountVirtioBlkDataDiskImageReadOnlyIfUsed(ctx, chrome.DefaultUser)
	if err != nil {
		return errors.Wrap(err, "failed to make Android /data directory available on host")
	}
	defer cleanupFunc(cleanupCtx)

	if err := testexec.CommandContext(ctx, "tar", "-cvpf", targetTar, "-C", gmsRootUnderHome, appChimera).Run(testexec.DumpLogOnError); err != nil {
		return errors.Wrap(err, "failed to compress GMS Core caches")
	}

	testing.ContextLogf(ctx, "Collecting stat results for files under %q", chimeraPath)

	// statResult holds data obtained by stat command.
	type statResult struct {
		path         string
		accessRights string // Obtained by "stat -c %a"
		numBlocks    string // Obtained by "stat -c %b"
	}
	var statResults []statResult
	filepath.Walk(chimeraPath, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		// Strip "/home/root/${USER_HASH}/android-data" prefix from the path.
		androidPath := strings.Replace(path, androidDataDir, "", 1)

		info, err = os.Lstat(path)
		if err != nil {
			return errors.Wrapf(err, "failed to stat %q", path)
		}
		if info.Mode()&os.ModeSymlink == os.ModeSymlink {
			// File is a symbolic link.
			realPath, err := os.Readlink(path)
			if err != nil {
				return errors.Wrapf(err, "failed to get real path for %q", path)
			}
			if !strings.HasPrefix(realPath, "/data/") {
				// File is a symbolic link to a file outside of /data/
				// and it is not exposed on CrOS file system.
				// Run stat command on Android file system via adb shell.
				out, err := a.Command(
					ctx, "stat", "-c", "%a:%b", realPath).Output(testexec.DumpLogOnError)
				statVals := strings.Split(strings.TrimSpace(string(out)), ":")
				if err != nil || len(statVals) != 2 {
					return errors.Wrapf(err, "failed to stat %q : %q", realPath, string(out))
				}
				statResults = append(statResults,
					statResult{path: androidPath, accessRights: statVals[0], numBlocks: statVals[1]})
				return nil
			}
		}

		// File is not a symbolic link, or a symbolic link to a file under /data/,
		// which is exposed on CrOS file system. Run stat command on CrOS.
		out, err := testexec.CommandContext(ctx, "stat", "-c", "%a:%b", path).Output()
		statVals := strings.Split(strings.TrimSpace(string(out)), ":")
		if err != nil || len(statVals) != 2 {
			return errors.Wrapf(err, "failed to stat %q : %q", path, string(out))
		}
		statResults = append(statResults,
			statResult{path: androidPath, accessRights: statVals[0], numBlocks: statVals[1]})
		return nil
	})

	testing.ContextLogf(ctx, "Generating %q from collected stat results", LayoutTxt)
	var layoutLines []string
	for _, r := range statResults {
		layoutLines = append(layoutLines, fmt.Sprintf("%s:%s:%s", r.path, r.accessRights, r.numBlocks))
	}
	sort.Strings(layoutLines)
	layout := strings.Join(layoutLines, "\n")
	if err := ioutil.WriteFile(filepath.Join(outputDir, LayoutTxt), []byte(layout), 0644); err != nil {
		return errors.Wrapf(err, "failed to generate %q for %q", LayoutTxt, chimeraPath)
	}

	// Packages cache.
	// Note, packages cache is in binary format since RVC.
	// Use helper to automatically decode it.
	src := filepath.Join(androidDataDir, packagesPath)
	packagesXML, err := a.ReadXMLFile(ctx, src)
	if err != nil {
		return errors.Wrapf(err, "failed to read and parse packages cache XML %q", packagesPath)
	}
	packagesPathLocal := filepath.Join(outputDir, PackagesCacheXML)
	if err := ioutil.WriteFile(packagesPathLocal, packagesXML, 0644); err != nil {
		return errors.Wrapf(err, "failed to write the parsed packages cache XML content to %q", packagesPathLocal)
	}

	// GSF cache
	src = filepath.Join(androidDataDir, gsfDatabase)
	dst := filepath.Join(outputDir, GSFCache)
	if err := fsutil.CopyFile(src, dst); err != nil {
		return err
	}

	// Extract GMS Core location and create manifest for this directory.
	gmsCorePath := regexp.MustCompile(`<package name=\"com\.google\.android\.gms\".+codePath=\"(\S+)\".+>`).FindStringSubmatch(string(packagesXML))
	if gmsCorePath == nil {
		return errors.Wrapf(err, "failed to parse %q", packagesPathLocal)
	}

	manifestPath := filepath.Join(outputDir, GMSCoreManifest)
	testing.ContextLogf(ctx, "Capturing GMS Core manifest for %q to %q", gmsCorePath[1], GMSCoreManifest)
	// stat -c "%n %s" "$0" gives name and file size in bytes
	// date +%s%N -r "$0" | cut -b1-13 gives modification time with millisecond resolution.
	// sha256sum -b "$0" gives sha256 check sum
	// tr \"\n\" \" \" to remove new line ending and have 3 commands outputs in one line.
	const perFileCmd = `stat -c "%n %s" "$0" | tr "\n" " "  && date +%s%N -r "$0" | cut -b1-13 | tr "\n" " "  && sha256sum -b "$0"`
	out, err := a.Command(
		ctx, "/system/bin/find", "-L", gmsCorePath[1], "-type", "f", "-exec", "sh", "-c", perFileCmd, "{}", ";").Output(testexec.DumpLogOnError)
	if err != nil {
		return errors.Wrapf(err, "failed to create GMS Core manifiest: %q", string(out))
	}

	if err := ioutil.WriteFile(manifestPath, out, 0644); err != nil {
		return errors.Wrapf(err, "failed to save GMS Core manifest to %q", manifestPath)
	}

	return nil
}

// CopyTTSCache waits for the TTS cache to be ready and copies them to the specified output directory.
func CopyTTSCache(ctx context.Context, outputDir string) error {
	const (
		ttsCacheAndroidPath = "/data/data/org.chromium.arc.intent_helper/files/tts_state.dat"
	)

	// OpenSession signs in as chrome.DefaultUser.
	androidDataDir, err := arc.AndroidDataDir(ctx, chrome.DefaultUser)
	if err != nil {
		return errors.Wrap(err, "failed to get android-data path")
	}

	ttsCachePath := filepath.Join(androidDataDir, ttsCacheAndroidPath)

	return arc.PollWithReadOnlyAndroidData(ctx, chrome.DefaultUser, func(context.Context) error {
		if err := checkPathNoMount(ctx, ttsCachePath, pathMustExistAndHasData); err != nil {
			return err
		}
		// Although CopyFile isn't retried, it needs to be called inside the polling function
		// because the source file is in Android /data, which needs to be mounted if virtio-blk
		// /data is used.
		if err := fsutil.CopyFile(ttsCachePath, filepath.Join(outputDir, TTSStateCache)); err != nil {
			return testing.PollBreak(err)
		}
		return nil
	}, &testing.PollOptions{Timeout: gmsCoreWaitTimeout, Interval: time.Second})
}

// CopyDexOptCache waits for the CPU to be idle and compress the DEX code
// compilation artifacts into a tar file in the specified output directory.
func CopyDexOptCache(ctx context.Context, outputDir string) error {
	const (
		androidDataPath      = "/data"
		dexOptCacheDirectory = "dalvik-cache"
	)

	cleanupCtx := ctx
	ctx, cancel := ctxutil.Shorten(ctx, 3*time.Second)
	defer cancel()

	// OpenSession signs in as chrome.DefaultUser.
	androidDataDir, err := arc.AndroidDataDir(ctx, chrome.DefaultUser)
	if err != nil {
		return errors.Wrap(err, "failed to get android-data path")
	}

	cleanupFunc, err := arc.MountVirtioBlkDataDiskImageReadOnlyIfUsed(ctx, chrome.DefaultUser)
	if err != nil {
		return errors.Wrap(err, "failed to make Android /data directory available on host")
	}
	defer cleanupFunc(cleanupCtx)

	dataPath := filepath.Join(androidDataDir, androidDataPath)
	fileCount := 0
	err = filepath.Walk(filepath.Join(dataPath, dexOptCacheDirectory), func(path string, info fs.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if info.Mode().IsRegular() {
			fileCount++
		}
		return nil
	})
	if err != nil {
		return errors.New("failed to count generated dexopt cache files")
	}
	testing.ContextLogf(ctx, "%d files were generated for DexOpt cache", fileCount)
	if fileCount == 0 {
		return errors.New("no dexopt cache was generated")
	}

	targetTar := filepath.Join(outputDir, DexOptCacheArchive)
	// PlayAutoInstall config is ignored because the apk is board-specific and will be ignored on boot.
	tarExcludeOption := "--exclude=vendor@app@PlayAutoInstallConfig@*"
	testing.ContextLogf(ctx, "Compressing DexOpt caches to %q", targetTar)
	if err := testexec.CommandContext(ctx, "tar", tarExcludeOption, "-cvpf", targetTar, "-C", dataPath, dexOptCacheDirectory).Run(testexec.DumpLogOnError); err != nil {
		return errors.Wrap(err, "failed to compress DexOpt caches")
	}

	return nil
}

// checkPathNoMount checks if specified path exists or does not exist depending on pathCondition c
// (i.e. pathMustExist).
func checkPathNoMount(ctx context.Context, path string, c pathCondition) error {
	statInfo, err := os.Stat(path)
	if err != nil && !os.IsNotExist(err) {
		return errors.Wrapf(err, "failed to stat %s", path)
	}
	exists := err == nil
	switch c {
	case pathMustExist:
		if !exists {
			return errors.Wrapf(err, "path %s still does not exist", path)
		}
	case pathMustExistAndHasData:
		if !exists {
			return errors.Wrapf(err, "path %s still does not exist", path)
		}
		if statInfo.Size() == 0 {
			return errors.Wrapf(err, "path %s exists but empty", path)
		}
	case pathMustNotExist:
		if exists {
			return errors.Wrapf(err, "path %s still exists", path)
		}
	}

	return nil
}

// checkPath wraps checkPathNoMount with optionally mounting Android /data disk image on host for
// virtio-blk /data enabled devices.
func checkPath(ctx context.Context, path string, c pathCondition) error {
	testing.ContextLogf(ctx, "Check path %q", path)

	cleanupCtx := ctx
	ctx, cancel := ctxutil.Shorten(ctx, 3*time.Second)
	defer cancel()

	cleanupFunc, err := arc.MountVirtioBlkDataDiskImageReadOnlyIfUsed(ctx, chrome.DefaultUser)
	if err != nil {
		return errors.Wrap(err, "failed to make Android /data directory available on host")
	}
	defer cleanupFunc(cleanupCtx)

	return checkPathNoMount(ctx, path, c)
}

// waitForPathStabilized waits up to gmsCoreWaitTimeout or ctx deadline for the specified path
// is stabilized.
func waitForPathStabilized(ctx context.Context, path string) error {
	const (
		// Minimal duration when file considered stable.
		minStableDuration = 20 * time.Second
	)

	testing.ContextLogf(ctx, "Waiting for %q stabilized", path)
	if err := arc.PollWithReadOnlyAndroidData(ctx, chrome.DefaultUser, func(ctx context.Context) error {
		statInfo, err := os.Stat(path)
		if err != nil {
			return arc.PollBreakIfNotEUCLEANOnVirtioBlkData(ctx, errors.Wrapf(err, "failed to stat  %s", path))
		}

		stableDuration := time.Now().Sub(statInfo.ModTime())
		if stableDuration < minStableDuration {
			return errors.Errorf("path %q is not yet stable. Time since modified: %s", path, stableDuration.String())
		}

		return nil
	}, &testing.PollOptions{Timeout: gmsCoreWaitTimeout, Interval: 5 * time.Second}); err != nil {
		return errors.Wrapf(err, "failed to wait for %s stabilized", path)
	}

	return nil
}

// waitForApksOptimized waits up to gmsCoreWaitTimeout or ctx deadline for all APKs in given root path
// is optimized, which means no *.flock locks and *.odex/*.vdex exist and matches actual APK count
// on PI and below.
func waitForApksOptimized(ctx context.Context, root string, sdkVersion int) error {
	testing.ContextLogf(ctx, "Waiting for APKs optimized %q", root)
	if err := arc.PollWithReadOnlyAndroidData(ctx, chrome.DefaultUser, func(ctx context.Context) error {
		// Calculate number of files per extension.
		perExtCnt := map[string]int{}
		// Modes for root of odex files.
		odexParentModes := map[string]os.FileMode{}
		if err := filepath.Walk(root, func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}
			if !info.IsDir() {
				ext := filepath.Ext(info.Name())
				perExtCnt[ext] = perExtCnt[ext] + 1
				if ext == ".odex" {
					parent := filepath.Dir(path)
					parentInfo, err := os.Stat(parent)
					if err != nil {
						return errors.Wrapf(err, "failed to stat odex parent %s", path)
					}
					odexParentModes[parent] = parentInfo.Mode()
				}
			}
			return nil
		}); err != nil {
			return arc.PollBreakIfNotEUCLEANOnVirtioBlkData(ctx, errors.Wrapf(err, "failed to walk %q", root))
		}

		for odexParent, mode := range odexParentModes {
			// Make sure parent has execute bits.
			if mode&0111 != 0111 {
				return errors.Errorf("odex parent %q has no execution bits in mode %s", odexParent, mode.String())
			}
		}

		apkCnt := perExtCnt[".apk"]
		if perExtCnt[".flock"] != 0 {
			return errors.Errorf("file lock detected in %q", root)
		}
		if apkCnt == 0 {
			return testing.PollBreak(errors.Errorf("no APK found in %q", root))
		}
		vdexCnt := perExtCnt[".vdex"]
		odexCnt := perExtCnt[".odex"]
		// Match internal GMS Core logic:
		// https://source.corp.google.com/piper///depot/google3/java/com/google/android/gmscore/integ/libs/chimera/module/src/com/google/android/chimera/container/DexOptUtils.java;l=34
		if sdkVersion < arc.SDKQ {
			if apkCnt != vdexCnt || apkCnt != odexCnt {
				return errors.Errorf("not everything yet optimized; APK count: %d, vdex: %d, odex: %d; expected each APK has odex and vdex", apkCnt, vdexCnt, odexCnt)
			}
		} else {
			if vdexCnt != 0 || odexCnt != 0 {
				return testing.PollBreak(errors.Errorf("optimization is not expected in Q+; APK count: %d, vdex: %d, odex: %d", apkCnt, vdexCnt, odexCnt))
			}
		}
		return nil
	}, &testing.PollOptions{Timeout: gmsCoreWaitTimeout, Interval: time.Second}); err != nil {
		return errors.Wrapf(err, "failed to wait for APKs optimized %s", root)
	}
	return nil
}
