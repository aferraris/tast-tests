// Copyright 2020 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package arc

// To update test parameters after modifying this file, run:
// TAST_GENERATE_UPDATE=1 ~/chromiumos/src/platform/tast/tools/go.sh test -count=1 go.chromium.org/tast-tests/cros/local/bundles/cros/arc/

import (
	"strings"
	"testing"

	"go.chromium.org/tast-tests/cros/common/genparams"
)

func genTestName(components []string) string {
	var nonEmptyComponents []string
	for _, s := range components {
		if len(s) == 0 {
			continue
		}
		nonEmptyComponents = append(nonEmptyComponents, s)
	}
	return strings.Join(nonEmptyComponents, "_")
}

func TestPowerIdlePerfParamsAreGenerated(t *testing.T) {
	type valMember struct {
		Key   string
		Value string
	}
	type paramData struct {
		Name         string
		Attr         []string
		SoftwareDeps []string
		HardwareDeps []string
		Fixture      string
		Timeout      string
		Extended     string
		Val          []valMember
	}
	var params []paramData
	for _, batteryMode := range []struct {
		name  string
		hwdep string
		val   []valMember
	}{
		{
			"",
			"hwdep.ForceDischarge()",
			[]valMember{{"setupOption", "setup.ForceBatteryDischarge"}},
		},
		{
			"nobatterymetrics",
			"hwdep.NoForceDischarge()",
			[]valMember{{"setupOption", "setup.NoBatteryDischarge"}},
		},
	} {
		for _, arcType := range []struct {
			name     string
			attr     string
			swdep    string
			fixture  string
			timeout  string
			extended string
		}{
			{
				"noarc",
				"",
				"arc", // to prevent _noarc tests from running on non-ARC boards
				"chromeLoggedInDisableSyncNoFwUpdate",
				"20 * time.Minute",
				"false",
			},
			{
				"noarc_extended",
				"crosbolt_weekly",
				"arc", // to prevent _noarc tests from running on non-ARC boards
				"chromeLoggedInDisableSyncNoFwUpdate",
				"75 * time.Minute",
				"true",
			},
			{
				"",
				"",
				"android_container",
				"arcBootedRestricted",
				"20 * time.Minute",
				"false",
			},
			{
				"vm",
				"",
				"android_vm",
				"arcBootedRestricted",
				"20 * time.Minute",
				"false",
			},
			{
				"extended",
				"crosbolt_weekly",
				"android_container",
				"arcBootedRestricted",
				"75 * time.Minute",
				"true",
			},
			{
				"vm_extended",
				"crosbolt_weekly",
				"android_vm",
				"arcBootedRestricted",
				"75 * time.Minute",
				"true",
			},
		} {
			name := genTestName([]string{arcType.name, batteryMode.name})
			p := paramData{
				Name:         string(name),
				SoftwareDeps: []string{arcType.swdep},
				HardwareDeps: []string{batteryMode.hwdep},
				Fixture:      arcType.fixture,
				Timeout:      arcType.timeout,
				Extended:     arcType.extended,
				Val:          batteryMode.val,
			}
			if arcType.attr != "" && batteryMode.name == "" {
				p.Attr = []string{arcType.attr}
			}
			params = append(params, p)
		}
	}

	code := genparams.Template(t, `{{ range . }}{
		{{ if .Name }}
		Name: {{ .Name | fmt }},
		{{ end }}
		{{ if .Attr }}
		ExtraAttr: {{ .Attr | fmt }},
		{{ end }}
		ExtraSoftwareDeps: {{ .SoftwareDeps | fmt }},
		ExtraHardwareDeps: hwdep.D({{ range .HardwareDeps }}{{ . }},{{ end }}),
		Val: testArgsForPowerIdlePerf{
			{{ range .Val }}{{ .Key }}: {{ .Value }},
			{{ end }}
			extendedTest: {{ .Extended }},
		},
		Fixture: "{{ .Fixture }}",
		Timeout: {{ .Timeout }},
	},
	{{ end }}`, params)
	genparams.Ensure(t, "power_idle_perf.go", code)
}
