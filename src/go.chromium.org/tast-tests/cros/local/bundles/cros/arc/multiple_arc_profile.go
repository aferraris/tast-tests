// Copyright 2021 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package arc

import (
	"context"
	"path/filepath"
	"time"

	androidui "go.chromium.org/tast-tests/cros/common/android/ui"
	"go.chromium.org/tast-tests/cros/common/dma"
	"go.chromium.org/tast-tests/cros/common/testexec"
	"go.chromium.org/tast-tests/cros/common/ui"
	"go.chromium.org/tast-tests/cros/local/accountmanager"
	"go.chromium.org/tast-tests/cros/local/apps"
	"go.chromium.org/tast-tests/cros/local/arc"
	"go.chromium.org/tast-tests/cros/local/arc/optin"
	"go.chromium.org/tast-tests/cros/local/arc/playstore"
	"go.chromium.org/tast-tests/cros/local/chrome"
	"go.chromium.org/tast-tests/cros/local/chrome/ash"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto/faillog"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto/launcher"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto/nodewith"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto/ossettings"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto/role"
	"go.chromium.org/tast-tests/cros/local/input"
	"go.chromium.org/tast/core/errors"
	"go.chromium.org/tast/core/testing"
)

func init() {
	testing.AddTest(&testing.Test{
		Func:           MultipleArcProfile,
		LifeCycleStage: testing.LifeCycleOwnerMonitored,
		LacrosStatus:   testing.LacrosVariantUnneeded,
		Desc:           "Verify that Second Account can be added from ARC Settings ",
		Contacts:       []string{"cros-arc-te@google.com", "arc-core@google.com", "jinrongwu@google.com"},
		// ChromeOS > Software > ARC++ > EngProd
		BugComponent: "b:1052117",
		Attr:         []string{"group:arc", "arc_core", "group:arc-functional"},
		SoftwareDeps: []string{"chrome"},
		Params: []testing.Param{{
			ExtraSoftwareDeps: []string{"android_container"},
		}, {
			Name:              "vm",
			ExtraAttr:         []string{"group:hw_agnostic"},
			ExtraSoftwareDeps: []string{"android_vm"},
		}},
		Timeout: chrome.LoginTimeout + arc.BootTimeout + 6*time.Minute,
		VarDeps: []string{ui.GaiaPoolDefaultVarName, "arc.parentUser", "arc.parentPassword"},
	})
}

func MultipleArcProfile(ctx context.Context, s *testing.State) {

	cr, err := chrome.New(ctx,
		chrome.GAIALoginPool(dma.CredsFromPool(ui.GaiaPoolDefaultVarName)),
		chrome.ARCSupported(),
		chrome.ExtraArgs(arc.DisableSyncFlags()...))
	if err != nil {
		s.Fatal("Failed to start Chrome: ", err)
	}
	defer cr.Close(ctx)

	tconn, err := cr.TestAPIConn(ctx)
	if err != nil {
		s.Fatal("Failed to connect Test API: ", err)
	}
	defer faillog.DumpUITreeOnError(ctx, s.OutDir(), s.HasError, tconn)

	if err := optin.PerformAndClose(ctx, cr, tconn); err != nil {
		s.Fatal("Failed to optin to Play Store and Close: ", err)
	}

	// Setup ARC.
	a, err := arc.New(ctx, s.OutDir(), cr.NormalizedUser())
	if err != nil {
		s.Fatal("Failed to connect to ARC: ", err)
	}
	defer a.Close(ctx)
	defer func() {
		if s.HasError() {
			if err := a.Command(ctx, "uiautomator", "dump").Run(testexec.DumpLogOnError); err != nil {
				s.Error("Failed to dump UIAutomator: ", err)
			} else if err := a.PullFile(ctx, "/sdcard/window_dump.xml",
				filepath.Join(s.OutDir(), "uiautomator_dump.xml")); err != nil {
				s.Error("Failed to pull UIAutomator dump: ", err)
			}
		}
	}()

	d, err := a.NewUIDevice(ctx)
	if err != nil {
		s.Fatal("Failed initializing UI Automator: ", err)
	}
	defer d.Close(ctx)

	s.Log("Add ARC Account")
	if err := addARCAccount(ctx, d, tconn, s); err != nil {
		s.Fatal("Failed to Add Account: ", err)
	}

	if err := arc.SwitchPlayStoreAccount(ctx, d, tconn, s.RequiredVar("arc.parentUser")); err != nil {
		s.Fatal("Failed to Switch Account: ", err)
	}

	var translateinstalled bool
	s.Log("Installing an app in new acount")

	if err := playstore.InstallApp(ctx, a, d, "com.google.android.apps.dynamite", &playstore.Options{}); err != nil {
		s.Log("Failed to install chat app: ", err)
		if err := playstore.InstallApp(ctx, a, d, "com.google.android.apps.translate", &playstore.Options{}); err != nil {
			s.Fatal("Failed to install translate app: ", err)
		}
		translateinstalled = true
	}

	// Check the newly downloaded app in Launcher.
	if translateinstalled {
		if err := launcher.LaunchAndWaitForAppOpen(tconn, apps.Translate)(ctx); err != nil {
			s.Fatal("Failed to launch Translate app: ", err)
		}
		return
	}
	// TODO(b/210702593): Replace with LaunchAndWaitForAppOpen once fixed.
	if err := uiauto.Retry(3, launcher.LaunchApp(tconn, apps.Chat.ShortName()))(ctx); err != nil {
		s.Fatal("Failed to launch Chat app after 3 retries: ", err)
	}

	ui := uiauto.New(tconn)
	chatButton := nodewith.Name(apps.Chat.Name).ClassName(ash.ShelfAppButtonClassName)
	if err := ui.WaitUntilExists(chatButton)(ctx); err != nil {
		s.Fatal("Failed to find Google Chat in Shelf: ", err)
	}

}

// openARCSettings opens the ARC Settings Page from Chrome Settings.
func openARCSettings(ctx context.Context, tconn *chrome.TestConn) error {
	settings, err := ossettings.LaunchAtPage(ctx, tconn, ossettings.Apps)
	if err != nil {
		return errors.Wrap(err, "failed to open settings page")
	}
	playStoreButton := nodewith.Name("Google Play Store").Role(role.Button)
	if err := uiauto.Combine("Open Android Settings",
		settings.FocusAndWait(playStoreButton),
		settings.LeftClick(playStoreButton),
		settings.LeftClick(nodewith.Name("Android Settings").Role(role.Link)),
	)(ctx); err != nil {
		return errors.Wrap(err, "failed to open ARC settings page")
	}
	return nil
}

// addARCAccount adds a second ARC account from ARC Settings->Accounts Screen.
func addARCAccount(ctx context.Context, arcDevice *androidui.Device, tconn *chrome.TestConn, s *testing.State) error {
	ui := uiauto.New(tconn)
	secondUser := s.RequiredVar("arc.parentUser")
	secondPassword := s.RequiredVar("arc.parentPassword")

	// Set up keyboard.
	kb, err := input.Keyboard(ctx)
	if err != nil {
		return errors.Wrap(err, "failed to get keyboard")
	}
	defer kb.Close(ctx)

	if err := uiauto.Retry(3, func(ctx context.Context) error {
		// Press "Esc" to close the window that left by previous try.
		if err := kb.Accel(ctx, "Esc"); err != nil {
			return errors.Wrap(err, "failed to press Esc key")
		}

		if err := openARCSettings(ctx, tconn); err != nil {
			s.Fatal("Failed to Open ARC Settings: ", err)
		}

		if err := arc.ClickAddAccountInSettings(ctx, arcDevice, tconn); err != nil {
			return errors.Wrap(err, "failed to open Add account dialog from ARC")
		}

		if err := accountmanager.AddAccount(ctx, tconn, secondUser, secondPassword); err != nil {
			return errors.Wrap(err, "failed to add account")
		}

		return nil
	})(ctx); err != nil {
		return errors.Wrap(err, "failed to add after retry")
	}

	if err := ui.WaitUntilExists(nodewith.Name("Android Settings").Role(role.Link).Focused())(ctx); err != nil {
		return errors.Wrap(err, "failed to find Android Settings link")
	}
	return nil
}
