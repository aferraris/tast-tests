// Copyright 2023 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package ureadahead contains helper functions to test host and guest ureadahead
// functionalities such as dumping generated pack file and validity checks.
package ureadahead
