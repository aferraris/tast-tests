// Copyright 2022 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package webstreaming contains the test code for WebStreaming.
package webstreaming

import (
	"context"
	"fmt"
	"time"

	"go.chromium.org/tast-tests/cros/common/action"
	"go.chromium.org/tast-tests/cros/common/perf"
	"go.chromium.org/tast-tests/cros/local/apps"
	"go.chromium.org/tast-tests/cros/local/chrome"
	"go.chromium.org/tast-tests/cros/local/chrome/apps/thirdparty/googledocs"
	"go.chromium.org/tast-tests/cros/local/chrome/browser"
	"go.chromium.org/tast-tests/cros/local/chrome/cuj"
	"go.chromium.org/tast-tests/cros/local/chrome/display"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto/faillog"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto/nodewith"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto/role"
	"go.chromium.org/tast-tests/cros/local/input"
	"go.chromium.org/tast-tests/cros/local/ui/cujrecorder"
	"go.chromium.org/tast/core/ctxutil"
	"go.chromium.org/tast/core/errors"
	"go.chromium.org/tast/core/testing"
)

// TestParams stores data common to the tests run in this package.
type TestParams struct {
	BrowserType browser.Type
	VideoOption VideoOption
}

const (
	crosVideoTitle  = "CrosVideo"
	googleDocsTitle = "Google Docs"
)

// Run runs the WebStreaming test.
func Run(ctx context.Context, cr *chrome.Chrome, outDir, traceConfigPath string, tabletMode bool, bt browser.Type, videoOption VideoOption) error {
	cleanupCtx := ctx
	ctx, cancel := ctxutil.Shorten(ctx, 15*time.Second)
	defer cancel()

	tconn, err := cr.TestAPIConn(ctx)
	if err != nil {
		return errors.Wrap(err, "failed to create test API connection")
	}

	kb, err := input.Keyboard(ctx)
	if err != nil {
		return errors.Wrap(err, "failed to open the keyboard")
	}
	defer kb.Close(ctx)

	// Give 10 seconds to set initial settings. It is critical to ensure
	// cleanupSetting can be executed with a valid context so it has its
	// own cleanup context from other cleanup functions. This is to avoid
	// other cleanup functions executed earlier to use up the context time.
	cleanupSettingsCtx := ctx
	ctx, cancel = ctxutil.Shorten(ctx, 10*time.Second)
	defer cancel()
	cleanupSetting, err := cuj.InitializeSetting(ctx, tconn)
	if err != nil {
		return errors.Wrap(err, "failed to set initial settings")
	}
	defer cleanupSetting(cleanupSettingsCtx)

	testing.ContextLog(ctx, "Start to get browser start time")
	l, browserStartTime, err := cuj.GetBrowserStartTime(ctx, tconn, true, tabletMode, bt)
	if err != nil {
		return errors.Wrap(err, "failed to get browser start time")
	}
	br := cr.Browser()
	if l != nil {
		defer l.Close(ctx)
		br = l.Browser()
	}
	bTconn, err := br.TestAPIConn(ctx)
	if err != nil {
		return errors.Wrapf(err, "failed to create Test API connection for %v browser", bt)
	}

	// uiHandler will be assigned with different instances for clamshell and tablet mode.
	var uiHandler cuj.UIActionHandler
	if tabletMode {
		if uiHandler, err = cuj.NewTabletActionHandler(ctx, tconn); err != nil {
			return errors.Wrap(err, "failed to create tablet action handler")
		}
	} else {
		if uiHandler, err = cuj.NewClamshellActionHandler(ctx, tconn); err != nil {
			return errors.Wrap(err, "failed to create clamshell action handler")
		}
	}
	defer uiHandler.Close(ctx)

	// Shorten the context to cleanup recorder.
	cleanUpRecorderCtx := ctx
	ctx, cancel = ctxutil.Shorten(ctx, 5*time.Second)
	defer cancel()

	options := cujrecorder.NewPerformanceCUJOptions()
	recorder, err := cujrecorder.NewRecorder(ctx, cr, bTconn, nil, options)
	if err != nil {
		return errors.Wrap(err, "failed to create a recorder")
	}
	defer recorder.Close(cleanUpRecorderCtx)
	if err := cuj.AddPerformanceCUJMetrics(bt, tconn, bTconn, recorder); err != nil {
		return errors.Wrap(err, "failed to add metrics to recorder")
	}
	chromeApp, err := apps.PrimaryBrowser(ctx, tconn)
	if err != nil {
		return errors.Wrap(err, "could not find the Chrome app")
	}

	cleanupGoogleDoc := func(ctx context.Context) error {
		// Maximize the Google Docs window to delete Docs.
		if err := maximizeWindowSize(ctx, tabletMode, tconn, bTconn); err != nil {
			testing.ContextLog(ctx, "Failed to maximize the Google Docs page")
		}
		if err := googledocs.DeleteDoc(tconn)(ctx); err != nil {
			// Only log the error.
			testing.ContextLog(ctx, "Failed to clean up the document: ", err)
		}
		if err := cuj.CloseAllTabs(ctx, bTconn, bt); err != nil {
			return err
		}
		return nil
	}
	var decodedFrames, droppedFrames, droppedFramesPer float64

	if err := recorder.Run(ctx, func(ctx context.Context) (retErr error) {
		// Start tracing now.
		if traceConfigPath != "" {
			if err := recorder.StartTracing(ctx, outDir, traceConfigPath); err != nil {
				return errors.Wrap(err, "failed to start tracing")
			}
			defer recorder.StopTracing(ctx)
		}

		if err := googledocs.NewGoogleDocs(ctx, tconn, br, uiHandler, true); err != nil {
			return err
		}
		defer func(ctx context.Context) {
			faillog.DumpUITreeWithScreenshotOnError(ctx, outDir, func() bool { return retErr != nil }, cr, "ui_dump_docs")
			// Close browser to generate LCP2 metrics.
			if err := cuj.RunAndWaitLCPHistograms(ctx, bTconn, cleanupGoogleDoc); err != nil {
				testing.ContextLog(ctx, "Failed to run and wait for LCP histograms to update: ", err)
			}
		}(cleanupCtx)

		video, err := NewCrosVideo(ctx, tconn, uiHandler, br)
		if err != nil {
			return errors.Wrap(err, "failed to open cros video")
		}
		defer func(ctx context.Context) {
			faillog.DumpUITreeWithScreenshotOnError(ctx, outDir, func() bool { return retErr != nil }, cr, "ui_dump")
			// Close browser to generate LCP2 metrics.
			if err := cuj.RunAndWaitLCPHistograms(ctx, bTconn, video.Close); err != nil {
				testing.ContextLog(ctx, "Failed to run and wait for LCP histograms to update: ", err)
			}
		}(cleanupCtx)

		setFramesData := func(ctx context.Context) error {
			decodedFrames, droppedFrames, droppedFramesPer, err = video.FramesData(ctx)
			return err
		}

		return uiauto.NamedCombine("run and collect data",
			video.Play(videoOption),
			uiHandler.SwitchToAppWindowByName(chromeApp.Name, googleDocsTitle),
			putDocsWindowSideBySide(tabletMode, tconn, bTconn),
			webStreamingScenario(tconn, kb, video),
			uiHandler.SwitchToAppWindowByName(chromeApp.Name, crosVideoTitle),
			video.Pause(),
			setFramesData,
		)(ctx)
	}); err != nil {
		return errors.Wrap(err, "failed to run the web streaming scenario")
	}

	pv := perf.NewValues()
	pv.Set(perf.Metric{
		Name:      "Browser.StartTime",
		Unit:      "ms",
		Direction: perf.SmallerIsBetter,
	}, float64(browserStartTime.Milliseconds()))
	pv.Set(perf.Metric{
		Name:      "TPS.AppSpot.DecodedFrames",
		Unit:      "count",
		Direction: perf.BiggerIsBetter,
	}, decodedFrames)
	pv.Set(perf.Metric{
		Name:      "TPS.AppSpot.DroppedFrames",
		Unit:      "count",
		Direction: perf.SmallerIsBetter,
	}, droppedFrames)
	pv.Set(perf.Metric{
		Name:      "TPS.CrosVideo.DroppedFramesPct",
		Unit:      "percent",
		Direction: perf.SmallerIsBetter,
	}, droppedFramesPer)

	// Use a short timeout value so it can return fast in case of failure.
	recordCtx, cancel := context.WithTimeout(ctx, time.Minute)
	defer cancel()
	if err := recorder.Record(recordCtx, pv); err != nil {
		return errors.Wrap(err, "failed to report")
	}
	if err := recorder.SaveTraceFiles(recordCtx); err != nil {
		testing.ContextLog(recordCtx, "Failed to save trace files: ", err)
	}
	if err = pv.Save(outDir); err != nil {
		return errors.Wrap(err, "failed to store values")
	}
	if err := recorder.SaveHistograms(outDir); err != nil {
		return errors.Wrap(err, "failed to save histogram raw data")
	}

	return nil
}

func webStreamingScenario(tconn *chrome.TestConn, kb *input.KeyboardEventWriter, video *CrosVideo) action.Action {
	return func(ctx context.Context) error {
		const (
			docParagraph  = "The Little Prince's story follows a young prince who visits various planets in space."
			repeatTimeout = 15 * time.Minute
			retryTimes    = 3
		)
		taskNumber := 0
		repeatTask := func(ctx context.Context) error {
			var color, fontSize string
			if taskNumber%2 == 0 {
				color = "red"
				fontSize = "10"
			} else {
				color = "blue"
				fontSize = "8"
			}
			ui := uiauto.New(tconn)
			reloadDialog := nodewith.Name("Unable to load file").Role(role.Dialog)
			reloadButton := nodewith.Name("Reload").Role(role.Button).Ancestor(reloadDialog)

			// Some low-end DUTs sometimes click on the node and don't respond, or nodes can't be found.
			// Add retry to solve this problem.
			return uiauto.Retry(retryTimes, uiauto.NamedCombine(fmt.Sprintf("repeat task, number %d", taskNumber),
				uiauto.IfSuccessThen(ui.Exists(reloadButton), ui.LeftClick(reloadButton)),
				googledocs.EditDoc(tconn, kb, docParagraph),
				kb.AccelAction("Ctrl+A"),
				googledocs.ChangeDocTextColor(tconn, color),
				googledocs.ChangeDocFontSize(tconn, fontSize),
				googledocs.UndoDoc(tconn),
				googledocs.RedoDoc(tconn),
				kb.AccelAction("Backspace"),
				video.VerifyPlaying,
			))(ctx)
		}
		// Repeat the task for 15 minutes.
		now := time.Now()
		after := now.Add(repeatTimeout)
		for {
			taskNumber++
			if err := repeatTask(ctx); err != nil {
				return err
			}
			now = time.Now()
			if now.After(after) {
				break
			}
		}

		return nil
	}
}

func putDocsWindowSideBySide(tabletMode bool, tconn, bTconn *chrome.TestConn) action.Action {
	return func(ctx context.Context) error {
		// Google Docs requires at least 320 px height to edit files correctly.
		const expectedHeight = 320
		return setWindowSizeWithWorkArea(ctx, tabletMode, tconn, bTconn, expectedHeight, 0)
	}
}

// maximizeWindowSize sets the last focused window to maximized size.
func maximizeWindowSize(ctx context.Context, tabletMode bool, tconn, bTconn *chrome.TestConn) error {
	return setWindowSizeWithWorkArea(ctx, tabletMode, tconn, bTconn, 0, 0)
}

// setWindowSizeWithWorkArea sets the last focused window with WorkArea size.
// The default values ​​are the height and width of the workArea for the internal display.
// For lacros windows, use the lacros TestConn. For ash, use the ash TestConn.
func setWindowSizeWithWorkArea(ctx context.Context, tabletMode bool, tconn, bTconn *chrome.TestConn, expectedHeight, expectedWidth int) error {
	// Tablet mode can't set the window size, so no need to set it.
	if tabletMode {
		return nil
	}
	// Obtain the latest display info after rotating the display.
	info, err := display.GetInternalInfo(ctx, tconn)
	if err != nil {
		return errors.Wrap(err, "failed to obtain internal display info")
	}
	if expectedHeight == 0 {
		expectedHeight = info.WorkArea.Height
	}
	if expectedWidth == 0 {
		expectedWidth = info.WorkArea.Width
	}
	return setWindowSize(ctx, bTconn, expectedHeight, expectedWidth)
}

// setWindowSize sets the last focused window to specific size.
// For lacros windows, use the lacros TestConn. For ash, use the ash TestConn.
func setWindowSize(ctx context.Context, tconn *chrome.TestConn, height, width int) error {
	script := fmt.Sprintf(`async () => {
        const win = await tast.promisify(chrome.windows.getLastFocused)();
        await tast.promisify(chrome.windows.update)(win.id, {width: %d, height: %d, state:"normal"});
	}`, width, height)

	if err := tconn.Call(ctx, nil, script); err != nil {
		return errors.Wrap(err, "setting window size failed")
	}

	return nil
}
