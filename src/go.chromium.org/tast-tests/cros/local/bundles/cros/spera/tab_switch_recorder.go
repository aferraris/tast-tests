// Copyright 2022 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package spera

import (
	"context"
	"time"

	"go.chromium.org/tast-tests/cros/local/bundles/cros/spera/tabswitch"
	"go.chromium.org/tast-tests/cros/local/chrome"
	"go.chromium.org/tast-tests/cros/local/chrome/browser"
	"go.chromium.org/tast-tests/cros/local/chrome/cuj"
	"go.chromium.org/tast-tests/cros/local/wpr"
	"go.chromium.org/tast/core/testing"
)

func init() {
	testing.AddTest(&testing.Test{
		Func:         TabSwitchRecorder,
		LacrosStatus: testing.LacrosVariantUnneeded,
		Desc:         "Run tab-switching test in chromewpr recording mode",
		Contacts:     []string{"chromeos-perf-reliability-eng@google.com", "cienet-development@googlegroups.com", "alstonhuang@google.com"},
		BugComponent: "b:1025042", // ChromeOS > EngProd > Platform > SPERA > Automation
		SoftwareDeps: []string{"chrome"},
		Timeout:      30 * time.Minute,
		Params: []testing.Param{
			{
				Name: "local",
				Val:  "local",
				Pre:  wpr.RecordMode("/tmp/archive.wprgo"),
			}, {
				Name: "remote",
				Val:  "remote",
				// Remote WPR proxy is set up manually. Use RemoteReplayMode() to
				// get remote WPR address and start chrome accordingly.
				Pre: wpr.RemoteReplayMode(),
			},
		},
		Vars: []string{
			"spera.cuj_mute",
			// The following vars are only used by remote mode.
			"spera.wpr_http_addr",
			"spera.wpr_https_addr",
		},
	})
}

// TabSwitchRecorder runs tab switching test in chrome wpr recording mode. It will
// record the premium scenario, which can be used for basic and plus testing as well.
//
// The test can either do recording on a DUT local WPR server, or a remote WPR server.
// Local WPR server will be set up automatically by the preconditon.
// Steps to do remote recording:
//  1. Manually run wpr in record mode on a remote server.
//  2. Run this test.
//  3. Manually terminate wpr to output a record file on remote server.
//  4. Check remote wpr configureation to find the record file
func TabSwitchRecorder(ctx context.Context, s *testing.State) {
	cr, ok := s.PreValue().(*chrome.Chrome)
	if !ok {
		s.Fatal("Failed to connect to Chrome")
	}
	// is the dut tablet or not shouldn't affect to recording web content
	// Currently recorder is supported for ash-Chrome only. We call Run2() with lFixtVal as nil.
	// If support of lacros is needed, we need to enhance the test to pass lacrosFixtValue.
	tabswitch.Run(ctx, s, cr, cuj.Advanced, browser.TypeAsh, false, true)
}
