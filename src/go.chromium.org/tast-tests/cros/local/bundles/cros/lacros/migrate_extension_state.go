// Copyright 2022 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package lacros

import (
	"context"
	"io/ioutil"
	"os"
	"path/filepath"

	"go.chromium.org/tast-tests/cros/local/bundles/cros/lacros/migrate"
	"go.chromium.org/tast-tests/cros/local/chrome"
	"go.chromium.org/tast-tests/cros/local/chrome/lacros"
	"go.chromium.org/tast-tests/cros/local/chrome/lacros/lacrosfixt"
	"go.chromium.org/tast/core/errors"
	"go.chromium.org/tast/core/fsutil"
	"go.chromium.org/tast/core/testing"
)

var extensionFiles = []string{
	"manifest.json",
	"index.html",
	"migrate_extension_state.js",
}

const (
	simpleExtensionID  = "jjbombdcioakijidkhacpmocpojelebc"
	simpleExtensionURL = "chrome-extension://" + simpleExtensionID + "/index.html"
)

func init() {
	testing.AddTest(&testing.Test{
		Func:         MigrateExtensionState,
		LacrosStatus: testing.LacrosVariantExists,
		Desc:         "Test Ash-to-Lacros profile migration handles extension states",
		Contacts: []string{
			"lacros-team@google.com",
			"ythjkt@google.com", // Test author
			"neis@google.com",
			"hidehiko@google.com",
		},
		BugComponent: "b:1456869",
		Attr:         []string{"group:mainline", "informational"},
		SoftwareDeps: []string{"chrome", "lacros"},
		Data:         extensionFiles,
	})
}

// MigrateExtensionState tests that the migration successfully migrates extension state by:
// 1. Store extension state by calling relevant APIs on the extension page on Ash.
// 2. Run profile migration from Ash to Lacros.
// 3. Check if the states created in step 1 can be found on Lacros.
func MigrateExtensionState(ctx context.Context, s *testing.State) {
	extDir, err := ioutil.TempDir("", "simple_extension.")
	if err != nil {
		s.Fatal("Failed to create simple_extension dir: ", err)
	}
	defer os.RemoveAll(extDir)

	for _, name := range extensionFiles {
		if err := fsutil.CopyFile(s.DataPath(name), filepath.Join(extDir, name)); err != nil {
			s.Fatal("Failed to copy file: ", err)
		}
	}

	if err := prepareExtensionState(ctx, extDir); err != nil {
		s.Fatal("Failed to prepare extension state: ", err)
	}

	opts := []lacrosfixt.Option{lacrosfixt.ChromeOptions(chrome.LacrosUnpackedExtension(extDir))}
	cr, err := migrate.Run(ctx, []chrome.Option{}, opts)
	if err != nil {
		s.Fatal("Failed to migrate profile: ", err)
	}
	defer cr.Close(ctx)

	if err := verifyExtensionStateOnLacros(ctx, s, cr); err != nil {
		s.Fatal("Calling verifyExtensionStateOnLacros failed: ", err)
	}
}

func prepareExtensionState(ctx context.Context, extDir string) error {
	// TODO(ythjkt): Call more extension api which store values on disk from https://developer.chrome.com/docs/extensions/reference/.
	cr, err := chrome.New(ctx, chrome.DisableFeatures("LacrosOnly"), chrome.UnpackedExtension(extDir))
	if err != nil {
		return errors.Wrap(err, "failed to start chrome")
	}
	defer cr.Close(ctx)

	if err := testing.Poll(ctx, func(ctx context.Context) error {
		if _, err := os.Stat(migrate.LacrosFirstRunPath); !os.IsNotExist(err) {
			return errors.Wrap(err, "'First Run' file exists or cannot be read")
		}
		return nil
	}, nil); err != nil {
		return errors.Wrap(err, "'First Run' file exists or cannot be read")
	}

	conn, err := cr.NewConn(ctx, simpleExtensionURL)
	if err != nil {
		return errors.Wrap(err, "failed to open extension page")
	}

	if err := conn.Call(ctx, nil, `setStorageSync`); err != nil {
		return errors.Wrap(err, "setStorageSync() failed")
	}

	if err := conn.Call(ctx, nil, "setStorageLocal"); err != nil {
		return errors.Wrap(err, "setStorageLocal() failed")
	}

	return nil
}

func verifyExtensionStateOnLacros(ctx context.Context, s *testing.State, cr *chrome.Chrome) error {
	if _, err := os.Stat(migrate.LacrosFirstRunPath); err != nil {
		return errors.Wrap(err, "failed to read 'First Run' file")
	}

	tconn, err := cr.TestAPIConn(ctx)
	if err != nil {
		return errors.Wrap(err, "failed to create a Test API connection")
	}

	l, err := lacros.Launch(ctx, tconn)
	if err != nil {
		return errors.Wrap(err, "failed to launch Lacros")
	}
	defer l.Close(ctx)

	conn, err := l.NewConn(ctx, simpleExtensionURL)
	if err != nil {
		return errors.Wrap(err, "failed to open the extension page")
	}
	defer conn.Close()

	if err := conn.Call(ctx, nil, "verifyStorageSync"); err != nil {
		return errors.Wrap(err, "verifyStorageSync() failed")
	}

	if err := conn.Call(ctx, nil, "verifyStorageLocal"); err != nil {
		return errors.Wrap(err, "verifyStorageLocal() failed")
	}

	return nil
}
