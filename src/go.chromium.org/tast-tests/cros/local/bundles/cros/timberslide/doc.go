// Copyright 2023 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package timberslide contains local Tast tests that check the functionality
// of the timberslide instances on ChromeOS.
package timberslide
