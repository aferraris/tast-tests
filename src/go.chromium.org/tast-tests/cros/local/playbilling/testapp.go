// Copyright 2021 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package playbilling

import (
	"context"
	"fmt"
	"time"

	"go.chromium.org/tast-tests/cros/common/android/ui"
	"go.chromium.org/tast-tests/cros/local/arc"
	"go.chromium.org/tast-tests/cros/local/chrome"
	"go.chromium.org/tast-tests/cros/local/webapk"
	"go.chromium.org/tast/core/errors"
)

// TestApp represents the Play Billing test PWA and ARC Payments Overlay.
type TestApp struct {
	cr          *chrome.Chrome
	pbconn      *chrome.Conn
	uiAutomator *ui.Device
	wm          *webapk.Manager
}

// NewTestApp returns a reference to a new Play Billing Test App.
func NewTestApp(ctx context.Context, arc *arc.ARC, d *ui.Device, wm *webapk.Manager) *TestApp {
	return &TestApp{
		pbconn:      nil,
		uiAutomator: d,
		wm:          wm,
	}
}

// Launch starts a new TestApp window.
func (ta *TestApp) Launch(ctx context.Context) error {
	if err := ta.wm.LaunchApp(ctx); err != nil {
		return errors.Wrap(err, "failed launching the test app")
	}

	pbconn, err := ta.wm.GetChromeConnection(ctx)
	if err != nil {
		return errors.Wrap(err, "failed getting connection for the test app")
	}

	ta.pbconn = pbconn

	return nil
}

// OpenBillingDialog clicks a button on the PWA to launch the Play Billing UI.
func (ta *TestApp) OpenBillingDialog(ctx context.Context, sku string) error {
	jsExpr := fmt.Sprintf("document.getElementById('%s')", sku)
	return ClickElementByCDP(ta.pbconn, jsExpr)(ctx)
}

// RequiredAuthConfirm clicks "Yes, always" button in required auth window.
func (ta *TestApp) RequiredAuthConfirm(ctx context.Context) error {
	return RequiredAuthConfirm(ta.uiAutomator)(ctx)
}

// CheckPaymentSuccessful checks for a presence of payment successful screen in the android ui tree.
func (ta *TestApp) CheckPaymentSuccessful(ctx context.Context) error {
	if err := CheckPaymentSuccessful(ta.uiAutomator)(ctx); err != nil {
		var msg string
		_ = ta.pbconn.Eval(ctx, "document.getElementById('errors').innerHTML", &msg)
		return errors.Wrap(err, msg)
	}
	return nil
}

// GetDetails calls dgapi GetDetails and returns result.
func (ta *TestApp) GetDetails(ctx context.Context, sku string) (string, error) {
	if err := ta.pbconn.WaitForExprFailOnErrWithTimeout(ctx, "window.getDetails != undefined", 30*time.Second); err != nil {
		return "", errors.Wrap(err, "failed to wait for getDetails to be defined")
	}
	var details string
	if err := ta.pbconn.Eval(ctx, fmt.Sprintf("getDetails('%s')", sku), &details); err != nil {
		return "", errors.Wrap(err, "failed to get details")
	}
	return details, nil
}
