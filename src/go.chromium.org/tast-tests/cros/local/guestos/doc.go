// Copyright 2023 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package guestos contains helper code for tests that are not specific to any
// type of guest OS.
package guestos
