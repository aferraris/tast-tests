// Copyright 2023 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package common defines the BluetoothFacade interface that may be implemented
// for different bluetooth stacks as well as common utilities that may be
// reused across implementations.
package common
