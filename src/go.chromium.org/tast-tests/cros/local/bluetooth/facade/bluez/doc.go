// Copyright 2023 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package bluez contains the objects and utilities necessary for implementing
// the BluetoothFacade interface for the bluez bluetooth stack.
package bluez
