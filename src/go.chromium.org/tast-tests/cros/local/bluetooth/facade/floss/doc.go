// Copyright 2023 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package floss contains the objects and utilities necessary for implementing
// the BluetoothFacade interface for the floss bluetooth stack.
package floss
