// Copyright 2021 The Chromium Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.27.1
// 	protoc        v3.21.9
// source: structured_data.proto

package proto

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

// Type of this event, which determines which log source the event is saved
// into. An event should have type RAW_STRING if and only if the event may
// contain raw string metrics, ie. strings that have not been HMAC'd. The
// UNKNOWN value is considered an error and should never be sent.
type StructuredEventProto_EventType int32

const (
	StructuredEventProto_UNKNOWN    StructuredEventProto_EventType = 0
	StructuredEventProto_REGULAR    StructuredEventProto_EventType = 1
	StructuredEventProto_RAW_STRING StructuredEventProto_EventType = 2
)

// Enum value maps for StructuredEventProto_EventType.
var (
	StructuredEventProto_EventType_name = map[int32]string{
		0: "UNKNOWN",
		1: "REGULAR",
		2: "RAW_STRING",
	}
	StructuredEventProto_EventType_value = map[string]int32{
		"UNKNOWN":    0,
		"REGULAR":    1,
		"RAW_STRING": 2,
	}
)

func (x StructuredEventProto_EventType) Enum() *StructuredEventProto_EventType {
	p := new(StructuredEventProto_EventType)
	*p = x
	return p
}

func (x StructuredEventProto_EventType) String() string {
	return protoimpl.X.EnumStringOf(x.Descriptor(), protoreflect.EnumNumber(x))
}

func (StructuredEventProto_EventType) Descriptor() protoreflect.EnumDescriptor {
	return file_structured_data_proto_enumTypes[0].Descriptor()
}

func (StructuredEventProto_EventType) Type() protoreflect.EnumType {
	return &file_structured_data_proto_enumTypes[0]
}

func (x StructuredEventProto_EventType) Number() protoreflect.EnumNumber {
	return protoreflect.EnumNumber(x)
}

// Deprecated: Do not use.
func (x *StructuredEventProto_EventType) UnmarshalJSON(b []byte) error {
	num, err := protoimpl.X.UnmarshalJSONEnum(x.Descriptor(), b)
	if err != nil {
		return err
	}
	*x = StructuredEventProto_EventType(num)
	return nil
}

// Deprecated: Use StructuredEventProto_EventType.Descriptor instead.
func (StructuredEventProto_EventType) EnumDescriptor() ([]byte, []int) {
	return file_structured_data_proto_rawDescGZIP(), []int{0, 0}
}

// One structured metrics event, containing several hashed and unhashed metrics
// related to a single event type, tied to a single pseudonymous user.
//
// Next tag: 5
type StructuredEventProto struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// A per-device, per-project ID that is used only for structured metrics.
	// The ID is rotated at least every 90 days.
	ProfileEventId *uint64 `protobuf:"fixed64,1,opt,name=profile_event_id,json=profileEventId" json:"profile_event_id,omitempty"`
	// The first 8 bytes of the MD5 hash of the event's name as a string. Each
	// name is defined in src/tools/metrics/structured/structured.xml, and this
	// will be the hash of one of those.
	EventNameHash *uint64                         `protobuf:"fixed64,2,opt,name=event_name_hash,json=eventNameHash" json:"event_name_hash,omitempty"`
	Metrics       []*StructuredEventProto_Metric  `protobuf:"bytes,3,rep,name=metrics" json:"metrics,omitempty"`
	EventType     *StructuredEventProto_EventType `protobuf:"varint,4,opt,name=event_type,json=eventType,enum=metrics.StructuredEventProto_EventType" json:"event_type,omitempty"`
	// The project name hash is the first 8 bytes of the MD5 hash of the project
	// name that is defined in src/tools/metrics/structured/structured.xml.
	ProjectNameHash *uint64 `protobuf:"fixed64,5,opt,name=project_name_hash,json=projectNameHash" json:"project_name_hash,omitempty"`
}

func (x *StructuredEventProto) Reset() {
	*x = StructuredEventProto{}
	if protoimpl.UnsafeEnabled {
		mi := &file_structured_data_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *StructuredEventProto) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*StructuredEventProto) ProtoMessage() {}

func (x *StructuredEventProto) ProtoReflect() protoreflect.Message {
	mi := &file_structured_data_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use StructuredEventProto.ProtoReflect.Descriptor instead.
func (*StructuredEventProto) Descriptor() ([]byte, []int) {
	return file_structured_data_proto_rawDescGZIP(), []int{0}
}

func (x *StructuredEventProto) GetProfileEventId() uint64 {
	if x != nil && x.ProfileEventId != nil {
		return *x.ProfileEventId
	}
	return 0
}

func (x *StructuredEventProto) GetEventNameHash() uint64 {
	if x != nil && x.EventNameHash != nil {
		return *x.EventNameHash
	}
	return 0
}

func (x *StructuredEventProto) GetMetrics() []*StructuredEventProto_Metric {
	if x != nil {
		return x.Metrics
	}
	return nil
}

func (x *StructuredEventProto) GetEventType() StructuredEventProto_EventType {
	if x != nil && x.EventType != nil {
		return *x.EventType
	}
	return StructuredEventProto_UNKNOWN
}

func (x *StructuredEventProto) GetProjectNameHash() uint64 {
	if x != nil && x.ProjectNameHash != nil {
		return *x.ProjectNameHash
	}
	return 0
}

// The top-level proto for structured metrics. One StructuredDataProto is
// uploaded per UMA upload containing structured metrics. Contains all
// structured events for that upload, and any other metadata.
type StructuredDataProto struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Events []*StructuredEventProto `protobuf:"bytes,1,rep,name=events" json:"events,omitempty"`
}

func (x *StructuredDataProto) Reset() {
	*x = StructuredDataProto{}
	if protoimpl.UnsafeEnabled {
		mi := &file_structured_data_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *StructuredDataProto) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*StructuredDataProto) ProtoMessage() {}

func (x *StructuredDataProto) ProtoReflect() protoreflect.Message {
	mi := &file_structured_data_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use StructuredDataProto.ProtoReflect.Descriptor instead.
func (*StructuredDataProto) Descriptor() ([]byte, []int) {
	return file_structured_data_proto_rawDescGZIP(), []int{1}
}

func (x *StructuredDataProto) GetEvents() []*StructuredEventProto {
	if x != nil {
		return x.Events
	}
	return nil
}

// All metric values for this event. Each metric has two properties defined in
// structured.xml that determine what is recorded.
//
//  1. Metric name. This is a string, and the first 8 bytes of its MD5 hash is
//     recorded as name_hash.
//
// 2. Kind. Each metric can store four kinds of values.
//
//   - int64. The client supplies an int64 value for the metric, and that
//     value is recorded as-is in value_int64.
//
//   - string. The client supplies a string value for the metric, which is
//     recorded as-is in value_string. This is sometimes referred to as a
//     "raw string" to differentiate from the following.
//
//   - hashed-string. The client supplies an arbitrary string for the metric.
//     The string itself is not recorded, instead, value_hmac records the
//     first 8 bytes of:
//
//     HMAC_SHA256(concat(string, metric_name), event_key)
//
//   - double. The client supplies a double value for the metric, which is
//     recorded as-is in value_double.
//
//     The event_key is a per-profile, per-client, per-project secret 32-byte
//     key used only for signing hashed values for this event. Keys should
//     never leave the device, and are rotated at least every 90 days.
//
//   - int64 array: This client supplies an array of int64 values for the
//     metric. Each metric will have an max defined by the metric definition.
type StructuredEventProto_Metric struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	NameHash *uint64 `protobuf:"fixed64,1,opt,name=name_hash,json=nameHash" json:"name_hash,omitempty"`
	// Types that are assignable to Value:
	//
	//	*StructuredEventProto_Metric_ValueHmac
	//	*StructuredEventProto_Metric_ValueInt64
	//	*StructuredEventProto_Metric_ValueString
	//	*StructuredEventProto_Metric_ValueDouble
	//	*StructuredEventProto_Metric_ValueRepeatedInt64
	Value isStructuredEventProto_Metric_Value `protobuf_oneof:"value"`
}

func (x *StructuredEventProto_Metric) Reset() {
	*x = StructuredEventProto_Metric{}
	if protoimpl.UnsafeEnabled {
		mi := &file_structured_data_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *StructuredEventProto_Metric) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*StructuredEventProto_Metric) ProtoMessage() {}

func (x *StructuredEventProto_Metric) ProtoReflect() protoreflect.Message {
	mi := &file_structured_data_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use StructuredEventProto_Metric.ProtoReflect.Descriptor instead.
func (*StructuredEventProto_Metric) Descriptor() ([]byte, []int) {
	return file_structured_data_proto_rawDescGZIP(), []int{0, 0}
}

func (x *StructuredEventProto_Metric) GetNameHash() uint64 {
	if x != nil && x.NameHash != nil {
		return *x.NameHash
	}
	return 0
}

func (m *StructuredEventProto_Metric) GetValue() isStructuredEventProto_Metric_Value {
	if m != nil {
		return m.Value
	}
	return nil
}

func (x *StructuredEventProto_Metric) GetValueHmac() uint64 {
	if x, ok := x.GetValue().(*StructuredEventProto_Metric_ValueHmac); ok {
		return x.ValueHmac
	}
	return 0
}

func (x *StructuredEventProto_Metric) GetValueInt64() int64 {
	if x, ok := x.GetValue().(*StructuredEventProto_Metric_ValueInt64); ok {
		return x.ValueInt64
	}
	return 0
}

func (x *StructuredEventProto_Metric) GetValueString() string {
	if x, ok := x.GetValue().(*StructuredEventProto_Metric_ValueString); ok {
		return x.ValueString
	}
	return ""
}

func (x *StructuredEventProto_Metric) GetValueDouble() float64 {
	if x, ok := x.GetValue().(*StructuredEventProto_Metric_ValueDouble); ok {
		return x.ValueDouble
	}
	return 0
}

func (x *StructuredEventProto_Metric) GetValueRepeatedInt64() *StructuredEventProto_Metric_RepeatedInt64 {
	if x, ok := x.GetValue().(*StructuredEventProto_Metric_ValueRepeatedInt64); ok {
		return x.ValueRepeatedInt64
	}
	return nil
}

type isStructuredEventProto_Metric_Value interface {
	isStructuredEventProto_Metric_Value()
}

type StructuredEventProto_Metric_ValueHmac struct {
	ValueHmac uint64 `protobuf:"fixed64,2,opt,name=value_hmac,json=valueHmac,oneof"`
}

type StructuredEventProto_Metric_ValueInt64 struct {
	ValueInt64 int64 `protobuf:"varint,3,opt,name=value_int64,json=valueInt64,oneof"`
}

type StructuredEventProto_Metric_ValueString struct {
	ValueString string `protobuf:"bytes,4,opt,name=value_string,json=valueString,oneof"`
}

type StructuredEventProto_Metric_ValueDouble struct {
	ValueDouble float64 `protobuf:"fixed64,5,opt,name=value_double,json=valueDouble,oneof"`
}

type StructuredEventProto_Metric_ValueRepeatedInt64 struct {
	ValueRepeatedInt64 *StructuredEventProto_Metric_RepeatedInt64 `protobuf:"bytes,6,opt,name=value_repeated_int64,json=valueRepeatedInt64,oneof"`
}

func (*StructuredEventProto_Metric_ValueHmac) isStructuredEventProto_Metric_Value() {}

func (*StructuredEventProto_Metric_ValueInt64) isStructuredEventProto_Metric_Value() {}

func (*StructuredEventProto_Metric_ValueString) isStructuredEventProto_Metric_Value() {}

func (*StructuredEventProto_Metric_ValueDouble) isStructuredEventProto_Metric_Value() {}

func (*StructuredEventProto_Metric_ValueRepeatedInt64) isStructuredEventProto_Metric_Value() {}

// Wrapper of repeated integer fields.
type StructuredEventProto_Metric_RepeatedInt64 struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Values []int64 `protobuf:"varint,1,rep,packed,name=values" json:"values,omitempty"`
}

func (x *StructuredEventProto_Metric_RepeatedInt64) Reset() {
	*x = StructuredEventProto_Metric_RepeatedInt64{}
	if protoimpl.UnsafeEnabled {
		mi := &file_structured_data_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *StructuredEventProto_Metric_RepeatedInt64) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*StructuredEventProto_Metric_RepeatedInt64) ProtoMessage() {}

func (x *StructuredEventProto_Metric_RepeatedInt64) ProtoReflect() protoreflect.Message {
	mi := &file_structured_data_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use StructuredEventProto_Metric_RepeatedInt64.ProtoReflect.Descriptor instead.
func (*StructuredEventProto_Metric_RepeatedInt64) Descriptor() ([]byte, []int) {
	return file_structured_data_proto_rawDescGZIP(), []int{0, 0, 0}
}

func (x *StructuredEventProto_Metric_RepeatedInt64) GetValues() []int64 {
	if x != nil {
		return x.Values
	}
	return nil
}

var File_structured_data_proto protoreflect.FileDescriptor

var file_structured_data_proto_rawDesc = []byte{
	0x0a, 0x15, 0x73, 0x74, 0x72, 0x75, 0x63, 0x74, 0x75, 0x72, 0x65, 0x64, 0x5f, 0x64, 0x61, 0x74,
	0x61, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x07, 0x6d, 0x65, 0x74, 0x72, 0x69, 0x63, 0x73,
	0x22, 0xa7, 0x05, 0x0a, 0x14, 0x53, 0x74, 0x72, 0x75, 0x63, 0x74, 0x75, 0x72, 0x65, 0x64, 0x45,
	0x76, 0x65, 0x6e, 0x74, 0x50, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x28, 0x0a, 0x10, 0x70, 0x72, 0x6f,
	0x66, 0x69, 0x6c, 0x65, 0x5f, 0x65, 0x76, 0x65, 0x6e, 0x74, 0x5f, 0x69, 0x64, 0x18, 0x01, 0x20,
	0x01, 0x28, 0x06, 0x52, 0x0e, 0x70, 0x72, 0x6f, 0x66, 0x69, 0x6c, 0x65, 0x45, 0x76, 0x65, 0x6e,
	0x74, 0x49, 0x64, 0x12, 0x26, 0x0a, 0x0f, 0x65, 0x76, 0x65, 0x6e, 0x74, 0x5f, 0x6e, 0x61, 0x6d,
	0x65, 0x5f, 0x68, 0x61, 0x73, 0x68, 0x18, 0x02, 0x20, 0x01, 0x28, 0x06, 0x52, 0x0d, 0x65, 0x76,
	0x65, 0x6e, 0x74, 0x4e, 0x61, 0x6d, 0x65, 0x48, 0x61, 0x73, 0x68, 0x12, 0x3e, 0x0a, 0x07, 0x6d,
	0x65, 0x74, 0x72, 0x69, 0x63, 0x73, 0x18, 0x03, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x24, 0x2e, 0x6d,
	0x65, 0x74, 0x72, 0x69, 0x63, 0x73, 0x2e, 0x53, 0x74, 0x72, 0x75, 0x63, 0x74, 0x75, 0x72, 0x65,
	0x64, 0x45, 0x76, 0x65, 0x6e, 0x74, 0x50, 0x72, 0x6f, 0x74, 0x6f, 0x2e, 0x4d, 0x65, 0x74, 0x72,
	0x69, 0x63, 0x52, 0x07, 0x6d, 0x65, 0x74, 0x72, 0x69, 0x63, 0x73, 0x12, 0x46, 0x0a, 0x0a, 0x65,
	0x76, 0x65, 0x6e, 0x74, 0x5f, 0x74, 0x79, 0x70, 0x65, 0x18, 0x04, 0x20, 0x01, 0x28, 0x0e, 0x32,
	0x27, 0x2e, 0x6d, 0x65, 0x74, 0x72, 0x69, 0x63, 0x73, 0x2e, 0x53, 0x74, 0x72, 0x75, 0x63, 0x74,
	0x75, 0x72, 0x65, 0x64, 0x45, 0x76, 0x65, 0x6e, 0x74, 0x50, 0x72, 0x6f, 0x74, 0x6f, 0x2e, 0x45,
	0x76, 0x65, 0x6e, 0x74, 0x54, 0x79, 0x70, 0x65, 0x52, 0x09, 0x65, 0x76, 0x65, 0x6e, 0x74, 0x54,
	0x79, 0x70, 0x65, 0x12, 0x2a, 0x0a, 0x11, 0x70, 0x72, 0x6f, 0x6a, 0x65, 0x63, 0x74, 0x5f, 0x6e,
	0x61, 0x6d, 0x65, 0x5f, 0x68, 0x61, 0x73, 0x68, 0x18, 0x05, 0x20, 0x01, 0x28, 0x06, 0x52, 0x0f,
	0x70, 0x72, 0x6f, 0x6a, 0x65, 0x63, 0x74, 0x4e, 0x61, 0x6d, 0x65, 0x48, 0x61, 0x73, 0x68, 0x1a,
	0xd1, 0x02, 0x0a, 0x06, 0x4d, 0x65, 0x74, 0x72, 0x69, 0x63, 0x12, 0x1b, 0x0a, 0x09, 0x6e, 0x61,
	0x6d, 0x65, 0x5f, 0x68, 0x61, 0x73, 0x68, 0x18, 0x01, 0x20, 0x01, 0x28, 0x06, 0x52, 0x08, 0x6e,
	0x61, 0x6d, 0x65, 0x48, 0x61, 0x73, 0x68, 0x12, 0x1f, 0x0a, 0x0a, 0x76, 0x61, 0x6c, 0x75, 0x65,
	0x5f, 0x68, 0x6d, 0x61, 0x63, 0x18, 0x02, 0x20, 0x01, 0x28, 0x06, 0x48, 0x00, 0x52, 0x09, 0x76,
	0x61, 0x6c, 0x75, 0x65, 0x48, 0x6d, 0x61, 0x63, 0x12, 0x21, 0x0a, 0x0b, 0x76, 0x61, 0x6c, 0x75,
	0x65, 0x5f, 0x69, 0x6e, 0x74, 0x36, 0x34, 0x18, 0x03, 0x20, 0x01, 0x28, 0x03, 0x48, 0x00, 0x52,
	0x0a, 0x76, 0x61, 0x6c, 0x75, 0x65, 0x49, 0x6e, 0x74, 0x36, 0x34, 0x12, 0x23, 0x0a, 0x0c, 0x76,
	0x61, 0x6c, 0x75, 0x65, 0x5f, 0x73, 0x74, 0x72, 0x69, 0x6e, 0x67, 0x18, 0x04, 0x20, 0x01, 0x28,
	0x09, 0x48, 0x00, 0x52, 0x0b, 0x76, 0x61, 0x6c, 0x75, 0x65, 0x53, 0x74, 0x72, 0x69, 0x6e, 0x67,
	0x12, 0x23, 0x0a, 0x0c, 0x76, 0x61, 0x6c, 0x75, 0x65, 0x5f, 0x64, 0x6f, 0x75, 0x62, 0x6c, 0x65,
	0x18, 0x05, 0x20, 0x01, 0x28, 0x01, 0x48, 0x00, 0x52, 0x0b, 0x76, 0x61, 0x6c, 0x75, 0x65, 0x44,
	0x6f, 0x75, 0x62, 0x6c, 0x65, 0x12, 0x66, 0x0a, 0x14, 0x76, 0x61, 0x6c, 0x75, 0x65, 0x5f, 0x72,
	0x65, 0x70, 0x65, 0x61, 0x74, 0x65, 0x64, 0x5f, 0x69, 0x6e, 0x74, 0x36, 0x34, 0x18, 0x06, 0x20,
	0x01, 0x28, 0x0b, 0x32, 0x32, 0x2e, 0x6d, 0x65, 0x74, 0x72, 0x69, 0x63, 0x73, 0x2e, 0x53, 0x74,
	0x72, 0x75, 0x63, 0x74, 0x75, 0x72, 0x65, 0x64, 0x45, 0x76, 0x65, 0x6e, 0x74, 0x50, 0x72, 0x6f,
	0x74, 0x6f, 0x2e, 0x4d, 0x65, 0x74, 0x72, 0x69, 0x63, 0x2e, 0x52, 0x65, 0x70, 0x65, 0x61, 0x74,
	0x65, 0x64, 0x49, 0x6e, 0x74, 0x36, 0x34, 0x48, 0x00, 0x52, 0x12, 0x76, 0x61, 0x6c, 0x75, 0x65,
	0x52, 0x65, 0x70, 0x65, 0x61, 0x74, 0x65, 0x64, 0x49, 0x6e, 0x74, 0x36, 0x34, 0x1a, 0x2b, 0x0a,
	0x0d, 0x52, 0x65, 0x70, 0x65, 0x61, 0x74, 0x65, 0x64, 0x49, 0x6e, 0x74, 0x36, 0x34, 0x12, 0x1a,
	0x0a, 0x06, 0x76, 0x61, 0x6c, 0x75, 0x65, 0x73, 0x18, 0x01, 0x20, 0x03, 0x28, 0x03, 0x42, 0x02,
	0x10, 0x01, 0x52, 0x06, 0x76, 0x61, 0x6c, 0x75, 0x65, 0x73, 0x42, 0x07, 0x0a, 0x05, 0x76, 0x61,
	0x6c, 0x75, 0x65, 0x22, 0x35, 0x0a, 0x09, 0x45, 0x76, 0x65, 0x6e, 0x74, 0x54, 0x79, 0x70, 0x65,
	0x12, 0x0b, 0x0a, 0x07, 0x55, 0x4e, 0x4b, 0x4e, 0x4f, 0x57, 0x4e, 0x10, 0x00, 0x12, 0x0b, 0x0a,
	0x07, 0x52, 0x45, 0x47, 0x55, 0x4c, 0x41, 0x52, 0x10, 0x01, 0x12, 0x0e, 0x0a, 0x0a, 0x52, 0x41,
	0x57, 0x5f, 0x53, 0x54, 0x52, 0x49, 0x4e, 0x47, 0x10, 0x02, 0x22, 0x4c, 0x0a, 0x13, 0x53, 0x74,
	0x72, 0x75, 0x63, 0x74, 0x75, 0x72, 0x65, 0x64, 0x44, 0x61, 0x74, 0x61, 0x50, 0x72, 0x6f, 0x74,
	0x6f, 0x12, 0x35, 0x0a, 0x06, 0x65, 0x76, 0x65, 0x6e, 0x74, 0x73, 0x18, 0x01, 0x20, 0x03, 0x28,
	0x0b, 0x32, 0x1d, 0x2e, 0x6d, 0x65, 0x74, 0x72, 0x69, 0x63, 0x73, 0x2e, 0x53, 0x74, 0x72, 0x75,
	0x63, 0x74, 0x75, 0x72, 0x65, 0x64, 0x45, 0x76, 0x65, 0x6e, 0x74, 0x50, 0x72, 0x6f, 0x74, 0x6f,
	0x52, 0x06, 0x65, 0x76, 0x65, 0x6e, 0x74, 0x73, 0x42, 0x58, 0x0a, 0x1f, 0x6f, 0x72, 0x67, 0x2e,
	0x63, 0x68, 0x72, 0x6f, 0x6d, 0x69, 0x75, 0x6d, 0x2e, 0x63, 0x6f, 0x6d, 0x70, 0x6f, 0x6e, 0x65,
	0x6e, 0x74, 0x73, 0x2e, 0x6d, 0x65, 0x74, 0x72, 0x69, 0x63, 0x73, 0x48, 0x03, 0x5a, 0x33, 0x67,
	0x6f, 0x2e, 0x63, 0x68, 0x72, 0x6f, 0x6d, 0x69, 0x75, 0x6d, 0x2e, 0x6f, 0x72, 0x67, 0x2f, 0x74,
	0x61, 0x73, 0x74, 0x2d, 0x74, 0x65, 0x73, 0x74, 0x73, 0x2f, 0x63, 0x72, 0x6f, 0x73, 0x2f, 0x6c,
	0x6f, 0x63, 0x61, 0x6c, 0x2f, 0x6d, 0x65, 0x74, 0x72, 0x69, 0x63, 0x73, 0x2f, 0x70, 0x72, 0x6f,
	0x74, 0x6f,
}

var (
	file_structured_data_proto_rawDescOnce sync.Once
	file_structured_data_proto_rawDescData = file_structured_data_proto_rawDesc
)

func file_structured_data_proto_rawDescGZIP() []byte {
	file_structured_data_proto_rawDescOnce.Do(func() {
		file_structured_data_proto_rawDescData = protoimpl.X.CompressGZIP(file_structured_data_proto_rawDescData)
	})
	return file_structured_data_proto_rawDescData
}

var file_structured_data_proto_enumTypes = make([]protoimpl.EnumInfo, 1)
var file_structured_data_proto_msgTypes = make([]protoimpl.MessageInfo, 4)
var file_structured_data_proto_goTypes = []interface{}{
	(StructuredEventProto_EventType)(0),               // 0: metrics.StructuredEventProto.EventType
	(*StructuredEventProto)(nil),                      // 1: metrics.StructuredEventProto
	(*StructuredDataProto)(nil),                       // 2: metrics.StructuredDataProto
	(*StructuredEventProto_Metric)(nil),               // 3: metrics.StructuredEventProto.Metric
	(*StructuredEventProto_Metric_RepeatedInt64)(nil), // 4: metrics.StructuredEventProto.Metric.RepeatedInt64
}
var file_structured_data_proto_depIdxs = []int32{
	3, // 0: metrics.StructuredEventProto.metrics:type_name -> metrics.StructuredEventProto.Metric
	0, // 1: metrics.StructuredEventProto.event_type:type_name -> metrics.StructuredEventProto.EventType
	1, // 2: metrics.StructuredDataProto.events:type_name -> metrics.StructuredEventProto
	4, // 3: metrics.StructuredEventProto.Metric.value_repeated_int64:type_name -> metrics.StructuredEventProto.Metric.RepeatedInt64
	4, // [4:4] is the sub-list for method output_type
	4, // [4:4] is the sub-list for method input_type
	4, // [4:4] is the sub-list for extension type_name
	4, // [4:4] is the sub-list for extension extendee
	0, // [0:4] is the sub-list for field type_name
}

func init() { file_structured_data_proto_init() }
func file_structured_data_proto_init() {
	if File_structured_data_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_structured_data_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*StructuredEventProto); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_structured_data_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*StructuredDataProto); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_structured_data_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*StructuredEventProto_Metric); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_structured_data_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*StructuredEventProto_Metric_RepeatedInt64); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	file_structured_data_proto_msgTypes[2].OneofWrappers = []interface{}{
		(*StructuredEventProto_Metric_ValueHmac)(nil),
		(*StructuredEventProto_Metric_ValueInt64)(nil),
		(*StructuredEventProto_Metric_ValueString)(nil),
		(*StructuredEventProto_Metric_ValueDouble)(nil),
		(*StructuredEventProto_Metric_ValueRepeatedInt64)(nil),
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_structured_data_proto_rawDesc,
			NumEnums:      1,
			NumMessages:   4,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_structured_data_proto_goTypes,
		DependencyIndexes: file_structured_data_proto_depIdxs,
		EnumInfos:         file_structured_data_proto_enumTypes,
		MessageInfos:      file_structured_data_proto_msgTypes,
	}.Build()
	File_structured_data_proto = out.File
	file_structured_data_proto_rawDesc = nil
	file_structured_data_proto_goTypes = nil
	file_structured_data_proto_depIdxs = nil
}
