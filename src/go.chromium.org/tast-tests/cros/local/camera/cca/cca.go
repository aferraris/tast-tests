// Copyright 2019 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package cca provides utilities to interact with Chrome Camera App.
package cca

import (
	"context"
	"encoding/json"
	"fmt"
	"image/jpeg"
	"io/ioutil"
	"os"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/mafredri/cdp/protocol/target"

	"go.chromium.org/tast-tests/cros/local/apps"
	"go.chromium.org/tast-tests/cros/local/camera/testutil"
	"go.chromium.org/tast-tests/cros/local/chrome"
	"go.chromium.org/tast-tests/cros/local/chrome/ash"
	"go.chromium.org/tast-tests/cros/local/cryptohome"
	"go.chromium.org/tast-tests/cros/local/input"
	"go.chromium.org/tast-tests/cros/local/power"
	"go.chromium.org/tast-tests/cros/local/screenshot"
	"go.chromium.org/tast-tests/cros/local/upstart"
	"go.chromium.org/tast/core/ctxutil"
	"go.chromium.org/tast/core/errors"
	"go.chromium.org/tast/core/testing"
)

// Facing is camera facing from JavaScript VideoFacingModeEnum.
type Facing string

// FPSObserver communicates to the FPS observer implementation on JS side.
type FPSObserver struct {
	jsObj *chrome.JSObject
}

const (
	// FacingBack is the constant string from JavaScript VideoFacingModeEnum.
	FacingBack Facing = "environment"
	// FacingFront is the constant string from JavaScript VideoFacingModeEnum.
	FacingFront Facing = "user"
	// FacingExternal is the constant string indicating external camera facing.
	FacingExternal Facing = "external"
)

// DeviceID is video device id from JavaScript navigator.mediaDevices.enumerateDevices.
type DeviceID string

// Mode is capture mode in CCA.
type Mode string

const (
	// ID is the app id of CCA.
	ID string = "hfhhnacclhffhdffklopdkcgdhifgngh"

	// Video is the mode used to record video.
	Video Mode = "video"
	// Photo is the mode used to take photo.
	Photo Mode = "photo"
	// Square is the mode used to take square photo.
	// TODO(b/215484798): Removed since there is no square mode in new UI.
	Square = "square"
	// Portrait is the mode used to take portrait photo.
	Portrait = "portrait"
	// Scan is the mode used to scan barcode/document.
	Scan = "scan"

	// Expert is the state used to indicate expert mode.
	Expert string = "expert"
	// SaveMetadata is the state used to indicate save metadata.
	SaveMetadata = "save-metadata"
)

// TimerState is the information of whether shutter timer is on.
type TimerState bool

const (
	// TimerOn means shutter timer is on.
	TimerOn TimerState = true
	// TimerOff means shutter timer is off.
	TimerOff = false
)

// MicState is the information of whether microphone is on.
type MicState bool

const (
	// MicOn means microphone is on.
	MicOn MicState = true
	// MicOff means microphone is off.
	MicOff = false
)

var (
	// PhotoPattern is the filename format of photos taken by CCA.
	PhotoPattern = regexp.MustCompile(`^IMG_\d{8}_\d{6}[^.]*\.jpg$`)
	// VideoPattern is the filename format of videos recorded by CCA.
	VideoPattern = regexp.MustCompile(`^VID_\d{8}_\d{6}[^.]*\.mp4$`)
	// gifPattern is the filename format of gif recorded by CCA.
	gifPattern = regexp.MustCompile(`^VID_\d{8}_\d{6}[^.]*\.gif$`)
	// portraitPattern is the filename format of portrait-mode photos taken by CCA.
	portraitPattern = regexp.MustCompile(`^IMG_\d{8}_\d{6}[^.]*\_BURST\d{5}_COVER.jpg$`)
	// portraitRefPattern is the filename format of the reference photo captured in portrait-mode.
	portraitRefPattern = regexp.MustCompile(`^IMG_\d{8}_\d{6}[^.]*\_BURST\d{5}.jpg$`)
	// DocumentPDFPattern is the filename format of the document PDF file.
	DocumentPDFPattern = regexp.MustCompile(`^SCN_\d{8}_\d{6}[^.]*\.pdf$`)
	// DocumentPhotoPattern is the filename format of the document photo file.
	DocumentPhotoPattern = regexp.MustCompile(`^SCN_\d{8}_\d{6}[^.]*\.jpg$`)
	// ErrVideoNotActive indicates that video is not active.
	ErrVideoNotActive = "Video is not active within given time"
)

// Orientation is the screen orientation from JavaScript window.screen.orientation.type.
type Orientation string

const (
	// PortraitPrimary is the primary portrait orientation.
	PortraitPrimary Orientation = "portrait-primary"
	// PortraitSecondary is the secondary portrait orientation.
	PortraitSecondary Orientation = "portrait-secondary"
	// LandscapePrimary is the primary landscape orientation.
	LandscapePrimary Orientation = "landscape-primary"
	// LandscapeSecondary is the secondary landscape orientation.
	LandscapeSecondary Orientation = "landscape-secondary"
)

// TimerDelay is default timer delay of CCA.
const TimerDelay time.Duration = 3 * time.Second

// Profile is type of encoder profile.
type Profile struct {
	Name  string
	Value int
}

var (
	// ProfileH264Baseline is h264 baseline profile.
	ProfileH264Baseline = Profile{"baseline", 66}
	// ProfileH264Main is h264 main profile.
	ProfileH264Main = Profile{"main", 77}
	// ProfileH264High is h264 high profile.
	ProfileH264High = Profile{"high", 100}
)

// Option returns the value of corresponding select-option.
func (p Profile) Option() string {
	return strconv.Itoa(int(p.Value))
}

// App represents a CCA (Chrome Camera App) instance.
type App struct {
	conn        *chrome.Conn
	cr          *chrome.Chrome
	outDir      string // Output directory to save the execution result
	appLauncher testutil.AppLauncher
	appWindow   *testutil.AppWindow
	cameraType  testutil.UseCameraType
}

// ErrJS represents an error occurs when executing JavaScript.
type ErrJS struct {
	msg string
}

// Error returns the wrapped message of a ErrJS.
func (e *ErrJS) Error() string {
	return e.msg
}

// Resolution represents dimension of video or photo.
type Resolution struct {
	Width  int `json:"width"`
	Height int `json:"height"`
}

// Range represents valid range of range type input element.
type Range struct {
	Max int `json:"max"`
	Min int `json:"min"`
}

// PTZSettings represents the current values for pan, tilt, and zoom.
type PTZSettings struct {
	Pan  float64 `json:"pan"`
	Tilt float64 `json:"tilt"`
	Zoom float64 `json:"zoom"`
}

// PowerTimeParams are time parameters used in power recording in CCA.
var PowerTimeParams = power.TimeParams{Interval: 5 * time.Second, Total: 300 * time.Second}

// Equal returns if PTZ settings a and b are equal.
func (a *PTZSettings) Equal(b *PTZSettings) bool {
	return a.Pan == b.Pan && a.Tilt == b.Tilt && a.Zoom == b.Zoom
}

// AspectRatio returns width divided by height as the aspect ratio of the resolution.
func (r *Resolution) AspectRatio() float64 {
	return float64(r.Width) / float64(r.Height)
}

// Init launches a CCA instance, evaluates the helper script within it and waits
// until its AppWindow interactable. The returned App instance must be closed
// when the test is finished.
func Init(ctx context.Context, cr *chrome.Chrome, outDir string, appLauncher testutil.AppLauncher, tb *testutil.TestBridge) (_ *App, retErr error) {
	if tb == nil {
		return nil, errors.New("test bridge is nil, resetTestBridge failed?")
	}
	// Since we don't use "cros-camera" service for fake camera, there is no need
	// to ensure it is running.
	if tb.CameraType != testutil.UseFakeVCDCamera {
		// Ensure that cros-camera service is running, because the service
		// might stopped due to the errors from some previous tests, and failed
		// to restart for some reasons.
		if err := upstart.EnsureJobRunning(ctx, "cros-camera"); err != nil {
			return nil, err
		}
	}

	conn, appWindow, err := testutil.LaunchApp(ctx, cr, tb, appLauncher)
	if err != nil {
		return nil, errors.Wrap(err, "failed when launching app")
	}

	if err := func() error {
		// Let CCA perform some one-time initialization after launched.  Otherwise
		// the first CheckVideoActive() might timed out because it's still
		// initializing, especially on low-end devices and when the system is busy.
		// Fail the test early if it's timed out to make it easier to figure out
		// the real reason of a test failure.
		if err := conn.Eval(ctx, `(async () => {
			const deadline = await new Promise(
				(resolve) => requestIdleCallback(resolve, {timeout: 30000}));
			if (deadline.didTimeout) {
			throw new Error('Timed out initializing CCA');
			}
		})()`, nil); err != nil {
			return err
		}

		return loadScripts(ctx, conn)
	}(); err != nil {
		if closeErr := testutil.CloseApp(ctx, cr, conn, appLauncher.UseSWAWindow); closeErr != nil {
			testing.ContextLog(ctx, "Failed to close app: ", closeErr)
		}
		if closeErr := conn.Close(); closeErr != nil {
			testing.ContextLog(ctx, "Failed to close app connection: ", closeErr)
		}
		if releaseErr := appWindow.Release(ctx); releaseErr != nil {
			testing.ContextLog(ctx, "Failed to release app window: ", releaseErr)
		}
		return nil, err
	}

	testing.ContextLog(ctx, "CCA launched")
	app := &App{conn, cr, outDir, appLauncher, appWindow, tb.CameraType}

	cleanupCtx := ctx
	ctx, cancel := ctxutil.Shorten(ctx, 3*time.Second)
	defer cancel()

	defer func(cleanupCtx context.Context) {
		if retErr == nil {
			return
		}

		if err := app.Close(cleanupCtx); err != nil {
			testing.ContextLog(cleanupCtx, "Failed to close app: ", err)
		}
	}(cleanupCtx)

	if err := app.WaitForVideoActive(ctx); err != nil {
		return nil, errors.Wrap(err, ErrVideoNotActive)
	}

	if err := app.WaitForState(ctx, "view-camera", true); err != nil {
		return nil, errors.Wrap(err, "failed to wait for view-camera becomes true")
	}

	// TODO(kamchonlathorn): Use state, e.g. HIDE_FLOATING_UI_FOR_TESTING, to
	// hide the dialog in CCA instead.
	if err := app.DisableSuperResIntroDialog(ctx); err != nil {
		testing.ContextLog(ctx, "Failed to force disable super resolution intro dialog")
	}

	testing.ContextLog(ctx, "CCA window is ready")
	return app, nil
}

// loadScripts loads the necessary scripts for running tests in CCA.
func loadScripts(ctx context.Context, conn *chrome.Conn) error {
	code := `(async function() {
		const {CCATest} = await import('/js/test/cca_test.js');
		window.CCATest = CCATest;
	})()`
	if err := conn.Eval(ctx, code, nil); err != nil {
		return errors.Wrap(err, "failed to load scripts from CCA")
	}
	return nil
}

// New launches a CCA instance. The returned App instance must be closed when the test is finished.
func New(ctx context.Context, cr *chrome.Chrome, outDir string, tb *testutil.TestBridge) (*App, error) {
	return Init(ctx, cr, outDir, testutil.AppLauncher{
		LaunchApp: func(ctx context.Context, tconn *chrome.TestConn) error {
			return apps.LaunchSystemWebApp(ctx, tconn, "Camera", "chrome://camera-app/views/main.html")
		},
		UseSWAWindow: true,
	}, tb)
}

// InstanceExists checks if there is any running CCA instance.
func InstanceExists(ctx context.Context, cr *chrome.Chrome) (bool, error) {
	checkPrefix := func(t *target.Info) bool {
		url := "chrome://camera-app/views/main.html"
		return strings.HasPrefix(t.URL, url)
	}
	return cr.IsTargetAvailable(ctx, checkPrefix)
}

// ClosingItself checks if CCA intends to close itself.
func (a *App) ClosingItself(ctx context.Context) (bool, error) {
	return a.appWindow.ClosingItself(ctx)
}

// checkJSError checks javascript error emitted by CCA error callback.
func (a *App) checkJSError(ctx context.Context) error {
	if a.appWindow == nil {
		// It might be closed already. Do nothing.
		return nil
	}
	errorInfos, err := a.appWindow.Errors(ctx)
	if err != nil {
		return err
	}

	jsErrors := make([]testutil.ErrorInfo, 0)
	jsWarnings := make([]testutil.ErrorInfo, 0)
	for _, err := range errorInfos {
		if err.Level == testutil.ErrorLevelWarning {
			jsWarnings = append(jsWarnings, err)
		} else if err.Level == testutil.ErrorLevelError {
			jsErrors = append(jsErrors, err)
		} else {
			return errors.Errorf("unknown error level: %v", err.Level)
		}
	}

	writeLogFile := func(lv testutil.ErrorLevel, errs []testutil.ErrorInfo) error {
		filename := fmt.Sprintf("CCA_JS_%v.log", lv)
		logPath := filepath.Join(a.outDir, filename)
		f, err := os.OpenFile(logPath, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
		if err != nil {
			return err
		}
		defer f.Close()
		for _, err := range errs {
			t := time.Unix(0, err.Time*1e6).UTC().Format("2006/01/02 15:04:05 [15:04:05.000]")
			f.WriteString(fmt.Sprintf("%v %v:\n", t, err.ErrorType))
			f.WriteString(err.Stack + "\n")
		}
		return nil
	}

	if err := writeLogFile(testutil.ErrorLevelWarning, jsWarnings); err != nil {
		return err
	}
	if err := writeLogFile(testutil.ErrorLevelError, jsErrors); err != nil {
		return err
	}
	if len(jsErrors) > 0 {
		return &ErrJS{fmt.Sprintf("there are %d JS errors, first error: type=%v. name=%v",
			len(jsErrors), jsErrors[0].ErrorType, jsErrors[0].ErrorName)}
	}
	return nil
}

// Close closes the App and the associated connection.
func (a *App) Close(ctx context.Context) (retErr error) {
	if a.conn == nil {
		// It's already closed. Do nothing.
		return nil
	}

	cleanupCtx := ctx
	ctx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()

	defer func(ctx context.Context) {
		reportOrLogError := func(err error) {
			if retErr == nil {
				retErr = err
			} else {
				testing.ContextLog(ctx, "Failed to close app: ", err)
			}
		}

		if err := testutil.CloseApp(ctx, a.cr, a.conn, a.appLauncher.UseSWAWindow); err != nil {
			reportOrLogError(errors.Wrap(err, "failed to close app"))
		}
		if err := a.conn.Close(); err != nil {
			reportOrLogError(errors.Wrap(err, "failed to Conn.Close()"))
		}
		if err := a.appWindow.WaitUntilClosed(ctx); err != nil {
			reportOrLogError(errors.Wrap(err, "failed to wait for appWindow close"))
		}
		if err := a.checkJSError(ctx); err != nil {
			reportOrLogError(errors.Wrap(err, "There are JS errors when running CCA"))
		}
		if err := a.appWindow.Release(ctx); err != nil {
			reportOrLogError(errors.Wrap(err, "failed to release app window"))
		}

		testing.ContextLog(ctx, "CCA closed")
		a.conn = nil
		a.appWindow = nil
	}(cleanupCtx)

	if err := a.conn.Eval(ctx, "CCATest.removeCacheData()", nil); err != nil {
		return errors.Wrap(err, "failed to clear cached data in local storage")
	}

	// TODO(b/144747002): Some tests (e.g. CCUIIntent) might trigger auto closing of CCA before
	// calling Close(). We should handle it gracefully to get the coverage report for them.
	err := a.OutputCodeCoverage(ctx)
	if err != nil {
		return err
	}
	return nil
}

// Restart restarts the App and resets the associated connection.
func (a *App) Restart(ctx context.Context, tb *testutil.TestBridge) error {
	if err := a.Close(ctx); err != nil {
		return err
	}
	newApp, err := Init(ctx, a.cr, a.outDir, a.appLauncher, tb)
	if err != nil {
		return err
	}
	*a = *newApp
	return nil
}

func (a *App) checkVideoState(ctx context.Context, active bool, duration time.Duration) error {
	cleanupCtx := ctx
	ctx, cancel := context.WithTimeout(ctx, 10*time.Second)
	defer cancel()

	code := fmt.Sprintf("CCATest.isVideoActive() === %t", active)
	if err := a.conn.WaitForExpr(ctx, code); err != nil {
		if a.cameraType != testutil.UseFakeVCDCamera {
			if jobErr := upstart.CheckJob(cleanupCtx, "cros-camera"); jobErr != nil {
				return errors.Wrap(jobErr, err.Error())
			}
		}
		return err
	}

	// GoBigSleepLint: Due to the pipeline delay in camera stack, animation
	// delay, and other reasons, sometimes a bug would be triggered after
	// several frames. Wait duration here and check that the state does not
	// change afterwards.
	if err := testing.Sleep(ctx, duration); err != nil {
		return err
	}

	var ok bool
	if err := a.conn.Eval(ctx, code, &ok); err != nil {
		return err
	}
	if !ok {
		return errors.Errorf("video state changed after %v", duration.Round(time.Millisecond))
	}
	return nil
}

// WaitForVideoActive waits for the video to become active for 1 second.
func (a *App) WaitForVideoActive(ctx context.Context) error {
	return a.checkVideoState(ctx, true, time.Second)
}

// CheckNoTemporalFile checks if there is any temporal file which don't match pattern |pat| left in |dir|
// and not in the exception list |exceptions| after the specified timestamp |ts|.
func (a *App) CheckNoTemporalFile(ctx context.Context, dir string, pat *regexp.Regexp, ts time.Time, exceptions []string) error {
	isFileInExceptions := func(fileName string, exceptions []string) bool {
		for _, exception := range exceptions {
			if fileName == exception {
				return true
			}
		}
		return false
	}
	return testing.Poll(ctx, func(ctx context.Context) error {
		files, err := ioutil.ReadDir(dir)
		if err != nil {
			return err
		}
		for _, file := range files {
			if file.ModTime().Before(ts) {
				continue
			}
			if !pat.MatchString(file.Name()) && !isFileInExceptions(file.Name(), exceptions) {
				return errors.Errorf("temporal file %v was found", file.Name())
			}
		}
		return nil
	}, &testing.PollOptions{Timeout: 5 * time.Second})
}

// WaitForFileSaved calls WaitForFileSavedFor with 5 second timeout.
func (a *App) WaitForFileSaved(ctx context.Context, dir string, pat *regexp.Regexp, ts time.Time) (os.FileInfo, error) {
	return a.WaitForFileSavedFor(ctx, dir, pat, ts, 5*time.Second)
}

// WaitForFileSavedFor waits for the presence of the captured file with file name matching the specified
// pattern, size larger than zero, and modified time after the specified timestamp.
func (a *App) WaitForFileSavedFor(ctx context.Context, dir string, pat *regexp.Regexp, ts time.Time, timeout time.Duration) (os.FileInfo, error) {
	var result os.FileInfo
	seen := make(map[string]struct{})
	if err := testing.Poll(ctx, func(ctx context.Context) error {
		files, err := ioutil.ReadDir(dir)
		if err != nil {
			return errors.Wrap(err, "failed to read the camera directory")
		}
		for _, file := range files {
			if file.Size() == 0 || file.ModTime().Before(ts) {
				continue
			}
			if _, ok := seen[file.Name()]; ok {
				continue
			}
			seen[file.Name()] = struct{}{}
			testing.ContextLog(ctx, "New file found: ", file.Name())
			if pat.MatchString(file.Name()) {
				testing.ContextLog(ctx, "Found a match: ", file.Name())
				result = file
				return nil
			}
		}
		return errors.New("no matching output file found")
	}, &testing.PollOptions{Timeout: timeout}); err != nil {
		return nil, errors.Wrapf(err, "no matching output file found after %v", timeout)
	}
	return result, nil
}

// CheckVideoInactive checks the video is inactive for 1 second.
func (a *App) CheckVideoInactive(ctx context.Context) error {
	return a.checkVideoState(ctx, false, time.Second)
}

// RestoreWindow restores the window, exiting a maximized, minimized, or fullscreen state.
func (a *App) RestoreWindow(ctx context.Context) error {
	return a.conn.Eval(ctx, "CCATest.restoreWindow()", nil)
}

// GetAverageOCRScanningLatency returns the average OCR scanning latency on preview in photo mode in milliseconds.
func (a *App) GetAverageOCRScanningLatency(ctx context.Context) (float64, error) {
	var averageLatency float64
	err := a.conn.Eval(ctx, "CCATest.getAverageOcrScanTime()", &averageLatency)
	return averageLatency, err
}

// GetDocumentReviewLastFileLatency returns the latency of saving images as a searchable PDF using OCR in document scanning mode.
func (a *App) GetDocumentReviewLastFileLatency(ctx context.Context) (float64, error) {
	var latency float64
	err := a.conn.Eval(ctx, "CCATest.getDocumentReviewLastFileProcessingTime()", &latency)
	return latency, err
}

// MinimizeWindow minimizes the window.
func (a *App) MinimizeWindow(ctx context.Context) error {
	return a.conn.Eval(ctx, "CCATest.minimizeWindow()", nil)
}

// MaximizeWindow maximizes the window.
func (a *App) MaximizeWindow(ctx context.Context) error {
	return a.conn.Eval(ctx, "CCATest.maximizeWindow()", nil)
}

// FullscreenWindow fullscreens the window.
func (a *App) FullscreenWindow(ctx context.Context) error {
	return a.conn.Eval(ctx, "CCATest.fullscreenWindow()", nil)
}

// GetNumOfCameras returns number of camera devices.
func (a *App) GetNumOfCameras(ctx context.Context) (int, error) {
	var numCameras int
	err := a.conn.Eval(ctx, "CCATest.getNumOfCameras()", &numCameras)
	return numCameras, err
}

// GetFacing returns the active camera facing.
func (a *App) GetFacing(ctx context.Context) (Facing, error) {
	var facing Facing
	if err := a.conn.Eval(ctx, "CCATest.getFacing()", &facing); err != nil {
		return "", err
	}
	return facing, nil
}

// GetPreviewViewportSize returns resolution of the preview view port.
func (a *App) GetPreviewViewportSize(ctx context.Context) (Resolution, error) {
	r, err := a.Size(ctx, PreviewViewport)
	if err != nil {
		return Resolution{-1, -1}, errors.Wrap(err, "failed to get the size of preview viewport")
	}
	return *r, nil
}

// GetScreenOrientation returns screen orientation.
func (a *App) GetScreenOrientation(ctx context.Context) (Orientation, error) {
	var orientation Orientation
	if err := a.conn.Eval(ctx, "CCATest.getScreenOrientation()", &orientation); err != nil {
		return "", errors.Wrap(err, "failed to get screen orientation")
	}
	return orientation, nil
}

// GetDeviceID returns the active camera device id.
func (a *App) GetDeviceID(ctx context.Context) (DeviceID, error) {
	var id DeviceID
	if err := a.conn.Eval(ctx, "CCATest.getDeviceId()", &id); err != nil {
		return "", err
	}
	return id, nil
}

// State returns whether a state is active in CCA.
func (a *App) State(ctx context.Context, state string) (bool, error) {
	// TODO(b/281625728): Remove this function once we change all usages to observe on UI instead.
	var result bool
	if err := a.conn.Call(ctx, &result, "CCATest.getState", state); err != nil {
		return false, errors.Wrapf(err, "failed to get state: %v", state)
	}
	return result, nil
}

// PreviewFrame grabs a frame from preview. The caller should be responsible for releasing the frame.
func (a *App) PreviewFrame(ctx context.Context) (*Frame, error) {
	if err := a.WaitForVideoActive(ctx); err != nil {
		return nil, errors.Wrap(err, "failed to wait for preview active")
	}
	var f chrome.JSObject
	if err := a.conn.Call(ctx, &f, "CCATest.getPreviewFrame"); err != nil {
		return nil, errors.Wrap(err, "failed to get preview frame")
	}
	return &Frame{&f}, nil
}

// TakeSinglePhoto takes a photo and saves it to the default location.
func (a *App) TakeSinglePhoto(ctx context.Context, timerState TimerState) ([]os.FileInfo, error) {
	return a.TakeSinglePhotoAndVerifySavedFiles(ctx, timerState, []*regexp.Regexp{PhotoPattern})
}

// TakePortraitPhoto takes a photo in portrait mode and saves it to the default location.
func (a *App) TakePortraitPhoto(ctx context.Context, timerState TimerState, hasHumanFace bool) ([]os.FileInfo, error) {
	if hasHumanFace {
		return a.TakeSinglePhotoAndVerifySavedFiles(ctx, timerState, []*regexp.Regexp{portraitRefPattern, portraitPattern})
	}
	return a.TakeSinglePhotoAndVerifySavedFiles(ctx, timerState, []*regexp.Regexp{portraitRefPattern})
}

// TakeSinglePhotoAndVerifySavedFiles verifies the saved photos files.
func (a *App) TakeSinglePhotoAndVerifySavedFiles(ctx context.Context, timerState TimerState, patterns []*regexp.Regexp) ([]os.FileInfo, error) {
	err := a.SetTimerOption(ctx, timerState)
	if err != nil {
		return nil, err
	}
	start := time.Now()

	testing.ContextLog(ctx, "Click on start shutter")
	if err = a.ClickShutter(ctx); err != nil {
		return nil, err
	}
	if err = a.WaitForState(ctx, "taking", false); err != nil {
		return nil, errors.Wrap(err, "capturing hasn't ended")
	}

	dir, err := a.SavedDir(ctx)
	if err != nil {
		return nil, err
	}

	var fileInfos []os.FileInfo
	for _, pattern := range patterns {
		info, err := a.WaitForFileSaved(ctx, dir, pattern, start)
		if err != nil {
			return nil, errors.Wrapf(err, "cannot find result picture with regexp: %v", pattern)
		}
		if elapsed := info.ModTime().Sub(start); timerState == TimerOn && elapsed < TimerDelay {
			return nil, errors.Errorf("the capture should happen after timer of %v, actual elapsed time %v", TimerDelay, elapsed)
		}
		fileInfos = append(fileInfos, info)
	}

	isExpert, err := a.State(ctx, Expert)
	if err != nil {
		return nil, err
	}
	isSaveMetadata, err := a.State(ctx, SaveMetadata)
	if err != nil {
		return nil, err
	}
	if !isExpert || !isSaveMetadata {
		return fileInfos, nil
	}

	metadataPatterns := getMetadataPatterns(fileInfos)
	for _, pattern := range metadataPatterns {
		info, err := a.WaitForFileSaved(ctx, dir, pattern, start)
		if err != nil {
			return nil, errors.Wrapf(err, "cannot find result metadata with regexp: %v", pattern)
		}

		if info.Size() == 0 {
			return nil, errors.Errorf("saved file %v is empty", info.Name())
		}

		if err != nil {
			return nil, err
		}

		path, err := a.FilePathInSavedDir(ctx, info.Name())
		if err != nil {
			return nil, err
		}

		var jsonString map[string]interface{}
		if content, err := os.ReadFile(path); err != nil {
			return nil, errors.Wrapf(err, "failed to read metadata file %v", info.Name())
		} else if err := json.Unmarshal(content, &jsonString); err != nil {
			return nil, errors.Wrapf(err, "not a valid json file %v", info.Name())
		}

		fileInfos = append(fileInfos, info)
	}

	return fileInfos, nil
}

func getMetadataPatterns(fileInfos []os.FileInfo) []*regexp.Regexp {
	// Matches the extension and the potential number suffix such as " (2).jpg".
	re := regexp.MustCompile(`( \(\d+\))?\.jpg$`)
	var patterns []*regexp.Regexp
	for _, info := range fileInfos {
		pattern := `^` + regexp.QuoteMeta(re.ReplaceAllString(info.Name(), "")) + `.*\.json$`
		patterns = append(patterns, regexp.MustCompile(pattern))
	}
	return patterns
}

// StartRecording starts recording a video.
func (a *App) StartRecording(ctx context.Context, timerState TimerState) (time.Time, error) {
	startTime := time.Now()
	if err := a.SetTimerOption(ctx, timerState); err != nil {
		return startTime, err
	}
	testing.ContextLog(ctx, "Click on start shutter")
	if err := a.ClickShutter(ctx); err != nil {
		return startTime, err
	}

	// Wait for end of timer and start of recording.
	if err := a.WaitForState(ctx, "recording", true); err != nil {
		return startTime, errors.Wrap(err, "recording is not started")
	}
	recordStartTime := time.Now()
	if timerState == TimerOn {
		// Assume that the delay between recording state being set and
		// time.Now() getting timestamp is small enough to be
		// neglected. Otherwise, it may miss the failure cases if
		// |autual recording start time|+ |this delay| > |startTime| +
		// |TimerDelay|.
		if delay := recordStartTime.Sub(startTime); delay < TimerDelay {
			return startTime, errors.Errorf("recording starts %v before timer finished", TimerDelay-delay)
		}
	}

	return startTime, nil
}

// StopRecording stops recording a video.
func (a *App) StopRecording(ctx context.Context, timerState TimerState, startTime time.Time) (os.FileInfo, time.Time, error) {
	testing.ContextLog(ctx, "Click on stop shutter")
	stopTime, err := a.TriggerStateChange(ctx, "recording", false, func() error {
		return a.ClickShutter(ctx)
	})
	if err != nil {
		return nil, time.Time{}, err
	}
	if err := a.WaitForState(ctx, "taking", false); err != nil {
		return nil, time.Time{}, errors.Wrap(err, "shutter is not ended")
	}
	dir, err := a.SavedDir(ctx)
	if err != nil {
		return nil, time.Time{}, err
	}
	info, err := a.WaitForFileSaved(ctx, dir, VideoPattern, startTime)
	if err != nil {
		return nil, time.Time{}, errors.Wrap(err, "cannot find result video")
	} else if elapsed := info.ModTime().Sub(startTime); timerState == TimerOn && elapsed < TimerDelay {
		return nil, time.Time{}, errors.Errorf("the capture happen after elapsed time %v, should be after %v timer", elapsed, TimerDelay)
	}
	return info, stopTime, nil
}

// RecordVideo records a video with duration length and save to default location.
func (a *App) RecordVideo(ctx context.Context, timerState TimerState, duration time.Duration) (os.FileInfo, error) {
	startTime, err := a.StartRecording(ctx, timerState)
	if err != nil {
		return nil, err
	}

	// GoBigSleepLint: Records the video for |duration| seconds.
	if err := testing.Sleep(ctx, duration); err != nil {
		return nil, err
	}

	info, _, err := a.StopRecording(ctx, timerState, startTime)
	if err != nil {
		return nil, err
	}
	return info, err
}

// RecordGif records a gif with maximal duration and |save| specify whether to save result gif in review page.
func (a *App) RecordGif(ctx context.Context, save bool) (os.FileInfo, error) {
	if visible, err := a.Visible(ctx, GifRecordingOption); err != nil {
		return nil, err
	} else if !visible {
		return nil, errors.Wrap(err, "failed to find gif recording option")
	}

	if err := a.Click(ctx, GifRecordingOption); err != nil {
		return nil, err
	}

	// Start recording and wait for record to maximal gif duration and review page opened.
	if err := a.ClickShutter(ctx); err != nil {
		return nil, err
	}
	if err := a.WaitForState(ctx, "recording", true); err != nil {
		return nil, errors.Wrap(err, "gif recording is not started")
	}
	if err := a.WaitForState(ctx, "view-review", true); err != nil {
		return nil, errors.Wrap(err, "review page not opened after recording gif")
	}
	if !save {
		if err := a.Click(ctx, GifReviewRetakeButton); err != nil {
			return nil, err
		}
		if err := a.WaitForState(ctx, "taking", false); err != nil {
			return nil, errors.Wrap(err, "shutter is ended after clicking retake")
		}

		return nil, nil
	}

	// Check saved file.
	beforeSaveTime := time.Now()
	if err := a.Click(ctx, GifReviewSaveButton); err != nil {
		return nil, err
	}
	dir, err := a.SavedDir(ctx)
	if err != nil {
		return nil, err
	}
	info, err := a.WaitForFileSaved(ctx, dir, gifPattern, beforeSaveTime)
	if err != nil {
		return nil, errors.Wrap(err, "cannot find result gif")
	}
	if err := a.WaitForState(ctx, "taking", false); err != nil {
		return nil, errors.Wrap(err, "shutter is ended after saving file")
	}

	return info, nil
}

// savedDir returns the path to the folder where captured files might be saved.
func savedDir(ctx context.Context, cr *chrome.Chrome) (string, error) {
	path, err := cryptohome.UserPath(ctx, cr.NormalizedUser())
	if err != nil {
		return "", err
	}
	return filepath.Join(path, "MyFiles", "Camera"), nil
}

// ClearSavedDir clears all files in the folder where captured files might be saved.
func ClearSavedDir(ctx context.Context, cr *chrome.Chrome) error {
	dir, err := savedDir(ctx, cr)
	if err != nil {
		return errors.Wrap(err, "failed to get saved directory")
	}

	// Since "MyFiles/Camera" folder is not deletable by users once it is
	// created by CCA, we have assumption in CCA that it won't be deleted during
	// user session. Therefore, instead of completely deleting the it, we clear
	// all the contents inside if it exists.
	files, err := os.ReadDir(dir)
	if err != nil {
		if os.IsNotExist(err) {
			return nil
		}
		return errors.Wrap(err, "failed to read saved directory")
	}
	for _, file := range files {
		path := filepath.Join(dir, file.Name())
		if err := os.RemoveAll(path); err != nil {
			return errors.Wrapf(err, "failed to remove file %v from saved directory", path)
		}
	}
	return nil
}

// SavedDir returns the path to the folder where captured files are saved.
func (a *App) SavedDir(ctx context.Context) (string, error) {
	return savedDir(ctx, a.cr)
}

// FilePathInSavedDir finds and returns the path of the target file in saved directory.
func (a *App) FilePathInSavedDir(ctx context.Context, name string) (string, error) {
	dir, err := savedDir(ctx, a.cr)
	if err != nil {
		return "", err
	}

	path := filepath.Join(dir, name)
	if _, err := os.Stat(path); err != nil {
		if os.IsNotExist(err) {
			return "", errors.New("file not found in saved path")
		}
		return "", err
	}
	return path, nil
}

// SaveCameraFolder saves the camera folder to the output directory to make debug easier.
func (a *App) SaveCameraFolder(ctx context.Context) error {
	cameraFolderPath, err := savedDir(ctx, a.cr)
	if err != nil {
		return errors.Wrap(err, "failed to get camera folder path")
	}
	if _, err := os.Stat(cameraFolderPath); err != nil {
		if os.IsNotExist(err) {
			// Nothing to do.
			return nil
		}
		return errors.Wrap(err, "failed to stat camera folder")
	}

	targetFolderPath := filepath.Join(a.outDir, fmt.Sprintf("cameraFolder"))
	if err := os.MkdirAll(targetFolderPath, 0755); err != nil {
		return errors.Wrap(err, "failed to make folder to save camera folder")
	}

	files, err := os.ReadDir(cameraFolderPath)
	if err != nil {
		return errors.Wrap(err, "failed to read camera folder")
	}
	for _, file := range files {
		// Filtering out hidden dotfiles like .Temp folder storing temporary files when recording videos.
		if strings.HasPrefix(file.Name(), ".") {
			continue
		}
		srcFilePath := filepath.Join(cameraFolderPath, file.Name())
		dstFilePath := filepath.Join(targetFolderPath, file.Name())
		data, err := os.ReadFile(srcFilePath)
		if err != nil {
			return errors.Wrapf(err, "failed to read file: %v", srcFilePath)
		}
		if err := os.WriteFile(dstFilePath, data, 0644); err != nil {
			return errors.Wrapf(err, "failed to write file: %v", dstFilePath)
		}
	}
	return nil
}

// CheckFacing returns an error if the active camera facing is not expected.
func (a *App) CheckFacing(ctx context.Context, expected Facing) error {
	if facing, err := a.GetFacing(ctx); err != nil {
		return err
	} else if facing != expected {
		return errors.Errorf("expected facing: %v; actual: %v", expected, facing)
	}
	return nil
}

// Mirrored returns whether mirroring is on.
func (a *App) Mirrored(ctx context.Context) (bool, error) {
	return a.State(ctx, "mirror")
}

// CheckReviewUIExists returns whether the review UI exists.
func (a *App) CheckReviewUIExists(ctx context.Context, mode Mode) error {
	if err := a.WaitForVisibleState(ctx, ReviewView, true); err != nil {
		return errors.New("review result is not shown")
	}

	if visible, err := a.Visible(ctx, ConfirmResultButton); err != nil {
		return err
	} else if !visible {
		return errors.New("confirm button is not shown")
	}

	if visible, err := a.Visible(ctx, CancelResultButton); err != nil {
		return err
	} else if !visible {
		return errors.New("cancel button is not shown")
	}
	return nil
}

// ConfirmResult clicks the confirm button or the cancel button according to the given isConfirmed.
func (a *App) ConfirmResult(ctx context.Context, isConfirmed bool, mode Mode) error {
	if err := a.WaitForState(ctx, "view-review", true); err != nil {
		return errors.Wrap(err, "failed to wait for review ui showing up")
	}

	if err := a.CheckReviewUIExists(ctx, mode); err != nil {
		return errors.Wrap(err, "check confirm UI failed")
	}

	button := CancelResultButton
	if isConfirmed {
		button = ConfirmResultButton
	}
	if err := a.Click(ctx, button); err != nil {
		return err
	}
	return nil
}

// OpenQRCodeScanMode switches to Scan mode and chooses QR Code option.
func (a *App) OpenQRCodeScanMode(ctx context.Context) error {
	if err := a.SwitchMode(ctx, Scan); err != nil {
		return errors.Wrap(err, "failed to switch to scan mode")
	}
	if err := a.Click(ctx, ScanBarcodeOption); err != nil {
		return errors.Wrap(err, "failed to click the scan barcode option")
	}
	return nil
}

// SetTimerOption sets the timer option to on/off.
func (a *App) SetTimerOption(ctx context.Context, state TimerState) error {
	active := state == TimerOn
	if timerOn, err := a.State(ctx, "timer"); err != nil {
		return errors.Wrap(err, "failed to get state timer")
	} else if timerOn != active {
		if err := a.Click(ctx, OpenTimerPanelButton); err != nil {
			return errors.Wrap(err, "failed to open timer option panel")
		}
		if active {
			if err := a.Click(ctx, TimerOption3Seconds); err != nil {
				return errors.Wrap(err, "failed to click the 3s timer button")
			}
			if err := a.WaitForState(ctx, "timer-3s", true); err != nil {
				return errors.Wrap(err, "failed to wait for 3s-timer being active")
			}
		} else {
			if err := a.Click(ctx, TimerOptionOff); err != nil {
				return errors.Wrap(err, "failed to click the off timer button")
			}
			if err := a.WaitForState(ctx, "timer", false); err != nil {
				return errors.Wrap(err, "failed to wait for timer being inactive")
			}
		}
	}
	return nil
}

// SetMicOption sets the mic option to on/off.
func (a *App) SetMicOption(ctx context.Context, state MicState) error {
	active := state == MicOn
	if micOn, err := a.State(ctx, "mic"); err != nil {
		return errors.Wrap(err, "failed to get state mic")
	} else if micOn != active {
		if err := a.Click(ctx, ToggleMicButton); err != nil {
			return errors.Wrap(err, "failed to click the microphone button")
		}
		if err := a.WaitForState(ctx, "mic", active); err != nil {
			return errors.Wrapf(err, "failed to wait for mic state being changed to %v", active)
		}
	}
	return nil
}

// ToggleExpertMode toggles expert mode and returns whether it's enabled after toggling.
func (a *App) ToggleExpertMode(ctx context.Context) (bool, error) {
	prev, err := a.State(ctx, Expert)
	if err != nil {
		return false, err
	}
	if err := a.conn.Eval(ctx, "CCATest.toggleExpertMode()", nil); err != nil {
		return false, errors.Wrap(err, "failed to toggle expert mode")
	}
	if err := a.WaitForState(ctx, "expert", !prev); err != nil {
		return false, errors.Wrap(err, "failed to wait for toggling expert mode")
	}
	return a.State(ctx, Expert)
}

// EnableExpertMode enables expert mode.
func (a *App) EnableExpertMode(ctx context.Context) error {
	prevEnabled, err := a.State(ctx, Expert)
	if err != nil {
		return errors.Wrap(err, "failed to get expert mode state")
	}
	if prevEnabled {
		return nil
	}

	enabled, err := a.ToggleExpertMode(ctx)
	if err != nil {
		return errors.Wrap(err, "failed to toggle expert mode")
	}
	if !enabled {
		return errors.New("unexpected state after toggling expert mode")
	}
	return nil
}

// SetEnableMultiStreamRecording enables/disables recording videos with multiple streams via expert mode.
func (a *App) SetEnableMultiStreamRecording(ctx context.Context, enabled bool) error {
	if err := a.EnableExpertMode(ctx); err != nil {
		return errors.Wrap(err, "failed to enable expert mode")
	}

	if err := a.OpenSettingMenu(ctx, MainMenu); err != nil {
		return errors.Wrap(err, "failed to open main menu")
	}
	defer a.CloseSettingMenu(ctx, MainMenu)

	if err := a.OpenSettingMenu(ctx, ExpertMenu); err != nil {
		return errors.Wrap(err, "failed to open expert menu")
	}
	defer a.CloseSettingMenu(ctx, ExpertMenu)

	if err := a.SetOptionChecked(ctx, MultistreamRecordingOption, enabled); err != nil {
		return errors.Wrap(err, "failed to enable multi-stream recording")
	}

	// TODO(b/275304766): Remove the sleep once we collect enough performance data and can decide which solution should be kept.
	//
	// GoBigSleepLint: Sleep here as a workaround since now we have two multi-stream options so CCA might reconfigure twice.
	//                 Since we need two options only temporarily to collect performance data, use a sleep as a workaround
	//                 instead of introducing complex mechanism in CCA.
	if err := testing.Sleep(ctx, 5*time.Second); err != nil {
		return errors.Wrap(err, "failed to sleep before waiting for video active")
	}

	if err := a.WaitForVideoActive(ctx); err != nil {
		return errors.Wrap(err, "failed to wait for video active")
	}

	return nil
}

// SetEnableMultiStreamRecordingChrome enables/disables recording videos with multiple streams (scale by Chrome version) via expert mode.
func (a *App) SetEnableMultiStreamRecordingChrome(ctx context.Context, enabled bool) error {
	if err := a.EnableExpertMode(ctx); err != nil {
		return errors.Wrap(err, "failed to enable expert mode")
	}

	if err := a.OpenSettingMenu(ctx, MainMenu); err != nil {
		return errors.Wrap(err, "failed to open main menu")
	}
	defer a.CloseSettingMenu(ctx, MainMenu)

	if err := a.OpenSettingMenu(ctx, ExpertMenu); err != nil {
		return errors.Wrap(err, "failed to open expert menu")
	}
	defer a.CloseSettingMenu(ctx, ExpertMenu)

	if err := a.SetOptionChecked(ctx, MultistreamRecordingChromeOption, enabled); err != nil {
		return errors.Wrap(err, "failed to enable multi-stream-chrome recording")
	}

	// TODO(b/275304766): Remove the sleep once we collect enough performance data and can decide which solution should be kept.
	//
	// GoBigSleepLint: Sleep here as a workaround since now we have two multi-stream options so CCA might reconfigure twice.
	//                 Since we need two options only temporarily to collect performance data, use a sleep as a workaround
	//                 instead of introducing complex mechanism in CCA.
	if err := testing.Sleep(ctx, 5*time.Second); err != nil {
		return errors.Wrap(err, "failed to sleep before waiting for video active")
	}

	if err := a.WaitForVideoActive(ctx); err != nil {
		return errors.Wrap(err, "failed to wait for video active")
	}

	return nil
}

// ClickShutter clicks the shutter button.
func (a *App) ClickShutter(ctx context.Context) error {
	if err := a.Click(ctx, Shutter); err != nil {
		return errors.Wrap(err, "failed to click shutter button")
	}
	return nil
}

// SwitchCamera switches to next camera device.
func (a *App) SwitchCamera(ctx context.Context) error {
	if err := a.WaitForVisibleState(ctx, SwitchDeviceButton, true); err != nil {
		return errors.Wrap(err, "failed to wait for switch device button to be visible")
	}
	if err := a.TriggerConfiguration(ctx, func() error {
		return a.Click(ctx, SwitchDeviceButton)
	}); err != nil {
		return errors.Wrap(err, "failed to switch camera")
	}
	return nil
}

// SwitchMode switches to specified capture mode.
func (a *App) SwitchMode(ctx context.Context, mode Mode) error {
	modeName := string(mode)
	if active, err := a.State(ctx, modeName); err != nil {
		return err
	} else if active {
		return nil
	}
	if err := a.conn.Call(ctx, nil, "CCATest.switchMode", modeName); err != nil {
		return errors.Wrapf(err, "failed to switch to mode %s", mode)
	}
	if err := a.WaitForState(ctx, "mode-switching", false); err != nil {
		return errors.Wrap(err, "failed to wait for finishing of mode switching")
	}
	if err := a.WaitForVideoActive(ctx); err != nil {
		return errors.Wrapf(err, "preview is inactive after switching to mode %s", mode)
	}
	// Owing to the mode retry mechanism in CCA, it may fallback to other mode when failing to
	// switch to specified mode. Verify the mode value again after switching.
	if active, err := a.State(ctx, modeName); err != nil {
		return errors.Wrapf(err, "failed to get mode state after switching to mode %s", mode)
	} else if !active {
		return errors.Wrapf(err, "failed to switch to mode %s", mode)
	}
	return nil
}

// WaitForState waits until state become active/inactive.
func (a *App) WaitForState(ctx context.Context, state string, active bool) error {
	// TODO(b/281625728): Remove this function once we change all usages to observe on UI instead.
	code := fmt.Sprintf("CCATest.getState(%q) === %t", state, active)
	if err := a.conn.WaitForExpr(ctx, code); err != nil {
		return errors.Wrapf(err, "failed to wait for state %s to set to %v", state, active)
	}
	return nil
}

// RunThroughCameras runs function f in app after switching to each available camera.
// The f is called with parameter of the switched camera facing.
// The error returned by f is passed to caller of this function.
func (a *App) RunThroughCameras(ctx context.Context, f func(Facing) error) error {
	numCameras, err := a.GetNumOfCameras(ctx)
	if err != nil {
		return errors.Wrap(err, "can't get number of cameras")
	}
	devices := make(map[DeviceID]Facing)
	for cam := 0; cam < numCameras; cam++ {
		if cam != 0 {
			if err := a.SwitchCamera(ctx); err != nil {
				return errors.Wrap(err, "failed to switch camera")
			}
		}
		id, err := a.GetDeviceID(ctx)
		if err != nil {
			return errors.Wrap(err, "failed to get device id")
		}
		facing, err := a.GetFacing(ctx)
		if err != nil {
			return errors.Wrap(err, "failed to get facing")
		}
		if _, ok := devices[id]; ok {
			continue
		}
		devices[id] = facing
		testing.ContextLogf(ctx, "Run f() on camera facing %q", facing)
		if err := f(facing); err != nil {
			return err
		}
	}
	if numCameras > 1 {
		// Switch back to the original camera.
		if err := a.SwitchCamera(ctx); err != nil {
			return errors.Wrap(err, "failed to switch to next camera")
		}
	}
	if len(devices) != numCameras {
		return errors.Errorf("failed to switch to some camera (tested cameras: %v)", devices)
	}
	return nil
}

// OutputCodeCoverage stops the profiling and output the code coverage information to the output
// directory.
func (a *App) OutputCodeCoverage(ctx context.Context) error {
	reply, err := a.conn.StopProfiling(ctx)
	if err != nil {
		return err
	}

	coverageData, err := json.Marshal(reply)
	if err != nil {
		return err
	}

	coverageDirPath := filepath.Join(a.outDir, fmt.Sprintf("coverage"))
	if _, err := os.Stat(coverageDirPath); os.IsNotExist(err) {
		if err := os.MkdirAll(coverageDirPath, 0755); err != nil {
			return err
		}
	} else if err != nil {
		return err
	}

	for idx := 0; ; idx++ {
		coverageFilePath := filepath.Join(coverageDirPath, fmt.Sprintf("coverage-%d.json", idx))
		if _, err := os.Stat(coverageFilePath); os.IsNotExist(err) {
			if err := os.WriteFile(coverageFilePath, coverageData, 0644); err != nil {
				return err
			}
			break
		} else if err != nil {
			return err
		}
	}
	return nil
}

// TriggerConfiguration triggers configuration by calling trigger() and waits
// for camera configuration finishing. This ensures that the
// |CameraConfiguringState| goes from false -> true -> false.
func (a *App) TriggerConfiguration(ctx context.Context, trigger func() error) error {
	if _, err := a.TriggerStateChange(ctx, "camera-configuring", true, trigger); err != nil {
		return errors.Wrap(err, "failed to wait for camera to start configured")
	}
	if err := a.WaitForState(ctx, "camera-configuring", false); err != nil {
		return errors.Wrap(err, "failed to wait for camera to finish configured")
	}
	return nil
}

// TriggerStateChange triggers |state| change by calling |trigger()|, waits for
// its value changing from |!expected| to |expected| and returns when the
// change happens.
func (a *App) TriggerStateChange(ctx context.Context, state string, expected bool, trigger func() error) (time.Time, error) {
	// TODO(b/281625728): Remove this function once we change all usages to observe on UI instead.
	var wrappedPromise chrome.JSObject
	if err := a.conn.Call(ctx, &wrappedPromise, `
	  (state, expected) => {
		const p = CCATest.waitStateChange(state, expected);
		return () => p;
	  }
	  `, state, expected); err != nil {
		return time.Time{}, errors.Wrapf(err, "failed to observe %v state with %v expected value", state, expected)
	}
	defer wrappedPromise.Release(ctx)

	if err := trigger(); err != nil {
		return time.Time{}, err
	}

	var ts int64
	if err := a.conn.Call(ctx, &ts, `(p) => p()`, &wrappedPromise); err != nil {
		return time.Time{}, errors.Wrapf(err, "failed to wait for %v state changing to %v", state, expected)
	}
	return time.Unix(0, ts*1e6), nil
}

// EnsureTabletModeEnabled makes sure that the tablet mode states of both
// device and app are enabled, and returns a function which reverts back to the
// original state.
func (a *App) EnsureTabletModeEnabled(ctx context.Context, enabled bool) (func(ctx context.Context) error, error) {
	tconn, err := a.cr.TestAPIConn(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get test api connection")
	}

	originallyEnabled, err := ash.TabletModeEnabled(ctx, tconn)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get tablet mode state")
	}

	cleanup, err := ash.EnsureTabletModeEnabled(ctx, tconn, enabled)
	if err != nil {
		return nil, errors.Wrapf(err, "failed to ensure tablet mode enabled(%v)", enabled)
	}

	cleanupAll := func(ctx context.Context) error {
		if err := cleanup(ctx); err != nil {
			return errors.Wrap(err, "failed to clean up tablet mode state")
		}
		if err := a.WaitForState(ctx, "tablet", originallyEnabled); err != nil {
			return errors.Wrapf(err, "failed to wait for original tablet mode enabled(%v)", originallyEnabled)
		}
		return nil
	}

	if err := a.WaitForState(ctx, "tablet", enabled); err != nil {
		if err := cleanupAll(ctx); err != nil {
			testing.ContextLog(ctx, "Failed to restore tablet mode state: ", err)
		}
		return nil, errors.Wrapf(err, "failed to wait for tablet mode enabled(%v)", enabled)
	}
	return cleanupAll, nil
}

// CheckFocusedElementVisited checks if the focused element is already visited by checking a set.
func (a *App) CheckFocusedElementVisited(ctx context.Context) (bool, error) {
	var visited bool
	if err := a.conn.Eval(ctx, "CCATest.checkFocusedElementVisited()", &visited); err != nil {
		return false, err
	}
	return visited, nil
}

// ReturnFocusedElementAriaLabel returns the aria-label of the focused element.
func (a *App) ReturnFocusedElementAriaLabel(ctx context.Context) (string, error) {
	var arialabel string
	if err := a.conn.Eval(ctx, "CCATest.getFocusedElementAriaLabel()", &arialabel); err != nil {
		return arialabel, err
	}
	return arialabel, nil
}

// Focus sets focus on CCA App window.
func (a *App) Focus(ctx context.Context) error {
	return a.conn.Eval(ctx, "CCATest.focusWindow()", nil)
}

// Refresh refreshes CCA.
func (a *App) Refresh(ctx context.Context, tb *testutil.TestBridge) error {
	newAppWindow, err := testutil.RefreshApp(ctx, a.conn, tb)
	if err != nil {
		return err
	}

	// Releases the previous app window.
	if err := a.appWindow.Release(ctx); err != nil {
		return errors.Wrap(err, "failed to release app window")
	}
	a.appWindow = newAppWindow

	if err := loadScripts(ctx, a.conn); err != nil {
		return errors.Wrap(err, "failed to load scripts")
	}
	return nil
}

// SaveScreenshot saves a screenshot in the outDir.
func (a *App) SaveScreenshot(ctx context.Context) error {
	image, err := screenshot.CaptureChromeImage(ctx, a.cr)
	if err != nil {
		return errors.Wrap(err, "failed to capture Chrome image")
	}

	filename := fmt.Sprintf("screenshot_%d.jpg", time.Now().UnixNano())
	path := filepath.Join(a.outDir, filename)

	file, err := os.Create(path)
	if err != nil {
		return errors.Wrap(err, "failed to create screenshot")
	}
	defer file.Close()

	options := jpeg.Options{Quality: 80}
	err = jpeg.Encode(file, image, &options)
	if err != nil {
		return errors.Wrap(err, "failed to encode screenshot to jpeg")
	}
	return nil
}

// CheckMode checks whether CCA window is in correct capture mode.
func (a *App) CheckMode(ctx context.Context, mode Mode) error {
	if result, err := a.State(ctx, string(mode)); err != nil {
		return errors.Wrap(err, "failed to check state")
	} else if !result {
		return errors.Errorf("CCA is not in the expected mode: %s", mode)
	}
	return nil
}

// CheckCameraFacing checks whether CCA is in correct facing if there's a camera with that facing.
func (a *App) CheckCameraFacing(ctx context.Context, facing Facing) error {
	initialFacing, err := a.GetFacing(ctx)
	if err != nil {
		return errors.Wrap(err, "failed to get initial facing")
	}
	if initialFacing == facing {
		return nil
	}
	// It may fail to open desired facing camera on device without camera of
	// that facing or on device without facing configurations which returns
	// facing unknown for every camera. Try to query facing from every
	// available camera to ensure it's a true failure.
	// TODO(pihsun): Change this to a more light-weight solution that use
	// enumerateDevices() in js instead of actually switching through all
	// cameras.
	return a.RunThroughCameras(ctx, func(f Facing) error {
		if f == facing {
			return errors.Errorf("camera in wrong facing: got %v, want %v", initialFacing, facing)
		}
		return nil
	})
}

// SwitchToTimeLapseMode switches the app to "Time Lapse" sub-mode under "Video" mode
func (a *App) SwitchToTimeLapseMode(ctx context.Context) error {
	if err := a.SwitchMode(ctx, Video); err != nil {
		return err
	}
	if err := a.Click(ctx, TimeLapseRecordingOption); err != nil {
		return errors.Wrap(err, "failed to click on time-lapse mode button")
	}
	if err := a.WaitForState(ctx, "record-type-time-lapse", true); err != nil {
		return errors.Wrap(err, "failed to wait for time-lapse mode state to be active")
	}
	return nil
}

// TimeLapseDuration returns the expected duration of time-lapse video recorded for |recordTime|.
func (a *App) TimeLapseDuration(ctx context.Context, recordTime time.Duration) (time.Duration, error) {
	var duration float64
	if err := a.conn.Call(ctx, &duration, "CCATest.getTimeLapseDuration", recordTime.Seconds()); err != nil {
		return 0, nil
	}
	return time.Duration(duration * float64(time.Second)), nil
}

// FPSObserver returns the FPS observer which can be later used to get average FPS.
func (a *App) FPSObserver(ctx context.Context) (*FPSObserver, error) {
	var fpsObserver chrome.JSObject
	if err := a.conn.Eval(ctx, "CCATest.getFpsObserver()", &fpsObserver); err != nil {
		return nil, errors.Wrap(err, "failed to get FPS observer")
	}
	return &FPSObserver{jsObj: &fpsObserver}, nil
}

// AverageFPS accepts a FPSObserver as the parameter and get the average FPS via the observer. This call also releases the observer object.
func (f *FPSObserver) AverageFPS(ctx context.Context) (float64, error) {
	var averageFPS float64
	if err := f.jsObj.Call(ctx, &averageFPS, "function() { return this.getAverageFps() ?? 0; }"); err != nil {
		return 0.0, err
	}
	return averageFPS, nil
}

// HideFloatingUI hides all toasts, nudges and tooltips from CCA.
func (a *App) HideFloatingUI(ctx context.Context) error {
	// TODO(pihsun): Remove the check after Chrome is upreved and the function always exist.
	if err := a.conn.Eval(ctx, "CCATest.hideFloatingUI?.()", nil); err != nil {
		return errors.Wrap(err, "failed to hide floating UI")
	}
	return nil
}

// DisableVideoResolutionFilter disables video resolution filter in CCA.
func (a *App) DisableVideoResolutionFilter(ctx context.Context) error {
	// TODO(pihsun): Remove the check after Chrome is upreved and the function always exist.
	if err := a.conn.Eval(ctx, "CCATest.disableVideoResolutionFilter?.()", nil); err != nil {
		return errors.Wrap(err, "failed to disable video resolution filter")
	}
	return nil
}

// Stop stops the given FPS observer and release the associated JS object.
func (f *FPSObserver) Stop(ctx context.Context) error {
	defer f.jsObj.Release(ctx)
	return f.jsObj.Call(ctx, nil, "function() { return this.stop(); }")
}

// EnableBypassCSP enables bypassing CSP. A refresh is needed as CSP bypassing
// happens during CSP initialization instead of evaluation.
func (a *App) EnableBypassCSP(ctx context.Context, tb *testutil.TestBridge) error {
	if err := a.conn.EnableBypassCSP(ctx); err != nil {
		return errors.Wrap(err, "failed to enable bypassing CSP")
	}
	if err := a.Refresh(ctx, tb); err != nil {
		return errors.Wrap(err, "failed to refresh")
	}
	if err := a.WaitForVideoActive(ctx); err != nil {
		return errors.Wrap(err, "preview is not shown after refreshing")
	}
	return nil
}

// EnableGa4Metrics enables CCA to send GA4 metrics during Tast tests. A mock url is
// required as we don't want to actually send metrics to GA4 backend.
func (a *App) EnableGa4Metrics(ctx context.Context, url string) error {
	if err := a.conn.Call(ctx, nil, "CCATest.setMeasurementProtocolUrl", url); err != nil {
		return errors.Wrap(err, "failed to set replace measurement protocol's URL")
	}
	if err := a.conn.Call(ctx, nil, "CCATest.enableGa4Metrics"); err != nil {
		return errors.Wrap(err, "failed to enable GA4 metrics")
	}
	return nil
}

// SwitchTo60FPS switch current camera's FPS setting to 60.
func (a *App) SwitchTo60FPS(ctx context.Context) error {
	if err := a.OpenSettingMenu(ctx, MainMenu); err != nil {
		return errors.Wrap(err, "failed to open main settings menu")
	}

	if err := a.OpenSettingMenu(ctx, VideoResolutionMenu); err != nil {
		return errors.Wrap(err, "failed to open video resolution settings menu")
	}

	if err := a.ClickWithIndex(ctx, FPS60Buttons, 0); err != nil {
		return errors.Wrap(err, "failed to click 60 FPS button of the first resolution")
	}
	if err := a.WaitForVisibleState(ctx, SettingsButton, true); err != nil {
		return errors.Wrap(err, "settings button is not shown")
	}
	if err := a.WaitForVideoActive(ctx); err != nil {
		return errors.Wrap(err, "preview is inactive after switching FPS setting")
	}
	return nil
}

// ChooseVideoResolution switches to the specified video resolution for the
// camera with the specified facing.
func (a *App) ChooseVideoResolution(ctx context.Context, facing Facing, resolution Resolution) error {
	// TODO(kamchonlathorn): Skip configuring if the current video resolution matches the requested one.
	return a.TriggerConfiguration(ctx, func() error {
		if err := a.conn.Call(ctx, nil, "CCATest.chooseVideoResolution", facing, resolution); err != nil {
			return errors.Wrapf(err, "failed to switch video resolution to %v on camera facing %v", resolution, facing)
		}
		return nil
	})
}

// GalleryButtonCoverURL gets the cover URL of the gallery button.
func (a *App) GalleryButtonCoverURL(ctx context.Context) (string, error) {
	var supportGetGalleryButtonCoverURL bool
	if err := a.conn.Eval(ctx, "CCATest.getGalleryButtonCoverURL !== undefined", &supportGetGalleryButtonCoverURL); err != nil {
		return "", err
	}
	// TODO(pihsun): Remove this once Chrome is uprev to the newer version.
	if !supportGetGalleryButtonCoverURL {
		return a.AttributeWithIndex(ctx, GalleryButtonCover, 0, "src")
	}
	var url string
	if err := a.conn.Call(ctx, &url, "CCATest.getGalleryButtonCoverURL"); err != nil {
		return "", err
	}
	return url, nil
}

// CurrentPTZSettings returns the current PTZ settings.
func (a *App) CurrentPTZSettings(ctx context.Context) (*PTZSettings, error) {
	var settings PTZSettings
	if err := a.conn.Call(ctx, &settings, "CCATest.getPTZSettings"); err != nil {
		return nil, errors.Wrap(err, "failed to get current PTZ settings")
	}
	return &settings, nil
}

// ClickPTZButtonAndWaitSettingsUpdate clicks on the specified PTZ button, and
// wait until the PTZ settings value are updated.
func (a *App) ClickPTZButtonAndWaitSettingsUpdate(ctx context.Context, ui UIComponentName) (*PTZSettings, error) {
	settings, err := a.CurrentPTZSettings(ctx)
	if err != nil {
		return nil, err
	}
	if err := a.ClickPTZButton(ctx, ui); err != nil {
		return nil, errors.Wrapf(err, "failed to click on %v button", ui)
	}

	// Wait until the PTZ values are updated.
	var newSettings *PTZSettings
	err = testing.Poll(ctx, func(ctx context.Context) error {
		newSettings, err = a.CurrentPTZSettings(ctx)
		if settings.Equal(newSettings) {
			return errors.Errorf("PTZ values %v are not updated after clicking %v button", settings, ui)
		}
		return nil
	}, &testing.PollOptions{Timeout: 5 * time.Second})

	return newSettings, err
}

// ZoomInFromPTZPanel performs zoom-in by opening PTZ panel, clicking on zoom-in
// button, and closing PTZ panel.
func (a *App) ZoomInFromPTZPanel(ctx context.Context) error {
	cleanupCtx := ctx
	ctx, cancel := ctxutil.Shorten(ctx, 5*time.Second)
	defer cancel()

	// Create keyboard to close the PTZ panel.
	keyboard, err := input.Keyboard(ctx)
	if err != nil {
		return errors.Wrap(err, "failed to create a keyboard")
	}
	defer keyboard.Close(cleanupCtx)

	if err := a.Click(ctx, OpenPTZPanelButton); err != nil {
		return errors.Wrap(err, "failed to open the PTZ panel")
	}

	ptz, err := a.ClickPTZButtonAndWaitSettingsUpdate(ctx, ZoomInButton)
	if err != nil {
		return errors.Wrap(err, "failed to click the zoom-in button")
	}
	testing.ContextLogf(ctx, "Zoom ratio is updated to %.1fx", ptz.Zoom)

	if err := keyboard.Accel(ctx, "Esc"); err != nil {
		return errors.Wrap(err, "failed to close the PTZ panel")
	}

	if err = a.WaitForState(ctx, "view-ptz-panel", false); err != nil {
		return errors.Wrap(err, "failed to wait for PTZ panel to close")
	}

	return nil
}

// DisableSuperResIntroDialog disables the super resolution introduction dialog
// if exists.
func (a *App) DisableSuperResIntroDialog(ctx context.Context) error {
	visible, err := a.State(ctx, "view-super-res-intro-dialog")
	if err != nil {
		return err
	}
	if !visible {
		return nil
	}

	// Press Esc to cancel the dialog.
	cleanupCtx := ctx
	ctx, cancel := ctxutil.Shorten(ctx, 5*time.Second)
	defer cancel()

	// Create keyboard to close the PTZ panel.
	keyboard, err := input.Keyboard(ctx)
	if err != nil {
		return errors.Wrap(err, "failed to create a keyboard")
	}
	defer keyboard.Close(cleanupCtx)

	if err := keyboard.Accel(ctx, "Esc"); err != nil {
		return errors.Wrap(err, "failed to close the PTZ panel")
	}

	if err = a.WaitForState(ctx, "view-super-res-intro-dialog", false); err != nil {
		return errors.Wrap(err, "failed to wait for the dialog to close")
	}

	return nil
}

// GetVidPid returns the active camera vid:pid. Return "" if MIPI.
func (a *App) GetVidPid(ctx context.Context) (string, error) {
	var vidPid string
	if err := a.conn.Eval(ctx, "CCATest.getVidPid()", &vidPid); err != nil {
		return "", err
	}
	return vidPid, nil
}

// EnterDocumentMode switches CCA to Scan mode and check if CCA lands on Document sub-mode.
func (a *App) EnterDocumentMode(ctx context.Context) error {
	if err := a.SwitchMode(ctx, Scan); err != nil {
		return errors.Wrap(err, "failed to switch to scan mode")
	}

	if checked, err := a.IsCheckedWithIndex(ctx, ScanDocumentModeOption, 0); err != nil {
		return errors.Wrap(err, "failed to check if it lands on document mode")
	} else if !checked {
		return errors.New("failed to land on document mode by default")
	}

	return nil
}
