// Copyright 2020 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package cca provides utilities to interact with Chrome Camera App.
package cca

import (
	"context"
	"fmt"
	"regexp"
	"time"

	"go.chromium.org/tast-tests/cros/common/perf"
	"go.chromium.org/tast-tests/cros/local/camera/testutil"
	mediacpu "go.chromium.org/tast-tests/cros/local/media/cpu"

	"go.chromium.org/tast/core/ctxutil"
	"go.chromium.org/tast/core/errors"
	"go.chromium.org/tast/core/testing"
)

const (
	// Time reserved for cleanup.
	cleanupTime = 10 * time.Second

	// Duration to wait for CPU to be stabalized.
	stabilizationDuration time.Duration = 5 * time.Second
)

type metricValuePair struct {
	metric perf.Metric
	value  float64
}

// PerfData saves performance record collected by performance test.
type PerfData struct {
	// metricValues saves metric-value pair.
	metricValues []metricValuePair

	// durations maps from event name to a list of durations for each
	// time the event happened aggregated from different app instance.
	durations map[string][]float64
}

// NewPerfData creates new PerfData instance.
func NewPerfData() *PerfData {
	return &PerfData{durations: make(map[string][]float64)}
}

// SetMetricValue sets the metric and the corresponding value.
func (p *PerfData) SetMetricValue(m perf.Metric, value float64) {
	p.metricValues = append(p.metricValues, metricValuePair{m, value})
}

// SetDuration sets perf event name and the duration that event takes.
func (p *PerfData) SetDuration(name string, duration float64) {
	var values []float64
	if v, ok := p.durations[name]; ok {
		values = v
	}
	p.durations[name] = append(values, duration)
}

func averageDurations(durations []float64) float64 {
	sum := 0.0
	for _, v := range durations {
		sum += v
	}
	return sum / float64(len(durations))
}

// Save saves perf data into output directory.
func (p *PerfData) Save(outDir string) error {
	pv := perf.NewValues()
	for _, pair := range p.metricValues {
		pv.Set(pair.metric, pair.value)
	}

	for name, values := range p.durations {
		pv.Set(perf.Metric{
			Name:      name,
			Unit:      "milliseconds",
			Direction: perf.SmallerIsBetter,
		}, averageDurations(values))
	}
	return pv.Save(outDir)
}

// measureStabilizedUsage measures the CPU and power usage after it's cooled down for stabilizationDuration.
func measureStabilizedUsage(ctx context.Context, measureDuration time.Duration) (map[string]float64, error) {
	testing.ContextLog(ctx, "Sleeping to wait for CPU usage to stabilize for ", stabilizationDuration)
	// GoBigSleepLint: Sleep to stabilize CPU before measuring the CPU usage.
	if err := testing.Sleep(ctx, stabilizationDuration); err != nil {
		return nil, errors.Wrap(err, "failed to wait for CPU usage to stabilize")
	}

	testing.ContextLog(ctx, "Measuring CPU and Power usage for each for ", measureDuration)
	return mediacpu.MeasureUsage(ctx, measureDuration)
}

// MeasurePreviewPerformance measures the performance of preview.
func MeasurePreviewPerformance(ctx context.Context, app *App, perfData *PerfData, facing Facing, measureDuration time.Duration) error {
	cleanupCtx := ctx
	ctx, cancel := ctxutil.Shorten(ctx, 5*time.Second)
	defer cancel()

	testing.ContextLog(ctx, "Switching to photo mode")
	if err := app.SwitchMode(ctx, Photo); err != nil {
		return errors.Wrap(err, "failed to switch to photo mode")
	}

	scanBarcode, err := app.State(ctx, "enable-scan-barcode")
	if err != nil {
		return errors.Wrap(err, "failed to check barcode state")
	}
	if scanBarcode {
		return errors.New("QR code detection should be off by default")
	}

	fpsObserver, err := app.FPSObserver(ctx)
	if err != nil {
		return errors.Wrap(err, "failed to get FPS observer")
	}
	defer fpsObserver.Stop(cleanupCtx)

	usage, err := measureStabilizedUsage(ctx, measureDuration)
	if err != nil {
		return errors.Wrap(err, "failed to measure CPU and power usage")
	}

	if cpuUsage, exist := usage["cpu"]; exist {
		testing.ContextLogf(ctx, "Measured preview CPU usage: %.1f%%", cpuUsage)

		perfData.SetMetricValue(perf.Metric{
			Name:      fmt.Sprintf("cpu_usage_preview-facing-%s", facing),
			Unit:      "percent",
			Direction: perf.SmallerIsBetter,
		}, cpuUsage)
	} else {
		testing.ContextLog(ctx, "Failed to measure preview CPU usage")
	}

	if powerUsage, exist := usage["power"]; exist {
		testing.ContextLogf(ctx, "Measured preview power usage: %.1f Watts", powerUsage)

		perfData.SetMetricValue(perf.Metric{
			Name:      fmt.Sprintf("power_usage_preview-facing-%s", facing),
			Unit:      "Watts",
			Direction: perf.SmallerIsBetter,
		}, powerUsage)
	} else {
		testing.ContextLog(ctx, "Failed to measure preview power usage")
	}

	fps, err := fpsObserver.AverageFPS(ctx)
	if err != nil {
		return errors.Wrap(err, "failed to measure average FPS")
	}
	testing.ContextLogf(ctx, "Measured preview FPS: %.1f", fps)
	perfData.SetMetricValue(perf.Metric{
		Name:      fmt.Sprintf("preview_fps-facing-%s", facing),
		Unit:      "fps",
		Direction: perf.BiggerIsBetter,
	}, fps)

	return nil
}

// MeasureQRPreviewPerformance measures the performance of preview with QR code detection.
func MeasureQRPreviewPerformance(ctx context.Context, app *App, perfData *PerfData, facing Facing, measureDuration time.Duration) error {
	cleanupCtx := ctx
	ctx, cancel := ctxutil.Shorten(ctx, 5*time.Second)
	defer cancel()

	if err := app.OpenQRCodeScanMode(ctx); err != nil {
		return errors.Wrap(err, "failed to open QR code scan mode")
	}

	fpsObserverQR, err := app.FPSObserver(ctx)
	if err != nil {
		return errors.Wrap(err, "failed to get FPS observer")
	}
	defer fpsObserverQR.Stop(cleanupCtx)

	usageQR, err := measureStabilizedUsage(ctx, measureDuration)
	if err != nil {
		return errors.Wrap(err, "failed to measure CPU and power usage with QR code detection")
	}

	if cpuUsageQR, exist := usageQR["cpu"]; exist {
		perfData.SetMetricValue(perf.Metric{
			Name:      fmt.Sprintf("cpu_usage_qrcode-facing-%s", facing),
			Unit:      "percent",
			Direction: perf.SmallerIsBetter,
		}, cpuUsageQR)
	} else {
		testing.ContextLog(ctx, "Failed to measure preview CPU usage with QR code detection")
	}

	if powerUsageQR, exist := usageQR["power"]; exist {
		perfData.SetMetricValue(perf.Metric{
			Name:      fmt.Sprintf("power_usage_qrcode-facing-%s", facing),
			Unit:      "Watts",
			Direction: perf.SmallerIsBetter,
		}, powerUsageQR)
	} else {
		testing.ContextLog(ctx, "Failed to measure preview power usage with QR code detection")
	}

	fpsQR, err := fpsObserverQR.AverageFPS(ctx)
	if err != nil {
		return errors.Wrap(err, "failed to measure average FPS")
	}
	testing.ContextLogf(ctx, "Measured QR code detection preview FPS: %.1f", fpsQR)
	perfData.SetMetricValue(perf.Metric{
		Name:      fmt.Sprintf("preview_fps_qrcode-facing-%s", facing),
		Unit:      "fps",
		Direction: perf.BiggerIsBetter,
	}, fpsQR)
	return nil
}

// MeasureRecordingPerformance measures the performance of normal video recording.
func MeasureRecordingPerformance(ctx context.Context, app *App, perfData *PerfData, facing Facing, measureDuration time.Duration) error {
	testing.ContextLog(ctx, "Switching to video mode")
	if err := app.SwitchMode(ctx, Video); err != nil {
		return errors.Wrap(err, "failed to switch to video mode")
	}
	return MeasureVideoRecordingPerformance(ctx, app, perfData, facing, "recording", measureDuration)
}

// MeasureTimeLapsePerformance measures the performance of time-lapse video recording.
func MeasureTimeLapsePerformance(ctx context.Context, app *App, perfData *PerfData, facing Facing, measureDuration time.Duration) error {
	testing.ContextLog(ctx, "Switch to time-lapse mode")
	if err := app.SwitchToTimeLapseMode(ctx); err != nil {
		return errors.Wrap(err, "failed to switch to time-lapse mode")
	}
	return MeasureVideoRecordingPerformance(ctx, app, perfData, facing, "time-lapse", measureDuration)
}

// MeasureVideoRecordingPerformance measures the performance of video recording.
func MeasureVideoRecordingPerformance(ctx context.Context, app *App, perfData *PerfData, facing Facing, mode string, measureDuration time.Duration) error {
	cleanupCtx := ctx
	ctx, cancel := ctxutil.Shorten(ctx, 5*time.Second)
	defer cancel()

	recordingStartTime, err := app.StartRecording(ctx, TimerOff)
	if err != nil {
		return errors.Wrap(err, "failed to start recording for performance measurement")
	}

	fpsObserver, err := app.FPSObserver(ctx)
	if err != nil {
		return errors.Wrap(err, "failed to get FPS observer")
	}
	defer fpsObserver.Stop(cleanupCtx)

	usage, err := measureStabilizedUsage(ctx, measureDuration)
	if err != nil {
		return errors.Wrap(err, "failed to measure CPU and power usage")
	}

	if _, _, err := app.StopRecording(ctx, false, recordingStartTime); err != nil {
		return errors.Wrap(err, "failed to stop recording for performance measurement")
	}

	if cpuUsage, exist := usage["cpu"]; exist {
		testing.ContextLogf(ctx, "Measured recording CPU usage: %.1f%%", cpuUsage)

		perfData.SetMetricValue(perf.Metric{
			Name:      fmt.Sprintf("cpu_usage_%s-facing-%s", mode, facing),
			Unit:      "percent",
			Direction: perf.SmallerIsBetter,
		}, cpuUsage)
	} else {
		testing.ContextLog(ctx, "Failed to measure recording CPU usage")
	}

	if powerUsage, exist := usage["power"]; exist {
		testing.ContextLogf(ctx, "Measured recording power usage: %.1f Watts", powerUsage)

		perfData.SetMetricValue(perf.Metric{
			Name:      fmt.Sprintf("power_usage_%s-facing-%s", mode, facing),
			Unit:      "Watts",
			Direction: perf.SmallerIsBetter,
		}, powerUsage)
	} else {
		testing.ContextLog(ctx, "Failed to measure recording power usage")
	}

	fps, err := fpsObserver.AverageFPS(ctx)
	if err != nil {
		return errors.Wrap(err, "failed to measure average FPS")
	}
	testing.ContextLogf(ctx, "Measured recording preview FPS: %.1f", fps)
	perfData.SetMetricValue(perf.Metric{
		Name:      fmt.Sprintf("preview_fps_%s-facing-%s", mode, facing),
		Unit:      "fps",
		Direction: perf.BiggerIsBetter,
	}, fps)
	return nil
}

// MeasureTakingPicturePerformance takes a picture and measure the performance of UI operations.
func MeasureTakingPicturePerformance(ctx context.Context, app *App, measureDuration time.Duration) error {
	if err := app.WaitForVideoActive(ctx); err != nil {
		return err
	}

	testing.ContextLog(ctx, "Switching to photo mode")
	if err := app.SwitchMode(ctx, Photo); err != nil {
		return err
	}

	if _, err := app.TakeSinglePhoto(ctx, TimerOff); err != nil {
		return err
	}

	return nil
}

// MeasureGifRecordingPerformance records a gif and measure the performance of UI operations.
func MeasureGifRecordingPerformance(ctx context.Context, app *App, measureDuration time.Duration) error {
	if err := app.WaitForVideoActive(ctx); err != nil {
		return err
	}
	testing.ContextLog(ctx, "Switching to video mode")
	if err := app.SwitchMode(ctx, Video); err != nil {
		return err
	}
	if _, err := app.RecordGif(ctx, true); err != nil {
		return err
	}

	return nil
}

// CollectPerfEvents returns a map containing all perf events collected since
// app launch. The returned map maps from event name to a list of durations for
// each time that event happened.
func (a *App) CollectPerfEvents(ctx context.Context, perfData *PerfData) error {
	entries, err := a.appWindow.Perfs(ctx)
	if err != nil {
		return err
	}

	informativeEventName := func(entry testutil.PerfEntry) string {
		perfInfo := entry.PerfInfo
		if len(perfInfo.Facing) > 0 {
			// To avoid containing invalid character in the metrics name, we should remove the non-Alphanumeric characters from the facing.
			// e.g. When the facing is not set, the corresponding string will be (not-set).
			reg := regexp.MustCompile("[^a-zA-Z0-9]+")
			validFacingString := reg.ReplaceAllString(perfInfo.Facing, "")
			return fmt.Sprintf(`%s-facing-%s`, entry.Event, validFacingString)
		}
		return entry.Event
	}

	durationMap := make(map[string][]float64)
	for _, entry := range entries {
		name := informativeEventName(entry)
		perfData.SetDuration(name, entry.Duration)
		var values []float64
		if v, ok := durationMap[name]; ok {
			values = v
		}
		durationMap[name] = append(values, entry.Duration)
	}

	for name, values := range durationMap {
		testing.ContextLogf(ctx, "Perf event: %s => %f ms", name, averageDurations(values))
	}
	return nil
}
