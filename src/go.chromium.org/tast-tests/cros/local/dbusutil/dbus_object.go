// Copyright 2019 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package dbusutil

import (
	"context"

	"github.com/godbus/dbus/v5"

	"go.chromium.org/tast/core/errors"
)

// DBusObject wraps a D-Bus interface, object and connection.
type DBusObject struct {
	iface string
	obj   dbus.BusObject
	conn  *dbus.Conn
}

const dbusGetPropsMethod = "org.freedesktop.DBus.Properties.Get"
const dbusGetAllPropsMethod = "org.freedesktop.DBus.Properties.GetAll"

// NewDBusObject creates a DBusObject.
func NewDBusObject(ctx context.Context, service, iface string, path dbus.ObjectPath) (*DBusObject, error) {
	conn, obj, err := ConnectNoTiming(ctx, service, path)
	if err != nil {
		return nil, err
	}
	return &DBusObject{
		iface: iface,
		conn:  conn,
		obj:   obj,
	}, nil
}

// Obj returns the dbus.BusObject for this object.
func (d *DBusObject) Obj() dbus.BusObject {
	return d.obj
}

// Iface returns the D-Bus interface for this type of object.
func (d *DBusObject) Iface() string {
	return d.iface
}

// Conn returns the D-Bus connection to service.
func (d *DBusObject) Conn() *dbus.Conn {
	return d.conn
}

// ObjectPath returns the path of the D-Bus object.
func (d *DBusObject) ObjectPath() dbus.ObjectPath {
	return d.obj.Path()
}

// String returns the path of the D-Bus object as a string.
// It is so named to conform to the Stringer interface.
func (d *DBusObject) String() string {
	return string(d.obj.Path())
}

// IfacePath builds a D-Bus path starting at this object's interface path.
func (d *DBusObject) IfacePath(subPath string) string {
	return BuildIfacePath(d.iface, subPath)
}

// Call calls the D-Bus method with argument against the designated D-Bus object.
func (d *DBusObject) Call(ctx context.Context, method string, args ...interface{}) *dbus.Call {
	return d.obj.CallWithContext(ctx, d.IfacePath(method), 0, args...)
}

// CallForBool calls the D-Bus method with argument against the designated D-Bus
// object and then returns the method's returned value as a bool.
func (d *DBusObject) CallForBool(ctx context.Context, method string, args ...interface{}) (bool, error) {
	c := d.Call(ctx, method, args...)
	if c.Err != nil {
		return false, c.Err
	}
	var result bool
	if err := c.Store(&result); err != nil {
		return false, errors.Wrapf(err, "failed to store response of method %q as a bool", method)
	}
	return result, nil
}

// CallForSuccess is the same as CallForBool, but fails of the returned bool
// is not true. This is useful for methods that return true only if they are
// successful and a return of false is a failure.
func (d *DBusObject) CallForSuccess(ctx context.Context, method string, args ...interface{}) error {
	result, err := d.CallForBool(ctx, method, args...)
	if err != nil {
		return err
	}
	if !result {
		return errors.Errorf("call to method %q returned false", method)
	}
	return nil
}

// CallForInt calls the D-Bus method with argument against the designated D-Bus
// object and then returns the method's returned value as an int.
func (d *DBusObject) CallForInt(ctx context.Context, method string, args ...interface{}) (int, error) {
	c := d.Call(ctx, method, args...)
	if c.Err != nil {
		return 0, c.Err
	}
	var result int
	if err := c.Store(&result); err != nil {
		return 0, errors.Wrapf(err, "failed to store response of method %q as an int", method)
	}
	return result, nil
}

// CallForUInt8 calls the D-Bus method with argument against the designated
// D-Bus object and then returns the method's returned value as an uint8.
func (d *DBusObject) CallForUInt8(ctx context.Context, method string, args ...interface{}) (uint8, error) {
	c := d.Call(ctx, method, args...)
	if c.Err != nil {
		return 0, c.Err
	}
	var result uint8
	if err := c.Store(&result); err != nil {
		return 0, errors.Wrapf(err, "failed to store response of method %q as a uint8", method)
	}
	return result, nil
}

// CallForUInt16 calls the D-Bus method with argument against the designated
// D-Bus object and then returns the method's returned value as an uint16.
func (d *DBusObject) CallForUInt16(ctx context.Context, method string, args ...interface{}) (uint16, error) {
	c := d.Call(ctx, method, args...)
	if c.Err != nil {
		return 0, c.Err
	}
	var result uint16
	if err := c.Store(&result); err != nil {
		return 0, errors.Wrapf(err, "failed to store response of method %q as a uint16", method)
	}
	return result, nil
}

// CallForUInt32 calls the D-Bus method with argument against the designated
// D-Bus object and then returns the method's returned value as an uint32.
func (d *DBusObject) CallForUInt32(ctx context.Context, method string, args ...interface{}) (uint32, error) {
	c := d.Call(ctx, method, args...)
	if c.Err != nil {
		return 0, c.Err
	}
	var result uint32
	if err := c.Store(&result); err != nil {
		return 0, errors.Wrapf(err, "failed to store response of method %q as a uint32", method)
	}
	return result, nil
}

// CallForUInt64 calls the D-Bus method with argument against the designated
// D-Bus object and then returns the method's returned value as an uint64.
func (d *DBusObject) CallForUInt64(ctx context.Context, method string, args ...interface{}) (uint64, error) {
	c := d.Call(ctx, method, args...)
	if c.Err != nil {
		return 0, c.Err
	}
	var result uint64
	if err := c.Store(&result); err != nil {
		return 0, errors.Wrapf(err, "failed to store response of method %q as a uint64", method)
	}
	return result, nil
}

// CallForString calls the D-Bus method with argument against the designated
// D-Bus object and then returns the method's returned value as a string.
func (d *DBusObject) CallForString(ctx context.Context, method string, args ...interface{}) (string, error) {
	c := d.Call(ctx, method, args...)
	if c.Err != nil {
		return "", c.Err
	}
	var result string
	if err := c.Store(&result); err != nil {
		return "", errors.Wrapf(err, "failed to store response of method %q as a string", method)
	}
	return result, nil
}

// Property calls org.freedesktop.DBus.Properties.Get and stores the result into
// val.
func (d *DBusObject) Property(ctx context.Context, propName string, val interface{}) error {
	return d.obj.CallWithContext(ctx, dbusGetPropsMethod, 0, d.iface, propName).Store(val)
}

// PropertyBool calls Property for a bool value.
func (d *DBusObject) PropertyBool(ctx context.Context, propName string) (bool, error) {
	var value bool
	if err := d.Property(ctx, propName, &value); err != nil {
		return false, err
	}
	return value, nil
}

// PropertyInt calls Property for an int value.
func (d *DBusObject) PropertyInt(ctx context.Context, propName string) (int, error) {
	var value int
	if err := d.Property(ctx, propName, &value); err != nil {
		return 0, err
	}
	return value, nil
}

// PropertyString calls Property for a string value.
func (d *DBusObject) PropertyString(ctx context.Context, propName string) (string, error) {
	var value string
	if err := d.Property(ctx, propName, &value); err != nil {
		return "", err
	}
	return value, nil
}

// PropertyStrings calls Property for a []string value.
func (d *DBusObject) PropertyStrings(ctx context.Context, propName string) ([]string, error) {
	var value []string
	if err := d.Property(ctx, propName, &value); err != nil {
		return nil, err
	}
	return value, nil
}

// PropertyUint8 calls Property for a uint8 value.
func (d *DBusObject) PropertyUint8(ctx context.Context, propName string) (uint8, error) {
	var value uint8
	if err := d.Property(ctx, propName, &value); err != nil {
		return 0, err
	}
	return value, nil
}

// PropertyInt16 calls Property for a int16 value.
func (d *DBusObject) PropertyInt16(ctx context.Context, propName string) (int16, error) {
	var value int16
	if err := d.Property(ctx, propName, &value); err != nil {
		return 0, err
	}
	return value, nil
}

// PropertyUint16 calls Property for a uint16 value.
func (d *DBusObject) PropertyUint16(ctx context.Context, propName string) (uint16, error) {
	var value uint16
	if err := d.Property(ctx, propName, &value); err != nil {
		return 0, err
	}
	return value, nil
}

// PropertyUint32 calls Property for a uint32 value.
func (d *DBusObject) PropertyUint32(ctx context.Context, propName string) (uint32, error) {
	var value uint32
	if err := d.Property(ctx, propName, &value); err != nil {
		return 0, err
	}
	return value, nil
}

// AllProperties calls org.freedesktop.DBus.Properties.GetAll and stores the
// result into val.
func (d *DBusObject) AllProperties(ctx context.Context) (map[string]interface{}, error) {
	val := make(map[string]interface{})
	if err := d.obj.CallWithContext(ctx, dbusGetAllPropsMethod, 0, d.iface).Store(val); err != nil {
		return nil, err
	}
	return val, nil
}

// SetProperty is a shortcut for calling SetProperty for this object.
// The property name is prepended with the iface path.
func (d *DBusObject) SetProperty(ctx context.Context, name string, value interface{}) error {
	return SetProperty(ctx, d.obj, d.IfacePath(name), value)
}

// CreateWatcher returns a SignalWatcher to observe the specified signals.
func (d *DBusObject) CreateWatcher(ctx context.Context, signalNames ...string) (*SignalWatcher, error) {
	specs := make([]MatchSpec, len(signalNames))
	for i, sigName := range signalNames {
		specs[i] = MatchSpec{
			Type:      "signal",
			Path:      d.obj.Path(),
			Interface: d.iface,
			Member:    sigName,
		}
	}
	watcher, err := NewSignalWatcher(ctx, d.conn, specs...)
	if err != nil {
		return nil, err
	}
	return watcher, nil
}

// BuildIfacePath builds a full D-Bus path starting at the given baseIface path.
func BuildIfacePath(baseIface, subPath string) string {
	return baseIface + "." + subPath
}
