// Copyright 2022 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package wifi contains fixtures and helper functionality for WiFi tests.
package wifi
