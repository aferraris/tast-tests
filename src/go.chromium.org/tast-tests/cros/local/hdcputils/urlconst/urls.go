// Copyright 2022 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package urlconst

const (
	// VP9Subsample is VP9 subsample content URL.
	VP9Subsample = "https://storage.googleapis.com/wvmedia/cenc/vp9/subsample/24fps/tears/tears.mpd"
	// VP9Superframe is VP9 superframes content URL.
	VP9Superframe = "https://storage.googleapis.com/wvmedia/cenc/vp9/superframes/30fps/llama/llama.mpd"
	// VP9UHD is VP9 Ultra HD content URL.
	VP9UHD = "https://storage.googleapis.com/wvmedia/cenc/vp9/subsample/24fps/tears/tears_uhd.mpd"
	// H264SD is h264 standard defintion content URL.
	H264SD = "https://storage.googleapis.com/wvmedia/cenc/h264/tears/tears_sd.mpd"
	// H264HD is h264 HD content URL.
	H264HD = "https://storage.googleapis.com/wvmedia/cenc/h264/tears/tears_hd.mpd"
	// H264UHD is h264 Ultra HD content URL.
	H264UHD = "https://storage.googleapis.com/wvmedia/cenc/h264/tears/tears_uhd.mpd"
	// H264Fullsample is h264 sample content URL.
	H264Fullsample = "https://storage.googleapis.com/wvmedia/cenc/h264/tears.mpd"
	// H264CBCS is h264 cbcs content URL.
	H264CBCS = "https://storage.googleapis.com/wvmedia/cbcs/h264/30fps/llama/llama.mpd"
	// H264Subsample is h264 sub sample content URL.
	H264Subsample = "https://storage.googleapis.com/wvmedia/cenc/h264/tears/tears.mpd"
	// HEVCclip is hevc content URL.
	HEVCclip = "https://storage.googleapis.com/wvmedia/cenc/hevc/tears/tears.mpd"
	// HEVCCBCS is hevc cbcs content URL.
	HEVCCBCS = "https://storage.googleapis.com/wvmedia/cbcs/hevc/enc_pattern_5_5/30fps/llama/llama.mpd"
	// HEVCCBCS2 is hevc cbcs2 content URL.
	HEVCCBCS2 = "https://storage.googleapis.com/wvmedia/cbcs/hevc/enc_pattern_10_0/30fps/llama/llama.mpd"
	// HEVC4K is hevc 4K content URL.
	HEVC4K = "https://storage.googleapis.com/wvmedia/cenc/hevc/tears/tears_uhd.mpd"
	// HEVCclipSD is hevc standard definition content URL.
	HEVCclipSD = "https://storage.googleapis.com/wvmedia/cenc/hevc/tears/tears_sd.mpd"
	// HEVCclipHD is hevc HD content URL.
	HEVCclipHD = "https://storage.googleapis.com/wvmedia/cenc/hevc/tears/tears_hd.mpd"
	// AV1 is av1 content URL.
	AV1 = "https://storage.googleapis.com/wvmedia/cenc/av1/mp4/24fps/llama/llama.mpd"
	// ProxyURL is common proxy URL.
	ProxyURL = "https://proxy.uat.widevine.com/proxy?provider=widevine_test&full_response=true?video_id=GTS_HW_SECURE_ALL"
	// ProxyHDCPV2 is HDCP v2 proxy URL.
	ProxyHDCPV2 = "https://proxy.uat.widevine.com/proxy?video_id=GTS_HW_SECURE_ALL_HDCP_V2"
	// ProxyHDCPV1 is HDCP v1 proxy URL.
	ProxyHDCPV1 = "https://proxy.uat.widevine.com/proxy?video_id=GTS_HW_SECURE_ALL_HDCP_V1"
)
