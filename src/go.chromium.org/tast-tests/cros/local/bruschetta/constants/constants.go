// Copyright 2023 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package constants

// BruschettaVMName is the name of the bru VM set in policy and displayed in UI.
const BruschettaVMName = "Reference VM"
