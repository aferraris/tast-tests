// Copyright 2023 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package constants contains shared constants related to Bruschetta. This is a
// separate package to avoid import cycles.
package constants
