// Copyright 2023 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

/*
Package certpageutils implements miscellaneous and unsorted helpers used on
certificate's settings page (not certificate manager) for testing
policies/operations on users certificates and CA certificates.
*/
package certpageutils
