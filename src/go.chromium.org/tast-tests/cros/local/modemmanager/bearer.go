// Copyright 2023 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package modemmanager

import (
	"context"

	"github.com/godbus/dbus/v5"

	"go.chromium.org/tast-tests/cros/common/mmconst"
	"go.chromium.org/tast-tests/cros/local/dbusutil"
	"go.chromium.org/tast/core/errors"
	"go.chromium.org/tast/core/testing"
)

// IPConfig represents a MM IPConfig dbus object
type IPConfig struct {
	Method uint32
	// optional values
	Address string
	Prefix  uint32
	DNS1    string
	DNS2    string
	DNS3    string
	Gateway string
	Mtu     uint32
}

// BearerProperties represents a MM Bearer properties dbus object
type BearerProperties struct {
	apn            string
	allowRoaming   bool
	allowedAuth    uint32
	apnType        uint32
	ipType         uint32
	multiplex      uint32
	password       string
	profile        *BearerProperties
	profileEnabled bool
	profileID      int32
	profileName    string
	profileSource  uint32
	user           string
	dbusMap        map[string]interface{}
	// Indicators of property existence
	HasApn            bool
	HasAllowRoaming   bool
	HasAllowedAuth    bool
	HasApnType        bool
	HasIPType         bool
	HasMultiplex      bool
	HasPassword       bool
	HasProfileEnabled bool
	HasProfileID      bool
	HasProfileName    bool
	HasProfileSource  bool
	HasSource         bool
	HasUser           bool
}

// InvalidProfileID is a sentinel value for profile ID when it is not present or when the profile is not valid.
const (
	InvalidProfileID = -1
)

// Bearer represents a MM Bearer dbus object
type Bearer struct {
	ph    *dbusutil.PropertyHolder
	props *dbusutil.Properties
	path  dbus.ObjectPath
	modem *Modem
}

// BearerConnectionError represents a MM Bearer error dbus object
type BearerConnectionError []string

// NewBearer creates a new Bearer adapter from bearer properties.
func NewBearer(ctx context.Context, bearerPath, modemPath dbus.ObjectPath) (*Bearer, error) {
	ph, err := dbusutil.NewPropertyHolder(ctx, DBusModemmanagerService, DBusModemmanagerBearerInterface, bearerPath)
	if err != nil {
		return nil, err
	}

	props, err := ph.GetProperties(ctx)
	if err != nil {
		return nil, err
	}
	// Verify that all mandatory properties exist.
	_, err = props.GetString(mmconst.BearerPropertyInterface)
	if err != nil {
		return nil, errors.Wrap(err, "missing interface property")
	}
	_, err = props.GetBool(mmconst.BearerPropertyConnected)
	if err != nil {
		return nil, errors.Wrap(err, "missing connected property")
	}
	_, err = props.GetStructOfStrings(mmconst.BearerPropertyConnectionError)
	if err != nil {
		return nil, errors.Wrap(err, "missing ConnectionError property")
	}
	_, err = props.GetBool(mmconst.BearerPropertyMultiplexed)
	if err != nil {
		return nil, errors.Wrap(err, "missing multiplexed property")
	}
	_, err = parseIPConfig(props, mmconst.BearerPropertyIP4Config)
	if err != nil {
		return nil, errors.Wrap(err, "failed to read Ip4Config")
	}
	_, err = parseIPConfig(props, mmconst.BearerPropertyIP6Config)
	if err != nil {
		return nil, errors.Wrap(err, "failed to read Ip6Config")
	}

	// Inner Properties
	innerPropsGet, err := props.Get(mmconst.BearerPropertyProperties)
	if err != nil {
		return nil, errors.Wrap(err, "failed to read bearer properties")
	}
	_, ok := innerPropsGet.(map[string]interface{})
	if !ok {
		return nil, errors.New("failed to parse bearer properties")
	}
	modem, err := NewModemFromPath(ctx, modemPath)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create modem object")
	}

	return &Bearer{ph: ph, path: bearerPath, props: props, modem: modem}, nil
}

func parseIPConfig(props *dbusutil.Properties, propertyName string) (*IPConfig, error) {
	ipConfigGet, err := props.Get(propertyName)
	if err != nil {
		return nil, errors.Wrap(err, "failed to read IPConfig")
	}
	ipConfigMap, ok := ipConfigGet.(map[string]interface{})
	if !ok {
		return nil, errors.New("failed to parse IPConfig")
	}
	var ipConfig IPConfig
	value, ok := ipConfigMap[mmconst.BearerPropertyIPMethod]
	if ok {
		ipConfig.Method, ok = value.(uint32)
	}
	if !ok {
		return nil, errors.New("failed to parse the |method| from IPConfig")
	}
	value, ok = ipConfigMap[mmconst.BearerPropertyIPAddress]
	if ok {
		ipConfig.Address, ok = value.(string)
	}
	value, ok = ipConfigMap[mmconst.BearerPropertyIPPrefix]
	if ok {
		ipConfig.Prefix, ok = value.(uint32)
	}
	value, ok = ipConfigMap[mmconst.BearerPropertyIPDns1]
	if ok {
		ipConfig.DNS1, ok = value.(string)
	}
	value, ok = ipConfigMap[mmconst.BearerPropertyIPDns2]
	if ok {
		ipConfig.DNS2, ok = value.(string)
	}
	value, ok = ipConfigMap[mmconst.BearerPropertyIPDns3]
	if ok {
		ipConfig.DNS3, ok = value.(string)
	}
	value, ok = ipConfigMap[mmconst.BearerPropertyIPGateway]
	if ok {
		ipConfig.Gateway, ok = value.(string)
	}
	value, ok = ipConfigMap[mmconst.BearerPropertyIPMtu]
	if ok {
		ipConfig.Mtu, ok = value.(uint32)
	}
	return &ipConfig, nil
}

// GetAPN gets the APN from the bearer properties
func (b *Bearer) GetAPN(ctx context.Context) (string, error) {
	properties := b.Properties(ctx)
	if properties.profile != nil && properties.profile.HasApn {
		return properties.profile.apn, nil
	}
	if properties.HasApn {
		return properties.apn, nil
	}
	return "", errors.New("failed to read the APN")
}

// IsAPNType checks if the APN is of the type |apnType|
func (b *Bearer) IsAPNType(ctx context.Context, apnType mmconst.BearerAPNType) (bool, error) {
	properties := b.Properties(ctx)
	if properties.profile != nil && properties.profile.HasApnType {
		return (properties.profile.apnType & uint32(apnType)) != 0, nil
	}
	if properties.HasApnType {
		return (properties.apnType & uint32(apnType)) != 0, nil
	}
	return false, errors.New("failed to read the APN type")
}

// IsIPType checks if the IP of the bearer is of the type |ipType|
func (b *Bearer) IsIPType(ctx context.Context, ipType mmconst.BearerIPFamily) (bool, error) {
	properties := b.Properties(ctx)
	if !properties.HasIPType {
		return false, errors.New("failed to read the IP type")
	}
	return (properties.ipType & uint32(ipType)) != 0, nil
}

// GetProfileID gets the profile ID from the bearer properties or
// InvalidProfileID if it is not present.
func (b *Bearer) GetProfileID(ctx context.Context) (int32, error) {
	properties := b.Properties(ctx)
	return properties.GetProfileID(ctx)
}

// Interface gets the Interface value
func (b *Bearer) Interface() string {
	value, err := b.props.GetString(mmconst.BearerPropertyInterface)
	if err != nil {
		panic("missing interface property")
	}
	return value
}

// Connected gets the Connected value
func (b *Bearer) Connected() bool {
	value, err := b.props.GetBool(mmconst.BearerPropertyConnected)
	if err != nil {
		panic("missing connected property")
	}
	return value
}

// ConnectionError gets the ConnectionError value
func (b *Bearer) ConnectionError() BearerConnectionError {
	value, err := b.props.GetStructOfStrings(mmconst.BearerPropertyConnectionError)
	if err != nil {
		panic("missing ConnectionError property")
	}
	return value
}

// ToString gets the error code from the ConnectionError value
func (e BearerConnectionError) ToString() (string, error) {
	if len(e) < 2 {
		return "", errors.Errorf("failed to parse connection error: %q", e)
	}
	return e[0], nil
}

// Multiplexed gets the Multiplexed value
func (b *Bearer) Multiplexed() bool {
	value, err := b.props.GetBool(mmconst.BearerPropertyMultiplexed)
	if err != nil {
		panic("missing multiplexed property")
	}
	return value
}

// IP4Config gets the IP4Config value
func (b *Bearer) IP4Config() *IPConfig {
	value, err := parseIPConfig(b.props, mmconst.BearerPropertyIP4Config)
	if err != nil {
		panic("failed to read Ip4Config")
	}
	return value
}

// IP6Config gets the IP6Config value
func (b *Bearer) IP6Config() *IPConfig {
	value, err := parseIPConfig(b.props, mmconst.BearerPropertyIP6Config)
	if err != nil {
		panic("failed to read Ip6Config")
	}
	return value
}

// GetProfileID gets the profile ID from the bearer properties or
// InvalidProfileID if it is not present.
func (bp *BearerProperties) GetProfileID(ctx context.Context) (int32, error) {
	if !bp.HasProfileID {
		return InvalidProfileID, errors.New("failed to read the profile ID")
	}
	return bp.profileID, nil
}

// GetProfileSource gets the profile source from the bearer properties if it exists.
func (bp *BearerProperties) GetProfileSource(ctx context.Context) (uint32, error) {
	if !bp.HasProfileSource {
		return 0, errors.New("failed to read the profile source")
	}
	return bp.profileSource, nil
}

// IsProfileSource checks if the profile source of the bearer is of the type |profileSource|
func (bp *BearerProperties) IsProfileSource(ctx context.Context, profileSource mmconst.BearerProfileSource) (bool, error) {
	if !bp.HasProfileSource {
		return false, errors.New("failed to read the profile source")
	}
	return (bp.profileSource & uint32(profileSource)) != 0, nil
}

// Print prints all the values from the dbus map.
func (bp *BearerProperties) Print(ctx context.Context) {
	testing.ContextLog(ctx, "Print profile: ")

	if bp.dbusMap != nil {
		for k, v := range bp.dbusMap {
			testing.ContextLog(ctx, k, " : ", v)
		}
	}

}

// Properties gets the Properties value
func (b *Bearer) Properties(ctx context.Context) BearerProperties {
	innerPropsGet, err := b.props.Get(mmconst.BearerPropertyProperties)
	if err != nil {
		panic("failed to read bearer properties")
	}
	innerProps, ok := innerPropsGet.(map[string]interface{})
	if !ok {
		panic("failed to parse bearer properties")
	}

	return b.modem.loadBearerPropertiesFromMap(ctx, innerProps, true)
}

// loadBearerPropertiesFromMap creates a BearerProperties object from a dus property map.
func (m *Modem) loadBearerPropertiesFromMap(ctx context.Context, dbusMap map[string]interface{}, loadProfiles bool) BearerProperties {

	properties := BearerProperties{}
	properties.dbusMap = dbusMap
	// APN
	value, ok := dbusMap[mmconst.BearerPropertyApn]
	if ok {
		properties.apn, ok = value.(string)
	}
	properties.HasApn = ok
	// AllowRoaming
	value, ok = dbusMap[mmconst.BearerPropertyAllowRoaming]
	if ok {
		properties.allowRoaming, ok = value.(bool)
	}
	properties.HasAllowRoaming = ok
	// AllowedAuth
	value, ok = dbusMap[mmconst.BearerPropertyAllowedAuth]
	if ok {
		properties.allowedAuth, ok = value.(uint32)
	}
	properties.HasAllowedAuth = ok
	// APN type
	value, ok = dbusMap[mmconst.BearerPropertyApnType]
	if ok {
		properties.apnType, ok = value.(uint32)
	}
	properties.HasApnType = ok
	// IP type
	value, ok = dbusMap[mmconst.BearerPropertyIPType]
	if ok {
		properties.ipType, ok = value.(uint32)
	}
	properties.HasIPType = ok
	// Multiplex
	value, ok = dbusMap[mmconst.BearerPropertyMultiplex]
	if ok {
		properties.multiplex, ok = value.(uint32)
	}
	properties.HasMultiplex = ok
	// Password
	value, ok = dbusMap[mmconst.BearerPropertyPassword]
	if ok {
		properties.password, ok = value.(string)
	}
	properties.HasPassword = ok
	// User
	value, ok = dbusMap[mmconst.BearerPropertyUser]
	if ok {
		properties.user, ok = value.(string)
	}
	properties.HasUser = ok
	// Profile ID
	value, ok = dbusMap[mmconst.BearerPropertyProfileID]
	if ok {
		properties.profileID, ok = value.(int32)
	}
	properties.HasProfileID = ok
	// Profile name
	value, ok = dbusMap[mmconst.BearerPropertyProfileName]
	if ok {
		properties.profileName, ok = value.(string)
	}
	properties.HasProfileName = ok
	// Profile source
	value, ok = dbusMap[mmconst.BearerPropertyProfileSource]
	if ok {
		properties.profileSource, ok = value.(uint32)
	}
	properties.HasProfileSource = ok

	properties.profile = nil

	if loadProfiles {
		// Read the inner properties of the profile if the bearer was created using a profile ID.
		if properties.HasProfileID && properties.profileID != InvalidProfileID {
			profiles, err := m.GetProfilesAsBearerProperties(ctx)
			if err != nil {
				panic(err)
			}
			for _, profile := range profiles {
				// This is almost certainly a malformed profile if either of these fail, but
				// it's not really what we're testing for here.
				if !profile.HasProfileID {
					continue
				}

				if properties.profileID != profile.profileID {
					continue
				}

				properties.profile = &profile
			}
		}

	}

	return properties
}

// GetProfilesAsBearerProperties gets the list of profiles
func (m *Modem) GetProfilesAsBearerProperties(ctx context.Context) ([]BearerProperties, error) {
	var allProperties []BearerProperties
	profiles, err := m.GetProfiles(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get profiles")
	}
	for _, profile := range profiles {

		allProperties = append(allProperties, m.loadBearerPropertiesFromMap(ctx, profile, false))
	}

	return allProperties, nil
}
