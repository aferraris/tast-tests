// Copyright 2022 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package googlemeet contains UI functions and common libraries on Google Meet App.
package googlemeet
