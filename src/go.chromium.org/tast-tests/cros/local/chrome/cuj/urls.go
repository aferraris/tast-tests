// Copyright 2022 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package cuj

const (
	// The following are URLs for Spera V1.

	// URLs used by QuickCheckCUJ2.

	// GoogleNewsURL specifies the URL for Google News.
	GoogleNewsURL = "https://news.google.com/"
	// GooglePhotosURL specifies the URL for Google Photos.
	GooglePhotosURL = "https://photos.google.com"

	// URLs used by VideoCUJ2.

	// YoutubeGoogleTVVideoURL specifies the URL of the YouTube video for Google TV.
	YoutubeGoogleTVVideoURL = "https://youtu.be/g309_g-RF_s"
	// YoutubeDeveloperKeynoteVideoURL specifies the URL of the YouTube video for Google I/O ‘21.
	YoutubeDeveloperKeynoteVideoURL = "https://youtu.be/D_mVOAXcrtc"
	// YoutubeStadiaGDCVideoURL specifies the URL of the YouTube video for Stadia GDC 2019.
	YoutubeStadiaGDCVideoURL = "https://youtu.be/nUih5C5rOrA"

	// URLs used by ProductivityCUJ.

	// NewGoogleDocsURL specifies the URL to create new Google Docs.
	NewGoogleDocsURL = "https://docs.new"
	// NewGoogleSlidesURL specifies the URL to create new Google Slide.
	NewGoogleSlidesURL = "https://slide.new"
	// NewGoogleSheetsURL specifies the URL to create new Google Slide.
	NewGoogleSheetsURL = "https://sheet.new"
	// GoogleDocsURL specifies the URL for Google Docs.
	GoogleDocsURL = "https://docs.google.com/document"
	// GoogleSlidesURL specifies the URL for Google Slides.
	GoogleSlidesURL = "http://docs.google.com/slides"
	// GoogleSheetsURL specifies the URL for Google Sheets.
	GoogleSheetsURL = "http://docs.google.com/spreadsheets"
	// Microsoft365URL specifies the URL of Microsoft 365 Home.
	Microsoft365URL = "https://www.microsoft365.com/"

	// URLs used by FrontlineWorkerCUJ.

	// GoogleURL specifies the URL for Google.
	GoogleURL = "https://google.com"
	// GoogleChatURL specifies the URL for Google Chat.
	GoogleChatURL = "https://chat.google.com"

	// URLs used by ZoomConfCUJ.

	// ZoomURL specifies the URL for Zoom.
	ZoomURL = "https://zoom.us/"
	// ZoomSignInURL specifies the URL for Zoom sign in.
	ZoomSignInURL = "https://zoom.us/google_oauth_signin"

	// URLs used by VideoEditingApp.

	// WeVideoURL specifies the URL for WeVideo.
	WeVideoURL = "https://www.wevideo.com/"

	// URLs used by TinkerCadCUJ.

	// TinkerCadSignInURL specifies the URL for TinkerCad sign in.
	TinkerCadSignInURL = "https://www.tinkercad.com/login"
	// TinkerCadDashboardURL specifies the URL for TinkerCad dashboard.
	TinkerCadDashboardURL = "https://www.tinkercad.com/dashboard"
	// TinkerCAD3DDesignsURL specifies the URL for TinkerCad dashboard of 3D designs.
	TinkerCAD3DDesignsURL = "https://www.tinkercad.com/dashboard?collection=designs&type=tinkercad"

	// URLs used by ClinicianWorkstationCUJ

	// WebMDURL specifies the URL for web MD.
	WebMDURL = "https://www.webmd.com/"
	// GoogleKeepURL specifies the URL for Google Keep.
	GoogleKeepURL = "https://keep.google.com/"
)
