// Copyright 2023 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package cuj

const (
	// The following are URLs for Spera V2.

	// External URLs used by TabSwitch.
	// These URLs will be used in spera.web_source is set to "external".

	// CnnAmericasURL specifies the URL for CNN's page covering news from Americas.
	CnnAmericasURL = "https://edition.cnn.com/americas"
	// CnnAsiaURL specifies the URL for CNN's page covering news from Asia.
	CnnAsiaURL = "https://edition.cnn.com/asia"
	// CnnAustraliaURL specifies the URL for CNN's page covering news from Australia.
	CnnAustraliaURL = "https://edition.cnn.com/australia"
	// CnnMiddleEastURL specifies the URL for CNN's page covering news from Middle-East.
	CnnMiddleEastURL = "https://edition.cnn.com/middle-east"
	// CnnWorldURL specifies the URL for CNN's covering news from the world.
	CnnWorldURL = "https://edition.cnn.com/world"

	// EspnCollegeBasketballURL specifies the URL for ESPN's college basketball page.
	EspnCollegeBasketballURL = "https://www.espn.com/mens-college-basketball/"
	// EspnNbaURL specifies the URL for ESPN's NBA page.
	EspnNbaURL = "https://www.espn.com/nba/"
	// EspnNflURL specifies the URL for ESPN's NFL page.
	EspnNflURL = "https://www.espn.com/nfl/"
	// EspnSoccerURL specifies the URL for ESPN's soccer page.
	EspnSoccerURL = "https://www.espn.com/soccer/"
	// EspnTennisURL specifies the URL for ESPN's tennis page.
	EspnTennisURL = "https://www.espn.com/tennis/"

	// HuluKidsURL specifies the URL for Hulu's Kids page.
	HuluKidsURL = "https://www.hulu.com/hub/kids"
	// HuluMoviesURL specifies the URL for Hulu's movies page.
	HuluMoviesURL = "https://www.hulu.com/hub/movies"

	// MediumAIURL specifies the URL for Medium's artificial-intelligence tag.
	MediumAIURL = "https://medium.com/tag/artificial-intelligence"
	// MediumBusinessURL specifies the URL for Medium's business tag.
	MediumBusinessURL = "https://medium.com/tag/business"
	// MediumSoftwareURL specifies the URL for Medium's software-engineering tag.
	MediumSoftwareURL = "https://medium.com/tag/software-engineering"
	// MediumStartupURL specifies the URL for Medium's startup tag.
	MediumStartupURL = "https://medium.com/tag/startup"
	// MediumWorkURL specifies the URL for Medium's work tag.
	MediumWorkURL = "https://medium.com/tag/work"

	// NetflixURL specifies the URL for Netflix's home page.
	NetflixURL = "https://help.netflix.com/en/"
	// PinterestURL specifies the URL for Pinterest's ideas page.
	PinterestURL = "https://www.pinterest.com/ideas/"

	// RedditAppleURL specifies the URL for Reddit's Apple page.
	RedditAppleURL = "https://www.reddit.com/r/apple"
	// RedditBrooklynURL specifies the URL for Reddit's Brooklyn Nine-Nine page.
	RedditBrooklynURL = "https://www.reddit.com/r/brooklynninenine"
	// RedditOlympicsURL specifies the URL for Reddit's Olympics page.
	RedditOlympicsURL = "https://www.reddit.com/r/olympics"
	// RedditProgrammingURL specifies the URL for Reddit's programming page.
	RedditProgrammingURL = "https://www.reddit.com/r/programming"
	// RedditTechNewsURL specifies the URL for Reddit's tech news.
	RedditTechNewsURL = "https://www.reddit.com/r/technews"
	// RedditWallStreetURL specifies the URL for Reddit's WallStreetBets page.
	RedditWallStreetURL = "https://www.reddit.com/r/wallstreetbets"

	// WikipediaAboutURL specifies the URL for Wikipedia's about page.
	WikipediaAboutURL = "https://en.wikipedia.org/wiki/Wikipedia:About"
	// WikipediaCommunityURL specifies the URL for Wikipedia's community page.
	WikipediaCommunityURL = "https://en.wikipedia.org/wiki/Wikipedia:Community_portal"
	// WikipediaContributionURL specifies the URL for Wikipedia's user contributions page.
	WikipediaContributionURL = "https://en.wikipedia.org/wiki/Help:User_contributions"
	// WikipediaCurrentEventsURL specifies the URL for Wikipedia's current events page.
	WikipediaCurrentEventsURL = "https://en.wikipedia.org/wiki/Portal:Current_events"
	// WikipediaHelpURL specifies the URL for Wikipedia's help page.
	WikipediaHelpURL = "https://en.wikipedia.org/wiki/Help:Contents"
	// WikipediaMainURL specifies the URL for Wikipedia's main page.
	WikipediaMainURL = "https://en.wikipedia.org/wiki/Main_Page"

	// YahooScienceURL specifies the URL for Yahoo's science page.
	YahooScienceURL = "https://news.yahoo.com/science/"
	// YahooFinanceWatchListURL specifies the URL for Yahoo's finance page.
	YahooFinanceWatchListURL = "https://finance.yahoo.com/watchlists/"
	// YahooUsURL specifies the URL for Yahoo's US page.
	YahooUsURL = "https://news.yahoo.com/us/"
	// YahooWorldURL specifies the URL for Yahoo's world page.
	YahooWorldURL = "https://news.yahoo.com/world/"

	// YoutubeURL specifies the URL for YouTube's home page.
	YoutubeURL = "https://www.youtube.com"

	// Google URLs used by TabSwitch.
	// These URLs will be used in spera.web_source is set to "google".

	// GoogleFinanceURL specifies the URL for Google Finance's home page.
	GoogleFinanceURL = "https://www.google.com/finance/"
	// GoogleFinanceIndexesURL specifies the URL for Google Finance's indexes page.
	GoogleFinanceIndexesURL = "https://www.google.com/finance/markets/indexes"
	// GoogleFinanceMostActiveURL specifies the URL for Google Finance's most active page.
	GoogleFinanceMostActiveURL = "https://www.google.com/finance/markets/most-active"
	// GoogleFinanceGainersURL specifies the URL for Google Finance's gainers page.
	GoogleFinanceGainersURL = "https://www.google.com/finance/markets/gainers"
	// GoogleFinanceLosersURL specifies the URL for Google Finance's losers page.
	GoogleFinanceLosersURL = "https://www.google.com/finance/markets/losers"
	// GoogleHelpChromeURL specifies the URL for Google Help's Chrome page.
	GoogleHelpChromeURL = "https://support.google.com/chrome/?hl=en"
	// GoogleHelpMailURL specifies the URL for Google Help's Mail page.
	GoogleHelpMailURL = "https://support.google.com/mail/?hl=en"
	// GoogleHelpGooglePlayURL specifies the URL for Google Help's Google Play page.
	GoogleHelpGooglePlayURL = "https://support.google.com/googleplay/?hl=en"
	// GoogleHelpMapURL specifies the URL for Google Help's Maps page.
	GoogleHelpMapURL = "https://support.google.com/maps/?hl=en"
	// GoogleHelpYoutubeURL specifies the URL for Google Help's Youtube page.
	GoogleHelpYoutubeURL = "https://support.google.com/youtube/?hl=en"
	// GoogleNonprofitsURL specifies the URL for Google Nonprofits' home page.
	GoogleNonprofitsURL = "https://www.google.com/intl/en-US/nonprofits/"
	// GoogleNonprofitsEligibilityURL specifies the URL for Google Nonprofits' eligibility page.
	GoogleNonprofitsEligibilityURL = "https://www.google.com/intl/en-US/nonprofits/eligibility"
	// GoogleNonprofitsGoogleAdGrantsURL specifies the URL for Google Nonprofits' google ad grants page.
	GoogleNonprofitsGoogleAdGrantsURL = "https://www.google.com/nonprofits/offerings/google-ad-grants/"
	// GoogleNonprofitsGoogleEarthAndMapsURL specifies the URL for Google Nonprofits' google earth and maps page.
	GoogleNonprofitsGoogleEarthAndMapsURL = "https://www.google.com/nonprofits/offerings/google-earth-and-maps/"
	// GoogleNonprofitsProductHelpURL specifies the URL for Google Nonprofits' product help page.
	GoogleNonprofitsProductHelpURL = "https://www.google.com/intl/en-US/nonprofits/resources/product-help"
	// GoogleNonprofitsSuccessStoriesURL specifies the URL for Google Nonprofits' success stories page.
	GoogleNonprofitsSuccessStoriesURL = "https://www.google.com/intl/en-US/nonprofits/success-stories"
	// GoogleNonprofitsWorkspaceURL specifies the URL for Google Nonprofits' workspace page.
	GoogleNonprofitsWorkspaceURL = "https://www.google.com/nonprofits/offerings/workspace/"
	// GooglePolicyURL specifies the URL for Google Policy's home page.
	GooglePolicyURL = "https://policies.google.com/"
	// GooglePolicyPrivacyURL specifies the URL for Google Policy's privacy page.
	GooglePolicyPrivacyURL = "https://policies.google.com/privacy"
	// GooglePolicyTechnologiesURL specifies the URL for Google Policy's technologies page.
	GooglePolicyTechnologiesURL = "https://policies.google.com/technologies"
	// GooglePolicyTermsURL specifies the URL for Google Policy's terms page.
	GooglePolicyTermsURL = "https://policies.google.com/terms"
	// GooglePolicyFAQURL specifies the URL for Google Policy's FAQ page.
	GooglePolicyFAQURL = "https://policies.google.com/faq"
	// GoogleStoreURL specifies the URL for Google Store's home page.
	GoogleStoreURL = "https://store.google.com/us/"
	// GoogleStoreInstallationURL specifies the URL for Google Store's installation page.
	GoogleStoreInstallationURL = "https://store.google.com/us/magazine/installation"
	// GoogleStoreRepairCenterURL specifies the URL for Google Store's repair center page.
	GoogleStoreRepairCenterURL = "https://store.google.com/us/magazine/repaircenter"
	// GoogleStorePhonesURL specifies the URL for Google Store's phones page.
	GoogleStorePhonesURL = "https://store.google.com/us/category/phones"
	// GoogleStoreSubscriptionsURL specifies the URL for Google Store's subscriptions page.
	GoogleStoreSubscriptionsURL = "https://store.google.com/us/category/subscriptions"
	// GoogleWorkspaceURL specifies the URL for Google Workspace's home page.
	GoogleWorkspaceURL = "https://workspace.google.com/intl/en/"
	// GoogleWorkspaceBusinessURL specifies the URL for Google Workspace's business page.
	GoogleWorkspaceBusinessURL = "https://workspace.google.com/intl/en/business"
	// GoogleWorkspaceDriveURL specifies the URL for Google Workspace's drive page.
	GoogleWorkspaceDriveURL = "https://workspace.google.com/intl/en/products/drive/"
	// GoogleWorkspaceFAQURL specifies the URL for Google Workspace's FAQ page.
	GoogleWorkspaceFAQURL = "https://workspace.google.com/intl/en/faq"
	// GoogleWorkspaceIntegrationsURL specifies the URL for Google Workspace's integrations page.
	GoogleWorkspaceIntegrationsURL = "https://workspace.google.com/intl/en/integrations/"
	// GoogleWorkspacePricingURL specifies the URL for Google Workspace's pricing page.
	GoogleWorkspacePricingURL = "https://workspace.google.com/intl/en/pricing"
	// GoogleWorkspaceResourcesURL specifies the URL for Google Workspace's resources page.
	GoogleWorkspaceResourcesURL = "https://workspace.google.com/intl/en/resources"
	// GoogleWorkspaceSecurityURL specifies the URL for Google Workspace's security page.
	GoogleWorkspaceSecurityURL = "https://workspace.google.com/intl/en/security"
	// GoogleWorkspaceGmailURL specifies the URL for Google Workspace's gmail page.
	GoogleWorkspaceGmailURL = "https://workspace.google.com/intl/en/products/gmail/"

	// External URLs used by MultiTaskingApp.
	// These URLs will be used in spera.MultiTaskingApp.web_source is set to "external".

	// CnnURL specifies the URL for CNN's main page.
	CnnURL = "https://edition.cnn.com/"
	// HuluURL specifies the URL for Hulu's home page.
	HuluURL = "https://www.hulu.com/"
	// RedditURL specifies the URL for Reddit's home page.
	RedditURL = "https://www.reddit.com/"
	// WikipediaURL specifies the URL for Wikipedia's home page.
	WikipediaURL = "https://www.wikipedia.org/"
	// YoutubeMusicURL specifies the URL for a YouTube Music channel.
	YoutubeMusicURL = "https://music.youtube.com/playlist?list=RDCLAK5uy_nZiG9ehz_MQoWQxY5yElsLHCcG0tv9PRg"

	// Google URLs used by MultiTaskingApp.

	// GmailURL specifies the URL for Gmail.
	GmailURL = "https://mail.google.com"
	// GoogleCalendarURL specifies the URL for Google Calendar.
	GoogleCalendarURL = "https://calendar.google.com/"

	// URLs used by GoogleMeet.

	// WebRTCInternalsURL specifies the URL for WebRTC internals.
	WebRTCInternalsURL = "chrome://webrtc-internals"

	// URLs used by WebStreaming.

	// CrosVideoURL specifies the URL for cros video.
	CrosVideoURL = "https://crosvideo.appspot.com/"

	// URLs used by VideoConfProxy.

	// VideoCallURL specifies the video URL for VideoConfProxy.
	VideoCallURL = "https://storage.googleapis.com/chromiumos-test-assets-public/tast/cros/spera/video_conf_proxy.html"
	// VideoCallDocsURL specifies the doc URL for VideoConfProxy.
	VideoCallDocsURL = "http://crospower.page.link/power_VideoCall_doc"
)
