// Copyright 2022 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package quicksettings

import (
	"context"
	"time"

	"go.chromium.org/tast-tests/cros/local/chrome"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto/nodewith"
	"go.chromium.org/tast-tests/cros/local/chrome/uiauto/role"
	"go.chromium.org/tast/core/errors"
	"go.chromium.org/tast/core/testing"
)

// NavigateToNetworkDetailedView will navigate to the detailed Network view
// within the Quick Settings. This is safe to call even when the Quick Settings
// are already open.
func NavigateToNetworkDetailedView(ctx context.Context, tconn *chrome.TestConn) error {
	ui := uiauto.New(tconn)

	networkDetailedView, err := NetworkDetailedView(ctx, tconn)
	if err != nil {
		return errors.Wrap(err, "failed to get network detailed view")
	}
	// The quicksettings could be collapsed during navigating to the certain view,
	// typically caused by pop-up window, notifications or other display rendering event, retrying it is essential.
	return testing.Poll(ctx, func(ctx context.Context) error {
		// Hide then show quicksettings to force its state to update.
		if err := Hide(ctx, tconn); err != nil {
			return err
		}
		if err := Show(ctx, tconn); err != nil {
			return err
		}

		return uiauto.Combine("click the Network item in quick settings",
			ui.WithTimeout(5*time.Second).LeftClick(FeatureTileNetwork),
			ui.WithTimeout(5*time.Second).WaitUntilExists(networkDetailedView),
		)(ctx)
	}, &testing.PollOptions{Timeout: time.Minute, Interval: time.Second})
}

// OpenNetworkSettings will open the Network settings within the Quick Settings.
// NavigateToNetworkDetailedView() must be called in advance.
func OpenNetworkSettings(ctx context.Context, tconn *chrome.TestConn) error {
	ui := uiauto.New(tconn)

	networkDetailedView, err := NetworkDetailedView(ctx, tconn)
	if err != nil {
		return errors.Wrap(err, "failed to get network detailed view")
	}
	networkSettingsButton := nodewith.HasClass("IconButton").Name("Network settings").Ancestor(QsRootFinder)
	return uiauto.Combine("click the Network settings",
		ui.LeftClick(networkSettingsButton),
		ui.WaitUntilGone(networkDetailedView),
	)(ctx)
}

// SelectNetwork selects a network with specified SSID from QuickSettings network detailed view.
func SelectNetwork(ctx context.Context, tconn *chrome.TestConn, ssid string) error {
	ui := uiauto.New(tconn)

	if err := NavigateToNetworkDetailedView(ctx, tconn); err != nil {
		return err
	}
	defer Hide(ctx, tconn)

	networkDetailedView, err := NetworkDetailedView(ctx, tconn)
	if err != nil {
		return errors.Wrap(err, "failed to get network detailed view")
	}

	networkNameFinder := nodewith.NameContaining(ssid).Role(role.Button).Ancestor(networkDetailedView)
	// Click the node by DoDefault since a UI click here is unstable.
	// The target node could be offscreen, even if it is onscreen, its location could be keep changing as it'll
	// change whenever the available network list change, which makes the click action highly unstable.
	if err := ui.DoDefault(networkNameFinder)(ctx); err != nil {
		return errors.Wrap(err, "failed to click the network from Quick Settings network detailed view")
	}

	return nil
}

// NetworkDetailedView returns the detailed Network view within Quick Settings.
// TODO(b/290943118): Remove unused ctx and tconn arguments.
func NetworkDetailedView(ctx context.Context, tconn *chrome.TestConn) (*nodewith.Finder, error) {
	return nodewith.HasClass("NetworkDetailedNetworkViewImpl").Ancestor(QsRootFinder), nil
}

// NetworkListItemView returns the network item list on the network view in Quick Settings.
// TODO(b/290943118): Remove unused ctx and tconn arguments.
func NetworkListItemView(ctx context.Context, tconn *chrome.TestConn) (*nodewith.Finder, error) {
	return nodewith.HasClass("NetworkListNetworkItemView").Ancestor(QsRootFinder), nil
}

// NetworkDetailedViewWifiToggleButton returns the WiFi toggle within the Network detailed view.
func NetworkDetailedViewWifiToggleButton(ctx context.Context, tconn *chrome.TestConn) (*nodewith.Finder, error) {
	networkDetailedView, err := NetworkDetailedView(ctx, tconn)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get network detailed view")
	}
	return nodewith.HasClass("Switch").NameContaining("Wi-Fi").Ancestor(networkDetailedView), nil
}

// NetworkDetailedViewMobileDataToggle returns the switch to enable/disable Mobile data within network quick settings.
func NetworkDetailedViewMobileDataToggle(ctx context.Context, tconn *chrome.TestConn) (*nodewith.Finder, error) {
	networkDetailedView, err := NetworkDetailedView(ctx, tconn)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get network detailed view")
	}
	return nodewith.Role(role.Button).NameContaining("Toggle mobile data").Ancestor(networkDetailedView), nil
}
