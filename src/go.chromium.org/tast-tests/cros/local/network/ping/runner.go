// Copyright 2020 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package ping provides a factory to run ping on DUT.
package ping

import (
	"go.chromium.org/tast-tests/cros/common/network/ping"
	"go.chromium.org/tast-tests/cros/local/network/cmd"
)

// NewLocalRunner creates a ping Runner on the given dut for local execution.
func NewLocalRunner() *ping.Runner {
	return ping.NewRunner(&cmd.LocalCmdRunner{})
}

// NewLocalRunnerWithoutLogOnError creates a ping Runner on the given dut for
// local execution. Different from NewLocalRunner(), the runner created by this
// function will not dump log on error, which could be helpful when a ping
// failure is expected.
func NewLocalRunnerWithoutLogOnError() *ping.Runner {
	return ping.NewRunner(&cmd.LocalCmdRunner{NoLogOnError: true})
}
