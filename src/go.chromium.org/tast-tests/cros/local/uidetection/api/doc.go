// Copyright 2023 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package uidetection provides protobuf interfaces for uidetection's gRPC API.
package uidetection
