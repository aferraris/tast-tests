// Copyright 2023 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package usb contains tests which exercise USB related functionality not
// exclusive to the USB-C port or involving USB Power Delivery.
package usb
