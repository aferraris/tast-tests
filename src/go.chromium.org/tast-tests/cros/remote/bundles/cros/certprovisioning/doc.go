// Copyright 2024 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package certprovisioning contains tests for ChromeOS built-in client
// certificate provisioning.
package certprovisioning
