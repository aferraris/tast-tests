// Copyright 2024 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package networkui contains tests that are owned by the ChromeOS Connectivity team. These tests exercise general network functionality in the Software layer.
package networkui
