// Copyright 2024 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package statefulmigration contains the e2e tests to validate the thinpool migration
package statefulmigration
