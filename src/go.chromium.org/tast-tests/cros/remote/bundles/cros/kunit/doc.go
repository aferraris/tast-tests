// Copyright 2024 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package kunit contains Tast test wrapper that runs kunit tests.
package kunit
