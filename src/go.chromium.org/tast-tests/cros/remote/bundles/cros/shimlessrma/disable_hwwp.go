// Copyright 2022 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package shimlessrma contains integration tests for Shimless RMA SWA.
package shimlessrma

import (
	"context"
	"strconv"
	"time"

	"go.chromium.org/tast-tests/cros/common/action"
	"go.chromium.org/tast-tests/cros/common/servo"
	"go.chromium.org/tast-tests/cros/remote/bundles/cros/shimlessrma/rmaweb"
	"go.chromium.org/tast-tests/cros/remote/firmware/fixture"
	"go.chromium.org/tast/core/ctxutil"
	"go.chromium.org/tast/core/testing"
	"go.chromium.org/tast/core/testing/hwdep"
)

type param struct {
	wp          rmaweb.WriteProtectDisableOption
	enroll      bool
	destination rmaweb.DestinationOption
}

func init() {
	testing.AddTest(&testing.Test{
		Func:         DisableHWWP,
		LacrosStatus: testing.LacrosVariantUnneeded,
		Desc:         "Can complete Shimless RMA successfully. Disable HWWP with Battery Disconnection",
		Contacts: []string{
			"chromeos-shimless-eng@google.com",
			"chenghan@google.com",
		},
		BugComponent: "b:1002147",
		Attr:         []string{"group:shimless_rma"},
		VarDeps: []string{
			"ui.signinProfileTestExtensionManifestKey",
		},
		Vars:         []string{"firmware.skipFlashUSB"},
		SoftwareDeps: []string{"chrome"},
		HardwareDeps: hwdep.D(hwdep.ChromeEC()),
		ServiceDeps: []string{
			"tast.cros.browser.ChromeService",
			"tast.cros.shimlessrma.AppService",
			"tast.cros.graphics.ScreenshotService",
		},
		Fixture: fixture.DevModeGBB,
		Timeout: 150 * time.Minute,
		Params: []testing.Param{{
			ExtraAttr: []string{"shimless_rma_normal"},
			Name:      "unenroll_sameuser_manual",
			Val: param{
				wp:          rmaweb.Manual,
				enroll:      false,
				destination: rmaweb.SameUser,
			},
		}, {
			ExtraAttr: []string{"shimless_rma_nodelocked"},
			Name:      "unenroll_sameuser_rsu",
			Val: param{
				wp:          rmaweb.Rsu,
				enroll:      false,
				destination: rmaweb.SameUser,
			},
		}, {
			ExtraAttr: []string{"shimless_rma_nodelocked"},
			Name:      "unenroll_diffuser_rsu",
			Val: param{
				wp:          rmaweb.Rsu,
				enroll:      false,
				destination: rmaweb.DifferentUser,
			},
		}, {
			ExtraAttr: []string{"shimless_rma_nodelocked"},
			Name:      "enroll_diffuser_rsu",
			Val: param{
				wp:          rmaweb.Rsu,
				enroll:      true,
				destination: rmaweb.DifferentUser,
			},
		}},
	})
}

func DisableHWWP(ctx context.Context, s *testing.State) {
	cleanupCtx := ctx
	ctx, cancel := ctxutil.Shorten(ctx, 10*time.Second)
	defer cancel()

	firmwareHelper := s.FixtValue().(*fixture.Value).Helper
	dut := firmwareHelper.DUT
	key := s.RequiredVar("ui.signinProfileTestExtensionManifestKey")
	p := s.Param().(param)
	wpOption := p.wp
	enroll := p.enroll
	destination := p.destination

	defer rmaweb.CleanupShimlessFiles(cleanupCtx, dut)

	if err := firmwareHelper.RequireServo(ctx); err != nil {
		s.Fatal("Fail to init servo: ", err)
	}

	s.Log("Setup USB Key")
	skipFlashUSB := false
	if skipFlashUSBStr, ok := s.Var("firmware.skipFlashUSB"); ok {
		var err error
		skipFlashUSB, err = strconv.ParseBool(skipFlashUSBStr)
		if err != nil {
			s.Fatalf("Invalid value for var firmware.skipFlashUSB: got %q, want true/false", skipFlashUSBStr)
		}
	}
	s.Logf("skipFlashUSB is %t", skipFlashUSB)

	// USB install only occurs in Manual option.
	// So always skip it for RSU testing.
	if !skipFlashUSB && wpOption == rmaweb.Manual {
		s.Log("Flash USB starts")
		cs := s.CloudStorage()
		if err := firmwareHelper.SetupUSBKey(ctx, cs); err != nil {
			s.Fatal("USBKey not working: ", err)
		}
	}

	uiHelper, err := rmaweb.NewUIHelper(ctx, dut, firmwareHelper, s.RPCHint(), key, false)
	if err != nil {
		s.Fatal("Fail to initialize RMA Helper: ", err)
	}
	// Restart will dispose resources, so don't dispose resources explicitly.
	errorHandler := rmaweb.CreateErrorHandler(ctx, &uiHelper, s.TestName())
	s.AttachErrorHandlers(errorHandler, errorHandler)

	if err := uiHelper.SetupInitStatus(ctx, enroll); err != nil {
		s.Fatal("Fail to setup init status: ", err)
	}

	uiHelper, err = rmaweb.NewUIHelper(ctx, dut, firmwareHelper, s.RPCHint(), key, true)
	if err != nil {
		s.Fatal("Fail to initialize RMA Helper: ", err)
	}
	// Restart will dispose resources, so don't dispose resources explicitly.

	if actions := generateActionCombinedToDisableWP(wpOption, enroll, destination, uiHelper); actions == nil {
		// We don't support this test case yet.
		s.Fatalf("The test case is not support yet. Enroll: %t, WP: %s, destination: %s ", enroll, wpOption, destination)
	} else {
		if err := actions(ctx); err != nil {
			s.Fatal("Fail to navigate to Disable Write Protect page and turn off write protect: ", err)
		}
	}

	// GoBigSleepLint: Wait for reboot start.
	// TODO(chenghan): Replace testing.Sleep with testing.Poll.
	if err := testing.Sleep(ctx, rmaweb.WaitForRebootStart); err != nil {
		s.Error("Fail to sleep: ", err)
	}

	uiHelper, err = rmaweb.NewUIHelper(ctx, dut, firmwareHelper, s.RPCHint(), key, true)
	if err != nil {
		s.Fatal("Fail to initialize RMA Helper: ", err)
	}
	// Restart will dispose resources, so don't dispose resources explicitly.

	if err := uiHelper.WriteProtectDisabledPageOperation(ctx); err != nil {
		s.Fatal("Fail to navigate to WP disable Complete page: ", err)
	}

	// GoBigSleepLint: Wait for reboot start.
	if err := testing.Sleep(ctx, rmaweb.WaitForRebootStart); err != nil {
		s.Error("Fail to sleep: ", err)
	}

	uiHelper, err = rmaweb.NewUIHelper(ctx, dut, firmwareHelper, s.RPCHint(), key, true)
	if err != nil {
		s.Fatal("Fail to initialize RMA Helper: ", err)
	}

	if err := uiHelper.DeviceInformationPageOperation(ctx); err != nil {
		s.Fatal("Fail to complete Device Information page operations: ", err)
	}

	// Always bypass calibration in this test. We have another test just for calibration.
	if err := uiHelper.BypassCalibration(ctx); err != nil {
		s.Fatal("Fail to bypass calibration after firmware installation: ", err)
	}

	// faft-cr50-pool cannot update firmware from USB.
	// Since we already run Manual test case (removal battery) in skylab and install firmware from USB,
	// we skip firmware installation in all RSU test cases.
	if wpOption == rmaweb.Manual && !skipFlashUSB {
		if err := uiHelper.WaitForFirmwareInstallation(ctx); err != nil {
			s.Fatal("Fail to navigate to firmware installation page and install firmware: ", err)
		}
	} else {
		if err := uiHelper.BypassFirmwareInstallation(ctx); err != nil {
			s.Fatal("Fail to navigate to firmware installation page and bypass firmware install: ", err)
		}
	}

	// GoBigSleepLint: Wait for reboot start.
	if err := testing.Sleep(ctx, rmaweb.WaitForRebootStart); err != nil {
		s.Error("Fail to sleep: ", err)
	}

	if err := firmwareHelper.Servo.RunCR50Command(ctx, "bpforce follow_batt_pres atboot"); err != nil {
		s.Fatal("Fail to connect battery: ", err)
	}

	if err := firmwareHelper.Servo.SetFWWPState(ctx, servo.FWWPStateOn); err != nil {
		s.Fatal("Fail to enable HWWP: ", err)
	}

	uiHelper, err = rmaweb.NewUIHelper(ctx, dut, firmwareHelper, s.RPCHint(), key, true)
	if err != nil {
		s.Fatal("Fail to initialize RMA Helper: ", err)
	}
	// Restart will dispose resources, so don't dispose resources explicitly.

	// Given the flakiness of USB on the lab devices, the USB may malfunction at the first place.
	// We should let the test continue if the USB is already unseen to the DUT.
	if err := uiHelper.FirmwareHelper.Servo.SetUSBMuxState(ctx, servo.USBMuxHost); err != nil {
		s.Log("Fail to set USB Mux state: ", err)
	}

	// GoBigSleepLint: Wait for provisioning and finalizing.
	if err := testing.Sleep(ctx, rmaweb.WaitForProvisionAndFinalize); err != nil {
		s.Error("Fail to sleep: ", err)
	}

	uiHelper, err = rmaweb.NewUIHelper(ctx, dut, firmwareHelper, s.RPCHint(), key, true)
	if err != nil {
		s.Fatal("Fail to initialize RMA Helper: ", err)
	}

	defer uiHelper.DisposeResource(cleanupCtx)

	storeLogFlag := rmaweb.NotStoreLog
	if wpOption == rmaweb.Manual && !skipFlashUSB {
		storeLogFlag = rmaweb.StoreLog
	}

	if err := uiHelper.RepairCompletedPageOperation(ctx, storeLogFlag); err != nil {
		s.Fatal("Fail to navigate to Repair Complete page: ", err)
	}

	if storeLogFlag == rmaweb.StoreLog {
		if err := uiHelper.VerifyLogIsSaved(ctx); err != nil {
			s.Fatal("Fail to verify that the log is saved in usb: ", err)
		}
	}
}

func generateActionCombinedToDisableWP(option rmaweb.WriteProtectDisableOption, enroll bool, destination rmaweb.DestinationOption, uiHelper *rmaweb.UIHelper) action.Action {

	if enroll && destination == rmaweb.DifferentUser && option == rmaweb.Rsu {
		return action.Combine("navigate to RSU page and turn off write protect",
			uiHelper.WelcomePageOperation,
			uiHelper.ComponentsPageOperation,
			uiHelper.OwnerPageOperation(destination),
			uiHelper.RSUPageOperation,
		)
	} else if !enroll && destination == rmaweb.DifferentUser && option == rmaweb.Rsu {
		return action.Combine("navigate to RSU page, choose different user and turn off write protect",
			uiHelper.WelcomePageOperation,
			uiHelper.ComponentsPageOperation,
			uiHelper.OwnerPageOperation(destination),
			uiHelper.WriteProtectPageChooseRSU,
			uiHelper.RSUPageOperation,
		)
	} else if !enroll && destination == rmaweb.SameUser && option == rmaweb.Rsu {
		return action.Combine("navigate to RSU page , choose same user and turn off write protect",
			uiHelper.WelcomePageOperation,
			uiHelper.ComponentsPageOperation,
			uiHelper.OwnerPageOperation(destination),
			uiHelper.WriteProtectPageChooseRSU,
			uiHelper.RSUPageOperation,
		)
	} else if !enroll && destination == rmaweb.SameUser && option == rmaweb.Manual {
		return action.Combine("navigate to Manual Disable Write Protect page, choose same user and turn off write protect",
			uiHelper.WelcomePageOperation,
			uiHelper.ComponentsPageOperation,
			uiHelper.OwnerPageOperation(destination),
			uiHelper.WriteProtectPageChooseManual,
		)
	}

	return nil
}
