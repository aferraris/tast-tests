// Copyright 2023 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package storage

import (
	"context"
	"time"

	"go.chromium.org/tast-tests/cros/common/bounds"
	tdreq "go.chromium.org/tast-tests/cros/common/testdevicerequirements"
	"go.chromium.org/tast-tests/cros/remote/bundles/cros/storage/util"
	"go.chromium.org/tast-tests/cros/remote/firmware/fixture"
	"go.chromium.org/tast/core/testing"
	"go.chromium.org/tast/core/testing/hwdep"
)

func init() {
	testing.AddTest(&testing.Test{
		Func: SocPerformance,
		Desc: "Measure performance of SoC storage link",
		Contacts: []string{
			"chromeos-storage@google.com",
			"dlunev@google.com", // Test author
		},
		BugComponent: "b:974567", // ChromeOS > Platform > System > Storage
		LacrosStatus: testing.LacrosVariantUnneeded,
		Data:         util.Configs,
		SoftwareDeps: []string{"crossystem"},
		Fixture:      fixture.USBDevModeWithReinstall,
		Attr:         []string{"group:storage-qual", "storage-qual_pdp_kpi", "storage-qual_pdp_stress"},
		Timeout:      10 * time.Minute,
		Params: []testing.Param{{
			Name: "nvme_link_bw",
			Val: []bounds.MetricBounds{{
				Metric: bounds.MatchRegexp(`_seq_read_read_bw`),
				Bounds: bounds.Min(2_048_000), // KiB/sec, 2000 MiB/sec
			}},
			ExtraHardwareDeps: hwdep.D(hwdep.Nvme()),
			ExtraRequirements: []string{tdreq.NvmePcieBW},
		}, {
			Name: "emmc_link_bw",
			Val: []bounds.MetricBounds{{
				Metric: bounds.MatchRegexp(`_seq_read_read_bw`),
				Bounds: bounds.Max(256_000), // KiB/sec, 250 MiB/sec
			}},
			ExtraHardwareDeps: hwdep.D(hwdep.EmmcOrBridge()),
			ExtraRequirements: []string{tdreq.EmmcControllerBW},
		}, {
			Name: "ufs_g3_link_bw",
			Val: []bounds.MetricBounds{{
				Metric: bounds.MatchRegexp(`_seq_read_read_bw`),
				Bounds: bounds.Min(512_000), // KiB/sec, 500 MiB/sec
			}},
			ExtraHardwareDeps: hwdep.D(hwdep.Ufs()),
			ExtraRequirements: []string{tdreq.UfsControllerG3BW},
		}, {
			Name: "ufs_g4_link_bw",
			Val: []bounds.MetricBounds{{
				Metric: bounds.MatchRegexp(`_seq_read_read_bw`),
				Bounds: bounds.Min(1_024_000), // KiB/sec, 1000 MiB/sec
			}},
			ExtraHardwareDeps: hwdep.D(hwdep.Ufs()),
			ExtraRequirements: []string{tdreq.UfsControllerG4BW},
		}},
	})
}

func SocPerformance(ctx context.Context, s *testing.State) {
	defer util.FatalIfBoundsCheckFail(ctx, s.Param().([]bounds.MetricBounds), s)

	bootIDChecker := util.NewBootIDChecker(ctx, s.DUT(), s)
	defer util.FatalIfBootIDChanged(ctx, bootIDChecker, s)

	resultWriter := &util.FioResultWriter{}
	defer resultWriter.Save(ctx, s.OutDir(), true)

	disk, err := util.GetInternalStorage(ctx, s.DUT())
	if err != nil {
		s.Fatal("Failed to get internal disk: ", err)
	}

	err = util.WriteAVLInfo(ctx, disk, s.OutDir())
	if err != nil {
		s.Fatal("Failed to write AVL info: ", err)
	}

	err = util.TestConfig{}.
		WithResultWriter(resultWriter).
		WithDisk(disk).
		WithRunTimeSec(300).
		WithJobFromFile(s.DataPath("seq_read")).
		Run(ctx, s.DUT())

	if err != nil {
		s.Fatal("Failed to run fio: ", err)
	}
}
