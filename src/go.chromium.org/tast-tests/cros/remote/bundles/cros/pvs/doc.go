// Copyright 2023 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package pvs contains the e2e tests to validate the Platform Validation Suite CLI
package pvs
