// Copyright 2022 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package firmware

import (
	"context"
	"path/filepath"
	"regexp"
	"time"

	fwCommon "go.chromium.org/tast-tests/cros/common/firmware"
	"go.chromium.org/tast-tests/cros/common/flashrom"
	"go.chromium.org/tast-tests/cros/remote/firmware"
	"go.chromium.org/tast-tests/cros/remote/firmware/fixture"
	fwpb "go.chromium.org/tast-tests/cros/services/cros/firmware"
	"go.chromium.org/tast/core/dut"
	"go.chromium.org/tast/core/errors"
	"go.chromium.org/tast/core/ssh"
	"go.chromium.org/tast/core/ssh/linuxssh"
	"go.chromium.org/tast/core/testing"
	"go.chromium.org/tast/core/testing/hwdep"
)

// Used to match the RO/RW versions reported by EC Tool, as declared here:
// https://chromium.googlesource.com/chromiumos/platform/ec/+/main/util/ectool.c
var (
	reROVersion = regexp.MustCompile(`RO version:\s*(\S+)\s`)
	reRWVersion = regexp.MustCompile(`RW version:\s*(\S+)\s`)
)

var (
	ecPathVar = testing.RegisterVarString(
		"firmware.ECPath",
		"$HOME/chromiumos/src/platform/ec/ec.bin",
		"It takes EC path as input for e.g. /path/to/ec.bin")
)

func init() {
	testing.AddTest(&testing.Test{
		Func: DisableEnableECSWSync,
		Desc: "Flash EC using flashrom and enable disable EC SW sync",
		Contacts: []string{
			"chromeos-faft@google.com",
			"intel.chrome.automation.team@intel.com",
			"ambalavanan.m.m@intel.com",
		},
		BugComponent: "b:792402", // ChromeOS > Platform > Enablement > Firmware > FAFT
		SoftwareDeps: []string{"flashrom"},
		HardwareDeps: hwdep.D(hwdep.ChromeEC()),
		ServiceDeps:  []string{"tast.cros.firmware.BiosService"},
		Attr:         []string{"group:intel-flashing"},
		Fixture:      fixture.NormalMode,
		Timeout:      10 * time.Minute,
		LacrosStatus: testing.LacrosVariantUnneeded,
	})
}

// DisableEnableECSWSync flashes EC using flashrom and enable disable EC SW sync.
func DisableEnableECSWSync(ctx context.Context, s *testing.State) {
	h := s.FixtValue().(*fixture.Value).Helper

	pathToEC := ecPathVar.Value()

	d := s.DUT()

	if err := h.RequireServo(ctx); err != nil {
		s.Fatal("Failed to init servo: ", err)
	}

	if err := h.RequireBiosServiceClient(ctx); err != nil {
		s.Fatal("Requiring BiosServiceClient: ", err)
	}

	s.Log("Backing up current EC_RW region for safety")
	ecPath, err := h.BiosServiceClient.BackupImageSection(ctx, &fwpb.FWSectionInfo{
		Programmer: fwpb.Programmer_ECProgrammer,
		Section:    fwpb.ImageSection_ECROImageSection,
	})
	if err != nil {
		s.Fatal("Failed to backup current EC_RW region: ", err)
	}
	s.Log("EC_RW region backup is stored at: ", ecPath.Path)

	ecPathLocal := filepath.Join("/usr/local", filepath.Base(pathToEC))
	s.Log("ecPathLocal: ", ecPathLocal)

	defer func(ctx context.Context) {
		h.CloseRPCConnection(ctx)

		s.Log("Restoring EC image")
		if err := d.Reboot(ctx); err != nil {
			s.Fatal("Failed to ensure the DUT is booted: ", err)
		}

		s.Log("Reconnecting to RPC services on DUT")
		if err := h.RequireRPCClient(ctx); err != nil {
			s.Fatal("Failed to reconnect to the RPC service on DUT: ", err)
		}

		s.Log("Reconnecting to BiosService on DUT")
		if err := h.RequireBiosServiceClient(ctx); err != nil {
			s.Fatal("Failed to reconnect to BiosServiceClient on DUT: ", err)
		}

		if _, err := h.BiosServiceClient.RestoreImageSection(ctx, ecPath); err != nil {
			s.Error("Failed to restore EC image: ", err)
		}
		s.Log("Removing EC image backup from DUT")
		if _, err := h.DUT.Conn().CommandContext(ctx, "rm", ecPath.Path).Output(ssh.DumpLogOnError); err != nil {
			s.Fatal("Failed to delete EC image from DUT: ", err)
		}

		if err := d.Reboot(ctx); err != nil {
			s.Fatal("Failed to ensure the DUT is booted")
		}
	}(ctx)

	ecKeyPath := map[string]string{pathToEC: ecPathLocal}
	if _, err := linuxssh.PutFiles(ctx, h.DUT.Conn(), ecKeyPath, linuxssh.DereferenceSymlinks); err != nil {
		s.Fatalf("Failed to send data to remote data path %v: %v", ecPathLocal, err)
	}

	ro1, rw1, err := checkECVersion(ctx, h.DUT)
	if err != nil {
		s.Fatal("Failed to read ectool version: ", err)
	}
	if ro1 != rw1 {
		s.Fatalf("Failed to verify EC version, versions should be the same, got: RO=%s, RW=%s, want: RO == RW", ro1, rw1)
	}

	s.Log("Setting GBB flag DISABLE_EC_SOFTWARE_SYNC")
	set := fwpb.GBBFlagsState{Set: []fwpb.GBBFlag{fwpb.GBBFlag_DISABLE_EC_SOFTWARE_SYNC}}
	if _, err := fwCommon.ClearAndSetGBBFlags(ctx, s.DUT(), &set); err != nil {
		s.Fatal("Failed to set GBB flag: ", err)
	}

	s.Log("Rebooting DUT")
	if err := h.DUT.Reboot(ctx); err != nil {
		s.Fatal("Failed to reboot DUT: ", err)
	}

	ro2, rw2, err := checkECVersion(ctx, h.DUT)
	if err != nil {
		s.Fatal("Failed to read ectool version: ", err)
	}
	if ro1 != ro2 || rw1 != rw2 {
		s.Fatalf("Failed to verify EC versions, versions should be the same, got %s=%s (RO), %s=%s (RW)", ro1, ro2, rw1, rw2)
	}

	if err := flashEC(ctx, h.DUT, ecPathLocal); err != nil {
		s.Fatal("Failed to flash EC: ", err)
	}

	s.Log("Cold Reboot DUT")
	ms, err := firmware.NewModeSwitcher(ctx, h)
	if err != nil {
		s.Fatal("Failed to create mode switcher: ", err)
	}
	if err := ms.ModeAwareReboot(ctx, firmware.ColdReset); err != nil {
		s.Fatal("Failed to perform mode aware reboot: ", err)
	}

	ro3, rw3, err := checkECVersion(ctx, h.DUT)
	if err != nil {
		s.Fatal("Failed to read ectool version: ", err)
	}
	if ro3 == ro2 || rw3 == rw2 {
		s.Fatalf("Failed to verify EC version, EC should change as per flashed EC binary got (%s, %s) RO (%s, %s) RW", ro2, ro3, rw2, rw3)
	}

	s.Log("Clearing GBB flag DISABLE_EC_SOFTWARE_SYNC")
	clear := fwpb.GBBFlagsState{Clear: []fwpb.GBBFlag{fwpb.GBBFlag_DISABLE_EC_SOFTWARE_SYNC}}
	if _, err := fwCommon.ClearAndSetGBBFlags(ctx, s.DUT(), &clear); err != nil {
		s.Fatal("Failed to clear GBB flag: ", err)
	}

	s.Log("Rebooting DUT")
	if err := h.DUT.Reboot(ctx); err != nil {
		s.Fatal("Failed to reboot DUT: ", err)
	}

	ro4, rw4, err := checkECVersion(ctx, h.DUT)
	if err != nil {
		s.Fatal("Failed to read ectool version: ", err)
	}
	if ro4 == rw4 && ro4 != ro3 {
		s.Fatalf("Failed to verify EC version, RW and RO regions of EC should not be the same version got %s=%s (RO, RW), RO version of EC should be version of EC flashed got %s=%s (RO, RO)", ro4, rw4, ro3, ro4)
	}
}

// checkECVersion returns ro/rw version from ectool.
func checkECVersion(ctx context.Context, dut *dut.DUT) (string, string, error) {
	ec := firmware.NewECTool(dut, firmware.ECToolNameMain)
	output, err := ec.Command(ctx, "version").Output(ssh.DumpLogOnError)
	if err != nil {
		return "", "", errors.Wrap(err, "failed to run 'ectool version' on DUT")
	}
	roVersion := reROVersion.FindSubmatch(output)
	if len(roVersion) == 0 {
		return "", "", errors.Errorf("failed to match regexp %s in ectool version output: %s", reROVersion, output)
	}
	rwVersion := reRWVersion.FindSubmatch(output)
	if len(rwVersion) == 0 {
		return "", "", errors.Errorf("failed to match regexp %s in ectool version output: %s", reRWVersion, output)
	}
	return string(roVersion[1]), string(rwVersion[1]), nil
}

// flashEC flashes DUT using flashrom.
func flashEC(ctx context.Context, dut *dut.DUT, imagePath string) (retErr error) {
	testing.ContextLogf(ctx, "Writing image from file %s", imagePath)

	var flashromConfig flashrom.Config
	flashromInstance, ctx, shutdown, _, err := flashromConfig.
		FlashromInit(flashrom.VerbosityInfo).
		ProgrammerInit(flashrom.ProgrammerEc, "").
		SetDut(dut).
		Probe(ctx)
	defer func() {
		if err := shutdown(); err != nil {
			if retErr == nil {
				retErr = errors.Wrap(err, "failed to shutdown flashromInstance")
			} else {
				testing.ContextLog(ctx, "Failed to shutdown flashromInstance: ", err)
			}
		}
	}()
	if err != nil {
		errors.Wrap(err, "flashrom probe failed, unable to build flashrom instance")
	}

	if out, err := flashromInstance.Write(ctx, imagePath, false, false, "", []string{}); err != nil {
		return errors.Wrapf(err, "failed to run flashrom cmd: %s", string(out))
	}

	return nil
}
