// Copyright 2022 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package firmware

import (
	"context"
	"time"

	"go.chromium.org/tast-tests/cros/common/firmware/bios"
	"go.chromium.org/tast-tests/cros/remote/firmware/fixture"

	"go.chromium.org/tast/core/testing"
)

func init() {
	// TODO(b/319107854): Remove this test and use firmware.CorruptFWBothAB.body_normal
	testing.AddTest(&testing.Test{
		Func: CorruptBothFWBodyAB,
		Desc: "Corrupt both copies of AP firmware, verify broken screen with reason 0x1b, restore backup via servo",
		Contacts: []string{
			"chromeos-faft@google.com",
			"jbettis@chromium.org",
		},
		BugComponent: "b:792402", // ChromeOS > Platform > Enablement > Firmware > FAFT
		Timeout:      25 * time.Minute,
		SoftwareDeps: []string{"crossystem", "flashrom"},
		LacrosStatus: testing.LacrosVariantUnneeded,
		Params: []testing.Param{
			{
				Name:    "normal_mode",
				Fixture: fixture.BootModeFixtureWithAPBackup(fixture.NormalMode),
				Val: &corruptTestVal{
					bios.FWBodyAImageSection, bios.FWBodyBImageSection,
				},
			},
		},
	})
}

func CorruptBothFWBodyAB(ctx context.Context, s *testing.State) {
	corruptFWSectionTest(ctx, s, CorruptFWBodySection, "RW firmware unable to verify firmware body")
}
