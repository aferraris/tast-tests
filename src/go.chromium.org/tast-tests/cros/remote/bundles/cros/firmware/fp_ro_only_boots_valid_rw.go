// Copyright 2021 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package firmware

import (
	"context"
	"time"

	"go.chromium.org/tast-tests/cros/common/tbdep"
	"go.chromium.org/tast-tests/cros/remote/dutfs"
	"go.chromium.org/tast-tests/cros/remote/firmware/fingerprint"
	"go.chromium.org/tast-tests/cros/remote/firmware/fingerprint/fixture"
	"go.chromium.org/tast-tests/cros/remote/firmware/fingerprint/rpcdut"
	"go.chromium.org/tast/core/ctxutil"
	"go.chromium.org/tast/core/errors"
	"go.chromium.org/tast/core/testing"
	"go.chromium.org/tast/core/testing/hwdep"
)

func init() {
	testing.AddTest(&testing.Test{
		Func: FpROOnlyBootsValidRW,
		Desc: "Verify the RO fingerprint firmware only boots valid RW firmware",
		Contacts: []string{
			"chromeos-fingerprint@google.com",
			"tomhughes@chromium.org", // Test author
		},
		// ChromeOS > Platform > Services > Fingerprint
		BugComponent: "b:782045",
		Attr:         []string{"group:fingerprint-cq", "group:fingerprint-release"},
		Timeout:      9 * time.Minute,
		SoftwareDeps: []string{"biometrics_daemon"},
		HardwareDeps: hwdep.D(hwdep.Fingerprint()),
		ServiceDeps:  []string{"tast.cros.platform.UpstartService", dutfs.ServiceName},
		TestBedDeps:  []string{tbdep.Fingerprint, tbdep.ServoStateWorking},
		Vars:         []string{"servo"},
		Fixture:      fixture.FingerprintImages,
		LacrosStatus: testing.LacrosVariantUnneeded,
	})
}

type testParams struct {
	firmwarePath                string
	expectedROVersion           string
	expectedRWVersion           string
	expectedRunningFirmwareCopy fingerprint.FWImageType
}

func testFlashingFirmwareVersion(ctx context.Context, d *rpcdut.RPCDUT, params *testParams) error {
	testing.ContextLog(ctx, "Saving current rollback information")
	rollbackInfo, err := fingerprint.RollbackInfo(ctx, d.DUT())
	if err != nil {
		return errors.Wrap(err, "failed to read rollback information")
	}

	testing.ContextLog(ctx, "Flashing firmware: ", params.firmwarePath)
	if err := fingerprint.FlashFirmwareUpdate(ctx, d, fingerprint.ImageTypeRW, params.firmwarePath); err != nil {
		return errors.Wrapf(err, "failed to flash firmware: %q", params.firmwarePath)
	}

	testing.ContextLog(ctx, "Checking for versions: RO: ", params.expectedROVersion, ", RW: ", params.expectedRWVersion)
	if err := fingerprint.CheckRunningFirmwareVersionMatches(ctx, d, params.expectedROVersion, params.expectedRWVersion); err != nil {
		return errors.Wrap(err, "unexpected firmware version")
	}

	testing.ContextLog(ctx, "Checking that ", params.expectedRunningFirmwareCopy, " firmware is running")
	if err := fingerprint.CheckRunningFirmwareCopy(ctx, d.DUT(), params.expectedRunningFirmwareCopy); err != nil {
		return errors.Wrap(err, "not running RO firmware")
	}

	testing.ContextLog(ctx, "Checking that rollback remains unchanged")
	if err := fingerprint.CheckRollbackState(ctx, d, rollbackInfo); err != nil {
		return errors.Wrap(err, "rollback information changed during test")
	}

	return nil
}

func FpROOnlyBootsValidRW(ctx context.Context, s *testing.State) {
	d, err := rpcdut.NewRPCDUT(ctx, s.DUT(), s.RPCHint())
	if err != nil {
		s.Fatal("Failed to connect RPCDUT: ", err)
	}
	defer d.Close(ctx)

	servoSpec, ok := s.Var("servo")
	if !ok {
		servoSpec = ""
	}
	firmwareFile, err := fingerprint.NewMPFirmwareFile(ctx, d)
	if err != nil {
		s.Fatal("Failed to create MP firmwareFile: ", err)
	}
	t, err := fingerprint.NewFirmwareTest(ctx, d, servoSpec, s.OutDir(), firmwareFile, true, true)
	if err != nil {
		s.Fatal("Failed to create new firmware test: ", err)
	}
	ctxForCleanup := ctx
	defer func() {
		if err := t.Close(ctxForCleanup); err != nil {
			s.Fatal("Failed to clean up: ", err)
		}
	}()
	ctx, cancel := ctxutil.Shorten(ctx, t.CleanupTime())
	defer cancel()

	testImages := s.FixtValue().(*fixture.ImagesTestData).TestImages

	if err := fingerprint.CheckRunningFirmwareCopy(ctx, d.DUT(), fingerprint.ImageTypeRW); err != nil {
		s.Fatal("Test expects RW firmware copy to be running")
	}

	// Hardware write protect must be enabled for the test to work correctly.
	if err := fingerprint.CheckWriteProtectStateCorrect(ctx, d.DUT(), true, true); err != nil {
		s.Fatal("Failed to validate write protect settings: ", err)
	}

	// Starts with MP-signed firmware. Then successively tries to flash three versions
	// to RW: dev, corrupted first byte, and corrupted last byte. Each of these should
	// flash successfully, but fail to boot (i.e., stay in RO mode). Finally,
	// flash an MP-signed version, which should successfully boot to RW.

	if err := testFlashingFirmwareVersion(ctx, d,
		&testParams{
			firmwarePath: testImages[fingerprint.TestImageTypeDev].Path,
			// RO version should remain unchanged.
			expectedROVersion: testImages[fingerprint.TestImageTypeOriginal].ROVersion,
			// RW version should match what we requested to be flashed.
			expectedRWVersion: testImages[fingerprint.TestImageTypeDev].RWVersion,
			// Signature check will fail, so we should be running RO.
			expectedRunningFirmwareCopy: fingerprint.ImageTypeRO,
		}); err != nil {
		s.Fatal("Dev key signed test failed: ", err)
	}

	// Note that the corrupted version has the same version string as the original version.
	if err := testFlashingFirmwareVersion(ctx, d,
		&testParams{
			firmwarePath: testImages[fingerprint.TestImageTypeCorruptFirstByte].Path,
			// RO version should remain unchanged.
			expectedROVersion: testImages[fingerprint.TestImageTypeOriginal].ROVersion,
			// RW version should match what we requested to be flashed.
			expectedRWVersion: testImages[fingerprint.TestImageTypeCorruptFirstByte].RWVersion,
			// Signature check will fail, so we should be running RO.
			expectedRunningFirmwareCopy: fingerprint.ImageTypeRO,
		}); err != nil {
		s.Fatal("Corrupt first byte test failed: ", err)
	}

	if err := testFlashingFirmwareVersion(ctx, d,
		&testParams{
			firmwarePath: testImages[fingerprint.TestImageTypeCorruptLastByte].Path,
			// RO version should remain unchanged.
			expectedROVersion: testImages[fingerprint.TestImageTypeOriginal].ROVersion,
			// RW version should match what we requested to be flashed.
			expectedRWVersion: testImages[fingerprint.TestImageTypeCorruptLastByte].RWVersion,
			// Signature check will fail, so we should be running RO.
			expectedRunningFirmwareCopy: fingerprint.ImageTypeRO,
		}); err != nil {
		s.Fatal("Corrupt last byte test failed: ", err)
	}

	if err := testFlashingFirmwareVersion(ctx, d,
		&testParams{
			firmwarePath: testImages[fingerprint.TestImageTypeOriginal].Path,
			// RO version should remain unchanged.
			expectedROVersion: testImages[fingerprint.TestImageTypeOriginal].ROVersion,
			// RW version should match what we requested to be flashed.
			expectedRWVersion: testImages[fingerprint.TestImageTypeOriginal].RWVersion,
			// Signature check will succeed, so we should be running RW.
			expectedRunningFirmwareCopy: fingerprint.ImageTypeRW,
		}); err != nil {
		s.Fatal("Good firmware test failed: ", err)
	}
}
