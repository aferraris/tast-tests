// Copyright 2023 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package firmware

import (
	"context"
	"time"

	"go.chromium.org/tast-tests/cros/remote/firmware/fixture"
	"go.chromium.org/tast/core/testing"
)

func init() {
	testing.AddTest(&testing.Test{
		Func:    ProvisionUSBKey,
		Desc:    "Write OS image to USB-key",
		Timeout: time.Hour,
		Contacts: []string{
			"peep-fleet-infra-sw@google.com",
		},
		BugComponent: "b:1296600", // Chrome Operations > Fleet > ChromeOS Fleet Reliability
		Fixture:      fixture.NormalMode,
		VarDeps:      []string{"servo"},
		LacrosStatus: testing.LacrosVariantUnneeded,
	})
}

// ProvisionUSBKey write image to USB-key.
func ProvisionUSBKey(ctx context.Context, s *testing.State) {
	h := s.FixtValue().(*fixture.Value).Helper

	if err := h.RequireServo(ctx); err != nil {
		s.Fatal("Failed to connect to servo: ", err)
	}

	cs := s.CloudStorage()
	if err := h.SetupUSBKey(ctx, cs); err != nil {
		s.Fatal("USBKey not working: ", err)
	}
}
