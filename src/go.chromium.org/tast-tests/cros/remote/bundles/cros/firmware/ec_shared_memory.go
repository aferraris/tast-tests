// Copyright 2022 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package firmware

import (
	"context"
	"strconv"
	"strings"
	"time"

	"go.chromium.org/tast-tests/cros/remote/firmware"
	"go.chromium.org/tast-tests/cros/remote/firmware/fixture"
	"go.chromium.org/tast/core/errors"
	"go.chromium.org/tast/core/testing"
	"go.chromium.org/tast/core/testing/hwdep"
)

func init() {
	testing.AddTest(&testing.Test{
		Func: ECSharedMemory,
		Desc: "Checks that there is still EC shared memory available",
		Contacts: []string{
			"chromeos-faft@google.com",
			"pf@semihalf.com",
		},
		BugComponent: "b:792402", // ChromeOS > Platform > Enablement > Firmware > FAFT
		Requirements: []string{"sys-fw-0022-v02"},
		Attr:         []string{"group:firmware", "firmware_ec", "firmware_bringup"},
		HardwareDeps: hwdep.D(hwdep.ChromeEC()),
		Fixture:      fixture.NormalMode,
		LacrosStatus: testing.LacrosVariantUnneeded,
	})
}

func ECSharedMemory(ctx context.Context, s *testing.State) {
	h := s.FixtValue().(*fixture.Value).Helper
	if err := h.RequireServo(ctx); err != nil {
		s.Fatal("Failed to init servo: ", err)
	}

	if err := h.Servo.RunECCommand(ctx, "chan save"); err != nil {
		s.Fatal("Failed to reset channel: ", err)
	}

	if err := h.Servo.RunECCommand(ctx, "chan 0"); err != nil {
		s.Fatal("Failed to reset channel: ", err)
	}

	defer func() {
		if err := h.Servo.RunECCommand(ctx, "chan restore"); err != nil {
			s.Fatal("Failed to reset channel: ", err)
		}
	}()

	s.Log("Check shared memory in normal operation")
	if err := checkSharedMemory(ctx, h); err != nil {
		s.Fatal("Failed to check shared memory: ", err)
	}

	// The crash type is arbitrary
	s.Log("Crash EC divzero")
	if err := h.Servo.RunECCommand(ctx, "crash divzero"); err != nil {
		s.Fatal("Failed to send 'crash divzero' to EC: ", err)
	}

	/**
	 * GoBigSleepLint: Wait for the EC to successfully crash before making the first attempt
	 * to connect.
	 *
	 * Attempting to connect to the EC without waiting can result in the first connection
	 * attempt occurring while the EC is still crashing/not ready - putting WaitConnect
	 * in a state where it it never reestablishes a connection to the EC
	 */
	testing.Sleep(ctx, 5*time.Second)

	if err := h.DUT.WaitConnect(ctx); err != nil {
		s.Fatal("Failed connect to DUT: ", err)
	}

	s.Log("Check shared memory after crash")
	if err := checkSharedMemory(ctx, h); err != nil {
		s.Fatal("Failed to check shared memory after crash: ", err)
	}

	activeCopy, err := h.Servo.GetString(ctx, "ec_active_copy")
	if err != nil {
		s.Fatal("EC active copy failed: ", err)
	}
	if !strings.HasPrefix(activeCopy, "RW") {
		s.Logf("EC active copy got %q want RW, perform sysjump", activeCopy)
		if err := h.Servo.RunECCommand(ctx, "sysjump RW"); err != nil {
			s.Fatal("Failed to send 'sysjump RW' to EC: ", err)
		}
		activeCopy, err = h.Servo.GetString(ctx, "ec_active_copy")
		if err != nil {
			s.Fatal("EC active copy failed: ", err)
		}
		if !strings.HasPrefix(activeCopy, "RW") {
			s.Fatalf("Expected EC to be in RW, but was %q", activeCopy)
		}
	}

	s.Log("Check shared memory after sysjump")
	if err := checkSharedMemory(ctx, h); err != nil {
		s.Fatal("Failed to check shared memory after sysjump: ", err)
	}

	s.Log("Rebooting EC to restore default state")
	if err := h.Servo.RunECCommand(ctx, "reboot"); err != nil {
		s.Fatal("Failed to reboot EC: ", err)
	}
}

func checkSharedMemory(ctx context.Context, h *firmware.Helper) error {
	const (
		// EC Shared Memory level which lead to warning
		warningLevel = 256
		// EC Shared Memory level which lead to error
		errorLevel = 0
	)

	var ecShmemStr string
	// After crash unaligned cmd, sometimes ec console needs some time to respond.
	testing.ContextLog(ctx, "Poll for ec shmem size")
	if err := testing.Poll(ctx, func(ctx context.Context) error {
		ecShmemOut, err := h.Servo.RunECCommandGetOutput(ctx, "shmem", []string{`Size:\s*(\d+)`})
		if err != nil {
			return errors.Wrap(err, "failed to read EC shared memory size")
		}
		ecShmemStr = ecShmemOut[0][1]
		return nil
	}, &testing.PollOptions{Timeout: 2 * time.Second, Interval: 500 * time.Millisecond}); err != nil {
		return errors.Wrap(err, "not all usb ports disabled")
	}

	testing.ContextLogf(ctx, "EC shared memory size is %s bytes", ecShmemStr)
	ecShmem, err := strconv.ParseInt(ecShmemStr, 10, 64)
	if err != nil {
		return errors.Wrapf(err, "failed to parse EC shared memory (%s) as int",
			ecShmemStr)
	}

	if ecShmem <= errorLevel {
		return errors.New("EC shared memory size is too small")
	} else if ecShmem <= warningLevel {
		testing.ContextLogf(ctx, "EC shared memory is less than %d bytes", warningLevel)
	}

	return nil
}
