// Copyright 2022 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package wifi

import (
	"context"
	"strconv"

	"go.chromium.org/tast-tests/cros/common/perf"
	"go.chromium.org/tast-tests/cros/common/tbdep"
	tdreq "go.chromium.org/tast-tests/cros/common/testdevicerequirements"
	"go.chromium.org/tast-tests/cros/common/wifi/p2p"
	"go.chromium.org/tast-tests/cros/remote/network/iperf"
	"go.chromium.org/tast-tests/cros/remote/wifi/iw"
	"go.chromium.org/tast-tests/cros/remote/wificell"

	"go.chromium.org/tast/core/testing"
	"go.chromium.org/tast/core/testing/hwdep"
)

func init() {
	testing.AddTest(&testing.Test{
		Func: P2PPerf,
		Desc: "Tests P2P performance between two chromebooks",
		Contacts: []string{
			"chromeos-wifi-champs@google.com", // WiFi oncall rotation
		},
		BugComponent: "b:893827", // ChromeOS > Platform > Connectivity > WiFi
		Attr:         []string{"group:wificell_cross_device", "wificell_cross_device_p2p", "wificell_cross_device_unstable"},
		TestBedDeps:  []string{tbdep.Wificell, tbdep.PeripheralWifiStateWorking},
		ServiceDeps:  []string{wificell.ShillServiceName},
		Fixture:      wificell.FixtureID(wificell.TFFeaturesCompanionDUT | wificell.TFFeaturesSelfManagedAP),
		HardwareDepsForAll: map[string]hwdep.Deps{
			"":    hwdep.D(hwdep.WifiP2P()),
			"cd1": hwdep.D(hwdep.WifiP2P()),
		},
		Requirements: []string{tdreq.WiFiGenSupportWFD},
	})
}

func P2PPerf(ctx context.Context, s *testing.State) {
	/*
		This test checks the p2p connection between two chromebooks by using
		the following steps:
		1- Configure the main DUT as a p2p group owner (GO).
		2- Configure the Companion DUT as a p2p client.
		3- Connect the the p2p client to the GO network.
		4- Route the IP address in both GO and client.
		5- Run Iperf TCP.
		6- Delete the IP route created in step 4.
		7- Deconfigure the p2p client.
		8- Deconfigure the p2p GO.
	*/
	tf := s.FixtValue().(*wificell.TestFixture)
	if err := tf.P2PConfigureGO(ctx, wificell.P2PDeviceDUT, p2p.SetFreq(5180)); err != nil {
		s.Fatal("Failed to configure the p2p group owner (GO): ", err)
	}
	defer func(ctx context.Context) {
		if err := tf.P2PDeconfigureGO(ctx); err != nil {
			s.Error("Failed to deconfigure the p2p group owner (GO): ", err)
		}
	}(ctx)
	ctx, cancel := tf.ReserveForDeconfigP2P(ctx)
	defer cancel()

	if err := tf.P2PConnect(ctx, wificell.P2PDeviceCompanionDUT); err != nil {
		s.Fatal("Failed to connect the p2p client to the p2p group owner (GO) network: ", err)
	}
	defer func(ctx context.Context) {
		if err := tf.P2PDisconnect(ctx); err != nil {
			s.Error("Failed to disconnect p2p client from GO: ", err)
		}
	}(ctx)
	ctx, cancel = tf.ReserveForDeconfigP2P(ctx)
	defer cancel()

	// Print the P2P channel configuration.
	iwr := iw.NewRemoteRunner(tf.P2PGOConn())
	iface, err := tf.P2PGOIface(ctx)
	if err != nil {
		s.Error("Failed to get P2P GO interface name: ", err)
	}
	chConfig, err := iwr.RadioConfig(ctx, iface)
	if err != nil {
		s.Error("Failed the P2P channel configuration: ", err)
	}

	testing.ContextLogf(ctx, "P2P channel configuration: Channel Number = %d, Frequency = %d, Width = %d", chConfig.Number, chConfig.Freq, chConfig.Width)

	finalResult, err := tf.P2PPerf(ctx)
	if err != nil {
		s.Fatal("Failed to run performance test: ", err)
	}

	pv := perf.NewValues()
	defer func() {
		if err := pv.Save(s.OutDir()); err != nil {
			s.Error("Failed to save perf data: ", err)
		}
	}()

	pv.Set(perf.Metric{
		Name:      "p2p_tcp_ave_tput_ch" + strconv.Itoa(chConfig.Number),
		Unit:      "Mbps",
		Direction: perf.BiggerIsBetter,
	}, float64(finalResult.Throughput/iperf.Mbps))
}
