// Copyright 2020 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package wifi

import (
	"context"
	"time"

	"go.chromium.org/tast-tests/cros/common/network/ping"
	"go.chromium.org/tast-tests/cros/common/perf"
	"go.chromium.org/tast-tests/cros/common/shillconst"
	"go.chromium.org/tast-tests/cros/common/tbdep"
	tdreq "go.chromium.org/tast-tests/cros/common/testdevicerequirements"
	"go.chromium.org/tast-tests/cros/common/wifi/security/wpa"
	"go.chromium.org/tast-tests/cros/remote/bundles/cros/wifi/wifiutil"
	remoteping "go.chromium.org/tast-tests/cros/remote/network/ping"
	"go.chromium.org/tast-tests/cros/remote/wificell"
	"go.chromium.org/tast-tests/cros/remote/wificell/hostapd"
	"go.chromium.org/tast-tests/cros/services/cros/wifi"
	"go.chromium.org/tast/core/ctxutil"
	"go.chromium.org/tast/core/testing"
)

func init() {
	testing.AddTest(&testing.Test{
		Func: PTK,
		Desc: "Verifies that pairwise temporal key rotations works as expected",
		Contacts: []string{
			"chromeos-wifi-champs@google.com", // WiFi oncall rotation
		},
		BugComponent:    "b:893827", // ChromeOS > Platform > Connectivity > WiFi
		Attr:            []string{"group:wificell", "wificell_func"},
		TestBedDeps:     []string{tbdep.Wificell, tbdep.WifiStateNormal, tbdep.BluetoothStateNormal, tbdep.PeripheralWifiStateWorking},
		ServiceDeps:     []string{wificell.ShillServiceName},
		Fixture:         wificell.FixtureID(wificell.TFFeaturesCapture),
		Timeout:         8 * time.Minute,
		Requirements:    []string{tdreq.WiFiProcPassFW, tdreq.WiFiProcPassAVL, tdreq.WiFiProcPassAVLBeforeUpdates, tdreq.WiFiProcPassMatfunc, tdreq.WiFiProcPassMatfuncBeforeUpdates},
		VariantCategory: `{"name": "WifiBtChipset_Soc_Kernel"}`,
	})
}

func PTK(ctx context.Context, s *testing.State) {
	// The ping configuration gives us around 75 seconds to ping,
	// which covers around 15 rekeys with 5 seconds period and allow 20% ping loss.
	const (
		rekeyPeriod      = 5
		pingCount        = 150
		pingInterval     = 0.5
		allowedLossCount = 30
	)

	tf := s.FixtValue().(*wificell.TestFixture)

	apOps := []hostapd.Option{
		hostapd.Mode(hostapd.Mode80211nPure),
		hostapd.Channel(1), hostapd.HTCaps(hostapd.HTCapHT20),
	}
	secConfFac := wpa.NewConfigFactory(
		"chromeos", wpa.Mode(wpa.ModeMixed),
		wpa.Ciphers(wpa.CipherTKIP, wpa.CipherCCMP),
		wpa.Ciphers2(wpa.CipherCCMP),
		wpa.PTKRekeyPeriod(rekeyPeriod),
	)
	ap, err := tf.ConfigureAP(ctx, apOps, secConfFac)
	if err != nil {
		s.Fatal("Failed to configure ap: ", err)
	}
	defer func(ctx context.Context) {
		if err := tf.DeconfigAP(ctx, ap); err != nil {
			s.Error("Failed to deconfig ap: ", err)
		}
	}(ctx)
	ctx, cancel := tf.ReserveForDeconfigAP(ctx, ap)
	defer cancel()

	s.Log("AP setup done; connecting")

	connectResp, err := tf.ConnectWifiAP(ctx, ap)
	if err != nil {
		s.Fatal("Failed to connect to WiFi: ", err)
	}
	servicePath := connectResp.ServicePath
	defer func(ctx context.Context) {
		if err := tf.CleanDisconnectWifi(ctx); err != nil {
			s.Error("Failed to disconnect WiFi: ", err)
		}
	}(ctx)
	ctx, cancel = tf.ReserveForDisconnect(ctx)
	defer cancel()

	// Total rekey count less 2 for a buffer. We expect 2 transitions (false -> true, true -> false) for each rekey
	rekeyCount := int(float64(pingCount)*pingInterval/float64(rekeyPeriod)) - 2
	if rekeyCount <= 0 {
		s.Fatal("Ping duration is too short")
	}
	props := make([]*wificell.ShillProperty, rekeyCount*2)
	for i := range props {
		props[i] = &wificell.ShillProperty{
			Property:       shillconst.ServicePropertyWiFiRekeyInProgress,
			Method:         wifi.ExpectShillPropertyRequest_ON_CHANGE,
			ExpectedValues: []interface{}{i%2 == 0},
		}
	}
	monitorProps := []string{shillconst.ServicePropertyIsConnected}
	pingBuffer := 20 * time.Second
	waitBuffer := 5 * time.Second
	waitCtx, cancel := context.WithTimeout(ctx, time.Duration(float64(pingCount)*pingInterval)*time.Second+pingBuffer+waitBuffer)
	defer cancel()
	waitForProps, err := tf.WifiClient().ExpectShillProperty(waitCtx, servicePath, props, monitorProps)

	iface, err := tf.ClientInterface(ctx)
	if err != nil {
		s.Fatal("DUT: failed to get the client WiFi interface: ", err)
	}

	s.Logf("Pinging with count=%d interval=%g second(s)", pingCount, pingInterval)
	// As we need to record ping loss, we cannot use tf.PingFromDUT() here.
	pingCtx, cancel := ctxutil.Shorten(waitCtx, waitBuffer)
	defer cancel()
	pr := remoteping.NewRemoteRunner(s.DUT().Conn())
	// Bind ping used in all WiFi Tests to WiFiInterface. Otherwise if the
	// WiFi interface is not up yet they will be routed through the Ethernet
	// interface. Also see b/225205611 for details.
	res, err := pr.Ping(pingCtx, ap.ServerIP().String(), ping.Count(pingCount),
		ping.Interval(pingInterval), ping.SaveOutput("ptk_ping.log"),
		ping.BindAddress(true), ping.SourceIface(iface))
	if err != nil {
		s.Fatal("Failed to ping from DUT: ", err)
	}
	s.Logf("Ping result=%+v", res)

	lossCount := res.Sent - res.Received
	if lossCount > allowedLossCount {
		s.Errorf("Unexpected packet loss: got %d, want <= %d", lossCount, allowedLossCount)
	}

	monitorResult, err := waitForProps()
	if err != nil {
		s.Error("Failed to wait for rekey events: ", err)
	}

	// Assert there was no disconnection during rekey process.
	if err := wifiutil.VerifyNoDisconnections(monitorResult); err != nil {
		s.Fatal("DUT: failed to stay connected during rekey process: ", err)
	}

	pv := perf.NewValues()
	pv.Set(perf.Metric{
		Name:      "ptk_ping_loss",
		Unit:      "percent",
		Direction: perf.SmallerIsBetter,
	}, res.Loss)
	if err := pv.Save(s.OutDir()); err != nil {
		s.Error("Failed to save perf data: ", err)
	}
}
