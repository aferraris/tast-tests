// Copyright 2020 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package wifi

import (
	"context"

	"go.chromium.org/tast-tests/cros/common/tbdep"
	tdreq "go.chromium.org/tast-tests/cros/common/testdevicerequirements"
	"go.chromium.org/tast-tests/cros/common/wifi/security"
	"go.chromium.org/tast-tests/cros/common/wifi/security/wpa"
	"go.chromium.org/tast-tests/cros/remote/wificell"
	"go.chromium.org/tast-tests/cros/remote/wificell/hostapd"
	"go.chromium.org/tast/core/errors"
	"go.chromium.org/tast/core/testing"
	"go.chromium.org/tast/core/timing"
)

func init() {
	testing.AddTest(&testing.Test{
		Func: SecChange,
		Desc: "Verifies that the DUT can connect to a BSS despite security changes",
		Contacts: []string{
			"chromeos-wifi-champs@google.com", // WiFi oncall rotation
		},
		BugComponent:    "b:893827", // ChromeOS > Platform > Connectivity > WiFi
		Attr:            []string{"group:wificell", "wificell_func", "wificell_cq"},
		TestBedDeps:     []string{tbdep.Wificell, tbdep.WifiStateNormal, tbdep.BluetoothStateNormal, tbdep.PeripheralWifiStateWorking},
		ServiceDeps:     []string{wificell.ShillServiceName},
		Fixture:         wificell.FixtureID(wificell.TFFeaturesCapture),
		Requirements:    []string{tdreq.WiFiProcPassFW, tdreq.WiFiProcPassAVL, tdreq.WiFiProcPassAVLBeforeUpdates, tdreq.WiFiProcPassMatfunc, tdreq.WiFiProcPassMatfuncBeforeUpdates},
		VariantCategory: `{"name": "WifiBtChipset_Soc_Kernel"}`,
	})
}

func SecChange(ctx context.Context, s *testing.State) {
	tf := s.FixtValue().(*wificell.TestFixture)

	// setUpAndConnect sets up the WiFi AP and verifies the DUT can connect to it.
	setUpAndConnect := func(ctx context.Context, options []hostapd.Option, fac security.ConfigFactory) (retErr error) {
		ctx, st := timing.Start(ctx, "setUpAndConnect")
		defer st.End()

		// collectErr logs the given err and returns it if the returning
		// error is is unspecified. Used in deferred functions.
		collectErr := func(err error) {
			s.Log("Error in setUpAndConnect: ", err)
			if retErr == nil {
				retErr = err
			}
		}
		ap, err := tf.ConfigureAP(ctx, options, fac)
		if err != nil {
			return errors.Wrap(err, "failed to configure ap")
		}
		defer func(ctx context.Context) {
			s.Log("Deconfiguring AP")
			if err := tf.DeconfigAP(ctx, ap); err != nil {
				collectErr(errors.Wrap(err, "failed to deconfig ap"))
			}
		}(ctx)
		ctx, cancel := tf.ReserveForDeconfigAP(ctx, ap)
		defer cancel()
		s.Log("AP setup done")

		if _, err := tf.ConnectWifiAP(ctx, ap); err != nil {
			return errors.Wrap(err, "failed to connect to WiFi")
		}
		defer func(ctx context.Context) {
			s.Log("Disconnecting")
			if err := tf.DisconnectWifi(ctx); err != nil {
				collectErr(errors.Wrap(err, "failed to disconnect"))
			}
			// Leave the profile entry as is, as we're going to verify
			// the behavior with it in next call.
		}(ctx)
		ctx, cancel = tf.ReserveForDisconnect(ctx)
		defer cancel()
		s.Log("Connected")

		if err := tf.PingFromDUT(ctx, ap.ServerIP().String()); err != nil {
			return errors.Wrap(err, "failed to ping server from DUT")
		}
		return nil
	}

	apOps := []hostapd.Option{
		hostapd.SSID(hostapd.RandomSSID("TAST_TEST_SecChange")),
		hostapd.Mode(hostapd.Mode80211nMixed),
		hostapd.Channel(48),
		hostapd.HTCaps(hostapd.HTCapHT40),
	}
	wpaOps := []wpa.Option{
		wpa.Mode(wpa.ModeMixed),
		wpa.Ciphers(wpa.CipherTKIP, wpa.CipherCCMP),
		wpa.Ciphers2(wpa.CipherCCMP),
	}
	wpaFac := wpa.NewConfigFactory("chromeos", wpaOps...)
	// Try connecting to a protected network (WPA).
	if err := setUpAndConnect(ctx, apOps, wpaFac); err != nil {
		s.Fatal("Failed to connect to a protected network (WPA): ", err)
	}
	// Assert that we can still connect to the open network with the same SSID.
	if err := setUpAndConnect(ctx, apOps, nil); err != nil {
		s.Fatal("Failed to connect to the open network: ", err)
	}
}
