// Copyright 2020 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// This test file generates the Params in simple_connect.go.
// Refer to go/tast-generate-params
// After modified, to overwrite the old Params, run:
// TAST_GENERATE_UPDATE=1 ~/chromiumos/src/platform/tast/tools/go.sh test -count=1 go.chromium.org/tast-tests/cros/remote/bundles/cros/wifi
// To check only, run:
// ~/chromiumos/src/platform/tast/fast_build.sh -t go.chromium.org/tast-tests/cros/remote/bundles/cros/wifi

package wifi

import (
	"fmt"
	"strconv"
	"strings"
	"testing"

	"go.chromium.org/chromiumos/config/go/test/lab/api"
	"go.chromium.org/tast-tests/cros/common/genparams"
	"go.chromium.org/tast-tests/cros/common/tbdep"
	tdreq "go.chromium.org/tast-tests/cros/common/testdevicerequirements"
)

type apConfigVal struct {
	// APOpts is used if it is not empty; Otherwise CommonAPOptions is used instead.
	APOpts          string // apOpts: []ap.Option{ %s },
	CommonAPOptions string // apOpts: wifiutil.CommonAPOptions( %s ),

	SecConfFac string
}

type simpleConnectParamsVal struct {
	Doc []string
	// A slice of configs for colocated AP's that are controlled by one
	// hostapd.
	APConfigs []apConfigVal
	// Override the SSIDs set by user or randomly generated in all hostapd
	// confs with same one that are randomly pre-generated.
	UseSameSSID      bool
	PingOps          string
	ExpectedFailure  bool
	ExpectedSecurity string
	// ExpectedKeyMgmt specifies the required AKM needed for the connection.
	// This is stricter than ExpectedSecurity and gives us higher granularity.
	ExpectedKeyMgmt string
}

type simpleConnectParams struct {
	Name                   string
	Fixture                string
	Doc                    []string
	ExtraAttr              []string
	ExtraSoftwareDeps      []string
	ExtraSoftwareDepsDoc   []string
	Val                    []simpleConnectParamsVal
	ExtraHardwareDeps      string
	ExtraHardwareDepsDoc   []string
	ExtraRequirements      []string
	ExtraTestBedDeps       []string
	DepsWifiRouterFeatures []api.WifiRouterFeature
}

func simpleConnectDocPref(text string) []string {
	return []string{"Verifies that DUT can connect to " + text}
}

const simpleConnectCommonSecApOpts = "ap.Mode(ap.Mode80211g), ap.Channel(1)"
const defaultFixture string = `wificell.FixtureID(wificell.TFFeaturesCapture)`

var dfsChannels = []int{
	52, 56, 60, 64, 100, 104, 108, 112, 116, 120, 124, 128, 132, 136, 140, 144,
}

func channelNeedsDfs(channel int) bool {
	for _, ch := range dfsChannels {
		if channel == ch {
			return true
		}
	}
	return false
}

func simpleConnect80211abg() []*simpleConnectParams {
	mkOps := func(mode string, channels ...int) []simpleConnectParamsVal {
		p := make([]simpleConnectParamsVal, len(channels))
		for i, ch := range channels {
			opts := fmt.Sprintf("ap.Mode(ap.Mode80211%s), ap.Channel(%d)", mode, ch)
			if channelNeedsDfs(ch) {
				opts += ", ap.SpectrumManagement()"
			}
			p[i].APConfigs = []apConfigVal{{APOpts: opts}}
		}
		return p
	}
	return []*simpleConnectParams{{
		Name:              "80211a",
		Fixture:           defaultFixture,
		Doc:               simpleConnectDocPref("an open 802.11a network on channels 48, 157."),
		Val:               mkOps("a", 48, 157),
		ExtraRequirements: []string{tdreq.WiFiGenSupportLegacy, tdreq.WiFiSecSupportOpen},
	}, {
		Name:              "80211b",
		Fixture:           defaultFixture,
		Doc:               simpleConnectDocPref("an open 802.11b network on channels 1, 6, 11."),
		Val:               mkOps("b", 1, 6, 11),
		ExtraRequirements: []string{tdreq.WiFiGenSupportLegacy},
	}, {
		Name:              "80211g",
		Fixture:           defaultFixture,
		Doc:               simpleConnectDocPref("an open 802.11g network on channels 1, 6, 11."),
		Val:               mkOps("g", 1, 6, 11),
		ExtraRequirements: []string{tdreq.WiFiGenSupportLegacy},
	}}
}

func simpleConnect80211n() []*simpleConnectParams {
	mkOps := func(htCaps string, channels ...int) []simpleConnectParamsVal {
		p := make([]simpleConnectParamsVal, len(channels))
		for i, ch := range channels {
			p[i].APConfigs = []apConfigVal{{APOpts: fmt.Sprintf("ap.Mode(ap.Mode80211nPure), ap.Channel(%d), ap.HTCaps(ap.HTCap%s)", ch, htCaps)}}
		}
		return p
	}
	return []*simpleConnectParams{{
		Name:              "80211n24ht20",
		Fixture:           defaultFixture,
		Doc:               simpleConnectDocPref("an open 802.11n network on 2.4GHz channels 1, 6, 11 with a channel width of 20MHz."),
		Val:               mkOps("HT20", 1, 6, 11),
		ExtraRequirements: []string{tdreq.WiFiGenSupportLegacy, tdreq.WiFiGenSupportLegacyBands},
	}, {
		Name:              "80211n24ht40",
		Fixture:           defaultFixture,
		Doc:               simpleConnectDocPref("an open 802.11n network on 2.4GHz channel 6 with a channel width of 40MHz."),
		Val:               mkOps("HT40", 6),
		ExtraRequirements: []string{tdreq.WiFiGenSupportLegacy, tdreq.WiFiGenSupportLegacyBands},
	}, {
		Name:              "80211n5ht20",
		Fixture:           defaultFixture,
		Doc:               simpleConnectDocPref("an open 802.11n network on 5GHz channel 48 with a channel width of 20MHz."),
		Val:               mkOps("HT20", 48),
		ExtraRequirements: []string{tdreq.WiFiGenSupportLegacy, tdreq.WiFiGenSupportLegacyBands},
	}, {
		Name:    "80211n5ht40",
		Fixture: defaultFixture,
		Doc: append(simpleConnectDocPref("an open 802.11n network on 5GHz channel 48"),
			"(40MHz channel with the second 20MHz chunk of the 40MHz channel on the channel below the center channel)."),
		Val:               mkOps("HT40Minus", 48),
		ExtraRequirements: []string{tdreq.WiFiGenSupportLegacy, tdreq.WiFiGenSupportLegacyBands},
	}}
}

func simpleConnect80211nsgi() *simpleConnectParams {
	return &simpleConnectParams{
		Name:      "80211nsgi",
		Fixture:   defaultFixture,
		Doc:       simpleConnectDocPref("an open 802.11n network on 5 GHz channel with short guard intervals enabled (both 20/40 Mhz)."),
		ExtraAttr: []string{"wificell_cq"},
		Val: []simpleConnectParamsVal{
			{APConfigs: []apConfigVal{{APOpts: "ap.Mode(ap.Mode80211nPure), ap.Channel(48), ap.HTCaps(ap.HTCapHT20, ap.HTCapSGI20)"}}},
			{APConfigs: []apConfigVal{{APOpts: "ap.Mode(ap.Mode80211nPure), ap.Channel(48), ap.HTCaps(ap.HTCapHT40Minus, ap.HTCapSGI40)"}}},
		},
		ExtraRequirements: []string{tdreq.WiFiGenSupportLegacy},
	}
}

func simpleConnect80211ac() []*simpleConnectParams {
	return []*simpleConnectParams{{
		Name:    "80211acvht20",
		Fixture: defaultFixture,
		Doc:     simpleConnectDocPref("an open 802.11ac network on channel 40 with a channel width of 20MHz."),
		Val: []simpleConnectParamsVal{{APConfigs: []apConfigVal{{APOpts: `
			ap.Mode(ap.Mode80211acPure), ap.Channel(40), ap.HTCaps(ap.HTCapHT20),
			ap.VHTChWidth(ap.VHTChWidth20Or40),
		`}}}},
		ExtraRequirements: []string{tdreq.WiFiGenSupportLegacy},
	}, {
		Name:    "80211acvht40",
		Fixture: defaultFixture,
		Doc:     simpleConnectDocPref("an open 802.11ac network on channel 48 with a channel width of 40MHz."),
		Val: []simpleConnectParamsVal{{APConfigs: []apConfigVal{{APOpts: `
			ap.Mode(ap.Mode80211acPure), ap.Channel(48), ap.HTCaps(ap.HTCapHT40),
			ap.VHTChWidth(ap.VHTChWidth20Or40),
		`}}}},
		ExtraRequirements: []string{tdreq.WiFiGenSupportLegacy},
	}, {
		Name:    "80211acvht80mixed",
		Fixture: defaultFixture,
		Doc:     simpleConnectDocPref("an open 802.11ac network on 5GHz channel 36 with center channel of 42 and channel width of 80MHz."),
		Val: []simpleConnectParamsVal{{APConfigs: []apConfigVal{{APOpts: `
			ap.Mode(ap.Mode80211acMixed), ap.Channel(36), ap.HTCaps(ap.HTCapHT40Plus),
			ap.VHTCaps(ap.VHTCapSGI80), ap.VHTCenterChannel(42), ap.VHTChWidth(ap.VHTChWidth80),
		`}}}},
		ExtraRequirements: []string{tdreq.WiFiGenSupportLegacy},
	}, {
		Name:    "80211acvht80pure",
		Fixture: defaultFixture,
		Doc: append(simpleConnectDocPref("an open 802.11ac network on channel 157 with center channel of 155 and channel width of 80MHz."),
			"The router is forced to use VHT WiFi standard."),
		Val: []simpleConnectParamsVal{{APConfigs: []apConfigVal{{APOpts: `
			ap.Mode(ap.Mode80211acPure), ap.Channel(157), ap.HTCaps(ap.HTCapHT40Plus),
			ap.VHTCaps(ap.VHTCapSGI80), ap.VHTCenterChannel(155), ap.VHTChWidth(ap.VHTChWidth80),
		`}}}},
		ExtraRequirements: []string{tdreq.WiFiGenSupportLegacy},
	}}
}

func simpleConnect80211ax() []*simpleConnectParams {
	return []*simpleConnectParams{{
		Name:      "80211axhe20",
		Fixture:   defaultFixture,
		Doc:       simpleConnectDocPref("an open 802.11ax network on channel 40 with a channel width of 20MHz."),
		ExtraAttr: []string{"wificell_unstable", "wificell_func_ax"},
		Val: []simpleConnectParamsVal{{APConfigs: []apConfigVal{{APOpts: `
			ap.Mode(ap.Mode80211axPure), ap.Channel(40), ap.HTCaps(ap.HTCapHT20),
			ap.HEChWidth(ap.HEChWidth20Or40),
		`}}}},
		ExtraHardwareDeps:      `hwdep.D(hwdep.Wifi80211ax())`,
		ExtraRequirements:      []string{tdreq.WiFiGenSupport80211ax, tdreq.WiFiRfSupport80211ax},
		DepsWifiRouterFeatures: []api.WifiRouterFeature{api.WifiRouterFeature_WIFI_ROUTER_FEATURE_IEEE_802_11_AX},
	}, {
		Name:      "80211axhe40",
		Fixture:   defaultFixture,
		Doc:       simpleConnectDocPref("an open 802.11ax network on channel 157 with a channel width of 40MHz."),
		ExtraAttr: []string{"wificell_unstable", "wificell_func_ax"},
		Val: []simpleConnectParamsVal{{APConfigs: []apConfigVal{{APOpts: `
			ap.Mode(ap.Mode80211axPure), ap.Channel(157), ap.HTCaps(ap.HTCapHT40, ap.HTCapLDPC),
			ap.HEChWidth(ap.HEChWidth20Or40),
		`}}}},
		ExtraHardwareDeps:      `hwdep.D(hwdep.Wifi80211ax())`,
		ExtraRequirements:      []string{tdreq.WiFiGenSupport80211ax, tdreq.WiFiRfSupport80211ax},
		DepsWifiRouterFeatures: []api.WifiRouterFeature{api.WifiRouterFeature_WIFI_ROUTER_FEATURE_IEEE_802_11_AX},
	}, {
		Name:      "80211axhe80mixed",
		Fixture:   defaultFixture,
		Doc:       simpleConnectDocPref("an open 802.11ax network on 5GHz channel 157 with center channel of 155 and channel width of 80MHz."),
		ExtraAttr: []string{"wificell_unstable", "wificell_func_ax"},
		Val: []simpleConnectParamsVal{{APConfigs: []apConfigVal{{APOpts: `
			ap.Mode(ap.Mode80211axMixed), ap.Channel(157), ap.HTCaps(ap.HTCapHT40Plus, ap.HTCapLDPC),
			ap.VHTCaps(ap.VHTCapSGI80), ap.VHTCenterChannel(155), ap.VHTChWidth(ap.VHTChWidth80), ap.HECenterChannel(155), ap.HEChWidth(ap.HEChWidth80),
		`}}}},
		ExtraHardwareDeps:      `hwdep.D(hwdep.Wifi80211ax())`,
		ExtraRequirements:      []string{tdreq.WiFiGenSupport80211ax, tdreq.WiFiRfSupport80211ax},
		DepsWifiRouterFeatures: []api.WifiRouterFeature{api.WifiRouterFeature_WIFI_ROUTER_FEATURE_IEEE_802_11_AX},
	}, {
		Name:      "80211axhe80pure",
		Fixture:   defaultFixture,
		ExtraAttr: []string{"wificell_unstable", "wificell_func_ax"},
		Doc: append(simpleConnectDocPref("an open 802.11ax network on channel 157 with center channel of 155 and channel width of 80MHz."),
			"The router is forced to use HE WiFi standard."),
		Val: []simpleConnectParamsVal{{APConfigs: []apConfigVal{{APOpts: `
			ap.Mode(ap.Mode80211axPure), ap.Channel(157), ap.HTCaps(ap.HTCapHT40Plus, ap.HTCapLDPC),
			ap.VHTCaps(ap.VHTCapSGI80), ap.VHTCenterChannel(155), ap.VHTChWidth(ap.VHTChWidth80), ap.HECenterChannel(155), ap.HEChWidth(ap.HEChWidth80),
		`}}}},
		ExtraHardwareDeps:      `hwdep.D(hwdep.Wifi80211ax())`,
		ExtraRequirements:      []string{tdreq.WiFiGenSupport80211ax, tdreq.WiFiRfSupport80211ax},
		DepsWifiRouterFeatures: []api.WifiRouterFeature{api.WifiRouterFeature_WIFI_ROUTER_FEATURE_IEEE_802_11_AX},
	}}
}

func simpleConnect80211axe() []*simpleConnectParams {
	return []*simpleConnectParams{{
		Name:      "80211axeowe",
		Fixture:   defaultFixture,
		Doc:       simpleConnectDocPref("an OWE 802.11ax network on 6GHz PSC channel 21 with a channel width of 20MHz."),
		ExtraAttr: []string{"wificell_unstable"},
		Val: []simpleConnectParamsVal{{
			APConfigs: []apConfigVal{{
				APOpts: `ap.Mode(ap.Mode80211axPure), ap.Channel(21), ap.HTCaps(ap.HTCapHT20),
					ap.HEChWidth(ap.HEChWidth20Or40), ap.OpClass(131), ap.PMF(ap.PMFRequired)`,
				SecConfFac: "owe.NewConfigFactory(owe.ModePureOWE)",
			}},
			ExpectedSecurity: "shillconst.SecurityOWE",
		}},
		ExtraHardwareDeps:      `hwdep.D(hwdep.Wifi80211ax6E())`,
		ExtraRequirements:      []string{tdreq.WiFiGenSupport6E, tdreq.WiFiRfSupport80211ax, tdreq.WiFiSecSupportOWE, tdreq.WiFiCertOWE},
		DepsWifiRouterFeatures: []api.WifiRouterFeature{api.WifiRouterFeature_WIFI_ROUTER_FEATURE_IEEE_802_11_AX_E},
	}, {
		Name:              "80211axe20",
		Fixture:           defaultFixture,
		Doc:               simpleConnectDocPref("a WPA3-SAE (\"pure\") 802.11ax network on 6GHz PSC channel 21 with a channel width of 20MHz."),
		ExtraAttr:         []string{"wificell_unstable"},
		ExtraSoftwareDeps: []string{"wpa3_sae"},
		Val: []simpleConnectParamsVal{{
			APConfigs: []apConfigVal{{
				APOpts: `ap.Mode(ap.Mode80211axPure), ap.Channel(21), ap.HTCaps(ap.HTCapHT20),
					ap.HEChWidth(ap.HEChWidth20Or40), ap.OpClass(131), ap.PMF(ap.PMFRequired)`,
				SecConfFac: `wpa.NewConfigFactory("chromeos",
					wpa.Mode(wpa.ModePureWPA3), wpa.Ciphers2(wpa.CipherCCMP),)`,
			}},
			ExpectedSecurity: wpaModeToShillSecurity(`PureWPA3`),
		}},
		ExtraHardwareDeps:      `hwdep.D(hwdep.Wifi80211ax6E())`,
		ExtraRequirements:      []string{tdreq.WiFiGenSupport6E, tdreq.WiFiRfSupport80211ax},
		DepsWifiRouterFeatures: []api.WifiRouterFeature{api.WifiRouterFeature_WIFI_ROUTER_FEATURE_IEEE_802_11_AX_E},
	}, {
		Name:              "80211axe40",
		Fixture:           defaultFixture,
		Doc:               simpleConnectDocPref("a WPA3-SAE (\"pure\") 802.11ax network on 6GHz PSC channel 21 with a channel width of 40MHz."),
		ExtraAttr:         []string{"wificell_unstable"},
		ExtraSoftwareDeps: []string{"wpa3_sae"},
		Val: []simpleConnectParamsVal{{
			APConfigs: []apConfigVal{{
				APOpts: `ap.Mode(ap.Mode80211axPure), ap.Channel(21), ap.HTCaps(ap.HTCapLDPC), ap.HECenterChannel(19),
					ap.HEChWidth(ap.HEChWidth20Or40), ap.OpClass(131), ap.PMF(ap.PMFRequired)`,
				SecConfFac: `wpa.NewConfigFactory("chromeos",
					wpa.Mode(wpa.ModePureWPA3), wpa.Ciphers2(wpa.CipherCCMP),)`,
			}},
			ExpectedSecurity: wpaModeToShillSecurity(`PureWPA3`),
		}},
		ExtraHardwareDeps:      `hwdep.D(hwdep.Wifi80211ax6E())`,
		ExtraRequirements:      []string{tdreq.WiFiGenSupport6E, tdreq.WiFiRfSupport80211ax},
		DepsWifiRouterFeatures: []api.WifiRouterFeature{api.WifiRouterFeature_WIFI_ROUTER_FEATURE_IEEE_802_11_AX_E},
	}, {
		Name:              "80211axe80mixed",
		Fixture:           defaultFixture,
		Doc:               simpleConnectDocPref("a WPA3-SAE (\"mixed\") 802.11ax network on 6GHz PSC channel 5 with center channel of 7 and channel width of 80MHz."),
		ExtraAttr:         []string{"wificell_unstable"},
		ExtraSoftwareDeps: []string{"wpa3_sae"},
		Val: []simpleConnectParamsVal{{
			APConfigs: []apConfigVal{{
				APOpts: `ap.Mode(ap.Mode80211axMixed), ap.Channel(5), ap.HTCaps(ap.HTCapLDPC),
					ap.VHTCaps(ap.VHTCapSGI80), ap.HECenterChannel(7), ap.HEChWidth(ap.HEChWidth80),
					ap.OpClass(131), ap.PMF(ap.PMFRequired)`,
				SecConfFac: `wpa.NewConfigFactory("chromeos",
					wpa.Mode(wpa.ModePureWPA3), wpa.Ciphers2(wpa.CipherCCMP),)`,
			}},
			ExpectedSecurity: wpaModeToShillSecurity(`PureWPA3`),
		}},
		ExtraHardwareDeps:      `hwdep.D(hwdep.Wifi80211ax6E())`,
		ExtraRequirements:      []string{tdreq.WiFiGenSupport6E, tdreq.WiFiRfSupport80211ax},
		DepsWifiRouterFeatures: []api.WifiRouterFeature{api.WifiRouterFeature_WIFI_ROUTER_FEATURE_IEEE_802_11_AX_E},
	}, {
		Name:              "80211axe80pure",
		Fixture:           defaultFixture,
		ExtraAttr:         []string{"wificell_unstable"},
		ExtraSoftwareDeps: []string{"wpa3_sae"},
		Doc: append(simpleConnectDocPref("a WPA3-SAE (\"pure\") 802.11ax network on 6GHz PSC channel 5 with center channel of 7 and channel width of 80MHz."),
			"The router is forced to use HE WiFi standard."),
		Val: []simpleConnectParamsVal{{
			APConfigs: []apConfigVal{{
				APOpts: `ap.Mode(ap.Mode80211axPure), ap.Channel(5), ap.HTCaps(ap.HTCapLDPC),
					ap.VHTCaps(ap.VHTCapSGI80), ap.HECenterChannel(7), ap.HEChWidth(ap.HEChWidth80),
					ap.OpClass(131), ap.PMF(ap.PMFRequired)`,
				SecConfFac: `wpa.NewConfigFactory("chromeos",
					wpa.Mode(wpa.ModePureWPA3), wpa.Ciphers2(wpa.CipherCCMP),)`,
			}},
			ExpectedSecurity: wpaModeToShillSecurity(`PureWPA3`),
		}},
		ExtraHardwareDeps:      `hwdep.D(hwdep.Wifi80211ax6E())`,
		ExtraRequirements:      []string{tdreq.WiFiGenSupport6E, tdreq.WiFiRfSupport80211ax},
		DepsWifiRouterFeatures: []api.WifiRouterFeature{api.WifiRouterFeature_WIFI_ROUTER_FEATURE_IEEE_802_11_AX_E},
	}, {
		Name:              "80211axe160mixed",
		Fixture:           defaultFixture,
		Doc:               simpleConnectDocPref("a WPA3-SAE (\"mixed\") 802.11ax network on 6GHz PSC channel 5 with center channel of 15 and channel width of 160MHz."),
		ExtraAttr:         []string{"wificell_unstable"},
		ExtraSoftwareDeps: []string{"wpa3_sae"},
		Val: []simpleConnectParamsVal{{
			APConfigs: []apConfigVal{{
				APOpts: `ap.Mode(ap.Mode80211axMixed), ap.Channel(5), ap.HTCaps(ap.HTCapLDPC),
					ap.VHTCaps(ap.VHTCapSGI160), ap.HECenterChannel(15), ap.HEChWidth(ap.HEChWidth160),
					ap.OpClass(131), ap.PMF(ap.PMFRequired)`,
				SecConfFac: `wpa.NewConfigFactory("chromeos",
					wpa.Mode(wpa.ModePureWPA3), wpa.Ciphers2(wpa.CipherCCMP),)`,
			}},
			ExpectedSecurity: wpaModeToShillSecurity(`PureWPA3`),
		}},
		ExtraHardwareDeps:      `hwdep.D(hwdep.Wifi80211ax6E())`,
		ExtraRequirements:      []string{tdreq.WiFiGenSupport6E, tdreq.WiFiRf6E160MHz},
		DepsWifiRouterFeatures: []api.WifiRouterFeature{api.WifiRouterFeature_WIFI_ROUTER_FEATURE_IEEE_802_11_AX_E},
	}, {
		Name:      "80211axe160pure",
		Fixture:   defaultFixture,
		ExtraAttr: []string{"wificell_unstable"},
		Doc: append(simpleConnectDocPref("a WPA3-SAE (\"pure\") 802.11ax network on 6GHz PSC channel 5 with center channel of 15 and channel width of 160MHz."),
			"The router is forced to use HE WiFi standard."),
		ExtraSoftwareDeps: []string{"wpa3_sae"},
		Val: []simpleConnectParamsVal{{
			APConfigs: []apConfigVal{{
				APOpts: `ap.Mode(ap.Mode80211axPure), ap.Channel(5), ap.HTCaps(ap.HTCapLDPC),
					ap.VHTCaps(ap.VHTCapSGI160), ap.HECenterChannel(15), ap.HEChWidth(ap.HEChWidth160),
					ap.OpClass(131), ap.PMF(ap.PMFRequired)`,
				SecConfFac: `wpa.NewConfigFactory("chromeos",
					wpa.Mode(wpa.ModePureWPA3), wpa.Ciphers2(wpa.CipherCCMP),)`,
			}},
			ExpectedSecurity: wpaModeToShillSecurity(`PureWPA3`),
		}},
		ExtraHardwareDeps:      `hwdep.D(hwdep.Wifi80211ax6E())`,
		ExtraRequirements:      []string{tdreq.WiFiGenSupport6E, tdreq.WiFiRf6E160MHz},
		DepsWifiRouterFeatures: []api.WifiRouterFeature{api.WifiRouterFeature_WIFI_ROUTER_FEATURE_IEEE_802_11_AX_E},
	}}
}

func simpleConnect80211be() []*simpleConnectParams {
	return []*simpleConnectParams{{
		Name:      "80211beeht20",
		Fixture:   defaultFixture,
		Doc:       simpleConnectDocPref("an open 802.11be network on channel 40 with a channel width of 20MHz."),
		ExtraAttr: []string{"wificell_func_be"},
		Val: []simpleConnectParamsVal{{
			APConfigs: []apConfigVal{{
				APOpts: `ap.Mode(ap.Mode80211bePure), ap.Channel(40), ap.HTCaps(ap.HTCapHT20),
					ap.EHTChWidth(ap.EHTChWidth20Or40), ap.EHTCenterChannel(40), ap.PMF(ap.PMFRequired)`,
				SecConfFac: `wpa.NewConfigFactory("chromeos",
					wpa.Mode(wpa.ModePureWPA3), wpa.Ciphers2(wpa.CipherCCMP),)`,
			}},
			ExpectedSecurity: wpaModeToShillSecurity(`PureWPA3`),
		}},
		ExtraHardwareDeps:      `hwdep.D(hwdep.Wifi80211be())`,
		DepsWifiRouterFeatures: []api.WifiRouterFeature{api.WifiRouterFeature_WIFI_ROUTER_FEATURE_IEEE_802_11_BE},
	}, {
		Name:      "80211beeht40",
		Fixture:   defaultFixture,
		Doc:       simpleConnectDocPref("an open 802.11be network on channel 157 with a channel width of 40MHz."),
		ExtraAttr: []string{"wificell_func_be"},
		Val: []simpleConnectParamsVal{{
			APConfigs: []apConfigVal{{
				APOpts: `ap.Mode(ap.Mode80211bePure), ap.Channel(157), ap.HTCaps(ap.HTCapHT40, ap.HTCapLDPC),
					ap.EHTChWidth(ap.EHTChWidth20Or40), ap.EHTCenterChannel(159), ap.PMF(ap.PMFRequired)`,
				SecConfFac: `wpa.NewConfigFactory("chromeos",
					wpa.Mode(wpa.ModePureWPA3), wpa.Ciphers2(wpa.CipherCCMP),)`,
			}},
			ExpectedSecurity: wpaModeToShillSecurity(`PureWPA3`),
		}},
		ExtraHardwareDeps:      `hwdep.D(hwdep.Wifi80211be())`,
		DepsWifiRouterFeatures: []api.WifiRouterFeature{api.WifiRouterFeature_WIFI_ROUTER_FEATURE_IEEE_802_11_BE},
	}, {
		Name:      "80211beeht80mixed",
		Fixture:   defaultFixture,
		Doc:       simpleConnectDocPref("an open 802.11be network on 5GHz channel 157 with center channel of 155 and channel width of 80MHz."),
		ExtraAttr: []string{"wificell_func_be"},
		Val: []simpleConnectParamsVal{{
			APConfigs: []apConfigVal{{
				APOpts: `ap.Mode(ap.Mode80211beMixed), ap.Channel(157), ap.HTCaps(ap.HTCapHT40Plus, ap.HTCapLDPC),
					ap.VHTCaps(ap.VHTCapSGI80), ap.VHTCenterChannel(155), ap.VHTChWidth(ap.VHTChWidth80),
					ap.EHTCenterChannel(155), ap.EHTChWidth(ap.EHTChWidth80), ap.PMF(ap.PMFRequired)`,
				SecConfFac: `wpa.NewConfigFactory("chromeos",
					wpa.Mode(wpa.ModePureWPA3), wpa.Ciphers2(wpa.CipherCCMP),)`,
			}},
			ExpectedSecurity: wpaModeToShillSecurity(`PureWPA3`),
		}},
		ExtraHardwareDeps:      `hwdep.D(hwdep.Wifi80211be())`,
		DepsWifiRouterFeatures: []api.WifiRouterFeature{api.WifiRouterFeature_WIFI_ROUTER_FEATURE_IEEE_802_11_BE},
	}, {
		Name:      "80211beeht80pure",
		Fixture:   defaultFixture,
		ExtraAttr: []string{"wificell_func_be"},
		Doc: append(simpleConnectDocPref("an open 802.11be network on channel 157 with center channel of 155 and channel width of 80MHz."),
			"The router is forced to use EHT WiFi standard."),
		Val: []simpleConnectParamsVal{{
			APConfigs: []apConfigVal{{
				APOpts: `ap.Mode(ap.Mode80211bePure), ap.Channel(157), ap.HTCaps(ap.HTCapHT40Plus, ap.HTCapLDPC),
					ap.VHTCaps(ap.VHTCapSGI80), ap.VHTCenterChannel(155), ap.VHTChWidth(ap.VHTChWidth80),
					ap.EHTCenterChannel(155), ap.EHTChWidth(ap.EHTChWidth80), ap.PMF(ap.PMFRequired)`,
				SecConfFac: `wpa.NewConfigFactory("chromeos",
					wpa.Mode(wpa.ModePureWPA3), wpa.Ciphers2(wpa.CipherCCMP),)`,
			}},
			ExpectedSecurity: wpaModeToShillSecurity(`PureWPA3`),
		}},
		ExtraHardwareDeps:      `hwdep.D(hwdep.Wifi80211be())`,
		DepsWifiRouterFeatures: []api.WifiRouterFeature{api.WifiRouterFeature_WIFI_ROUTER_FEATURE_IEEE_802_11_BE},
	}}
}

func simpleConnectGCMP() []*simpleConnectParams {
	return []*simpleConnectParams{{
		Name:    "gcmp_128",
		Fixture: defaultFixture,
		Doc: append(simpleConnectDocPref("a WiFi5 GCMP-128 network on WPA3."),
			"TODO(b/337103884) Replace TestBedDep with GCMP-feature requirement once available."),
		Val: []simpleConnectParamsVal{{
			APConfigs: []apConfigVal{{
				APOpts: `ap.Mode(ap.Mode80211acPure), ap.Channel(36), ap.HTCaps(ap.HTCapHT40Plus),
					ap.VHTCenterChannel(42), ap.VHTChWidth(ap.VHTChWidth80), ap.PMF(ap.PMFRequired)`,
				SecConfFac: `wpa.NewConfigFactory("chromeos",
					wpa.Mode(wpa.ModePureWPA3), wpa.Ciphers2(wpa.CipherGCMP128),
					wpa.GroupCipherOverride(wpa.CipherGCMP128))`,
			}},
			ExpectedSecurity: wpaModeToShillSecurity(`PureWPA3`),
		}},
		ExtraHardwareDeps:      `hwdep.D(hwdep.WifiGCMP())`,
		DepsWifiRouterFeatures: []api.WifiRouterFeature{api.WifiRouterFeature_WIFI_ROUTER_FEATURE_IEEE_802_11_AX},
	}, {
		Name:    "gcmp_256",
		Fixture: defaultFixture,
		Doc: append(simpleConnectDocPref("a WiFi5 GCMP-256 network on WPA3."),
			"TODO(b/337103884) Replace TestBedDep with GCMP-feature requirement once available."),
		Val: []simpleConnectParamsVal{{
			APConfigs: []apConfigVal{{
				APOpts: `ap.Mode(ap.Mode80211acPure), ap.Channel(36), ap.HTCaps(ap.HTCapHT40Plus),
					ap.VHTCenterChannel(42), ap.VHTChWidth(ap.VHTChWidth80), ap.PMF(ap.PMFRequired)`,
				SecConfFac: `wpa.NewConfigFactory("chromeos",
					wpa.Mode(wpa.ModePureWPA3), wpa.Ciphers2(wpa.CipherGCMP256),
					wpa.GroupCipherOverride(wpa.CipherGCMP256))`,
			}},
			ExpectedSecurity: wpaModeToShillSecurity(`PureWPA3`),
		}},
		ExtraHardwareDeps:      `hwdep.D(hwdep.WifiGCMP())`,
		DepsWifiRouterFeatures: []api.WifiRouterFeature{api.WifiRouterFeature_WIFI_ROUTER_FEATURE_IEEE_802_11_AX},
	}, {
		Name:    "gcmp_ccmp",
		Fixture: defaultFixture,
		Doc: append(simpleConnectDocPref("a WiFi5 network that supports CCMP and GCMP on WPA2."),
			"TODO(b/337103884) Replace TestBedDep with GCMP-feature requirement once available."),
		Val: []simpleConnectParamsVal{{
			APConfigs: []apConfigVal{{
				APOpts: `ap.Mode(ap.Mode80211acPure), ap.Channel(36), ap.HTCaps(ap.HTCapHT40Plus),
								ap.VHTCenterChannel(42), ap.VHTChWidth(ap.VHTChWidth80), ap.PMF(ap.PMFRequired)`,
				SecConfFac: `wpa.NewConfigFactory("chromeos",
								wpa.Mode(wpa.ModePureWPA2), wpa.Ciphers2(wpa.CipherCCMP, wpa.CipherGCMP128))`,
			}},
			ExpectedSecurity: wpaModeToShillSecurity(`PureWPA2`),
		}},
		DepsWifiRouterFeatures: []api.WifiRouterFeature{api.WifiRouterFeature_WIFI_ROUTER_FEATURE_IEEE_802_11_AX},
	}}
}

func simpleConnectOWE() []*simpleConnectParams {
	return []*simpleConnectParams{{
		Name:    "owe",
		Fixture: defaultFixture,
		Doc:     simpleConnectDocPref("an OWE network on 2.4GHz."),
		Val: []simpleConnectParamsVal{{
			APConfigs: []apConfigVal{{
				APOpts:     simpleConnectCommonSecApOpts,
				SecConfFac: "owe.NewConfigFactory(owe.ModePureOWE)",
			}},
			ExpectedSecurity: "shillconst.SecurityOWE",
		}},
		ExtraHardwareDepsDoc: []string{"Skip Marvell WiFi since they do not support OWE."},
		ExtraHardwareDeps:    `hwdep.D(hwdep.WifiNotMarvell())`,
		ExtraRequirements:    []string{tdreq.WiFiSecSupportOWE, tdreq.WiFiCertOWE},
	}}
}

func simpleConnectHidden() []*simpleConnectParams {
	return []*simpleConnectParams{{
		Name:    "hidden24g",
		Fixture: defaultFixture,
		Doc:     simpleConnectDocPref("a hidden network on 2.4GHz channel."),
		Val: []simpleConnectParamsVal{
			{APConfigs: []apConfigVal{{APOpts: "ap.Mode(ap.Mode80211g), ap.Channel(6), ap.Hidden()"}}},
		},
	}, {
		Name:    "hidden5ht20",
		Fixture: defaultFixture,
		Doc:     simpleConnectDocPref("a hidden network on 5GHz channels."),
		Val: []simpleConnectParamsVal{
			{APConfigs: []apConfigVal{{APOpts: "ap.Mode(ap.Mode80211nPure), ap.Channel(36), ap.HTCaps(ap.HTCapHT20), ap.Hidden()"}}},
			{APConfigs: []apConfigVal{{APOpts: "ap.Mode(ap.Mode80211nPure), ap.Channel(48), ap.HTCaps(ap.HTCapHT20), ap.Hidden()"}}},
		},
		ExtraHardwareDepsDoc: []string{"TODO(b/189972561) Enable this test on Trogdor once active scanning on 5 GHz channel is enabled."},
		ExtraHardwareDeps:    `hwdep.D(hwdep.SkipOnPlatform("strongbad", "strongbad64", "strongbad-kernelnext", "trogdor", "trogdor64", "trogdor-kernelnext"))`,
	}}
}

func simpleConnectWEP() []*simpleConnectParams {
	mkP := func(keyLen int) *simpleConnectParams {
		ret := &simpleConnectParams{
			Name:              "wep" + strconv.Itoa(keyLen),
			Fixture:           defaultFixture,
			Doc:               simpleConnectDocPref(fmt.Sprintf("a WEP network with both open and shared system authentication and %d-bit pre-shared keys.", keyLen)),
			ExtraHardwareDeps: `hwdep.D(hwdep.WifiWEP())`,
			ExtraRequirements: []string{tdreq.WiFiSecSupportWEP},
		}
		for _, algo := range []string{"Open", "Shared"} {
			for key := 0; key < 4; key++ {
				ret.Val = append(ret.Val, simpleConnectParamsVal{
					APConfigs: []apConfigVal{{
						APOpts:     simpleConnectCommonSecApOpts,
						SecConfFac: fmt.Sprintf("wep.NewConfigFactory(wep%dKeys(), wep.DefaultKey(%d), wep.AuthAlgs(wep.AuthAlgo%s))", keyLen, key, algo),
					}},
					ExpectedSecurity: `shillconst.SecurityWEP`,
				})
			}
		}
		return ret
	}
	return []*simpleConnectParams{mkP(40), mkP(104)}
}

func simpleConnectWEPHidden() *simpleConnectParams {
	var p []simpleConnectParamsVal
	for _, keyLen := range []int{40, 104} {
		for _, algo := range []string{"Open", "Shared"} {
			p = append(p, simpleConnectParamsVal{
				APConfigs: []apConfigVal{{
					APOpts:     simpleConnectCommonSecApOpts + ", ap.Hidden()",
					SecConfFac: fmt.Sprintf("wep.NewConfigFactory(wep%dKeysHidden(), wep.AuthAlgs(wep.AuthAlgo%s))", keyLen, algo),
				}},
				ExpectedSecurity: `shillconst.SecurityWEP`,
			})
		}
	}
	return &simpleConnectParams{
		Name:              "wephidden",
		Fixture:           defaultFixture,
		Doc:               simpleConnectDocPref("a hidden WEP network with open/shared system authentication and 40/104-bit pre-shared keys."),
		Val:               p,
		ExtraHardwareDeps: `hwdep.D(hwdep.WifiWEP())`,
		ExtraRequirements: []string{tdreq.WiFiSecSupportWEP},
	}
}

func wpaModeToShillSecurity(mode string) string {
	switch mode {
	case `PureWPA`:
		return `shillconst.SecurityWPA`
	case `PureWPA2`:
		return `shillconst.SecurityWPA2`
	case `PureWPA3`:
		return `shillconst.SecurityWPA3`
	case `PureWPA3Ext`:
		return `shillconst.SecurityWPA3`
	case `Mixed`:
		return `shillconst.SecurityWPAWPA2`
	case `MixedWPA3`:
		return `shillconst.SecurityWPA2WPA3`
	case `MixedWPA3Ext`:
		return `shillconst.SecurityWPA2WPA3`
	}
	return ``
}

func wpaModeToShillEntSecurity(mode string) string {
	return wpaModeToShillSecurity(mode) + `Enterprise`
}

func simpleConnectWPA() []*simpleConnectParams {
	const (
		tkip = 1 << iota
		ccmp
	)
	mkCipher := func(c int) string {
		var ret []string
		if c&tkip > 0 {
			ret = append(ret, "wpa.CipherTKIP")
		}
		if c&ccmp > 0 {
			ret = append(ret, "wpa.CipherCCMP")
		}
		return strings.Join(ret, ", ")
	}
	mkOps := func(pmf, mode string, cipher1, cipher2 int) []simpleConnectParamsVal {
		var cipher []string
		if cipher1 > 0 {
			cipher = append(cipher, "wpa.Ciphers("+mkCipher(cipher1)+")")
		}
		if cipher2 > 0 {
			cipher = append(cipher, "wpa.Ciphers2("+mkCipher(cipher2)+")")
		}
		return []simpleConnectParamsVal{{
			APConfigs: []apConfigVal{{
				APOpts: simpleConnectCommonSecApOpts + ", " + pmf,
				SecConfFac: fmt.Sprintf(`wpa.NewConfigFactory(
					"chromeos", wpa.Mode(wpa.Mode%s),
					%s,
				)`, mode, strings.Join(cipher, ", ")),
			}},
			ExpectedSecurity: wpaModeToShillSecurity(mode),
		}}
	}
	return []*simpleConnectParams{{
		Name:    "wpatkip",
		Fixture: defaultFixture,
		Doc:     simpleConnectDocPref("a protected network supporting for pure WPA with TKIP."),
		Val:     mkOps("", "PureWPA", tkip, 0),
	}, {
		Name:    "wpaccmp",
		Fixture: defaultFixture,
		Doc:     simpleConnectDocPref("a protected network supporting for pure WPA with AES based CCMP."),
		Val:     mkOps("", "PureWPA", ccmp, 0),
	}, {
		Name:      "wpamulti",
		Fixture:   defaultFixture,
		Doc:       simpleConnectDocPref("a protected network supporting for pure WPA with both AES based CCMP and TKIP."),
		ExtraAttr: []string{"wificell_cq"},
		Val:       mkOps("", "PureWPA", tkip|ccmp, 0),
	}, {
		Name:              "wpa2tkip",
		Fixture:           defaultFixture,
		Doc:               simpleConnectDocPref("a protected network supporting for WPA2 (aka RSN) with TKIP. Some AP still uses TKIP in WPA2."),
		Val:               mkOps("", "PureWPA2", 0, tkip),
		ExtraRequirements: []string{tdreq.WiFiSecSupportWPA2Personal},
	}, {
		Name:    "wpa2pmf",
		Fixture: defaultFixture,
		Doc: append(simpleConnectDocPref("an AP broadcasting a WPA2 network using AES based CCMP."),
			"In addition, the client must also support 802.11w protected management frames."),
		ExtraAttr:         []string{"wificell_cq"},
		Val:               mkOps("ap.PMF(ap.PMFRequired)", "PureWPA2", 0, ccmp),
		ExtraRequirements: []string{tdreq.WiFiGenSupportPMF, tdreq.WiFiSecSupportWPA2Personal},
	}, {
		Name:    "wpa2pmfsha256",
		Fixture: defaultFixture,
		Doc: append(simpleConnectDocPref("an AP broadcasting a WPA2 network using AES based CCMP."),
			"In addition, the client must also support 802.11w protected management frames.",
			"And the client uses WPA-PSK-SHA256 for key management suite"),
		ExtraRequirements: []string{tdreq.WiFiGenSupportPMF, tdreq.WiFiSecSupportWPA2Personal},
		Val: []simpleConnectParamsVal{{
			APConfigs: []apConfigVal{{
				APOpts: simpleConnectCommonSecApOpts + ", ap.PMF(ap.PMFRequired)",
				SecConfFac: fmt.Sprintf(`wpa.NewConfigFactory(
					"chromeos", wpa.Mode(wpa.ModePureWPA2), wpa.KeyMgmt([]string{wpa.KeyMgmtWPAPSKSHA256}),
					wpa.Ciphers2(wpa.CipherCCMP),
				)`),
			}},
			ExpectedSecurity: wpaModeToShillSecurity(`PureWPA2`),
		}, {
			APConfigs: []apConfigVal{{
				APOpts: simpleConnectCommonSecApOpts + ", ap.PMF(ap.PMFRequired)",
				SecConfFac: fmt.Sprintf(`wpa.NewConfigFactory(
					"chromeos", wpa.Mode(wpa.ModePureWPA2), wpa.KeyMgmt([]string{%s, %s}),
					wpa.Ciphers2(wpa.CipherCCMP),
				)`, "wpa.KeyMgmtWPAPSK", "wpa.KeyMgmtWPAPSKSHA256"),
			}},
			ExpectedSecurity: wpaModeToShillSecurity(`PureWPA2`),
		}},
	}, {
		Name:    "wpa2pmfoptional",
		Fixture: defaultFixture,
		Doc: append(simpleConnectDocPref("an AP broadcasting a WPA2 network using AES based CCMP."),
			"In addition, the client may also negotiate use of 802.11w protected management frames."),
		Val:               mkOps("ap.PMF(ap.PMFOptional)", "PureWPA2", 0, ccmp),
		ExtraRequirements: []string{tdreq.WiFiGenSupportPMF, tdreq.WiFiSecSupportWPA2Personal},
	}, {
		Name:              "wpa2",
		Fixture:           defaultFixture,
		Doc:               simpleConnectDocPref("a protected network supporting for WPA2 (aka RSN) and encrypted under AES."),
		Val:               mkOps("", "PureWPA2", 0, ccmp),
		ExtraRequirements: []string{tdreq.WiFiSecSupportWPA2Personal},
	}, {
		Name:    "wpamixed",
		Fixture: defaultFixture,
		Doc:     simpleConnectDocPref("a protected network supporting for both WPA and WPA2 with TKIP/AES supported for WPA and AES supported for WPA2."),
		Val:     mkOps("", "Mixed", tkip|ccmp, ccmp),
	}}
}

func simpleConnectWPA3() []*simpleConnectParams {
	mkOps := func(pmf, mode, keyMgmtOverride string) []simpleConnectParamsVal {
		return []simpleConnectParamsVal{{
			APConfigs: []apConfigVal{{
				APOpts: fmt.Sprintf(`
					ap.Mode(ap.Mode80211acMixed), ap.Channel(36), ap.HTCaps(ap.HTCapHT40Plus),
					ap.VHTCenterChannel(42), ap.VHTChWidth(ap.VHTChWidth80),
					ap.PMF(ap.PMF%s),
				`, pmf),
				SecConfFac: fmt.Sprintf(`wpa.NewConfigFactory(
					"chromeos", wpa.Mode(wpa.Mode%s),
					wpa.Ciphers2(wpa.CipherCCMP),
				)`, mode),
			}},
			ExpectedSecurity: wpaModeToShillSecurity(mode),
			ExpectedKeyMgmt:  keyMgmtOverride,
		}}
	}
	return []*simpleConnectParams{{
		Name:              "wpa3mixed",
		Fixture:           defaultFixture,
		Doc:               simpleConnectDocPref("an AP in WPA2/WPA3 mixed mode. WiFi alliance suggests PMF in this mode."),
		Val:               mkOps("Optional", "MixedWPA3", ""),
		ExtraRequirements: []string{tdreq.WiFiGenSupportPMF, tdreq.WiFiSecSupportWPA3Personal},
	}, {
		Name:      "wpa3extmixed",
		Fixture:   defaultFixture,
		ExtraAttr: []string{"wificell_unstable"},
		// We enforce wpa3_sae support here as we expect devices to connect to SAE-EXT-KEY mode in mixed mode.
		ExtraSoftwareDeps: []string{"wpa3_sae"},
		ExtraSoftwareDepsDoc: []string{
			"Not all WiFi chips support SAE. We enable the feature as a Software dependency for now, but eventually",
			"this will require a hardware dependency (crbug.com/1070299).",
		},
		Doc:               simpleConnectDocPref(`an AP in WPA3-SAE-EXT ("mixed") mode. WiFi alliance requires PMF in this mode.`),
		Val:               mkOps("Optional", "MixedWPA3Ext", "wpa.KeyMgmtSAEEXT"),
		ExtraRequirements: []string{tdreq.WiFiGenSupportPMF, tdreq.WiFiSecSupportWPA3Personal},
		// TODO(b/339435290) Refine testbed dep to work for routers w/ AKM24 support.
		DepsWifiRouterFeatures: []api.WifiRouterFeature{api.WifiRouterFeature_WIFI_ROUTER_FEATURE_IEEE_802_11_AX},
	}, {
		Name:              "wpa3",
		Fixture:           defaultFixture,
		ExtraSoftwareDeps: []string{"wpa3_sae"},
		ExtraSoftwareDepsDoc: []string{
			"Not all WiFi chips support SAE. We enable the feature as a Software dependency for now, but eventually",
			"this will require a hardware dependency (crbug.com/1070299).",
		},
		Doc:               simpleConnectDocPref(`an AP in WPA3-SAE ("pure") mode. WiFi alliance requires PMF in this mode.`),
		Val:               mkOps("Required", "PureWPA3", "wpa.KeyMgmtSAE"),
		ExtraRequirements: []string{tdreq.WiFiGenSupportPMF, tdreq.WiFiSecSupportWPA3Personal},
	}, {
		Name:              "wpa3ext",
		Fixture:           defaultFixture,
		ExtraAttr:         []string{"wificell_unstable"},
		ExtraSoftwareDeps: []string{"wpa3_sae"},
		ExtraSoftwareDepsDoc: []string{
			"Not all WiFi chips support SAE. We enable the feature as a Software dependency for now, but eventually",
			"this will require a hardware dependency (crbug.com/1070299).",
		},
		Doc:               simpleConnectDocPref(`an AP in WPA3-SAE-EXT ("pure") mode. WiFi alliance requires PMF in this mode.`),
		Val:               mkOps("Required", "PureWPA3Ext", "wpa.KeyMgmtSAEEXT"),
		ExtraRequirements: []string{tdreq.WiFiGenSupportPMF, tdreq.WiFiSecSupportWPA3Personal},
		// TODO(b/339435290) Refine testbed dep to work for routers w/ AKM24 support.
		DepsWifiRouterFeatures: []api.WifiRouterFeature{api.WifiRouterFeature_WIFI_ROUTER_FEATURE_IEEE_802_11_AX},
	}}
}

func simpleConnectWPAVHT80() *simpleConnectParams {
	return &simpleConnectParams{
		Name:    "wpavht80",
		Fixture: defaultFixture,
		Doc:     simpleConnectDocPref("a protected 802.11ac network supporting for WPA."),
		Val: []simpleConnectParamsVal{{
			APConfigs: []apConfigVal{{
				APOpts: `ap.Mode(ap.Mode80211acPure), ap.Channel(36), ap.HTCaps(ap.HTCapHT40Plus),
					ap.VHTCenterChannel(42), ap.VHTChWidth(ap.VHTChWidth80)`,
				SecConfFac: `wpa.NewConfigFactory(
					"chromeos", wpa.Mode(wpa.ModePureWPA),
					wpa.Ciphers(wpa.CipherTKIP, wpa.CipherCCMP))`,
			}},
			ExpectedSecurity: wpaModeToShillSecurity(`PureWPA`),
		}},
	}
}

func simpleConnectWPAOddPassphrase() *simpleConnectParams {
	var p []simpleConnectParamsVal
	for _, pw := range []string{`"\xe4\xb8\x80\xe4\xba\x8c\xe4\xb8\x89"`, `"abcdef\xc2\xa2"`, `" !\"#$%&'()>*+,-./:;<=>?@[\\]^_{|}~"`} {
		temp := `wpa.NewConfigFactory(
			%s, wpa.Mode(wpa.Mode%s),
			%s,
		)`
		p = append(p, simpleConnectParamsVal{
			APConfigs: []apConfigVal{{
				APOpts:     simpleConnectCommonSecApOpts,
				SecConfFac: fmt.Sprintf(temp, pw, "PureWPA", "wpa.Ciphers(wpa.CipherTKIP)"),
			}},
			ExpectedSecurity: wpaModeToShillSecurity("PureWPA"),
		}, simpleConnectParamsVal{
			APConfigs: []apConfigVal{{
				APOpts:     simpleConnectCommonSecApOpts,
				SecConfFac: fmt.Sprintf(temp, pw, "PureWPA2", "wpa.Ciphers2(wpa.CipherCCMP)"),
			}},
			ExpectedSecurity: wpaModeToShillSecurity("PureWPA2"),
		})
	}
	return &simpleConnectParams{
		Name:    "wpaoddpassphrase",
		Fixture: defaultFixture,
		Doc:     simpleConnectDocPref("a protected network whose WPA passphrase can be pure unicode, mixed unicode and ASCII, and all the punctuations."),
		Val:     p,
	}
}

func simpleConnectWPAHidden() *simpleConnectParams {
	var p []simpleConnectParamsVal
	for _, c := range []struct{ mode, cipher string }{
		{"PureWPA", "wpa.Ciphers(wpa.CipherTKIP)"},
		{"PureWPA", "wpa.Ciphers(wpa.CipherTKIP, wpa.CipherCCMP)"},
		{"PureWPA2", "wpa.Ciphers2(wpa.CipherCCMP)"},
		{"Mixed", "wpa.Ciphers(wpa.CipherTKIP, wpa.CipherCCMP), wpa.Ciphers2(wpa.CipherCCMP)"},
	} {
		p = append(p, simpleConnectParamsVal{
			APConfigs: []apConfigVal{{
				APOpts: simpleConnectCommonSecApOpts + ", ap.Hidden()",
				SecConfFac: fmt.Sprintf(`wpa.NewConfigFactory(
					"chromeos", wpa.Mode(wpa.Mode%s),
					%s,
				)`, c.mode, c.cipher),
			}},
			ExpectedSecurity: wpaModeToShillSecurity(c.mode),
		})
	}
	return &simpleConnectParams{
		Name:    "wpahidden",
		Fixture: defaultFixture,
		Doc:     simpleConnectDocPref("a hidden network supporting for WPA with TKIP, WPA with TKIP/AES, WPA2 with AES, and mixed WPA with TKIP/AES and WPA2 with AES."),
		Val:     p,
	}
}

func simpleConnectRawPMK() *simpleConnectParams {
	return &simpleConnectParams{
		Name:    "raw_pmk",
		Fixture: defaultFixture,
		Doc:     simpleConnectDocPref("a WPA network using a raw PMK value instead of an ASCII passphrase."),
		Val: []simpleConnectParamsVal{{
			APConfigs: []apConfigVal{{
				APOpts: simpleConnectCommonSecApOpts,
				SecConfFac: `wpa.NewConfigFactory(
					strings.Repeat("0123456789abcdef", 4), // length = 64.
					wpa.Mode(wpa.ModePureWPA),
					wpa.Ciphers(wpa.CipherTKIP))`,
			}},
			ExpectedSecurity: wpaModeToShillSecurity(`PureWPA`),
		}},
	}
}

func simpleConnectDFS() []*simpleConnectParams {
	return []*simpleConnectParams{{
		Name:    "dfs",
		Fixture: defaultFixture,
		Doc: append(simpleConnectDocPref("an open network on a DFS channel."),
			"DFS (dynamic frequency selection) channels are channels that may be unavailable if radar interference is detected.",
			"See: https://en.wikipedia.org/wiki/Dynamic_frequency_selection, https://en.wikipedia.org/wiki/List_of_WLAN_channels"),
		ExtraAttr: []string{"wificell_cq"},
		Val: []simpleConnectParamsVal{
			{APConfigs: []apConfigVal{{APOpts: "ap.Mode(ap.Mode80211nMixed), ap.Channel(120), ap.HTCaps(ap.HTCapHT40), ap.SpectrumManagement()"}}},
			{APConfigs: []apConfigVal{{APOpts: "ap.Mode(ap.Mode80211nMixed), ap.Channel(136), ap.HTCaps(ap.HTCapHT40), ap.SpectrumManagement()"}}},
		},
	}}
}

func simpleConnectSSIDLimits() *simpleConnectParams {
	return &simpleConnectParams{
		Name:      "ssid_limits",
		Fixture:   defaultFixture,
		Doc:       simpleConnectDocPref("a networks with the longest and shortest SSID."),
		ExtraAttr: []string{"wificell_cq"},
		Val: []simpleConnectParamsVal{
			{APConfigs: []apConfigVal{{CommonAPOptions: `ap.SSID("a")`}}},
			{APConfigs: []apConfigVal{{CommonAPOptions: `ap.SSID(strings.Repeat("MaxLengthSSID", 4)[:32])`}}},
		},
	}
}

func simpleConnectNonASCIISSID() *simpleConnectParams {
	return &simpleConnectParams{
		Name:                 "non_ascii_ssid",
		Fixture:              defaultFixture,
		Doc:                  []string{"This test case verifies that the DUT accepts ascii and non-ascii type characters as the SSID."},
		ExtraHardwareDepsDoc: []string{"TODO(b/158150763): Skip Marvell WiFi as there's a known issue to make the test always fail."},
		ExtraHardwareDeps:    "hwdep.D(hwdep.WifiNotMarvell())",
		Val: []simpleConnectParamsVal{
			{
				Doc: []string{
					"TODO(crbug.com/1082582): shill don't allow leading 0x00 now, so let's append it in the",
					"end to keep the coverage.",
				},
				APConfigs: []apConfigVal{{CommonAPOptions: `ap.SSID(byteSequenceStr(1, 31) + "\x00")`}},
			},
			{APConfigs: []apConfigVal{{CommonAPOptions: "ap.SSID(byteSequenceStr(32, 63))"}}},
			{APConfigs: []apConfigVal{{CommonAPOptions: "ap.SSID(byteSequenceStr(64, 95))"}}},
			{APConfigs: []apConfigVal{{CommonAPOptions: "ap.SSID(byteSequenceStr(96, 127))"}}},
			{APConfigs: []apConfigVal{{CommonAPOptions: "ap.SSID(byteSequenceStr(128, 159))"}}},
			{APConfigs: []apConfigVal{{CommonAPOptions: "ap.SSID(byteSequenceStr(160, 191))"}}},
			{APConfigs: []apConfigVal{{CommonAPOptions: "ap.SSID(byteSequenceStr(192, 223))"}}},
			{APConfigs: []apConfigVal{{CommonAPOptions: "ap.SSID(byteSequenceStr(224, 255))"}}},
			{
				Doc:       []string{"Valid Unicode characters."},
				APConfigs: []apConfigVal{{CommonAPOptions: `ap.SSID("\xe4\xb8\xad\xe5\x9b\xbd")`}},
			},
			{
				Doc:       []string{"Single extended ASCII character (a-grave)."},
				APConfigs: []apConfigVal{{CommonAPOptions: `ap.SSID("\xe0")`}},
			},
			{
				Doc:       []string{"Mix of ASCII and Unicode characters as SSID."},
				APConfigs: []apConfigVal{{CommonAPOptions: `ap.SSID("Chrome\xe7\xac\x94\xe8\xae\xb0\xe6\x9c\xac")`}},
			},
		},
	}
}

func simpleConnect8021xWEP() *simpleConnectParams {
	return &simpleConnectParams{
		Name:    "8021xwep",
		Fixture: defaultFixture,
		Doc:     simpleConnectDocPref("a protected network supporting for dynamic WEP encryption."),
		ExtraHardwareDepsDoc: []string{"Skip on Marvell because of 8021xwep test failure post security fixes b/187853331, no plans to fix.",
			"Skip on trogdor and strongbad board because of 8021xwep test regression post Qualcomm FW746 b/194644867,",
			"Qualcomm looks at the security fixes in the FW.",
			"TODO(b/194644867): revisit after FW fix and verification."},
		ExtraHardwareDeps: `hwdep.D(hwdep.WifiNotMarvell(), hwdep.SkipOnPlatform("trogdor", "strongbad", "trogdor-kernelnext"), hwdep.WifiWEP())`,
		ExtraRequirements: []string{tdreq.WiFiSecSupportWEP},
		Val: []simpleConnectParamsVal{{
			APConfigs: []apConfigVal{{
				APOpts: simpleConnectCommonSecApOpts,
				SecConfFac: `dynamicwep.NewConfigFactory(
					eapCert1.CACred.Cert, eapCert1.ServerCred,
					dynamicwep.ClientCACert(eapCert1.CACred.Cert),
					dynamicwep.ClientCred(eapCert1.ClientCred),
					dynamicwep.RekeyPeriod(10))`,
			}},
			PingOps:          "ping.Count(15), ping.Interval(1)",
			ExpectedSecurity: `shillconst.SecurityWEP`,
		}},
	}
}

func simpleConnect8021xWPA() *simpleConnectParams {
	return &simpleConnectParams{
		Name:                 "8021xwpa",
		Fixture:              defaultFixture,
		Doc:                  simpleConnectDocPref("a protected network supporting for WPA-EAP encryption."),
		ExtraHardwareDepsDoc: []string{"TODO(b/189986748): Remove the skiplist once those flaky boards have reached AUE."},
		ExtraHardwareDeps:    `hwdep.D(hwdep.SkipOnPlatform("banjo", "candy", "gnawty", "kip", "ninja", "sumo", "swanky", "winky"))`,
		ExtraRequirements:    []string{tdreq.WiFiSecSupportWPA2Enterprise},
		Val: []simpleConnectParamsVal{{
			APConfigs: []apConfigVal{{
				APOpts: simpleConnectCommonSecApOpts,
				SecConfFac: `wpaeap.NewConfigFactory(
					eapCert1.CACred.Cert, eapCert1.ServerCred,
					wpaeap.ClientCACert(eapCert1.CACred.Cert),
					wpaeap.ClientCred(eapCert1.ClientCred))`,
			}},
			ExpectedSecurity: `shillconst.SecurityWPAEnterprise`,
		}, {
			Doc: []string{"Failure due to lack of CACert on client."},
			APConfigs: []apConfigVal{{
				APOpts: simpleConnectCommonSecApOpts,
				SecConfFac: `wpaeap.NewConfigFactory(
					eapCert1.CACred.Cert, eapCert1.ServerCred,
					wpaeap.ClientCred(eapCert1.ClientCred))`,
			}},
			ExpectedFailure: true,
		}, {
			Doc: []string{"Failure due to unmatched CACert."},
			APConfigs: []apConfigVal{{
				APOpts: simpleConnectCommonSecApOpts,
				SecConfFac: `wpaeap.NewConfigFactory(
					eapCert1.CACred.Cert, eapCert1.ServerCred,
					wpaeap.ClientCACert(eapCert2.CACred.Cert),
					wpaeap.ClientCred(eapCert1.ClientCred))`,
			}},
			ExpectedFailure: true,
		}, {
			Doc: []string{"Should succeed if we specify that we have no CACert."},
			APConfigs: []apConfigVal{{
				APOpts: simpleConnectCommonSecApOpts,
				SecConfFac: `wpaeap.NewConfigFactory(
					eapCert1.CACred.Cert, eapCert1.ServerCred,
					wpaeap.ClientCred(eapCert1.ClientCred),
					wpaeap.NotUseSystemCAs())`,
			}},
			ExpectedSecurity: `shillconst.SecurityWPAEnterprise`,
		}, {
			Doc: []string{"Failure due to wrong certificate chain on client."},
			APConfigs: []apConfigVal{{
				APOpts: simpleConnectCommonSecApOpts,
				SecConfFac: `wpaeap.NewConfigFactory(
					eapCert1.CACred.Cert, eapCert1.ServerCred,
					wpaeap.ClientCACert(eapCert1.CACred.Cert),
					wpaeap.ClientCred(eapCert2.ClientCred))`,
			}},
			ExpectedFailure: true,
		}, {
			Doc: []string{"Failure due to expired cert on server."},
			APConfigs: []apConfigVal{{
				APOpts: simpleConnectCommonSecApOpts,
				SecConfFac: `wpaeap.NewConfigFactory(
					eapCert1.CACred.Cert, eapCert1.ExpiredServerCred,
					wpaeap.ClientCACert(eapCert1.CACred.Cert),
					wpaeap.ClientCred(eapCert1.ClientCred))`,
			}},
			ExpectedFailure: true,
		}},
	}
}

func simpleConnect8021xWPA3() []*simpleConnectParams {
	mkOps := func(pmf, mode string) []simpleConnectParamsVal {
		return []simpleConnectParamsVal{{
			APConfigs: []apConfigVal{{
				APOpts: fmt.Sprintf("%s, ap.PMF(ap.PMF%s)", simpleConnectCommonSecApOpts, pmf),
				SecConfFac: fmt.Sprintf(`wpaeap.NewConfigFactory(
					eapCert1.CACred.Cert, eapCert1.ServerCred,
					wpaeap.ClientCACert(eapCert1.CACred.Cert),
					wpaeap.ClientCred(eapCert1.ClientCred),
					wpaeap.Mode(wpa.Mode%s),
				)`, mode),
			}},
			ExpectedSecurity: wpaModeToShillEntSecurity(mode),
		}}
	}
	return []*simpleConnectParams{
		{
			Name:              "8021xwpa3mixed",
			Fixture:           defaultFixture,
			Doc:               simpleConnectDocPref("an WPA3-Enterprise-transition AP"),
			Val:               mkOps("Optional", "MixedWPA3"),
			ExtraRequirements: []string{tdreq.WiFiSecSupportWPA3Enterprise},
		},
		{
			Name:              "8021xwpa3",
			Fixture:           defaultFixture,
			Doc:               simpleConnectDocPref("an WPA3-Enterprise-only AP"),
			Val:               mkOps("Required", "PureWPA3"),
			ExtraRequirements: []string{tdreq.WiFiSecSupportWPA3Enterprise},
		},
	}

}

func simpleConnectTunneled1x() []*simpleConnectParams {
	mkP := func(outer, inner string, extraAttr []string) *simpleConnectParams {
		ret := &simpleConnectParams{
			Name:      "8021x" + strings.ToLower(outer) + "_" + strings.ToLower(inner),
			Fixture:   defaultFixture,
			Doc:       []string{fmt.Sprintf("Verifies that DUT can connect to a protected network supporting for %s authentication with tunneled %s.", outer, inner)},
			ExtraAttr: extraAttr,
		}
		ret.Val = append(ret.Val, simpleConnectParamsVal{
			APConfigs: []apConfigVal{{
				APOpts: simpleConnectCommonSecApOpts,
				SecConfFac: fmt.Sprintf(`tunneled1x.NewConfigFactory(
					eapCert1.CACred.Cert, eapCert1.ServerCred, eapCert1.CACred.Cert, "testuser", "password",
					tunneled1x.Mode(wpa.ModePureWPA2),
					tunneled1x.OuterProtocol(tunneled1x.Layer1Type%s),
					tunneled1x.InnerProtocol(tunneled1x.Layer2Type%s),
				)`, outer, inner),
			}},
			ExpectedSecurity: wpaModeToShillEntSecurity("PureWPA2"),
		})
		for i := 0; i < 3; i++ {
			ret.Val = append(ret.Val, simpleConnectParamsVal{
				APConfigs: []apConfigVal{{
					APOpts: simpleConnectCommonSecApOpts,
					SecConfFac: fmt.Sprintf(`tunneled1x.NewConfigFactory(
						eapCert3.CACred.Cert, eapCert3.ServerCred, eapCert3.CACred.Cert, "testuser", "password",
						tunneled1x.OuterProtocol(tunneled1x.Layer1Type%s),
						tunneled1x.InnerProtocol(tunneled1x.Layer2Type%s),
						tunneled1x.AltSubjectMatch([]string{eapCert3AltSub[%d]}),
					)`, outer, inner, i),
				}},
				ExpectedSecurity: wpaModeToShillEntSecurity("PureWPA"),
			})
		}
		ret.Val = append(ret.Val, simpleConnectParamsVal{
			Doc: []string{
				"Should success since having multiple entries in 'altsubject_match' is treated as OR, not AND.",
				"For more information about how wpa_supplicant uses altsubject_match field:",
				"https://w1.fi/cgit/hostap/plain/wpa_supplicant/wpa_supplicant.conf",
			},
			APConfigs: []apConfigVal{{
				APOpts: simpleConnectCommonSecApOpts,
				SecConfFac: fmt.Sprintf(`tunneled1x.NewConfigFactory(
					eapCert3.CACred.Cert, eapCert3.ServerCred, eapCert3.CACred.Cert, "testuser", "password",
					tunneled1x.OuterProtocol(tunneled1x.Layer1Type%s),
					tunneled1x.InnerProtocol(tunneled1x.Layer2Type%s),
					tunneled1x.AltSubjectMatch([]string{`+"`"+`{"Type":"DNS","Value":"wrong_dns.com"}`+"`"+`, eapCert3AltSub[0]}),
				)`, outer, inner),
			}},
			ExpectedSecurity: wpaModeToShillEntSecurity("PureWPA"),
		})
		ret.Val = append(ret.Val, simpleConnectParamsVal{
			APConfigs: []apConfigVal{{
				APOpts: simpleConnectCommonSecApOpts,
				SecConfFac: fmt.Sprintf(`tunneled1x.NewConfigFactory(
					eapCert3.CACred.Cert, eapCert3.ServerCred, eapCert3.CACred.Cert, "testuser", "password",
					tunneled1x.OuterProtocol(tunneled1x.Layer1Type%s),
					tunneled1x.InnerProtocol(tunneled1x.Layer2Type%s),
					tunneled1x.DomainSuffixMatch(eapCert3DomainSuffix),
				)`, outer, inner),
			}},
			ExpectedSecurity: wpaModeToShillEntSecurity("PureWPA"),
		})
		ret.Val = append(ret.Val, simpleConnectParamsVal{
			Doc: []string{
				"Should succeed since having multiple entries in 'domain_suffix_match' is treated as OR, not AND.",
				"For more information about how wpa_supplicant uses domain_suffix_match field:",
				"https://w1.fi/cgit/hostap/plain/wpa_supplicant/wpa_supplicant.conf",
			},
			APConfigs: []apConfigVal{{
				APOpts: simpleConnectCommonSecApOpts,
				SecConfFac: fmt.Sprintf(`tunneled1x.NewConfigFactory(
					eapCert3.CACred.Cert, eapCert3.ServerCred, eapCert3.CACred.Cert, "testuser", "password",
					tunneled1x.OuterProtocol(tunneled1x.Layer1Type%s),
					tunneled1x.InnerProtocol(tunneled1x.Layer2Type%s),
					tunneled1x.DomainSuffixMatch([]string{"wrongdomain1.com", eapCert3DomainSuffix[0], "wrongdomain1.com"}),
				)`, outer, inner),
			}},
			ExpectedSecurity: wpaModeToShillEntSecurity("PureWPA"),
		})
		return ret
	}
	mkPFail := func(outer, inner string, extraAttr []string) *simpleConnectParams {
		ret := &simpleConnectParams{
			Name:    "8021x" + strings.ToLower(outer) + "_fail",
			Fixture: defaultFixture,
			Doc: []string{
				fmt.Sprintf("Verifies that DUT CANNOT connect to a %s network with wrong settings.", outer),
				"We do these tests for only one inner authentication protocol because we",
				"presume that supplicant reuses this code between inner authentication types."},
			ExtraAttr: extraAttr,
		}
		for _, v := range []struct{ doc, sec string }{
			{"Failure due to bad password.",
				`tunneled1x.NewConfigFactory(
				eapCert1.CACred.Cert, eapCert1.ServerCred, eapCert1.CACred.Cert, "testuser", "password",
				tunneled1x.OuterProtocol(tunneled1x.Layer1Type%s),
				tunneled1x.InnerProtocol(tunneled1x.Layer2Type%s),
				tunneled1x.ClientPassword("wrongpassword"),
			)`},
			{"Failure due to wrong client CA.",
				`tunneled1x.NewConfigFactory(
				eapCert1.CACred.Cert, eapCert1.ServerCred, eapCert2.CACred.Cert, "testuser", "password",
				tunneled1x.OuterProtocol(tunneled1x.Layer1Type%s),
				tunneled1x.InnerProtocol(tunneled1x.Layer2Type%s),
			)`},
			{"Failure due to expired server cred.",
				`tunneled1x.NewConfigFactory(
				eapCert1.CACred.Cert, eapCert1.ExpiredServerCred, eapCert1.CACred.Cert, "testuser", "password",
				tunneled1x.OuterProtocol(tunneled1x.Layer1Type%s),
				tunneled1x.InnerProtocol(tunneled1x.Layer2Type%s),
			)`},
			{"Failure due to that a subject alternative name (SAN) is set but does not match any of the server certificate SANs.",
				`tunneled1x.NewConfigFactory(
				eapCert3.CACred.Cert, eapCert3.ServerCred, eapCert3.CACred.Cert, "testuser", "password",
				tunneled1x.OuterProtocol(tunneled1x.Layer1Type%s),
				tunneled1x.InnerProtocol(tunneled1x.Layer2Type%s),
				tunneled1x.AltSubjectMatch([]string{` + "`" + `{"Type":"DNS","Value":"wrong_dns.com"}` + "`" + `}),
			)`},
			{"Failure due the set domain suffix match that does not match any of the dNSName in the server certificate SANs.",
				`tunneled1x.NewConfigFactory(
				eapCert3.CACred.Cert, eapCert3.ServerCred, eapCert3.CACred.Cert, "testuser", "password",
				tunneled1x.OuterProtocol(tunneled1x.Layer1Type%s),
				tunneled1x.InnerProtocol(tunneled1x.Layer2Type%s),
				tunneled1x.DomainSuffixMatch([]string{"wrongdomain.com"}),
			)`},
		} {
			ret.Val = append(ret.Val, simpleConnectParamsVal{
				Doc: []string{v.doc},
				APConfigs: []apConfigVal{{
					APOpts:     simpleConnectCommonSecApOpts,
					SecConfFac: fmt.Sprintf(v.sec, outer, inner),
				}},
				ExpectedFailure: true,
			})
		}
		return ret
	}
	return []*simpleConnectParams{
		mkPFail("PEAP", "MSCHAPV2", nil),
		mkP("PEAP", "MSCHAPV2", nil),
		mkP("PEAP", "MD5", nil),
		mkP("PEAP", "GTC", nil),
		mkPFail("TTLS", "MD5", nil),
		mkP("TTLS", "MSCHAPV2", nil),
		mkP("TTLS", "MD5", nil),
		mkP("TTLS", "GTC", nil),
		mkP("TTLS", "TTLSMSCHAPV2", nil),
		mkP("TTLS", "TTLSMSCHAP", nil),
		mkP("TTLS", "TTLSPAP", nil),
	}
}

func simpleConnectPower() []*simpleConnectParams {
	const powerFixture string = `wificell.FixtureID(wificell.TFFeaturesPower)`
	return []*simpleConnectParams{{
		Name:      "powern",
		Fixture:   powerFixture,
		Doc:       simpleConnectDocPref("an open 802.11n network on 5 GHz channel and records power measurements."),
		ExtraAttr: []string{"wificell_unstable"},
		Val: []simpleConnectParamsVal{{
			APConfigs: []apConfigVal{{APOpts: "ap.Mode(ap.Mode80211nPure), ap.Channel(48), ap.HTCaps(ap.HTCapHT20)"}},
			PingOps:   "ping.Count(100), ping.Interval(1)",
		}},
	}, {
		Name:      "powerac",
		Fixture:   powerFixture,
		Doc:       simpleConnectDocPref("an open 802.11ac network on channel 40 with a channel width of 20MHz and records power measurements."),
		ExtraAttr: []string{"wificell_unstable"},
		Val: []simpleConnectParamsVal{{
			APConfigs: []apConfigVal{{APOpts: `ap.Mode(ap.Mode80211acPure), ap.Channel(40), ap.HTCaps(ap.HTCapHT20), ap.VHTChWidth(ap.VHTChWidth20Or40)`}},
			PingOps:   "ping.Count(100), ping.Interval(1)",
		}},
	}}
}

func TestSimpleConnect(t *testing.T) {
	var ps []*simpleConnectParams
	ps = append(ps, simpleConnect80211abg()...)
	ps = append(ps, simpleConnect80211n()...)
	ps = append(ps, simpleConnect80211nsgi())
	ps = append(ps, simpleConnect80211ac()...)
	ps = append(ps, simpleConnect80211ax()...)
	ps = append(ps, simpleConnect80211axe()...)
	ps = append(ps, simpleConnect80211be()...)
	ps = append(ps, simpleConnectGCMP()...)
	ps = append(ps, simpleConnectOWE()...)
	ps = append(ps, simpleConnectHidden()...)
	ps = append(ps, simpleConnectWEP()...)
	ps = append(ps, simpleConnectWEPHidden())
	ps = append(ps, simpleConnectWPA()...)
	ps = append(ps, simpleConnectWPA3()...)
	ps = append(ps, simpleConnectWPAVHT80())
	ps = append(ps, simpleConnectWPAOddPassphrase())
	ps = append(ps, simpleConnectWPAHidden())
	ps = append(ps, simpleConnectRawPMK())
	ps = append(ps, simpleConnectDFS()...)
	ps = append(ps, simpleConnectSSIDLimits())
	ps = append(ps, simpleConnectNonASCIISSID())
	ps = append(ps, simpleConnect8021xWEP())
	ps = append(ps, simpleConnect8021xWPA())
	ps = append(ps, simpleConnect8021xWPA3()...)
	ps = append(ps, simpleConnectTunneled1x()...)
	ps = append(ps, simpleConnectPower()...)

	for _, p := range ps {
		p.ExtraTestBedDeps = append(p.ExtraTestBedDeps, tbdep.WifiRouterFeatures(p.DepsWifiRouterFeatures...)...)
	}

	genparams.Ensure(t, "simple_connect.go", genparams.Template(t, `{{ range . }}{
	{{ range .Doc }}
	// {{ . }}
	{{ end }}
	Name: {{ .Name | fmt }},
	{{ if .Fixture }}
	Fixture: {{ .Fixture }},
	{{ end }}
	{{ if .ExtraAttr }}
	ExtraAttr: {{ .ExtraAttr | fmt }},
	{{ end }}
	{{ if .ExtraSoftwareDeps }}
	{{ range .ExtraSoftwareDepsDoc }}
	// {{ . }}
	{{ end }}
	ExtraSoftwareDeps: {{ .ExtraSoftwareDeps | fmt }},
	{{ end }}
	Val:  []simpleConnectTestcase{ {{ range .Val }} {
		{{ range .Doc }}
		// {{ . }}
		{{ end }}
		apConfigs: []ap.ApConfig{ {{ range .APConfigs }} {
			{{ if .APOpts }}
			ApOpts: []ap.Option{ {{ .APOpts }} },
			{{ else if .CommonAPOptions }}
			ApOpts: wifiutil.CommonAPOptions({{ .CommonAPOptions }}),
			{{ end }}
			{{ if .SecConfFac }}
			SecConfFac: {{ .SecConfFac }},
			{{ end }}
		}, {{ end }} },
		{{ if .UseSameSSID }}
		useSameSSID: true,
		{{ end }}
		{{ if .PingOps }}
		pingOps: []ping.Option{ {{ .PingOps }} },
		{{ end }}
		{{ if .ExpectedFailure }}
		expectedFailure: true,
		{{ end }}
		{{ if .ExpectedSecurity }}
		expectedSecurity: {{ .ExpectedSecurity }},
		{{ end }}
		{{ if .ExpectedKeyMgmt }}
		expectedKeyMgmt: {{ .ExpectedKeyMgmt }},
		{{ end }}
	}, {{ end }} },
	{{ if .ExtraHardwareDeps }}
	{{ range .ExtraHardwareDepsDoc }}
	// {{ . }}
	{{ end }}
	ExtraHardwareDeps: {{ .ExtraHardwareDeps }},
	{{ end }}
	{{ if .ExtraRequirements }}
	ExtraRequirements: {{ .ExtraRequirements | fmt }},
	{{ end }}
	{{ if .ExtraTestBedDeps }}
	ExtraTestBedDeps: {{ .ExtraTestBedDeps | fmt }},
	{{ end }}
},{{ end }}`, ps))
}
