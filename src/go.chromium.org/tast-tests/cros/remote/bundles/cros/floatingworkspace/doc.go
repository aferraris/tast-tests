// Copyright 2023 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package floatingworkspace contains remote Tast tests that exercise ChromeOS Floating Workspace.
package floatingworkspace
