// Copyright 2024 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package ex350 provides utilities for communicating with ex350 compliance testers.
package ex350
