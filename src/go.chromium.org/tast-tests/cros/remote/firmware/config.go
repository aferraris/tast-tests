// Copyright 2020 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// This file implements Config, a collection of platform-specific attributes used for FW testing.

package firmware

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"path/filepath"
	"regexp"
	"strings"
	"time"

	"go.chromium.org/tast/core/errors"
)

// ModeSwitcherType represents which methods the platform uses for switching between DUT boot modes.
type ModeSwitcherType string

// Currently, there are exactly three possible values for ModeSwitcherType.
const (
	KeyboardDevSwitcher      ModeSwitcherType = "keyboard_dev_switcher"
	MenuSwitcher             ModeSwitcherType = "menu_switcher"
	TabletDetachableSwitcher ModeSwitcherType = "tablet_detachable_switcher"
)

// ECCapability represents one feature that the EC can control.
type ECCapability string

// These are the ECCapabilities currently described in fw-testing-configs.
const (
	ECARM      ECCapability = "arm"
	ECBattery  ECCapability = "battery"
	ECCBI      ECCapability = "cbi"
	ECCharging ECCapability = "charging"
	ECKeyboard ECCapability = "keyboard"
	ECLid      ECCapability = "lid"
	ECX86      ECCapability = "x86"
)

// MiniDiagCapability represents one feature that the MiniDiag contains.
type MiniDiagCapability string

// These are the MiniDiagCapabilities currently described in fw-testing-configs.
const (
	CbmemPreservedByAPReset MiniDiagCapability = "cbmem_preserved_by_ap_reset"
	EventLogLaunchCount     MiniDiagCapability = "event_log_launch_count"
	EventLogTestReport      MiniDiagCapability = "event_log_test_report"
)

// USBEnablePin represents each object in the list in the config files for the key "custom_usb_enable_pins".
type USBEnablePin struct {
	Name      string
	Ioex      bool
	ActiveLow bool
}

// ConfigFile is the relative path from data/ to the .json file containing all platforms' config data.
// It is intended to be used in the Data field of a testing.Test declaration.
var ConfigFile = "CONSOLIDATED.json"

// defaultName is the name of the CONSOLIDATED.json attribute containing default values.
const defaultName = "DEFAULTS"

// Config contains platform-specific attributes.
// Fields are documented in autotest/server/cros/faft/configs/DEFAULTS.json.
type Config struct {
	ACOnCanWakeApFromUlp             bool                 `json:"ac_on_can_wake_ap_from_ulp"`
	ChargerProfileOverride           bool                 `json:"charger_profile_override"`
	ChromeEC                         bool                 `json:"chrome_ec"`
	ECCapability                     []ECCapability       `json:"ec_capability"`
	GSCCanWakeECWithReset            bool                 `json:"gsc_can_wake_ec_with_reset"`
	HasKeyboard                      bool                 `json:"has_keyboard"`
	Hibernate                        bool                 `json:"hibernate"`
	IsDetachable                     bool                 `json:"is_detachable"`
	LidWakeFromPowerOff              bool                 `json:"lid_wake_from_power_off"`
	MiniDiagCapability               []MiniDiagCapability `json:"minidiag_capability"`
	MiniDiagEnabled                  bool                 `json:"minidiag_enabled"`
	MiniOSEnabled                    bool                 `json:"minios_enabled"`
	ModeSwitcherType                 ModeSwitcherType     `json:"mode_switcher_type"`
	NoBrokenScreenInDev              bool                 `json:"no_broken_screen_in_dev"`
	Parent                           string               `json:"parent"`
	Platform                         string               `json:"platform"`
	PowerButtonDevSwitch             bool                 `json:"power_button_dev_switch"`
	RawUSBEnablePins                 []json.RawMessage    `json:"custom_usb_enable_pins"`
	RecButtonDevSwitch               bool                 `json:"rec_button_dev_switch"`
	RecForceMRC                      bool                 `json:"rec_force_mrc"`
	SMMStore                         bool                 `json:"smm_store"`
	USBAPortCount                    *int                 `json:"usb_a_port_count"`
	UsbcInputVoltageLimit            int                  `json:"usbc_input_voltage_limit"`
	MaxChargingPower                 float64              `json:"max_charging_power"`
	UsbcVoltageOnShutdownAndFullBatt int                  `json:"usbc_voltage_on_shutdown_and_full_batt"`

	// Raw duration fields represent a quantity of seconds.
	// They are used during NewConfig to populate actual duration fields, which are defined below.
	// Generally, these raw fields should not be accessed by tests and libraries.
	// Nevertheless, the raw fields must be exported in order for them to be set by json.Unmarshal.
	RawDelayRebootToPing             float64 `json:"delay_reboot_to_ping"`
	RawUSBImageBootTimeout           float64 `json:"usb_image_boot_timeout"`
	RawECBootToPwrButton             float64 `json:"ec_boot_to_pwr_button"`
	RawFirmwareScreen                float64 `json:"firmware_screen"`
	RawFirmwareScreenRecMode         float64 `json:"firmware_screen_rec_mode"`
	RawHoldPwrButtonNoPowerdShutdown float64 `json:"hold_pwr_button_nopowerd_shutdown"`
	RawHoldPwrButtonPowerOff         float64 `json:"hold_pwr_button_poweroff"`
	RawHoldPwrButtonPowerOn          float64 `json:"hold_pwr_button_poweron"`
	RawKeypressDelay                 float64 `json:"keypress_delay"`
	RawShutdown                      float64 `json:"shutdown"`
	RawShutdownTimeout               float64 `json:"shutdown_timeout"`
	RawSoftwareSyncUpdate            float64 `json:"software_sync_update"`
	RawUSBPlug                       float64 `json:"usb_plug"`
	RawRecInvalidScreen              float64 `json:"rec_invalid_screen"`
	RawMiniOSScreen                  float64 `json:"minios_screen"`
	RawECWatchdogPeriod              float64 `json:"ec_watchdog_period"`
	RawECWatchdogLeadingTime         float64 `json:"ec_watchdog_warning_leading_time"`

	// Actual duration fields are populated during NewConfig based on raw JSON duration values, defined above.
	DelayRebootToPing             time.Duration
	USBImageBootTimeout           time.Duration
	ECBootToPwrButton             time.Duration
	FirmwareScreen                time.Duration
	FirmwareScreenRecMode         time.Duration
	HoldPwrButtonNoPowerdShutdown time.Duration
	HoldPwrButtonPowerOff         time.Duration
	HoldPwrButtonPowerOn          time.Duration
	KeypressDelay                 time.Duration
	RecInvalidScreen              time.Duration
	MiniOSScreen                  time.Duration
	ECWatchdogPeriod              time.Duration
	ECWatchdogLeadingTime         time.Duration
	// Shutdown is supposed to be the time the DUT takes to power off.
	//
	// Deprecated: Do not use this, just wait for G3/S5 power states instead.
	Shutdown time.Duration
	// ShutdownTimeout is the max time a DUT might take to power off. Use this when verifying that a DUT did not power off.
	// I.e. Make sure that after ShutdownTimeout the power state is still S0 or ssh is working, depending on the circumstance.
	ShutdownTimeout    time.Duration
	SoftwareSyncUpdate time.Duration
	USBPlug            time.Duration

	// Models maps DUT model names to overriding config JSON objects.
	Models map[string]json.RawMessage `json:"models"`

	// Holds a list of objects under the "custom_usb_enable_pins" key in the configs.
	USBEnablePins []USBEnablePin
}

// CfgPlatformFromLSBBoard interprets a board name that would come from /etc/lsb-release, and returns the name of the platform whose config should be loaded.
func CfgPlatformFromLSBBoard(board string) string {
	// Remove hyphenated suffixes: ex. "samus-kernelnext" becomes "samus"
	board = strings.SplitN(board, "-", 2)[0]
	// If the board name is given as board_variant, take just the variant: ex. "veyron_minnie" becomes "minnie"
	board = strings.Split(board, "_")[strings.Count(board, "_")]
	return board
}

// loadBytes reads '${platform}.json' from configDataDir and returns it as a slice of bytes.
func loadBytes(configDataDir, platform string) ([]byte, error) {
	fp := filepath.Join(configDataDir, fmt.Sprintf("%s.json", platform))
	b, err := ioutil.ReadFile(fp)
	if err != nil {
		return nil, errors.Wrapf(err, "reading datafile %s", fp)
	}
	return b, nil
}

// parentFromBytes finds the name of the parent platform referenced by a config's JSON bytes.
func parentFromBytes(b []byte) (string, error) {
	var cfg Config
	if err := json.Unmarshal(b, &cfg); err != nil {
		return "", errors.Wrapf(err, "unmarshaling json bytes %s", b)
	}
	if cfg.Parent == "" && cfg.Platform != "" {
		return defaultName, nil
	}
	return cfg.Parent, nil
}

// NewConfig creates a new Config matching the DUT platform.
// cfgFilepath should take s.DataPath(firmware.ConfigFile).
func NewConfig(cfgFilepath, board, model string) (*Config, error) {
	// Load CONSOLIDATED.json as bytes
	b, err := ioutil.ReadFile(cfgFilepath)
	if err != nil {
		return nil, errors.Wrapf(err, "reading config file %s", cfgFilepath)
	}

	// Unmarshal consolidated JSON bytes onto a map from platform name to raw JSON
	var consolidated map[string]json.RawMessage
	if err := json.Unmarshal(b, &consolidated); err != nil {
		return nil, errors.Wrapf(err, "unmarshaling consolidated JSON bytes %s", b)
	}

	// inherits is an array of platform names to be inherited, from most specific (board) to most general (DEFAULTS).
	var inherits []string
	for platform := CfgPlatformFromLSBBoard(board); platform != ""; {
		b, ok := consolidated[platform]
		if !ok {
			return nil, errors.Errorf("consolidated JSON did not contain platform %s", platform)
		}
		parent, err := parentFromBytes(b)
		if err != nil {
			return nil, errors.Wrapf(err, "determining parent from bytes for %s", platform)
		}
		inherits = append(inherits, platform)
		platform = parent
	}

	// Unmarshal JSON bytes in order from most general (DEFAULTS) to most specific (board).
	var cfg Config
	for i := len(inherits) - 1; i >= 0; i-- {
		if err := json.Unmarshal(consolidated[inherits[i]], &cfg); err != nil {
			return nil, errors.Wrapf(err, "failed to unmarshal config for %q", inherits[i])
		}
	}

	// Unmarshal model-level config on top of the existing config.
	// Models are only expected to be defined in the lowest-level (board) config files, not in parent config files.
	if modelCfg, ok := cfg.Models[model]; ok {
		if err := json.Unmarshal(modelCfg, &cfg); err != nil {
			return nil, errors.Wrapf(err, "failed to unmarshal model-level config for %q", model)
		}
	}

	// Populate actual durations based on raw JSON values.
	cfg.DelayRebootToPing = toSeconds(cfg.RawDelayRebootToPing)
	cfg.USBImageBootTimeout = toSeconds(cfg.RawUSBImageBootTimeout)
	cfg.ECBootToPwrButton = toSeconds(cfg.RawECBootToPwrButton)
	cfg.FirmwareScreen = toSeconds(cfg.RawFirmwareScreen)
	cfg.FirmwareScreenRecMode = toSeconds(cfg.RawFirmwareScreenRecMode)
	cfg.HoldPwrButtonNoPowerdShutdown = toSeconds(cfg.RawHoldPwrButtonNoPowerdShutdown)
	cfg.HoldPwrButtonPowerOff = toSeconds(cfg.RawHoldPwrButtonPowerOff)
	cfg.HoldPwrButtonPowerOn = toSeconds(cfg.RawHoldPwrButtonPowerOn)
	cfg.KeypressDelay = toSeconds(cfg.RawKeypressDelay)
	cfg.Shutdown = toSeconds(cfg.RawShutdown)
	cfg.ShutdownTimeout = toSeconds(cfg.RawShutdownTimeout)
	cfg.SoftwareSyncUpdate = toSeconds(cfg.RawSoftwareSyncUpdate)
	cfg.USBPlug = toSeconds(cfg.RawUSBPlug)
	cfg.RecInvalidScreen = toSeconds(cfg.RawRecInvalidScreen)
	cfg.MiniOSScreen = toSeconds(cfg.RawMiniOSScreen)
	cfg.ECWatchdogPeriod = toSeconds(cfg.RawECWatchdogPeriod)
	cfg.ECWatchdogLeadingTime = toSeconds(cfg.RawECWatchdogLeadingTime)
	// Parse list of raw json objects into go structs
	cfg.USBEnablePins = parseRawUSBEnablePins(cfg.RawUSBEnablePins)

	return &cfg, nil
}

// parseRawUSBEnablePins takes the list of json objects and converts them to a list of structs.
func parseRawUSBEnablePins(pinsList []json.RawMessage) []USBEnablePin {
	// Look for ones with non-empty name.
	reNonEmptyName := regexp.MustCompile(`\S+`)
	enablePins := make([]USBEnablePin, 0)
	for _, pin := range pinsList {
		pinStruct := USBEnablePin{}
		json.Unmarshal(pin, &pinStruct)
		if match := reNonEmptyName.FindStringSubmatch(pinStruct.Name); match != nil {
			enablePins = append(enablePins, pinStruct)
		}
	}
	return enablePins
}

// HasECCapability checks whether cfg has a certain ECCapability.
func (cfg *Config) HasECCapability(ecc ECCapability) bool {
	for _, capability := range cfg.ECCapability {
		if ecc == capability {
			return true
		}
	}
	return false
}

// toSeconds casts a float64 to a time.Duration, in seconds.
func toSeconds(f float64) time.Duration {
	// The 1000* factor enables a non-integer f to be cast as a time.Duration, an integer field.
	return time.Duration(1000*f) * time.Millisecond
}

// HasMiniDiagCapability checks whether cfg has a certain MiniDiagCapability.
func (cfg *Config) HasMiniDiagCapability(mdc MiniDiagCapability) bool {
	for _, capability := range cfg.MiniDiagCapability {
		if mdc == capability {
			return true
		}
	}
	return false
}
