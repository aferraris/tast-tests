// Copyright 2020 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package reporters

import (
	"context"
	"fmt"
	"regexp"
	"strings"

	"go.chromium.org/tast/core/errors"
)

// CrossystemParam represents known Crossystem attributes.
type CrossystemParam string

// Crossystem params used by tests, add more as needed.
const (
	CrossystemParamBackupNvramRequest CrossystemParam = "backup_nvram_request"
	CrossystemParamDevBootUsb         CrossystemParam = "dev_boot_usb"
	CrossystemParamDevBootSignedOnly  CrossystemParam = "dev_boot_signed_only"
	CrossystemParamDevDefaultBoot     CrossystemParam = "dev_default_boot"
	CrossystemParamDevBootAltfw       CrossystemParam = "dev_boot_altfw"
	CrossystemParamDevswBoot          CrossystemParam = "devsw_boot"
	CrossystemParamFwid               CrossystemParam = "fwid"
	CrossystemParamFWTryNext          CrossystemParam = "fw_try_next"
	CrossystemParamFWTryCount         CrossystemParam = "fw_try_count"
	CrossystemParamFWUpdatetries      CrossystemParam = "fwupdate_tries"
	CrossystemParamHwid               CrossystemParam = "hwid"
	CrossystemParamKernkeyVfy         CrossystemParam = "kernkey_vfy"
	CrossystemParamLocIdx             CrossystemParam = "loc_idx"
	CrossystemParamMainfwAct          CrossystemParam = "mainfw_act"
	CrossystemParamMainfwType         CrossystemParam = "mainfw_type"
	CrossystemParamMiniOSPriority     CrossystemParam = "minios_priority"
	CrossystemParamRoFwid             CrossystemParam = "ro_fwid"
	CrossystemParamWpswCur            CrossystemParam = "wpsw_cur"
	CrossystemParamRecoveryReason     CrossystemParam = "recovery_reason"
	CrossystemParamRecoveryRequest    CrossystemParam = "recovery_request"
	CrossystemParamFWTried            CrossystemParam = "fw_tried"
	CrossystemParamFWResult           CrossystemParam = "fw_result"
	CrossystemParamFWPrevTried        CrossystemParam = "fw_prev_tried"
	CrossystemParamFWPrevResult       CrossystemParam = "fw_prev_result"
	CrossystemParamTpmFwVer           CrossystemParam = "tpm_fwver"
	CrossystemParamTpmKernelVer       CrossystemParam = "tpm_kernver"
)

var (
	knownCrossystemParams = []CrossystemParam{
		CrossystemParamDevswBoot,
		CrossystemParamFWTryCount,
		CrossystemParamFWTryNext,
		CrossystemParamKernkeyVfy,
		CrossystemParamMainfwAct,
		CrossystemParamMainfwType,
		CrossystemParamWpswCur,
		CrossystemParamRecoveryReason,
		CrossystemParamFWTried,
		CrossystemParamFWResult,
		CrossystemParamFWPrevTried,
		CrossystemParamFWPrevResult,
	}
	rCrossystemLine = regexp.MustCompile(`^([^ =]*) *= *(.*[^ ]) *# [^#]*$`)
)

// Crossystem returns crossystem output as a map.
// Any required params not found in the output will cause an error.
// You must add `SoftwareDeps: []string{"crossystem"},` to your `testing.Test` to use this.
func (r *Reporter) Crossystem(ctx context.Context, requiredKeys ...CrossystemParam) (map[CrossystemParam]string, error) {
	lines, err := r.CommandOutputLines(ctx, "crossystem")
	if err != nil {
		return nil, err
	}

	parsed, err := parseCrossystemOutput(lines)
	if err != nil {
		return nil, errors.Wrap(err, "failed to parse 'crossystem' output")
	}
	for _, k := range requiredKeys {
		if _, found := parsed[k]; !found {
			return parsed, errors.Errorf("required param %q not found in output: %v", k, parsed)
		}
	}
	return parsed, nil
}

// CrossystemParam returns the value of the param from crossystem <param> command.
func (r *Reporter) CrossystemParam(ctx context.Context, param CrossystemParam) (string, error) {
	res, err := r.CommandOutput(ctx, "crossystem", string(param))
	if err != nil {
		return "", err
	}
	return string(res), nil
}

// CrossystemSetParam sets the value of the param with crossystem param=value.
func (r *Reporter) CrossystemSetParam(ctx context.Context, param CrossystemParam, value string) error {
	if _, err := r.CommandOutput(ctx, "crossystem", fmt.Sprintf("%v=%s", param, value)); err != nil {
		return err
	}
	return nil
}

// parseCrossystemOutput converts lines of crossystem output to a map.
// Duplicate params will return an error to match behavior in FAFT.
func parseCrossystemOutput(outputLines []string) (map[CrossystemParam]string, error) {
	all := make(map[string]string)
	for _, line := range outputLines {
		kv := rCrossystemLine.FindStringSubmatch(strings.TrimSpace(line))
		if kv == nil {
			return nil, errors.Errorf("failed to parse crossystem line %q", line)
		}
		if _, existing := all[kv[1]]; existing {
			return nil, errors.Errorf("duplicate crossystem param %v, existing value %v, parsing line %q", kv[1], all[kv[1]], line)
		}
		all[kv[1]] = kv[2]
	}

	return filterCrossystemParams(all), nil
}

// filterCrossystemParams removes any param from m that are not known.
func filterCrossystemParams(m map[string]string) map[CrossystemParam]string {
	filtered := make(map[CrossystemParam]string)
	for _, k := range knownCrossystemParams {
		if _, found := m[string(k)]; found {
			filtered[k] = m[string(k)]
		}
	}
	return filtered
}

// CheckFWVersion verifies that the DUT's active firmware version (A, B) matches an expected firmware version.
func (r *Reporter) CheckFWVersion(ctx context.Context, expected string) (bool, error) {
	curr, err := r.CrossystemParam(ctx, CrossystemParamMainfwAct)
	if err != nil {
		return false, errors.Wrap(err, "determining DUT firmware version")
	}
	return curr == expected, nil
}

// GetMiniOSPriority gets current MiniOS priority.
func (r *Reporter) GetMiniOSPriority(ctx context.Context) (string, error) {
	return r.CrossystemParam(ctx, CrossystemParamMiniOSPriority)
}

// CrossystemChecker verifies if the provided crossystem key-value pairs match the actual device values.
func (r *Reporter) CrossystemChecker(ctx context.Context, expected map[CrossystemParam]string) (bool, error) {
	for key, expectedVal := range expected {
		actualVal, err := r.CrossystemParam(ctx, key)
		if err != nil {
			return false, errors.Wrapf(err, "failed to get crossystem %v value", key)
		}
		if actualVal != expectedVal {
			return false, nil
		}
	}
	return true, nil
}
