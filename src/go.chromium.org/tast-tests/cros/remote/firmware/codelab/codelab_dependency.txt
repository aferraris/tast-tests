// Copyright 2021 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package firmware

import (
	"context"

	"go.chromium.org/tast/core/testing"
	"go.chromium.org/tast/core/testing/hwdep"
)

func init() {
	testing.AddTest(&testing.Test{
		Func: CodelabDependency,
		Desc: "Demonstrates common functionality for remote firmware tests",
		Contacts: []string{
			"chromeos-faft@google.com", // Owning team list
			"me@chromium.org",          // Test author
		},
		BugComponent: "b:792402", // ChromeOS > Platform > Enablement > Firmware > FAFT
		// TODO: When stable, change firmware_unstable to a different attr.
		Attr:         []string{"group:firmware", "firmware_unstable"},
		HardwareDeps: hwdep.D(hwdep.ChromeEC()),
	})
}

// CodelabDependency demonstrates using a HardwareDep in a firmware test.
func CodelabDependency(ctx context.Context, s *testing.State) {
	s.Log("FAFT stands for Fully Automated Firmware Test")
}
