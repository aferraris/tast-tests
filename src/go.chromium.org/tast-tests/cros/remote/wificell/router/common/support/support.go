// Copyright 2022 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package support

import (
	"context"
	"net"
	"strings"

	"go.chromium.org/tast-tests/cros/common/wifi/iw"
	"go.chromium.org/tast-tests/cros/remote/wificell/dhcp"
	"go.chromium.org/tast-tests/cros/remote/wificell/framesender"
	"go.chromium.org/tast-tests/cros/remote/wificell/hostapd"
	"go.chromium.org/tast-tests/cros/remote/wificell/http"
	"go.chromium.org/tast-tests/cros/remote/wificell/pcap"
	"go.chromium.org/tast/core/errors"
)

// RouterType is an enum indicating what type of router style a router is.
type RouterType int

const (
	// LegacyT is the legacy router type.
	LegacyT RouterType = iota
	// AxT is the ax router type.
	AxT
	// MtkOpenWrtT is the MTK router type.
	MtkOpenWrtT
	// OpenWrtT is the openwrt router type.
	OpenWrtT
	// UbuntuT is the Ubuntu router type.
	UbuntuT
	// SoftAPT is the Soft AP router type.
	SoftAPT
	// UnknownT is an unknown router type.
	UnknownT
)

// ParseRouterType parses a RouterType from a string.
func ParseRouterType(rTypeStr string) (RouterType, error) {
	var rType RouterType
	switch strings.ToLower(rTypeStr) {
	case "legacy":
		rType = LegacyT
	case "ax", "gtax11000", "ax6100", "gtaxe11000":
		rType = AxT
	case "mtk":
		rType = MtkOpenWrtT
	case "openwrt":
		rType = OpenWrtT
	case "ubuntu":
		rType = UbuntuT
	case "unknown", "auto":
		rType = UnknownT
	default:
		return -1, errors.Errorf("invalid RouterType %q", rTypeStr)
	}
	return rType, nil
}

// String returns RouterType as a string.
func (rt RouterType) String() string {
	var typeStr string
	switch rt {
	case LegacyT:
		typeStr = "Legacy"
	case AxT:
		typeStr = "AX"
	case MtkOpenWrtT:
		typeStr = "Mtk"
	case OpenWrtT:
		typeStr = "OpenWrt"
	case UbuntuT:
		typeStr = "UbuntuT"
	case UnknownT:
		typeStr = "Unknown"
	default:
		typeStr = string(rune(rt))
	}
	return typeStr
}

// Router contains the basic methods that must be implemented across all routers.
type Router interface {
	// Close cleans the resource used by Router.
	Close(ctx context.Context) error
	// RouterName returns the name of the managed router device.
	RouterName() string
	// RouterType returns the router type.
	RouterType() RouterType
	// RouterModel returns unique router model name.
	RouterModel() string
	// StartReboot initiates a reboot of the router host.
	//
	// Close must be called prior to StartReboot, not after.
	//
	// This Router instance will be unable to interact with the host after calling
	// this, as the connection to the host will be severed. To use this host
	// again, create a new Router instance with a new connection after the host is
	// fully rebooted.
	StartReboot(ctx context.Context) error
}

// Logs shall be implemented if the router supports log collection.
type Logs interface {
	Router
	// CollectLogs downloads log files from router to OutDir.
	CollectLogs(ctx context.Context) error
}

// Capture shall be implemented if the router supports pcap capture.
type Capture interface {
	Router
	// StartCapture starts a packet capturer.
	// For 2.4/5GHz channels, operating class is irrelevant and should be set to 0.
	StartCapture(ctx context.Context, name string, ch, opClass int, freqOps []iw.SetFreqOption, pcapOps ...pcap.Option) (*pcap.Capturer, error)
	// StartRawCapturer starts a capturer on an existing interface on the router instead of a
	// monitor type interface.
	StartRawCapturer(ctx context.Context, name, iface string, ops ...pcap.Option) (*pcap.Capturer, error)
	// StopCapture stops the packet capturer and releases related resources.
	StopCapture(ctx context.Context, capturer *pcap.Capturer) error
	// StopRawCapturer stops the packet capturer (no extra resources to release).
	StopRawCapturer(ctx context.Context, capturer *pcap.Capturer) error
	// ReserveForStopCapture returns a shortened ctx with cancel function.
	ReserveForStopCapture(ctx context.Context, capturer *pcap.Capturer) (context.Context, context.CancelFunc)
	// ReserveForStopRawCapturer returns a shortened ctx with cancel function.
	ReserveForStopRawCapturer(ctx context.Context, capturer *pcap.Capturer) (context.Context, context.CancelFunc)
}

// Hostapd shall be implemented if the router supports hostapd.
type Hostapd interface {
	Router
	// StartHostapd starts the hostapd server.
	StartHostapd(ctx context.Context, name string, confs ...*hostapd.Config) (*hostapd.Server, error)
	// StopHostapd stops the hostapd server.
	StopHostapd(ctx context.Context, hs *hostapd.Server) error
	// ReconfigureHostapd restarts the hostapd server with the new config. It preserves the interface and the name of the old hostapd server.
	ReconfigureHostapd(ctx context.Context, hs *hostapd.Server, conf *hostapd.Config) (*hostapd.Server, error)
}

// DHCP shall be implemented if the router supports DHCP configuration. If DNS functionality is
// not required, set dnsOpt to nil.
type DHCP interface {
	Router
	// StartDHCP starts the DHCP server and configures the server IP.
	StartDHCP(ctx context.Context, name, iface string, ipStart, ipEnd, serverIP, broadcastIP net.IP, mask net.IPMask, dnsOpt *dhcp.DNSOption) (*dhcp.Server, error)
	// StopDHCP stops the DHCP server and flushes the interface.
	StopDHCP(ctx context.Context, ds *dhcp.Server) error
}

// HTTP shall be implemented if the router supports HTTP configuration.
type HTTP interface {
	Router
	// StartHTTP starts the HTTP server.
	StartHTTP(ctx context.Context, name, iface, redirectAddr string, port, statusCode int) (_ *http.Server, retErr error)
	// StopHTTP stops the HTTP server.
	StopHTTP(ctx context.Context, httpServer *http.Server) error
}

// FrameSender shall be implemented if the router can send management frames.
type FrameSender interface {
	Router
	// CloseFrameSender closes frame sender and releases related resources.
	CloseFrameSender(ctx context.Context, s *framesender.Sender) error
	// NewFrameSender creates a frame sender object.
	NewFrameSender(ctx context.Context, iface string) (*framesender.Sender, error)
}

// IfaceManipulation shall be implemented if the router can modify its iface configuration.
type IfaceManipulation interface {
	Router
	// SetAPIfaceDown brings down the interface that the APIface uses.
	SetAPIfaceDown(ctx context.Context, iface string) error
	// MAC returns the MAC address of iface on this router.
	MAC(ctx context.Context, iface string) (net.HardwareAddr, error)
}

// Bridge shall be implemented if the router supports network bridges.
type Bridge interface {
	Router
	// NewBridge returns a bridge name for tests to use.
	NewBridge(ctx context.Context) (string, error)
	// ReleaseBridge releases the bridge.
	ReleaseBridge(ctx context.Context, br string) error
}

// Veth shall be implemented if the router supports veths (virtual ethernet devices).
type Veth interface {
	Router
	// NewVethPair returns a veth pair for tests to use.
	NewVethPair(ctx context.Context) (string, string, error)
	// ReleaseVethPair release the veth pair.
	ReleaseVethPair(ctx context.Context, veth string) error
}

// VethBridgeBinding shall be implemented if the router supports bridges, veths, and can bind bridges and veths.
type VethBridgeBinding interface {
	Router
	Bridge
	Veth
	// BindVethToBridge binds the veth to bridge.
	BindVethToBridge(ctx context.Context, veth, br string) error
	// UnbindVeth unbinds the veth to any other interface.
	UnbindVeth(ctx context.Context, veth string) error
}

// IfaceBridgeBinding shall be implemented if the router supports bridges, veths, and can bind bridges and veths.
type IfaceBridgeBinding interface {
	Router
	Bridge
	// BindIfaceToBridge binds the iface to bridge.
	BindIfaceToBridge(ctx context.Context, iface, br string) error
	// UnbindIface unbinds the iface to any other interface.
	UnbindIface(ctx context.Context, iface string) error
}
