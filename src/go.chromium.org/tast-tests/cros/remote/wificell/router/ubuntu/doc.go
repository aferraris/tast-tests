// Copyright 2023 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package ubuntu provides utilities for accessing or controlling Ubuntu router architecture.
//
// This Ubuntu router implementation is used to manage Ubuntu routers.
package ubuntu
