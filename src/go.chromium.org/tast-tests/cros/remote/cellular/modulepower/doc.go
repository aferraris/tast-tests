// Copyright 2024 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package modulepower contains utilities for performing intrusive power measurements of individual modules.
package modulepower
