// Copyright 2021 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package nearbyshare contains fixtures meta tests use.
package nearbyshare

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"github.com/golang/protobuf/ptypes/empty"

	"go.chromium.org/tast-tests/cros/common/cros/nearbyshare"
	nearbycommon "go.chromium.org/tast-tests/cros/common/cros/nearbyshare"
	"go.chromium.org/tast-tests/cros/services/cros/nearbyservice"
	"go.chromium.org/tast/core/dut"
	"go.chromium.org/tast/core/rpc"
	"go.chromium.org/tast/core/ssh/linuxssh"
	"go.chromium.org/tast/core/testing"
)

// resetTimeout is the timeout duration to trying reset of the current fixture.
const resetTimeout = 1 * time.Minute

const (
	advertisementMonitoring = "BluetoothAdvertisementMonitoring"
	backgroundScanning      = "NearbySharingBackgroundScanning"
	webRTC                  = "NearbySharingWebRtc"
	wlan                    = "NearbySharingWifiLan"
	floss                   = "Floss"
	selfShare               = "NearbySharingSelfShare"
)

// NewNearbyShareFixture creates a fixture for Nearby Share tests in different configurations.
func NewNearbyShareFixture(dataUsage nearbycommon.DataUsage, visibility nearbycommon.Visibility, skipReceiverOnboarding bool, enabledFeatures, disabledFeatures []string) testing.FixtureImpl {
	return &nearbyShareFixture{
		dataUsage:              dataUsage,
		visibility:             visibility,
		skipReceiverOnboarding: skipReceiverOnboarding,
		sameGaiaLogin:          false,
		enabledFeatures:        enabledFeatures,
		disabledFeatures:       disabledFeatures,
		// TODO(crbug/1127165): Remove after data is supported in fixture.
		testFiles: []string{"small_jpg.zip", "small_png.zip", "big_txt.zip"},
	}
}

// NewNearbyShareSelfShareFixture creates a fixture for Nearby Share Self Share tests in different configurations.
func NewNearbyShareSelfShareFixture(dataUsage nearbycommon.DataUsage, visibility nearbycommon.Visibility, skipReceiverOnboarding bool, enabledFeatures, disabledFeatures []string) testing.FixtureImpl {
	return &nearbyShareFixture{
		dataUsage:              dataUsage,
		visibility:             visibility,
		skipReceiverOnboarding: skipReceiverOnboarding,
		sameGaiaLogin:          true,
		enabledFeatures:        append(enabledFeatures, selfShare),
		disabledFeatures:       disabledFeatures,
		// TODO(crbug/1127165): Remove after data is supported in fixture.
		testFiles: []string{"small_jpg.zip", "small_png.zip", "big_txt.zip"},
	}
}

func init() {
	// We have a lot of fixtures for all the various Nearby Share use cases we test.
	// Add new non-parent fixtures in separate files and functions to keep this one manageable.
	addBasicOfflineFixtures()
	addBasicOnlineFixtures()
	addBackgroundScanningFixtures()
	addWebRTCAndWLANFixtures()

	// Floss-enabled duplicates
	addFlossBasicOfflineFixtures()
	addFlossBasicOnlineFixtures()
	addFlossBackgroundScanningFixtures()
	addFlossWebRTCAndWLANFixtures()
}

type nearbyShareFixture struct {
	dataUsage              nearbycommon.DataUsage
	visibility             nearbycommon.Visibility
	skipReceiverOnboarding bool
	sameGaiaLogin          bool
	enabledFeatures        []string
	disabledFeatures       []string
	testFiles              []string

	// Sender and receiver devices.
	d1 *dut.DUT
	d2 *dut.DUT

	// These two vars actually have type *nearbyservice.nearbyShareServiceClient
	sender   nearbyservice.NearbyShareServiceClient
	receiver nearbyservice.NearbyShareServiceClient

	// Path on the sender where the test files are stored.
	remoteFilePath string

	// Attributes for both chromebooks.
	attributes []byte

	// RPC connections to the chromebooks. They are initialized in SetUp,
	// and must be closed in TearDown to free the underlying SSH connections.
	senderRPCClient   *rpc.Client
	receiverRPCClient *rpc.Client
}

// FixtData holds information made available to tests that specify this Fixture.
type FixtData struct {
	RemoteFilePath        string
	SenderDisplayName     string
	SenderUsername        string
	SenderPassword        string
	ReceiverUsername      string
	ReceiverPassword      string
	ReceiverDisplayName   string
	Sender                nearbyservice.NearbyShareServiceClient
	SenderSendPath        string
	Receiver              nearbyservice.NearbyShareServiceClient
	ReceiverDownloadsPath string
}

// Setup logs in, enables Nearby Share and moves test files to Sender DUT.
func (f *nearbyShareFixture) SetUp(ctx context.Context, s *testing.FixtState) interface{} {
	d1 := s.DUT()
	f.d1 = d1
	d2 := s.CompanionDUT("cd1")
	if d2 == nil {
		s.Fatal("Failed to get companion DUT cd1")
	}
	f.d2 = d2
	s.Log("Preparing to move remote data files to DUT1 (Sender)")
	tempdir, err := d1.Conn().CommandContext(ctx, "mktemp", "-d", "/tmp/nearby_share_XXXXXX").Output()
	if err != nil {
		s.Fatal("Failed to create remote data path directory: ", err)
	}
	remoteDir := strings.TrimSpace(string(tempdir))
	f.remoteFilePath = remoteDir

	s.Log("Moving data files to DUT1 (Sender)")
	for _, dataFileName := range f.testFiles {
		dataPath := s.DataPath(dataFileName)
		dutFilePath := filepath.Join(remoteDir, dataFileName)
		if _, err := linuxssh.PutFiles(ctx, d1.Conn(), map[string]string{dataPath: dutFilePath}, linuxssh.DereferenceSymlinks); err != nil {
			s.Fatalf("Failed to send data to remote data path %v: %v", remoteDir, err)
		}
	}

	var keepState bool
	if val, ok := s.Var(nearbycommon.KeepStateVar); ok {
		b, err := strconv.ParseBool(val)
		if err != nil {
			s.Fatalf("Unable to convert %v var to bool: %v", nearbycommon.KeepStateVar, err)
		}
		keepState = b
	}

	// Get nearby static IDs for the two devices.
	nearbyStaticIDSender, err := d1.Conn().CommandContext(ctx, "sh", "-c", nearbycommon.NearbyShareStaticIDCmd).Output()
	if err != nil {
		s.Fatal("Failed to generate nearby static ID on sender: ", err)
	}
	nearbyStaticIDSenderStr := strings.TrimSpace(string(nearbyStaticIDSender))
	s.Logf("Nearby Static ID of Sender: %s", nearbyStaticIDSenderStr)
	nearbyStaticIDReceiver, err := d2.Conn().CommandContext(ctx, "sh", "-c", nearbycommon.NearbyShareStaticIDCmd).Output()
	if err != nil {
		s.Fatal("Failed to generate nearby static ID on receiver: ", err)
	}
	nearbyStaticIDReceiverStr := strings.TrimSpace(string(nearbyStaticIDReceiver))
	s.Logf("Nearby Static ID of Receiver: %s", nearbyStaticIDReceiverStr)

	// Login and setup Nearby Share on DUT 1 (Sender).
	cl1, err := rpc.Dial(s.FixtContext(), d1, s.RPCHint())
	if err != nil {
		s.Fatal("Failed to connect to the RPC service on the DUT: ", err)
	}
	f.senderRPCClient = cl1
	const crosBaseName = "cros_test"
	senderDisplayName := nearbycommon.RandomDeviceName(crosBaseName)
	s.Log("Enabling Nearby Share on DUT1 (Sender). Name: ", senderDisplayName)
	senderUsername := s.RequiredVar("nearbyshare.cros_username")
	senderPassword := s.RequiredVar("nearbyshare.cros_password")
	sender, err := f.enableNearbyShare(ctx, s, cl1, senderDisplayName, senderUsername, senderPassword, "", nearbyStaticIDSenderStr, keepState /*skipOnboarding=*/, true, f.enabledFeatures, f.disabledFeatures)
	if err != nil {
		s.Fatal("Failed to enable Nearby Share on DUT1 (Sender): ", err)
	}
	f.sender = sender

	// Login and setup Nearby Share on DUT 2 (Receiver).
	cl2, err := rpc.Dial(s.FixtContext(), d2, s.RPCHint())
	if err != nil {
		s.Fatal("Failed to dial rpc service on DUT2: ", err)
	}
	f.receiverRPCClient = cl2
	receiverDisplayName := nearbycommon.RandomDeviceName(crosBaseName)
	s.Log("Enabling Nearby Share on DUT2 (Receiver). Name: ", receiverDisplayName)

	// Features like Self Share require that both devices are logged into the same Gaia.
	s.Log("Logging into both DUTs with the same GAIA login = ", f.sameGaiaLogin)
	receiverUsername := s.RequiredVar("nearbyshare.cros_username")
	receiverPassword := s.RequiredVar("nearbyshare.cros_password")
	if !f.sameGaiaLogin {
		receiverUsername = s.RequiredVar("nearbyshare.cros2_username")
		receiverPassword = s.RequiredVar("nearbyshare.cros2_password")
	}

	s.Log("skipReceiverOnboarding = ", f.skipReceiverOnboarding)
	receiver, err := f.enableNearbyShare(ctx, s, cl2, receiverDisplayName, receiverUsername, receiverPassword, senderUsername, nearbyStaticIDReceiverStr, keepState, f.skipReceiverOnboarding, f.enabledFeatures, f.disabledFeatures)
	if err != nil {
		s.Fatal("Failed to enable Nearby Share on DUT2 (Receiver): ", err)
	}
	f.receiver = receiver
	s.Log("Getting device attributes about the sender")
	senderAttrsRes, err := f.sender.CrOSAttributes(ctx, &empty.Empty{})
	if err != nil {
		s.Error("Failed to save device attributes about the sender: ", err)
	}
	s.Log("Getting device attributes about the receiver")
	receiverAttrsRes, err := f.receiver.CrOSAttributes(ctx, &empty.Empty{})
	if err != nil {
		s.Error("Failed to save device attributes about the receiver: ", err)
	}
	var senderAttributes *nearbycommon.CrosAttributes
	var receiverAttributes *nearbycommon.CrosAttributes
	if err := json.Unmarshal([]byte(senderAttrsRes.Attributes), &senderAttributes); err != nil {
		s.Error("Failed to unmarshal sender's attributes: ", err)
	}
	if err := json.Unmarshal([]byte(receiverAttrsRes.Attributes), &receiverAttributes); err != nil {
		s.Error("Failed to unmarshal receiver's: ", err)
	}
	attributes := struct {
		Sender   *nearbycommon.CrosAttributes
		Receiver *nearbycommon.CrosAttributes
	}{Sender: senderAttributes, Receiver: receiverAttributes}
	crosLog, err := json.MarshalIndent(attributes, "", "\t")
	if err != nil {
		s.Fatal("Failed to format device metadata for logging: ", err)
	}
	f.attributes = crosLog
	return &FixtData{
		RemoteFilePath:        f.remoteFilePath,
		Sender:                f.sender,
		SenderSendPath:        filepath.Join(attributes.Sender.DownloadsPath, nearbycommon.SendFolderName),
		ReceiverDownloadsPath: attributes.Receiver.DownloadsPath,
		Receiver:              f.receiver,
		SenderDisplayName:     senderDisplayName,
		SenderUsername:        senderUsername,
		SenderPassword:        senderPassword,
		ReceiverDisplayName:   receiverDisplayName,
		ReceiverUsername:      receiverUsername,
		ReceiverPassword:      receiverPassword,
	}
}

// enableNearbyShare is a helper function to enable Nearby Share on each DUT.
// senderUsername is only used when the device visibility is "Some contacts".
// keepState is used to optionally preserve user accounts on the DUT.
// Sender devices should pass an empty string since the visibility setting is only relevant to receivers.
func (f *nearbyShareFixture) enableNearbyShare(ctx context.Context, s *testing.FixtState, cl *rpc.Client, deviceName, username, password, senderUsername, nearbyStaticID string, keepState, skipOnboarding bool, enabledFeatures, disabledFeatures []string) (nearbyservice.NearbyShareServiceClient, error) {
	// Connect to the Nearby Share Service so we can execute local code on the DUT.
	ns := nearbyservice.NewNearbyShareServiceClient(cl.Conn)
	testing.ContextLog(ctx, "Logging into ChromeOS")

	loginReq := &nearbyservice.CrOSLoginRequest{Username: username, Password: password, NearbyStaticId: nearbyStaticID, KeepState: keepState, EnabledFlags: enabledFeatures, DisabledFlags: disabledFeatures}
	if _, err := ns.NewChromeLogin(ctx, loginReq); err != nil {
		s.Fatal("Failed to start Chrome: ", err)
	}

	if skipOnboarding {
		testing.ContextLog(ctx, "Enabling Nearby Share and configuring it's settings")
		req := &nearbyservice.CrOSSetupRequest{DataUsage: int32(f.dataUsage), Visibility: int32(f.visibility), DeviceName: deviceName, SenderUsername: senderUsername}

		if _, err := ns.CrOSSetup(ctx, req); err != nil {
			s.Fatal("Failed to setup Nearby Share: ", err)
		}
	}
	return ns, nil
}

// TearDown removes the test files from the sender and and closes the services on both DUTs.
func (f *nearbyShareFixture) TearDown(ctx context.Context, s *testing.FixtState) {
	// Delete the test files from the sender.
	if err := f.d1.Conn().CommandContext(ctx, "rm", "-r", f.remoteFilePath).Run(); err != nil {
		s.Error("Failed to remove test files from the sender: ", err)
	}
	// Shut down the nearby share service connections.
	if _, err := f.sender.CloseChrome(ctx, &empty.Empty{}); err != nil {
		s.Error("Failed to shutdown nearby share service connection for sender: ", err)
	}
	if _, err := f.receiver.CloseChrome(ctx, &empty.Empty{}); err != nil {
		s.Error("Failed to shutdown nearby share service connections for receiver: ", err)
	}
	// Close gRPC clients to free underlying SSH resources.
	if err := f.senderRPCClient.Close(ctx); err != nil {
		s.Error("Failed to close gRPC client for sender: ", err)
	}
	if err := f.receiverRPCClient.Close(ctx); err != nil {
		s.Error("Failed to close gRPC client for receiver: ", err)
	}
}

func (f *nearbyShareFixture) Reset(ctx context.Context) error { return nil }

// PreTest is run before each test in the fixture..
func (f *nearbyShareFixture) PreTest(ctx context.Context, s *testing.FixtTestState) {
	if err := ioutil.WriteFile(filepath.Join(s.OutDir(), "device_attributes.json"), f.attributes, 0644); err != nil {
		s.Fatal("Failed to write CrOS attributes to output file: ", err)
	}

	testing.ContextLog(ctx, "Starting logging on sender")
	if _, err := f.sender.StartLogging(ctx, &empty.Empty{}); err != nil {
		s.Log("Failed to start logging on the sender: ", err)
	}
	testing.ContextLog(ctx, "Starting logging on receiver")
	if _, err := f.receiver.StartLogging(ctx, &empty.Empty{}); err != nil {
		s.Log("Failed to start logging on the receiver: ", err)
	}
}

// PostTest will pull the logs from the DUT and delete leftover logs and test files.
func (f *nearbyShareFixture) PostTest(ctx context.Context, s *testing.FixtTestState) {
	// Save logs on each DUT.
	if _, err := f.sender.SaveLogs(ctx, &nearbyservice.SaveLogsRequest{SaveUiLogs: s.HasError()}); err != nil {
		s.Error("Failed to save nearby share logs on the sender: ", err)
	}
	if _, err := f.receiver.SaveLogs(ctx, &nearbyservice.SaveLogsRequest{SaveUiLogs: s.HasError()}); err != nil {
		s.Error("Failed to save nearby share logs on the receiver: ", err)
	}
	// Pull the log files back to the host.
	logsToSave := []string{nearbyshare.ChromeLog, nearbyshare.MessageLog, nearbyshare.BtsnoopLog}
	if s.HasError() {
		logsToSave = append(logsToSave, "faillog")
	}
	duts := []*dut.DUT{f.d1, f.d2}
	tags := []string{"sender", "receiver"}
	for i, dut := range duts {
		logFiles, err := dut.Conn().CommandContext(ctx, "ls", nearbyshare.NearbyLogDir).Output()
		if err != nil {
			testing.ContextLog(ctx, "Failed to get list of log files in remote DUTs nearby temp dir: ", err)
		} else {
			testing.ContextLog(ctx, "Files in remote DUTs nearby temp dir: ", strings.Replace(string(logFiles), "\n", " ", -1))
		}
		for _, log := range logsToSave {
			logPathSrc := filepath.Join(nearbyshare.NearbyLogDir, log)
			logPathDst := filepath.Join(s.OutDir(), log+"_"+tags[i])
			if err := linuxssh.GetFile(ctx, dut.Conn(), logPathSrc, logPathDst, linuxssh.PreserveSymlinks); err != nil {
				testing.ContextLogf(ctx, "Failed to save %s to %s. Error: %s", logPathSrc, logPathDst, err)
			}
		}
		// Delete the log files so that we have a clean run for parameterized tests.
		if err := dut.Conn().CommandContext(ctx, "rm", "-r", nearbyshare.NearbyLogDir).Run(); err != nil {
			testing.ContextLog(ctx, "Failed to remove the log files at end of test: ", err)
		}
	}
	// Delete the sent files from the Downloads folder on the receiver.
	if _, err := f.receiver.ClearTransferredFiles(ctx, &empty.Empty{}); err != nil {
		s.Error("Failed to clear transferred files from the Downloads folder on the receiver: ", err)
	}
}
