// Copyright 2024 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package fileutils provides utilities for interacting with regular files and
// directories on any host, including the host running Tast.
//
// Most utilities require Linux-based hosts, as they use common linux commands.
package fileutils
