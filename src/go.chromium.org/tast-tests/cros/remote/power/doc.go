// Copyright 2023 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package power implements the libraries and utilities which are used for
// remote power tests.
package power
