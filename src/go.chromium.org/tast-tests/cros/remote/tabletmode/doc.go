// Copyright 2023 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package tabletmode provides helper functions for switching between tabletmode
// and laptop mode on convertibles and detachables for remote Tast tests.
package tabletmode
