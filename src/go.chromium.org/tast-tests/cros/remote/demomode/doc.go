// Copyright 2023 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package demomode contains the Demo Mode fixtures for setting up a
// device into Demo Mode
package demomode
